﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CommonLayer.Model
{
    /// <summary>
    /// This EventLog class file is having all the EventLog table related properties
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks> 
    public class EventLogDTO
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key]
        public int Id { get; set; }
        public int? EventId { get; set; }
        public string ApplicationName { get; set; }
        public string LogLevel { get; set; }
        public string Message { get; set; }
        public DateTime? CreatedTime { get; set; }
    }
}
