﻿using System;
using System.Collections.Generic;
using System.Text;

namespace  PTPL.SOP_System.Common
{
    public static class CommonDeclarations
    {
        public static string ConnectionString { get; set; }
        public static string APIVersion { get; set; }
        public static string Secret { get; set; }

    }
    public class ErrorInfo
    {
        public Enum StatusCode { get; set; }
        public string Message { get; set; }
        public int? Id { get; set; }
    }
}
