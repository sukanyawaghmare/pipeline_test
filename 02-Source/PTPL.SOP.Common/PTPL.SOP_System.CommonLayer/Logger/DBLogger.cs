﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;

namespace  PTPL.SOP_System.Common.Logger
{
    /// <summary>
    /// Defines the <see cref="DbLogger" />
    /// </summary>
    internal sealed class DbLogger : ILogger
    {
        /// <summary>
        /// Defines the _categoryName
        /// </summary>
        private readonly string _categoryName;

        /// <summary>
        /// Defines the _filter
        /// </summary>
        private readonly Func<string, LogLevel, bool> _filter;

        /// <summary>
        /// Defines the _helper
        /// </summary>
        private readonly MySqlHelper _helper;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbLogger"/> class.
        /// </summary>
        /// <param name="categoryName">The categoryName<see cref="string"/></param>
        /// <param name="filter">The filter</param>
        /// <param name="connectionString">The connectionString<see cref="string"/></param>
        public DbLogger(string categoryName, Func<string, LogLevel, bool> filter, string connectionString = null)
        {
            _categoryName = categoryName;
            _filter = filter;
            _helper = new MySqlHelper(connectionString);
        }

        /// <summary>
        /// The Log
        /// </summary>
        /// <typeparam name="TState"></typeparam>
        /// <param name="logLevel">The logLevel<see cref="LogLevel"/></param>
        /// <param name="eventId">The eventId<see cref="EventId"/></param>
        /// <param name="state">The state<see cref="TState"/></param>
        /// <param name="exception">The exception<see cref="Exception"/></param>
        /// <param name="formatter">The formatter</param>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
            {
                return;
            }

            if (formatter == null)
            {
                throw new ArgumentNullException(nameof(formatter));
            }

            string message = formatter(state, exception);
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (exception != null)
            {
                message += "\n" + exception;
            }

            SopEventLog eventLog = new SopEventLog
            {
                Message = message,
                EventId = eventId.Id,
                ApplicationName = "VisiSOP-API",
                LogLevel = logLevel.ToString(),
                CreatedTime = DateTime.UtcNow
            };
            _helper.InsertLog(eventLog);
        }

        /// <summary>
        /// The IsEnabled
        /// </summary>
        /// <param name="logLevel">The logLevel<see cref="LogLevel"/></param>
        /// <returns>The <see cref="bool"/></returns>
        public bool IsEnabled(LogLevel logLevel)
        {
            return (_filter?.Invoke(_categoryName, logLevel) != false);
        }

        /// <summary>
        /// The BeginScope
        /// </summary>
        /// <typeparam name="TState"></typeparam>
        /// <param name="state">The state<see cref="TState"/></param>
        /// <returns>The <see cref="IDisposable"/></returns>
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
    }
}
