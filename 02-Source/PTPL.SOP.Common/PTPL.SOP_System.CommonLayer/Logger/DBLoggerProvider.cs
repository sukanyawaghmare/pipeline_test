﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace  PTPL.SOP_System.Common.Logger
{
    /// <summary>
    ///     Defines the <see cref="DbLoggerProvider" />
    /// </summary>
    internal sealed class DbLoggerProvider : ILoggerProvider
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DbLoggerProvider" /> class.
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <param name="connectionStr">The connectionStr<see cref="string" /></param>
        public DbLoggerProvider(Func<string, LogLevel, bool> filter, string connectionStr)
        {
            _filter = filter;
            _connectionString = connectionStr;
        }

        #endregion Constructors

        #region Fields

        /// <summary>
        ///     Defines the _connectionString
        /// </summary>
        private readonly string _connectionString;

        /// <summary>
        ///     Defines the _filter
        /// </summary>
        private readonly Func<string, LogLevel, bool> _filter;

        #endregion Fields

        #region Methods

        /// <summary>
        ///     The CreateLogger
        /// </summary>
        /// <param name="categoryName">The categoryName<see cref="string" /></param>
        /// <returns>The <see cref="ILogger" /></returns>
        public ILogger CreateLogger(string categoryName)
        {
            return new DbLogger(categoryName, _filter, _connectionString);
        }

        public void Dispose()
        {

        }

        #endregion Methods
    }
}
