﻿using PTPL.SOP_System.DataEntities;
using System.Data;
using System.Data.SqlClient;

namespace  PTPL.SOP_System.Common.Logger
{
    /// <summary>
    /// Defines the <see cref="MySqlHelper" />
    /// </summary>
    // ReSharper disable once ClassCanBeSealed.Global
    internal class MySqlHelper
    {
        /// <summary>
        /// Gets or sets the ConnectionString
        /// </summary>
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Local
        private string ConnectionString { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MySqlHelper"/> class.
        /// </summary>
        /// <param name="connectionStr">The connectionStr<see cref="string"/></param>
        public MySqlHelper(string connectionStr)
        {
            ConnectionString = CommonDeclarations.ConnectionString;
        }

        /// <summary>
        /// The InsertLog
        /// </summary>
        /// <param name="log">The log<see cref="EventLogDTO"/></param>
        /// <returns>The <see cref="bool"/></returns>

        public bool InsertLog(SopEventLog log)
        {
            bool result;
            using SqlConnection connection = new SqlConnection(ConnectionString);
            if (connection.State == ConnectionState.Open)
            {
                connection.Close();
            }
            else
            {
                connection.Open();
            }

            SqlCommand cmd = new SqlCommand(ConnectionString, connection)
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = "LogErrorToDb"
            };
            cmd.Parameters.AddWithValue(parameterName: "Id", value: log.Id);
            cmd.Parameters.AddWithValue(parameterName: "EventId", value: log.EventId);
            cmd.Parameters.AddWithValue(parameterName: "ApplicationName", value: log.ApplicationName);
            cmd.Parameters.AddWithValue(parameterName: "LogLevel", value: log.LogLevel);
            cmd.Parameters.AddWithValue(parameterName: "Message", value: log.Message);
            cmd.Parameters.AddWithValue(parameterName: "CreatedTime", value: log.CreatedTime);
            int count = cmd.ExecuteNonQuery();
            result = count > 0;
            return result;
        }
    }
}
