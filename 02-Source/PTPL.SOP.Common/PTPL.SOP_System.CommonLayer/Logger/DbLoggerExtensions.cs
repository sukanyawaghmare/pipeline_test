﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace  PTPL.SOP_System.Common.Logger
{
    /// <summary>
    /// Defines the <see cref="DbLoggerExtensions" />
    /// </summary>
    public static class DbLoggerExtensions
    {
        /// <summary>
        /// The AddContext
        /// </summary>
        /// <param name="factory">The factory<see cref="ILoggerFactory"/></param>
        /// <param name="filter">The filter</param>
        /// <param name="connection">The connectionStr<see cref="string"/></param>
        /// <returns>The <see cref="ILoggerFactory"/></returns>
        public static ILoggerFactory AddContext(this ILoggerFactory factory,
        Func<string, LogLevel, bool> filter = null, string connection = null)
        {
            factory.AddProvider(new DbLoggerProvider(filter, connection));
            return factory;
        }

        /// <summary>
        /// The AddContext
        /// </summary>
        /// <param name="factory">The factory<see cref="ILoggerFactory"/></param>
        /// <param name="minLevel">The minLevel<see cref="LogLevel"/></param>
        /// <param name="connection">The connectionStr<see cref="string"/></param>
        /// <returns>The <see cref="ILoggerFactory"/></returns>
        public static ILoggerFactory AddContext(this ILoggerFactory factory, LogLevel minLevel, string connection)
        {
            return AddContext(
                factory,
                (_, logLevel) => logLevel >= minLevel, connection);
        }
    }
}
