﻿using PTPL_SOP_System_UI.Admin.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI
{
    class MyViewModel : ViewModelBase
    {
        private ObservableCollection<Club> clubs;

        public ObservableCollection<Club> Clubs
        {
            get
            {
                if (this.clubs == null)
                {
                    this.clubs = this.CreateClubs();
                }

                return this.clubs;
            }
        }

        private ObservableCollection<Club> CreateClubs()
        {
            ObservableCollection<Club> clubs = new ObservableCollection<Club>();
            Club club;

            //club = new Club("Press", "", "", "", false, false, false, false, false, false, false, false, false, false, "");

            //club = new Club("", "PR 01", "Press Real Change", "PR 01 - Press Real Change", false, false, true, false, true, true, false, false, true, true, "Reel Change");

            //club = new Club("", "PR 02", "Press shutdown procedure", "PR 01 - Press Real Change", false, false, true, false, true, true, false, false, true, true, "Nitrogen Problem");

            club = new Club("SL's", "", "", "", false, false, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.01", "Cold Start Procedures for Extruders 1, 2 & 3", "EXT.01 - Cold Start Procedures for Extruders 1, 2 & 3", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.02", "CWarm Start Procedure for Extruder 1, 2 & 3", "EXT.02 - Warm Start Procedure for Extruder 1, 2 & 3", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.03", "Cooling System Checks and Bleeding the Cooling Rolls for Extruders 1 & 2", "EXT.03 - Cooling System Checks and Bleeding the Cooling Rolls for Extruders 1 & 2", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.04", "Checking Thermocouples for improper heating at the Extruder Barrel and Die", "EXT.04 - Checking Thermocouples for improper heating at the Extruder Barrel and Die", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.05", "Setting up the Temperature Profile Extruder 2 Welex Controller", "EXT.05 - Setting up the Temperature Profile Extruder 2 Welex Controller", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.06", "Die Gap Measurements", "EXT.06 - Die Gap Measurements", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.07", "Resin Set-Up & Change-Over Procedures", "EXT.07 - Resin Set-Up & Change-Over Procedures", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.08-1", "Blend Check & Set Procedure for Extruder 1", "EXT.08-1 - Blend Check & Set Procedure for Extruder 1", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.08-2", "Blend Check Procedure for Extruders 1 & 2", "EXT.08-2 - Blend Check Procedure for Extruders 1 & 2", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.09", "Operation of Edge Trim Grinders", "EXT.09 - Operation of Edge Trim Grinders", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.10", "", "", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "EXT.11", "Extrusion Paperwork", "EXT.11 - Extrusion Paperwork", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("Press", "", "", "", true, true, true, false, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.01", "Press Reel Change", "PR.01 - Press Reel Change", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.02", "Press Shut-Down Procedure", "PR.02 - Press Shut-Down Procedure", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.03", "Recharging the Nitrodyne System", "PR.03 - Recharging the Nitrodyne System", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.04", "Measurements and QC Checks", "PR.04 - Measurements and QC Checks", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.05", "Press QC Adjustments", "PR.05 - Press QC Adjustments", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.06", "Flagging Non-Conforming Product at the Press", "PR.06 - Flagging Non-Conforming Product at the Press", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.07", "Press Clean Outs", "PR.07 - Press Clean Outs", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);
            club = new Club("", "PR.07", "Performing a Product or Tool Change at the Press", "PR.08 - Performing a Product or Tool Change at the Press", false, false, false, true, true, true, false, false, true, true, "");
            clubs.Add(club);



            return clubs;
        }
    }
}
