﻿#pragma checksum "..\..\..\..\..\Admin\Views\TemplateMaster.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "060FC74EE233C5C23F0BA315E299E6AF63B944DFE641E170AD7A5E1DF0A63E49"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Microsoft.Xaml.Behaviors;
using Microsoft.Xaml.Behaviors.Core;
using Microsoft.Xaml.Behaviors.Input;
using Microsoft.Xaml.Behaviors.Layout;
using Microsoft.Xaml.Behaviors.Media;
using PTPL_SOP_System_UI;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.Admin.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.Docking;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.FixedDocumentViewersUI;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.LayoutControl;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.MultiColumnComboBox;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.RadialMenu;
using Telerik.Windows.Controls.ScheduleView;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Controls.Wizard;
using Telerik.Windows.Data;
using Telerik.Windows.Documents.Model;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Media.Imaging.ImageEditorCommands.RoutedCommands;
using Telerik.Windows.Media.Imaging.Tools.UI;
using Telerik.Windows.Shapes;


namespace PTPL_SOP_System_UI.Admin.Views {
    
    
    /// <summary>
    /// TemplateMaster
    /// </summary>
    public partial class TemplateMaster : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 52 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadBusyIndicator busyIndicator1;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadToggleButton btnRefresh;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid templateGrid;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadGridView gridTemplateMaster;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDataPager radDataPager;
        
        #line default
        #line hidden
        
        
        #line 147 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid sp_AddTemplate;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtblockAddSOPWI;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabControl tmpRadControl;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem tabSopInfo;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSopID;
        
        #line default
        #line hidden
        
        
        #line 196 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSopTemplateName;
        
        #line default
        #line hidden
        
        
        #line 201 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDateTimePicker txtIssuedDate;
        
        #line default
        #line hidden
        
        
        #line 207 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtOriginatorName;
        
        #line default
        #line hidden
        
        
        #line 212 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadDateTimePicker txtEffectiveDate;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRevisionName;
        
        #line default
        #line hidden
        
        
        #line 223 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txStatusName;
        
        #line default
        #line hidden
        
        
        #line 228 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox drpSopCategory;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox drpArea;
        
        #line default
        #line hidden
        
        
        #line 240 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox drpMachine;
        
        #line default
        #line hidden
        
        
        #line 253 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox drpEventType;
        
        #line default
        #line hidden
        
        
        #line 260 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadComboBox drpAssinee;
        
        #line default
        #line hidden
        
        
        #line 268 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEstimation;
        
        #line default
        #line hidden
        
        
        #line 277 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSOPWIID;
        
        #line default
        #line hidden
        
        
        #line 281 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSOPWINumber;
        
        #line default
        #line hidden
        
        
        #line 285 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSOPWIRevision;
        
        #line default
        #line hidden
        
        
        #line 292 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFilenmame;
        
        #line default
        #line hidden
        
        
        #line 294 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBrowse;
        
        #line default
        #line hidden
        
        
        #line 301 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTemplateDecription;
        
        #line default
        #line hidden
        
        
        #line 304 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadButton btnCancel;
        
        #line default
        #line hidden
        
        
        #line 306 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadButton btnNext;
        
        #line default
        #line hidden
        
        
        #line 308 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadButton btnSave;
        
        #line default
        #line hidden
        
        
        #line 310 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadButton btnUpdate;
        
        #line default
        #line hidden
        
        
        #line 316 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem customeForm;
        
        #line default
        #line hidden
        
        
        #line 319 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem EventInstruction;
        
        #line default
        #line hidden
        
        
        #line 322 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadTabItem tb_addSOPApprovals;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/PTPL_SOP_System_UI;component/admin/views/templatemaster.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            ((PTPL_SOP_System_UI.Admin.Views.TemplateMaster)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.busyIndicator1 = ((Telerik.Windows.Controls.RadBusyIndicator)(target));
            return;
            case 3:
            this.btnRefresh = ((Telerik.Windows.Controls.RadToggleButton)(target));
            
            #line 58 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.btnRefresh.Click += new System.Windows.RoutedEventHandler(this.btnRefresh_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 62 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            ((Telerik.Windows.Controls.RadToggleButton)(target)).Click += new System.Windows.RoutedEventHandler(this.AddTemplate_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.templateGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.gridTemplateMaster = ((Telerik.Windows.Controls.RadGridView)(target));
            return;
            case 7:
            
            #line 131 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            ((Telerik.Windows.Controls.RadContextMenu)(target)).ItemClick += new Telerik.Windows.RadRoutedEventHandler(this.RadContextMenu_ItemClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.radDataPager = ((Telerik.Windows.Controls.RadDataPager)(target));
            return;
            case 9:
            this.sp_AddTemplate = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.txtblockAddSOPWI = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.tmpRadControl = ((Telerik.Windows.Controls.RadTabControl)(target));
            
            #line 165 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.tmpRadControl.SelectionChanged += new Telerik.Windows.Controls.RadSelectionChangedEventHandler(this.tmpRadControl_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.tabSopInfo = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            case 13:
            this.txtSopID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.txtSopTemplateName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.txtIssuedDate = ((Telerik.Windows.Controls.RadDateTimePicker)(target));
            return;
            case 16:
            this.txtOriginatorName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.txtEffectiveDate = ((Telerik.Windows.Controls.RadDateTimePicker)(target));
            return;
            case 18:
            this.txtRevisionName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.txStatusName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 20:
            this.drpSopCategory = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 21:
            this.drpArea = ((Telerik.Windows.Controls.RadComboBox)(target));
            
            #line 234 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.drpArea.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.drpArea_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 22:
            this.drpMachine = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 23:
            this.drpEventType = ((Telerik.Windows.Controls.RadComboBox)(target));
            
            #line 253 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.drpEventType.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.drpEventType_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 24:
            this.drpAssinee = ((Telerik.Windows.Controls.RadComboBox)(target));
            return;
            case 25:
            this.txtEstimation = ((System.Windows.Controls.TextBox)(target));
            
            #line 268 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.txtEstimation.LostFocus += new System.Windows.RoutedEventHandler(this.txtEstimation_LostFocus);
            
            #line default
            #line hidden
            return;
            case 26:
            this.txtSOPWIID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.txtSOPWINumber = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.txtSOPWIRevision = ((System.Windows.Controls.TextBox)(target));
            return;
            case 29:
            this.txtFilenmame = ((System.Windows.Controls.TextBox)(target));
            return;
            case 30:
            this.btnBrowse = ((System.Windows.Controls.Button)(target));
            
            #line 294 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.btnBrowse.Click += new System.Windows.RoutedEventHandler(this.btnBrowse_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.txtTemplateDecription = ((System.Windows.Controls.TextBox)(target));
            return;
            case 32:
            this.btnCancel = ((Telerik.Windows.Controls.RadButton)(target));
            
            #line 304 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.RadButton_Cancel_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.btnNext = ((Telerik.Windows.Controls.RadButton)(target));
            
            #line 306 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.btnNext.Click += new System.Windows.RoutedEventHandler(this.btnNext_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            this.btnSave = ((Telerik.Windows.Controls.RadButton)(target));
            
            #line 308 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.btnSave.Click += new System.Windows.RoutedEventHandler(this.btnSave_Click);
            
            #line default
            #line hidden
            return;
            case 35:
            this.btnUpdate = ((Telerik.Windows.Controls.RadButton)(target));
            
            #line 310 "..\..\..\..\..\Admin\Views\TemplateMaster.xaml"
            this.btnUpdate.Click += new System.Windows.RoutedEventHandler(this.btnUpdate_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.customeForm = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            case 37:
            this.EventInstruction = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            case 38:
            this.tb_addSOPApprovals = ((Telerik.Windows.Controls.RadTabItem)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

