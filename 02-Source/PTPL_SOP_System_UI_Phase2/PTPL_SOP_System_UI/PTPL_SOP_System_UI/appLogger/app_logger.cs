﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Configuration;
using System.Windows;

namespace PTPL_SOP_System_UI
{
    public static class app_logger
    {
        #region Variables



        public static readonly Logger appLogger = LogManager.GetCurrentClassLogger();



        #endregion
    
        #region 
        /// <summary>
        /// Logger Implementation 
        /// </summary>
        /// <param name="username"></param>
        public static void CreateLogger(string username)
        {
            try
            {
                LoggingConfiguration config = new LoggingConfiguration();
                string LogDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();
                //string LogDirTrace = ConfigurationManager.AppSettings["LogDirCognex"].ToString();
                bool DebugEnvt = Convert.ToBoolean(ConfigurationManager.AppSettings["DebugEnvironment"]);

                FileTarget fileTarget1 = new FileTarget();
                config.AddTarget("file", fileTarget1);
                string filepath1 = LogDir + username + " " + DateTime.Now.ToString("MM-dd-yyyy|") + DateTime.Now.ToLongTimeString() + "_Error_log";

                FileTarget fileTarget2 = new FileTarget();
                config.AddTarget("file", fileTarget2);
                string filepath2 = LogDir + username + " " + DateTime.Now.ToString("MM-dd-yyyy|") + DateTime.Now.ToLongTimeString() + "_Event_log";

                FileTarget fileTarget3 = new FileTarget();
                config.AddTarget("file", fileTarget3);
                string filepath3 = LogDir + username + " " + DateTime.Now.ToString("MM-dd-yyyy|") + DateTime.Now.ToLongTimeString() + "_Trace_log";

                fileTarget1.FileName = filepath1;
                fileTarget1.Layout = @"${date:format=MM-dd-yyyy\:HH\:mm\:ss}|${level}|${logger}|${message}";

                fileTarget2.FileName = filepath2;
                fileTarget2.Layout = @"${date:format=MM-dd-yyyy\:HH\:mm\:ss}|${level}|${logger}|${message}";

                fileTarget3.FileName = filepath3;
                fileTarget3.Layout = @"${date:format=MM-dd-yyyy\:HH\:mm\:ss}|${level}|${logger}|${message}";

                LoggingRule rule1 = new LoggingRule("*", LogLevel.Error, fileTarget1);
                rule1.Final = true;
                config.LoggingRules.Add(rule1);


                LoggingRule rule2 = new LoggingRule("*", LogLevel.Info, fileTarget2);
                config.LoggingRules.Add(rule2);



                //LoggingRule rule3 = new LoggingRule("*", LogLevel., fileTarget3);
                //config.LoggingRules.Add(rule3);
                config.AddRule(LogLevel.Trace, LogLevel.Trace, fileTarget3);
                LogManager.Configuration = config;
            }
            catch (Exception ex)
            {
                appLogger.Error("Error occurred due to  " + ex.Message);
            }
        }



        #endregion
    }
}