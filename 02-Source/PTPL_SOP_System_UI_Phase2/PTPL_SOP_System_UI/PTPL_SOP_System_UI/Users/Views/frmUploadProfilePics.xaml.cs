﻿using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.Views;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Users.Views
{
    /// <summary>
    ///  Interaction logic for frmUploadProfilePic.xaml
    ///  Created By :  Chetan 
    ///  Created on : 20 March 2019
    ///  -----------------------------------------------------------------------
    ///  Modified By :  
    ///  Modified on : 
    ///  Purpose:     
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks
    public partial class frmUploadProfilePics : UserControl
    {
        #region Variable
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        static string imageBase64 = "";
        int i = 0;
        UserData _userData;
        HttpClient client = new HttpClient();
        frmUserMaster userMaster = new frmUserMaster();
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public delegate void DataChangedEventHandler(object sender, EventArgs e);

        public event DataChangedEventHandler DataChanged;
        #endregion

        #region Constructor
        /// <summary>
        /// This frmUploadProfilePics constructor which is initializing the component and maintaining the user session.
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        public frmUploadProfilePics()
        {
            InitializeComponent();
            _userData = Application.Current.Properties["userData"] as UserData;
            PopulateFields();
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method use to populate the paramater on UI Form
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async void PopulateFields()
        {
            busyIndicator.IsBusy = true;

            txtFname.Text = _userData.firstName;
            txtLname.Text = _userData.lastName;
           

            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\images\\" + _userData.username.ToLower() + ".png";
            ImageViewer1.Source = new BitmapImage(new Uri(path, UriKind.Absolute));


           
            await GetUserData(Convert.ToInt32(_userData.id));
            busyIndicator.IsBusy = false;

        }
        #endregion

        #region Events
        /// <summary>
        /// This event fires onclick of browse button. And used to select image.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Image not found"></exception>
        /// <exception cref="Image is not in proper format and size"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        /// 
        string profilemessage = "";
        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            
            var watch = Stopwatch.StartNew();
            try
            {
                string image = "";
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.DefaultExt = ".pdf"; // Required file extension 
                fileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png"; // Optional file extensions
                fileDialog.ShowDialog();
                txtImageName.Text = fileDialog.SafeFileName;
                fileDialog.RestoreDirectory = true;
                if (!string.IsNullOrEmpty(txtImageName.Text))
                {

                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(fileDialog.FileName);
                    bitmap.EndInit();
                    ImageViewer1.Source = bitmap;
                    imageBase64 = Convert.ToBase64String(File.ReadAllBytes(fileDialog.FileName));
                }

                profilemessage = "Please login back to see updated profile picture.";
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Image unable to upload " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Uanble to upload profile picture");

            }

        }
        /// <summary>
        /// This Events fires onclick of update button. And used to update data into database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="400">API calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        public bool btnUpdate = false;
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            btnUpdate = true;
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;

            if (string.IsNullOrEmpty(txtFname.Text))
            {
                RadWindow.Alert("First Name should not be empty");
                txtFname.Focus();
                return;
            }


            if (string.IsNullOrEmpty(txtLname.Text))
            {
                RadWindow.Alert("Last Name should not be empty");
                txtLname.Focus();
                return;
            }

            if (string.IsNullOrEmpty(txtEmail.Text))
            {
                RadWindow.Alert("Email ID should not be empty");
                txtEmail.Focus();
                return;
            }
            else if (!Regex.IsMatch(txtEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                txtEmail_block1.Text = "Please Enter Valid Email Id";
                txtEmail.Select(0, txtEmail.Text.Length);
                txtEmail.Focus();
                return;
            }

            
            GetUserData(Convert.ToInt32(_userData.id));
            
            watch.Stop();
            app_logger.appLogger.Info("[INFO : " + _userData.username + " profile updated successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
            
            RadWindow.Alert(new DialogParameters { Header = "Success", Content = "Your profile updated successfully. " + profilemessage });

            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            radNavigation.Content = new frmHomePage();
            radNavigation.SelectedIndex = 1;

        }
        #endregion


        /// <summary>
        /// This is getJPGFromImageControl method to get the image and save into memory stream
        /// </summary>
        /// <param name="imageC"></param>
        /// <exception cref="Image not found"></exception>
        /// <exception cref="Image is not in proper format and size"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        public byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }


        /// <summary>
        /// This is GetUserData method used for to get user data which accepting uid i.e. user id as parameter.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">API not found</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> GetUserData(int uid)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;


            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/GetUserById?Id=" + uid;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {


                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            var userModal = JsonConvert.DeserializeObject<UserMasterModel>(finalList.ToString());

                            if (btnUpdate == true)
                            {
                                userModal.uid = uid;
                                userModal.firstName = txtFname.Text;
                                userModal.lastName = txtLname.Text;
                                userModal.email = txtEmail.Text;
                                userModal.location = txtLocation.Text;
                                userModal.contactNumber = txtContact.Text;
                                userModal.profilePicture = getJPGFromImageControl(ImageViewer1.Source as BitmapImage);

                                PostUserData(userModal);
                            }
                            else
                            { 
                                uid = userModal.uid;
                                txtUname.Text = userModal.userName;
                                txtFname.Text = userModal.firstName;
                                txtLname.Text = userModal.lastName;
                                txtEmail.Text = userModal.email;
                                txtContact.Text = userModal.contactNumber;
                                txtLocation.Text = userModal.location;
                                if (userModal.role == 1)
                                {
                                    txtRole.Text = "Admin";
                                }
                                if (userModal.role == 2)
                                {
                                    txtRole.Text = "Supervisor";
                                }
                                if (userModal.role == 3)
                                {
                                    txtRole.Text = "Operator";
                                }
                                ImageViewer1.Source = ByteImageConverter.ByteToImage(userModal.profilePicture);
                               
                            }

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling updating API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                
            }
            return result;

        }


        /// <summary>
        /// This is Cancel button click event on User Information tab
        /// </summary>        
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;

            radNavigation.SelectedIndex = 1;

            radNavigation.Content = new frmHomePage();            

        }



        /// <summary>
        /// This Post method for updating user information which is accepting as userModal  as parameter  accepting userModal as parameter
        /// </summary>
        /// <param name="userModal"></param>
        ///<returns>Success-StatusCode-200</returns>
        /// <exception cref="400">API calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void PostUserPassword(UserPasswordChangeModel userModal1)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client1 = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/ChangeUserPassword";
                    client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    var data = JsonConvert.SerializeObject(userModal1);
                    using (HttpResponseMessage response1 = client1.PostAsJsonAsync(apiUrl, userModal1).Result)
                    {
                        if (response1.IsSuccessStatusCode)
                        {
                            var responseData1 = response1.Content.ReadAsStringAsync().Result;
                            //var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling AddUpdateUser API" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
            }
        }


        /// <summary>
        /// This Post method for updating user information which is accepting as userModal  as parameter  accepting userModal as parameter
        /// </summary>
        /// <param name="userModal"></param>
        ///<returns>Success-StatusCode-200</returns>
        /// <exception cref="400">API calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void PostUserData(UserMasterModel userModal)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client1 = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/AddUpdateUser";
                    client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    var data = JsonConvert.SerializeObject(userModal);
                    using (HttpResponseMessage response1 = client1.PostAsJsonAsync(apiUrl, userModal).Result)
                    {
                        if (response1.IsSuccessStatusCode)
                        {
                            var responseData1 = response1.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling AddUpdateUser API" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
            }
        }


        /// <summary>
        /// This is get and post method for updating user password which is accepting as userModal  as parameter  accepting userModal as parameter
        /// </summary>
        /// <param name="userModal"></param>
        ///<returns>Success-StatusCode-200</returns>
        /// <exception cref="400">API calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> ChangePassword(int uid)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/GetUserById?Id=" + uid;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {

                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];

                            var userModal1 = JsonConvert.DeserializeObject<UserPasswordChangeModel>(finalList.ToString());

                            if (btnUpdatePwdclick == true)
                            {
                                if (userModal1.password != oldPwd.Password)
                                {
                                    RadWindow.Alert("Your old password is not correct");

                                    return false;
                                }

                                if (userModal1.password == oldPwd.Password)
                                {
                                    userModal1.uid = uid;
                                    userModal1.newPassword = confirmPwd.Password;
                                    PostUserPassword(userModal1);

                                    RadWindow.Alert("Password updated successfully.");
                                    oldPwd.Password = "";
                                    newPwd.Password = "";
                                    confirmPwd.Password = "";
                                    tabUserInfo.IsSelected = true;

                                }

                            }

                            result = true;
                            
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling updating API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error occurred while updating user. Please try again");
            }
            return result;
        }

        /// <summary>
        /// This is Cancel button event occurred when user click on cancel button Change password tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnCancelPwd_Click(object sender, RoutedEventArgs e)
        {
            oldPwd.Password = "";
            newPwd.Password = "";
            confirmPwd.Password = "";
            tabUserInfo.IsSelected = true;
        }



        /// <summary>
        /// This is Update button event occurred when user click on udpate button Change password tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="400">API calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        bool btnUpdatePwdclick = false;
        private void btnUpdatePwd_Click(object sender, RoutedEventArgs e)
        {
            btnUpdatePwdclick = true;

            if (string.IsNullOrEmpty(oldPwd.Text))
            {
                RadWindow.Alert("Please enter old password");
                oldPwd.Focus();
                return;
            }

            if (string.IsNullOrEmpty(newPwd.Text))
            {
                RadWindow.Alert("Please enter new password");
                newPwd.Focus();
                return;
            }

            if (string.IsNullOrEmpty(confirmPwd.Text))
            {
                RadWindow.Alert("Please confirm new password");
                confirmPwd.Focus();
                return;
            }

            if (newPwd.Password != confirmPwd.Password)
            {
                RadWindow.Alert("New password and confirm password is not matching.");
                confirmPwd.Focus();
                return;
            }

            ChangePassword(Convert.ToInt32(_userData.id));
        }
    }

    /// <summary>
    /// This ByteImageConverter class which is converting image into byte format
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>    

    public class ByteImageConverter
    {
        public static ImageSource ByteToImage(byte[] imageData)
        {
            BitmapImage biImg = new BitmapImage();
            MemoryStream ms = new MemoryStream(imageData);
            biImg.BeginInit();
            biImg.StreamSource = ms;
            biImg.EndInit();

            ImageSource imgSrc = biImg as ImageSource;

            return imgSrc;
        }
    }

    #region Properties
    /// <summary>
    /// This UserMasterModel class file is having all the  properties  whcih required to get the data
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>
    public class UserMasterModel
    {
        public int uid { get; set; }

        public string userName { get; set; }

        public string password { get; set; }

        public string firstName { get; set; }

        public int? role { get; set; }
        public string lastName { get; set; }

        public byte[] profilePicture { get; set; }

        public bool isActive { get; set; }

        public string email { get; set; }

        public string contactNumber { get; set; }

        public string location { get; set; }

    
    }


    public class UserPasswordChangeModel
    {
        public int uid { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
    }


    #endregion
}
