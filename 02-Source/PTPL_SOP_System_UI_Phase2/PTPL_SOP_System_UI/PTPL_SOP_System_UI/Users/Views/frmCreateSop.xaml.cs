﻿using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.Admin.Views;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using Telerik.Windows.Documents.Fixed.FormatProviders;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf;
using Telerik.Windows.Documents.Fixed.Model;

namespace PTPL_SOP_System_UI.Users.Views
{
    /// <summary>
    ///  Interaction logic for frmCreateSOP.xaml and  this frmCreateSop class file is having all the frmCreateSop related properties and methods/functions
    ///  Created By :  Vishal 
    ///  Created on : 10 March 2019
    ///  -----------------------------------------------------------------------
    ///  Modified By : Chetan
    ///  Modified on : 1 April 2019
    ///  Purpose: Adding Validataions & Binding Assinee Data with Dropdown list
    ///  -------------------------------------------------------------------------
    ///  Modified By : Chetan 
    ///  Modified on : 9 April 2020
    ///  Purpose:  Added functionality of Add Attachments , Remove attachments     
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>   
    /// ///  -------------------------------------------------------------------------
    ///  Modified By : Vishal 
    ///  Modified on : 21 April 2020
    ///  Purpose:  Bug fixing     
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>    

    public partial class frmCreateSop : UserControl
    {

        #region Variables
        /// <summary>
        /// Static connection is used to connect to the API.
        /// Private static Logger is use to log error in the log files.
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>

        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        static int loggerId = 0;
        int i = 0;
        DateTime statrDate, planedFinishDate;
        static string originalEstimation = "";
        //private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        string streamReads = "";
        static double fileSizeCount = 0;
        UserData _userData;
        HttpClient client = new HttpClient();
        List<Attachments> attachmentsList = new List<Attachments>();
        bool returnValue = false;

        


        int sopId;
        #endregion

        #region Constructor
        /// <summary>
        /// In this contructor we are maintaining user session and sopId and by default it has InitializeComponent method to initilized this form.
        /// </summary>
        /// <param name="_sopId"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        public frmCreateSop(int _sopId)
        {
            _userData = Application.Current.Properties["userData"] as UserData;
            this.sopId = _sopId;
            InitializeComponent();
            

        }
        #endregion

        #region WorkInstructionLogger

        #region Events
        /// <summary>
        /// This Event Fires when area selection changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void drpArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            await BindMachine();
            busyIndicator1.IsBusy = false;

        }
        /// <summary>
        /// This Event fires when usercontrol fully loaded. and it is used to initilize the values
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {

            sopHeader.Text = Application.Current.Properties["loggerHeader"].ToString();
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            logDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();
            txtSopNumber.Focus();
            await BindCategory();
            await BindEventType();
            // await BindMachine();
            loggerId = sopId;
            await BindAttachementsGrid();
            await BindArea();
            await BindUsers();
            await bindSOPFileName();
            busyIndicator1.IsBusy = false;
            lblStatus.Text = "Not Started";
            txtSopNumber.Focus();
            txtStartDate.Text = "Auto Assigned";
            txtPlannedFinishDate.Text = "Auto Assigned";
            txtActuallFinishDate.Text = "Auto Assigned";
            btnSave.Content = "Save";
            btnNext.IsEnabled = false;
            if(sopId==0)
            {
                btnSave.IsEnabled = true; ;
                btnNext.IsEnabled = false;
                EnableAllFields();
            }
           
            tabCustomDocuments.Content = new frmCustomForm("Render");

            tabReviewApproval.Content = new frmSopApprovals();

            if (sopId > 0)
            {
                var resultSopData = bindSOPLoggerDataById(sopId);
                if (Application.Current.Properties["loggerEditView"].ToString() == "View")
                {
                    //btnNext.Content = "Next";
                    //btnNext.IsEnabled = false;
                    btnSave.IsEnabled = false;
                    EnableDisableControls(false);
                    btnAddAttchments.IsEnabled = false;
                    txtAttachedFileName.IsEnabled = false;
                    txtAttachementName.IsEnabled = false;
                    btnBrowse.IsEnabled = false;
                    btnComplete.IsEnabled = false;
                    btnNext.IsEnabled = true;

                }
                else if (Application.Current.Properties["loggerEditView"].ToString() == "Edit")
                {
                    //btnNext.Content = "Update";
                    btnSave.Content = "Update";
                    btnNext.IsEnabled = true;
                    
                    //btnSave.Content = "Save";
                }
            }
            else
            {
                Application.Current.Properties["loggerEditView"] = "";
            }
        }
        /// <summary>
        /// This event fires when user unloaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void UserControl_Unloaded(object sender, System.Windows.RoutedEventArgs e)
        {
            Application.Current.Properties["createSOPId"] = 0;
            txtSopNumber.Focus();

        }

        protected void EnableAllFields()
        {
            txtComments.IsEnabled = true;
            txtSopNumber.IsEnabled = true;
            drpEvent.IsEnabled = true;
            drpCategory.IsEnabled = true;
            drpArea.IsEnabled = true;
            drpMachine.IsEnabled = true;
            drpAssinee.IsEnabled = true;
            drpSOPFileName.IsEnabled = true;

        }
        protected void DisableAllFields()
        {
            txtComments.IsEnabled = false;
            txtSopNumber.IsEnabled = false;
            drpEvent.IsEnabled = false;
            drpCategory.IsEnabled = false;
            drpArea.IsEnabled = false;
            drpMachine.IsEnabled = false;
            drpAssinee.IsEnabled = false;
            drpSOPFileName.IsEnabled = false;
        }
        /// <summary>
        /// this event fire onclick of next button. and used to navigate the pages.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Event is not loaded"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        /// 

        private async void btnNext_Click(object sender, RoutedEventArgs e)
        {
            bool result = false;
            apiUrl = string.Empty;

            busyIndicator1.IsBusy = true;
            try
            {
                GetFileNameVM files = drpSOPFileName.SelectedItem as GetFileNameVM;
                Application.Current.Properties["plannedFinishDate"] = txtPlannedFinishDate.Text;
                Application.Current.Properties["logeerID"] = loggerId;
              
                if (files.fileName=="")
                {
                    tabCustomDocuments.IsEnabled = true;
                    tabCustomDocuments.IsSelected = true;
                    InstructDemo.IsSelected = false;
                    InstructDemo.IsEnabled = false;
                    busyIndicator1.IsBusy = false;
                    return;
                }
               

                using (var client = new HttpClient())
                {
                    


                    apiUrl = apiEndPoint + apiVersion + "/TemplateMaster/GetDocumentByFileName?fileName=" + files.fileName;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templatesData"];
                            DocumentTemplate deserializedList = JsonConvert.DeserializeObject<DocumentTemplate>(finalList.ToString());

                            if (!String.IsNullOrEmpty(deserializedList.fileStream.ToString()))
                            {
                                string base64 = deserializedList.fileStream;
                                MemoryStream stream = new MemoryStream(Convert.FromBase64String(base64));
                                FormatProviderSettings settings = new FormatProviderSettings(ReadingMode.OnDemand);
                                PdfFormatProvider provider = new PdfFormatProvider(stream, settings);
                                RadFixedDocument doc = provider.Import();
                                this.pdfViewer.Document = doc;
                                InstructDemo.IsSelected = true;
                                tabCustomDocuments.IsEnabled = false;
                                InstructDemo.IsEnabled = true;
                            }
                            else
                            {
                                // RadWindow.Alert("Document not found for selected items");
                                tabCustomDocuments.IsEnabled = true;
                                InstructDemo.IsSelected = false;
                                InstructDemo.IsEnabled = false;
                                busyIndicator1.IsBusy = false;
                                return;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RadWindow.Alert("No document found.");
            }
            busyIndicator1.IsBusy = false;

          
    

            if (lblStatus.Text == "Completed")
            {
                chkAgreement.IsChecked = true;
            }
           

        }

        /// <summary>
        /// This Event fires after interval of each seconds. depends on what frequency is set for timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Time format is incorrect"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            i++;
            int j = 0;
            string timerValue = "00:0";
            if (i > 9 && j == 0)
            {
                timerValue = "00:" + i;
            }
            else if (i == 60 && j == 0)
            {
                i = 0;
                j++;
                timerValue = "0" + j + ":0" + i;
            }
            else
            {
                timerValue = timerValue + i;
            }
            btnTimer.Content = timerValue;
        }

        /// <summary>
        /// this event fire onclick of create button. it used to create the data in database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref=""></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void btnComplete_Click(object sender, RoutedEventArgs e)
        {
            
            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            busyIndicator1.IsBusy = true;
            var watch = Stopwatch.StartNew();
            try
            {

                planedFinishDate = Convert.ToDateTime(txtPlannedFinishDate.Text);
                statrDate = Convert.ToDateTime(txtStartDate.Text);


                if (btnSave.Content.ToString() == "Update" || btnSave.Content.ToString() == "Save")
                {

                    if (btnSave.Content.ToString() == "Update")
                    { 
                        busyIndicator1.IsBusy = true;
                        await SaveSOP(statrDate, planedFinishDate);
                        busyIndicator1.IsBusy = false;
                    }
                    if (chkAgreement.IsChecked == true)
                    {
                        var saveFlag = await UpdateSopLoggerStatus();

                        if (saveFlag)
                        {
                            // if attachments are present tthen only call Create Attachments functions
                            if (gridAttachments.Items.Count > 0)
                            {
                                await CreateAttachments();
                            }
                           
                           
                            watch.Stop();
                            RadWindow.Alert(new DialogParameters { Header = "Success", Content = "SOP Logger Updated/Created successfully with Status." });
                            app_logger.appLogger.Info("[INFO : SOP Logger Updated/Created successfully with Status ]" + $" | {watch.ElapsedMilliseconds} ms");


                            if (Application.Current.Properties["Home"].ToString() == "HomePage")
                            {

                                radNavigation.Content = new frmHomePage();
                                Application.Current.Properties["Home"] = "";
                            }
                            else
                            {
                                radNavigation.Content = new frmUserLandingPage();

                            } 
                        }
                        else
                        {
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Unable to save SOP ]" + $" | {watch.ElapsedMilliseconds} ms");
                            RadWindow.Alert("Unable to save the SOP. Please try again.");
                        }

                    }
                    else
                    {
                        watch.Stop();
                        RadWindow.Alert(new DialogParameters { Header = "Success", Content = "SOP Logger udpated successfully" });
                        app_logger.appLogger.Info("[INFO : SOP Logger udpated successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        //radNavigation.Content = new frmUserLandingPage();
                        if (Application.Current.Properties["Home"].ToString() == "HomePage")
                        {

                            radNavigation.Content = new frmHomePage();
                            Application.Current.Properties["Home"] = "";
                        }
                        else
                        {
                            radNavigation.Content = new frmUserLandingPage();

                        }
                        return;
                    }
                }

            }

            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : While saving the SOP logger" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Can not complete logger without Save.");
            }

            busyIndicator1.IsBusy = false;
        }
        /// <summary>
        /// This event fires on click of checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// /// <remarks>Code documentation and Review is pending</remarks>

        private void chkAgreement_Checked(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                if (lblStatus.Text == "Completed")
                {
                    btnComplete.IsEnabled = false;
                    chkAgreement.IsEnabled = false;

                }
                else
                {
                    if (btnComplete.IsEnabled)
                        btnComplete.IsEnabled = false;
                    else if (!btnComplete.IsEnabled)
                        btnComplete.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Failed to check agreement document. Please read document carefully.");
                app_logger.appLogger.Error("[ERROR : Faile to check agreement document because of: " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// This evet handled by pdf viwer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// /// <remarks>Code documentation and Review is pending</remarks>
        private void tbCurrentPage_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            TextBox textBox = sender as TextBox;
            try
            {
                if (textBox != null)
                {
                    if (e.Key == System.Windows.Input.Key.Enter)
                    {
                        textBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Failed to check agreement document. Please read document carefully.");
                app_logger.appLogger.Error("[ERROR : Faile to check agreement document because of: " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }

        private async void OnClosedExit(object sender, WindowClosedEventArgs e)
        {
            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            var result = e.DialogResult;
            if (result == true)
            {
                
                busyIndicator1.IsBusy = true;

                if (Application.Current.Properties["Home"].ToString() == "HomePage")
                {
                    radNavigation.Content = new frmHomePage();
                    Application.Current.Properties["Home"] = "";
                }
                else
                {

                    if (btnSave_cliked == true)
                    {
                        radNavigation.Content = new frmUserLandingPage();
                    }

                    tabSopInfo.IsSelected = true;

                    radNavigation.Content = new frmUserLandingPage();
                }

                busyIndicator1.IsBusy = false;

            }
        }
        /// <summary>
        /// This event fire onclick of exit button. and it is used to Navigation of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {

            

            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;

            RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed =  OnClosedExit, Content = " Do you want to exit ?", Header = "Warning" });

            

        }
        /// <summary>
        /// This event fire onclick of Cancle button. and it is used to Navigation of page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();

            if (Application.Current.Properties["Home"].ToString() == "HomePage")
            {
                RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                radNavigation.Content = new frmHomePage();
                Application.Current.Properties["Home"] = "";
            }
            else
            {
                try
                {
                    Application.Current.Properties["createSOPId"] = 0;
                    RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    radNavigation.Content = new frmUserLandingPage();
                }
                catch (Exception ex)
                {
                    watch.Stop();
                    RadWindow.Alert("Something went wrong. Please try again.");
                    app_logger.appLogger.Error("[ERROR : Please try again because  " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                }
            }

        }
        /// <summary>
        /// This enevt fires when scrollbar of pdf viwer changing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="">PDF unable to load</exception>
        /// <remarks>Code documentation and Review is pending</remarks>


        private void pdfViewer_Scroll(object sender, ScrollEventArgs e)
        {
            if (Application.Current.Properties["loggerEditView"].ToString() == "View")
            {
                chkAgreement.IsEnabled = false;
                btnComplete.IsEnabled = false;
            }
            
            else if (Convert.ToInt32(pdfViewer.VerticalScrollBar.Value) >= Convert.ToInt32(pdfViewer.VerticalScrollBar.Maximum))
            {
                if (lblStatus.Text == "Completed")
                {
                    chkAgreement.IsEnabled = false;
                    chkAgreement.IsChecked = true;
                }
                else
                {
                    chkAgreement.IsEnabled = true;
                }
            }
        }


    
        /// <summary>
        /// This event fires when selection of equipment changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="">Unable to bind SOP File name</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void drpEquipment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bindSOPFileName();
        }
        private async void drpSOPFileName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            Application.Current.Properties["sopLoggerID"] = drpSOPFileName.SelectedValue;
            GetFileNameVM files = drpSOPFileName.SelectedItem as GetFileNameVM;
            Application.Current.Properties["sopLoggerNumber"] = files.sopNumber;
            originalEstimation = await GetEstimationBySopId();
            busyIndicator1.IsBusy = false;
        }
        /// <summary>
        /// This event fires onclick of browse button. And used to select file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// /// <exception cref="Error-StatusCode-400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                // fileDialog.DefaultExt = ".pdf"; // Required file extension 
                //fileDialog.Filter = "Text documents (.pdf)|*.pdf"; // Optional file extensions

                fileDialog.ShowDialog();
                txtAttachedFileName.Text = fileDialog.SafeFileName;
                if (!string.IsNullOrEmpty(txtAttachedFileName.Text))
                {
                    byte[] stremeBuffer = ReadFile(fileDialog.FileName);
                    streamReads = Convert.ToBase64String(stremeBuffer);
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to browse " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Please try again.");
            }

        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method used to Bind  databse table data with Area Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">GetAllAreas calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindArea()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/AreaMaster/GetAllAreas";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["areasData"];
                            List<AreaMaster> deserializedList = JsonConvert.DeserializeObject<List<AreaMaster>>(finalList.ToString());
                            drpArea.ItemsSource = deserializedList;
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please try again.");
                app_logger.appLogger.Error("[ERROR : Area bind failed becasue of : " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// his Method used to Bind  databse table data with Category Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">BindCategory calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindCategory()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/CategoryMaster/GetAllCategorys";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["categorysData"];
                            List<CategoryMaster> deserializedList = JsonConvert.DeserializeObject<List<CategoryMaster>>(finalList.ToString());
                            drpCategory.ItemsSource = deserializedList;

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while binding the category " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to bind the Area. Please try again.");
                result = false;
            }
            return result;

        }
        /// <summary>
        /// his Method used to Bind  databse table data with Event Type Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">BindEventType calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindEventType()
        {

            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTypes/GetAllEventTypes";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventDataList"];
                            List<EventTypeMaster> deserializedList = JsonConvert.DeserializeObject<List<EventTypeMaster>>(finalList.ToString());
                            drpEvent.ItemsSource = deserializedList;
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Event type binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to bind Event type. Please try again or check with administrator.");
                result = false;
            }
            return result;

        }
        /// <summary>
        /// This Method used to Bind  databse table data with Machine Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">BindMachine calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindMachine()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/MachineMaster/GetMachineByAreaId?Id=" + drpArea.SelectedValue;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["machinesData"];
                            // DataTable deserializedList = JsonConvert.DeserializeObject<DataTable>(finalList.ToString());
                            // DataTable dt = new DataTable();
                            //dt = deserializedList.;
                            //drpMachine.ItemsSource = 
                            List<MachineMaster> deserializedList = JsonConvert.DeserializeObject<List<MachineMaster>>(finalList.ToString());
                            drpMachine.ItemsSource = deserializedList;
                            drpMachine.DisplayMemberPath = "machineName";
                            drpMachine.SelectedValuePath = "uid";
                            result = true;
                           if (Application.Current.Properties["loggerEditView"]!=null)
                            {
                                drpMachine.SelectedValue = Convert.ToInt32(Application.Current.Properties["selectedMachineID"]);
                            }
                               
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Machine binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to bind Machine or equipement. Please try again or check with administrator.");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// his Method used to Bind  databse table data with Users Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">BindUser calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindUsers()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            List<Assignee> deserializedList = JsonConvert.DeserializeObject<List<Assignee>>(finalList.ToString());
                            drpAssinee.ItemsSource = deserializedList;
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : User binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to select user. Please try again or check with administrator.");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// his Method used to Bind  databse table data with SOP WI Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">bindSOPFileName calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> bindSOPFileName()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/TemplateMaster/GetAllMachineTemplate";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templatesData"];

                            List<GetFileNameVM> deserializedList = JsonConvert.DeserializeObject<List<GetFileNameVM>>(finalList.ToString());
                            List<GetFileNameVM> listFinal = new List<GetFileNameVM>();
                            foreach(var item in deserializedList)
                            {
                                GetFileNameVM getFileNameVM = new GetFileNameVM();
                                getFileNameVM.uid = item.uid;
                                getFileNameVM.templateName = item.sopNumber + " - " + item.templateName;
                                getFileNameVM.sopNumber = item.sopNumber;
                                getFileNameVM.fileName = item.fileName;
                                listFinal.Add(getFileNameVM);
                            }



                            drpSOPFileName.SelectedValuePath = "uid";
                            drpSOPFileName.DisplayMemberPath = "templateName";
                            drpSOPFileName.ItemsSource = listFinal;// deserializedList;

                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : SOP File name binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to select SOP file. Please try again or check with administrator.");
            }
            return result;
        }
        /// <summary>
        /// his Method used create sop logger in database
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">SaveSOP calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> SaveSOP(DateTime startDate, DateTime plannedFinished)
        {
            bool result = false;
            apiUrl = string.Empty;
            var watch = Stopwatch.StartNew();
            try
            {
                int val = 0;
                
                if (btnSave.Content.ToString() != "Update")
                {
                    val = 0;
                    lblStatus.Text = "In-progress";
                }
                else
                {
                    val = sopId;
                }
                var documentModal = new
                {
                    sopDocId = val == 0 ? 0 : val,
                    sopName = txtSopNumber.Text,
                    sopDesc = txtComments.Text,
                    eventType = drpEvent.SelectedValue,
                    sopCategory = drpCategory.SelectedValue,
                    sopwiId = drpSOPFileName.SelectedValue,
                    machineId = drpMachine.SelectedValue,
                    startTime = startDate,
                    planFinishDate = plannedFinished,
                    assignee = drpAssinee.SelectedValue,
                    areaId = drpArea.SelectedValue,
                    actualFinish = 0,
                    complianceStatus = lblStatus.Text,
                    status = lblStatus.Text,
                    createdBy = Convert.ToInt32(_userData.id),
                    createdDate = DateTime.Now,
                    modifiedBy = Convert.ToInt32(_userData.id)
                };

                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/DocumentMaster/AddUpdateDocument";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);

                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, documentModal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);
                            Application.Current.Properties["logeerID"] =  loggerId = resultData.id;

                            // This lines of Code is to send an confirmation email to logged In user email id.

                            bool emailNotification = Convert.ToBoolean(ConfigurationManager.AppSettings["emailNotification"]);

                            if (emailNotification == true)
                            {
                                var apiEmailResult = sendEmailNotification(Convert.ToInt32(_userData.id));
                            }

                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error while saving or updating SOP " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return result;
        }
        /// <summary>
        /// This Method used to Bind  databse table data with Grid
        /// </summary>
        /// <param name="sopID">Accept sopID as parameter</param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">bindSOPLoggerDataById calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> bindSOPLoggerDataById(int sopID)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    DataSet Searchresult = new DataSet();
                    //apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/Document/GetAllDocDetails";
                    apiUrl = apiEndPoint + apiVersion + "/DocumentMaster/GetDocumentById?Id=" + sopID;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["documentsData"];
                            getByIdSOPModel gridResult = JsonConvert.DeserializeObject<getByIdSOPModel>(finalList.ToString());

                            txtSopNumber.Text = gridResult.sopuid.ToString();
                            txtSopNumber.Text = gridResult.sopName;
                            drpCategory.SelectedValue = gridResult.sopCategory;
                            drpEvent.SelectedValue = gridResult.eventType;
                            txtComments.Text = gridResult.sopDesc;
                            drpArea.SelectedValue = gridResult.areaId;
                            drpAssinee.SelectedValue = gridResult.assigneeId;
                            Application.Current.Properties["selectedMachineID"] = drpMachine.SelectedValue = gridResult.machineId;
                            drpSOPFileName.SelectedValue = gridResult.sopwiId;
                             txtStartDate.Text = commonDeclaration.GetConvertedDate(gridResult.startTime.ToString(),_userData.systemTimeZone);
                            txtPlannedFinishDate.Text = commonDeclaration.GetConvertedDate(gridResult.planFinishDate.ToString(), _userData.systemTimeZone);
                            txtActuallFinishDate.Text = commonDeclaration.GetConvertedDate(gridResult.actualFinishDate.ToString(),_userData.systemTimeZone);
                            lblStatus.Text = gridResult.status;
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

                if(result == true)
                {
                    /// Added by ganesh
                    GetFileNameVM files = drpSOPFileName.SelectedItem as GetFileNameVM;
                    apiUrl = apiEndPoint + apiVersion + "/TemplateMaster/GetDocumentByFileName?fileName=" + files.fileName;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage responseFile = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (responseFile.IsSuccessStatusCode)
                        {
                            var responseDataFile = responseFile.Content.ReadAsStringAsync().Result;
                            var joFile = JObject.Parse(responseDataFile);
                            var finalListFile = joFile["templatesData"];
                            DocumentTemplate deserializedList = JsonConvert.DeserializeObject<DocumentTemplate>(finalListFile.ToString());

                            if (!String.IsNullOrEmpty(deserializedList.fileStream))
                            {
                                string base64 = deserializedList.fileStream;
                                MemoryStream stream = new MemoryStream(Convert.FromBase64String(base64));
                                FormatProviderSettings settings = new FormatProviderSettings(ReadingMode.OnDemand);
                                PdfFormatProvider provider = new PdfFormatProvider(stream, settings);
                                RadFixedDocument doc = provider.Import();
                                this.pdfViewer.Document = doc;
                            }
                            else
                            {
                                RadWindow.Alert("Document not found for selected items");
                                busyIndicator1.IsBusy = false;
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                    // End here
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : bindSOPLoggerDataById binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to bind SOP logger by it's id. Please try again.");
            }
            return result;
        }
        protected async Task<string> GetEstimationBySopId()
        {
            string result = "";
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    DataSet Searchresult = new DataSet();
                    apiUrl = apiEndPoint + apiVersion + "/TemplateMaster/GetTemplatesById?Id=" + drpSOPFileName.SelectedValue;
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {

                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templateData"];
                            var gridResult = JsonConvert.DeserializeObject<dynamic>(finalList.ToString());
                            result = gridResult.originalEstimateTime;
                        }
                        else
                        {
                            result = "";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred in GetEstimationBySopId " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error occurred in Estimation");
                return "";
            }
            return result;
        }

        #endregion

        #endregion

        #region Attachments

        #region Methods
        /// <summary>
        /// This Method used to Read File selected from Ui & COnvert the file into file stream.
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            try
            {
                int length = (int)fileStream.Length;  // get file length
                fileSizeCount = length / 1024;
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }
        /// <summary>
        /// This Method used to biand the Grid of attachments
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">BindAttachementsGrid calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindAttachementsGrid()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/loggerAttachmentMaster/GetloggerAttachmentByDocId?id=" + loggerId;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["loggerAttachment"];
                            List<Attachments> deserializedList = JsonConvert.DeserializeObject<List<Attachments>>(finalList.ToString());
                            
                            gridAttachments.Items.AddRange(deserializedList);
                            gridAttachments.Visibility = Visibility.Visible;

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : BindAttachementsGrid binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to bind attachments. Please try again.");
            }
            return result;
        }
        /// <summary>
        /// This method used to clear the selection
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected void ClearInputFields()
        {
            txtattachDescription.Text = "";
            txtAttachedFileName.Text = "";
            txtAttachementName.Text = "";
        }
        /// <summary>
        /// This method is use to call API to send conformation email to assigned user after successfully creating the SOP logger object.
        /// </summary>
        /// <param name="wiId"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected static string sendEmailNotification(int assigneeId)
        {
            string apiResult = string.Empty;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    //apiurl = apiEndPoint + apiVersion + "/EventTriggerSchedule/GetEventTriScheTemplateByWIId?Id=" + wiId;
                    apiUrl = "http://3.216.232.120/visiSOPAPIver1/api/v1.0/UserMaster/SentMailToUserByUId?Id=" + assigneeId;
                    //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = client.GetAsync(apiUrl).Result)
                    {
                        //WriteToFile("response from API:  " + response);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            apiResult = jo["userData"].ToString();
                            //WriteToFile("api retrun with result = SOP-logger created confirmation email is sent successfully to  " + assigneeId + ", " + response);
                        }
                        else
                        {
                            apiResult = "Email not sent : due to :" + response;
                            //WriteToFile("api retrun with result = Email not sent :: due to :" + response);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred in while sending Email notification " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in sending the notification");
            }

            return apiResult;
        }
        /// <summary>
        /// This method used to convert file extention to filetype
        /// </summary>
        /// <param name="fileExt"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected string GetFileType(string fileExt)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                var mimeTypes = new Dictionary<string, string>();
                mimeTypes.Add("3dm", "x-world/x-3dmf");
                mimeTypes.Add("3dmf", "x-world/x-3dmf");
                mimeTypes.Add("a", "application/octet-stream");
                mimeTypes.Add("aab", "application/x-authorware-bin");
                mimeTypes.Add("aam", "application/x-authorware-map");
                mimeTypes.Add("aas", "application/x-authorware-seg");
                mimeTypes.Add("abc", "text/vnd.abc");
                mimeTypes.Add("acgi", "text/html");
                mimeTypes.Add("afl", "video/animaflex");
                mimeTypes.Add("ai", "application/postscript");
                mimeTypes.Add("aif", "audio/aiff");
                mimeTypes.Add("aifc", "audio/aiff");
                mimeTypes.Add("aiff", "audio/aiff");
                mimeTypes.Add("aim", "application/x-aim");
                mimeTypes.Add("aip", "text/x-audiosoft-intra");
                mimeTypes.Add("ani", "application/x-navi-animation");
                mimeTypes.Add("aos", "application/x-nokia-9000-communicator-add-on-software");
                mimeTypes.Add("aps", "application/mime");
                mimeTypes.Add("arc", "application/octet-stream");
                mimeTypes.Add("arj", "application/arj");
                mimeTypes.Add("art", "image/x-jg");
                mimeTypes.Add("asf", "video/x-ms-asf");
                mimeTypes.Add("asm", "text/x-asm");
                mimeTypes.Add("asp", "text/asp");
                mimeTypes.Add("asx", "application/x-mplayer2");
                mimeTypes.Add("au", "audio/basic");
                mimeTypes.Add("avi", "video/avi");
                mimeTypes.Add("avs", "video/avs-video");
                mimeTypes.Add("bcpio", "application/x-bcpio");
                mimeTypes.Add("bin", "application/octet-stream");
                mimeTypes.Add("bm", "image/bmp");
                mimeTypes.Add("bmp", "image/bmp");
                mimeTypes.Add("boo", "application/book");
                mimeTypes.Add("book", "application/book");
                mimeTypes.Add("boz", "application/x-bzip2");
                mimeTypes.Add("bsh", "application/x-bsh");
                mimeTypes.Add("bz", "application/x-bzip");
                mimeTypes.Add("bz2", "application/x-bzip2");
                mimeTypes.Add("c", "text/plain");
                mimeTypes.Add("c++", "text/plain");
                mimeTypes.Add("cat", "application/vnd.ms-pki.seccat");
                mimeTypes.Add("cc", "text/plain");
                mimeTypes.Add("ccad", "application/clariscad");
                mimeTypes.Add("cco", "application/x-cocoa");
                mimeTypes.Add("cdf", "application/cdf");
                mimeTypes.Add("cer", "application/pkix-cert");
                mimeTypes.Add("cha", "application/x-chat");
                mimeTypes.Add("chat", "application/x-chat");
                mimeTypes.Add("class", "application/java");
                mimeTypes.Add("com", "application/octet-stream");
                mimeTypes.Add("conf", "text/plain");
                mimeTypes.Add("cpio", "application/x-cpio");
                mimeTypes.Add("cpp", "text/x-c");
                mimeTypes.Add("cpt", "application/x-cpt");
                mimeTypes.Add("crl", "application/pkcs-crl");
                mimeTypes.Add("css", "text/css");
                mimeTypes.Add("def", "text/plain");
                mimeTypes.Add("der", "application/x-x509-ca-cert");
                mimeTypes.Add("dif", "video/x-dv");
                mimeTypes.Add("dir", "application/x-director");
                mimeTypes.Add("dl", "video/dl");
                mimeTypes.Add("doc", "application/msword");
                mimeTypes.Add("dot", "application/msword");
                mimeTypes.Add("dp", "application/commonground");
                mimeTypes.Add("drw", "application/drafting");
                mimeTypes.Add("dump", "application/octet-stream");
                mimeTypes.Add("dv", "video/x-dv");
                mimeTypes.Add("dvi", "application/x-dvi");
                mimeTypes.Add("dwf", "drawing/x-dwf (old)");
                mimeTypes.Add("dwg", "application/acad");
                mimeTypes.Add("dxf", "application/dxf");
                mimeTypes.Add("eps", "application/postscript");
                mimeTypes.Add("es", "application/x-esrehber");
                mimeTypes.Add("etx", "text/x-setext");
                mimeTypes.Add("evy", "application/envoy");
                mimeTypes.Add("exe", "application/octet-stream");
                mimeTypes.Add("f", "text/plain");
                mimeTypes.Add("f90", "text/x-fortran");
                mimeTypes.Add("fdf", "application/vnd.fdf");
                mimeTypes.Add("fif", "image/fif");
                mimeTypes.Add("fli", "video/fli");
                mimeTypes.Add("flv", "video/x-flv");
                mimeTypes.Add("for", "text/x-fortran");
                mimeTypes.Add("fpx", "image/vnd.fpx");
                mimeTypes.Add("g", "text/plain");
                mimeTypes.Add("g3", "image/g3fax");
                mimeTypes.Add("gif", "image/gif");
                mimeTypes.Add("gl", "video/gl");
                mimeTypes.Add("gsd", "audio/x-gsm");
                mimeTypes.Add("gtar", "application/x-gtar");
                mimeTypes.Add("gz", "application/x-compressed");
                mimeTypes.Add("h", "text/plain");
                mimeTypes.Add("help", "application/x-helpfile");
                mimeTypes.Add("hgl", "application/vnd.hp-hpgl");
                mimeTypes.Add("hh", "text/plain");
                mimeTypes.Add("hlp", "application/x-winhelp");
                mimeTypes.Add("htc", "text/x-component");
                mimeTypes.Add("htm", "text/html");
                mimeTypes.Add("html", "text/html");
                mimeTypes.Add("htmls", "text/html");
                mimeTypes.Add("htt", "text/webviewhtml");
                mimeTypes.Add("htx", "text/html");
                mimeTypes.Add("ice", "x-conference/x-cooltalk");
                mimeTypes.Add("ico", "image/x-icon");
                mimeTypes.Add("idc", "text/plain");
                mimeTypes.Add("ief", "image/ief");
                mimeTypes.Add("iefs", "image/ief");
                mimeTypes.Add("iges", "application/iges");
                mimeTypes.Add("igs", "application/iges");
                mimeTypes.Add("ima", "application/x-ima");
                mimeTypes.Add("imap", "application/x-httpd-imap");
                mimeTypes.Add("inf", "application/inf");
                mimeTypes.Add("ins", "application/x-internett-signup");
                mimeTypes.Add("ip", "application/x-ip2");
                mimeTypes.Add("isu", "video/x-isvideo");
                mimeTypes.Add("it", "audio/it");
                mimeTypes.Add("iv", "application/x-inventor");
                mimeTypes.Add("ivr", "i-world/i-vrml");
                mimeTypes.Add("ivy", "application/x-livescreen");
                mimeTypes.Add("jam", "audio/x-jam");
                mimeTypes.Add("jav", "text/plain");
                mimeTypes.Add("java", "text/plain");
                mimeTypes.Add("jcm", "application/x-java-commerce");
                mimeTypes.Add("jfif", "image/jpeg");
                mimeTypes.Add("jfif-tbnl", "image/jpeg");
                mimeTypes.Add("jpe", "image/jpeg");
                mimeTypes.Add("jpeg", "image/jpeg");
                mimeTypes.Add("jpg", "image/jpeg");
                mimeTypes.Add("jps", "image/x-jps");
                mimeTypes.Add("js", "application/x-javascript");
                mimeTypes.Add("jut", "image/jutvision");
                mimeTypes.Add("kar", "audio/midi");
                mimeTypes.Add("ksh", "application/x-ksh");
                mimeTypes.Add("la", "audio/nspaudio");
                mimeTypes.Add("lam", "audio/x-liveaudio");
                mimeTypes.Add("latex", "application/x-latex");
                mimeTypes.Add("lha", "application/lha");
                mimeTypes.Add("lhx", "application/octet-stream");
                mimeTypes.Add("list", "text/plain");
                mimeTypes.Add("lma", "audio/nspaudio");
                mimeTypes.Add("log", "text/plain");
                mimeTypes.Add("lsp", "application/x-lisp");
                mimeTypes.Add("lst", "text/plain");
                mimeTypes.Add("lsx", "text/x-la-asf");
                mimeTypes.Add("ltx", "application/x-latex");
                mimeTypes.Add("lzh", "application/octet-stream");
                mimeTypes.Add("lzx", "application/lzx");
                mimeTypes.Add("m", "text/plain");
                mimeTypes.Add("m1v", "video/mpeg");
                mimeTypes.Add("m2a", "audio/mpeg");
                mimeTypes.Add("m2v", "video/mpeg");
                mimeTypes.Add("m3u", "audio/x-mpequrl");
                mimeTypes.Add("man", "application/x-troff-man");
                mimeTypes.Add("map", "application/x-navimap");
                mimeTypes.Add("mar", "text/plain");
                mimeTypes.Add("mbd", "application/mbedlet");
                mimeTypes.Add("mc$", "application/x-magic-cap-package-1.0");
                mimeTypes.Add("mcd", "application/mcad");
                mimeTypes.Add("mcf", "image/vasa");
                mimeTypes.Add("mcp", "application/netmc");
                mimeTypes.Add("me", "application/x-troff-me");
                mimeTypes.Add("mht", "message/rfc822");
                mimeTypes.Add("mhtml", "message/rfc822");
                mimeTypes.Add("mid", "audio/midi");
                mimeTypes.Add("midi", "audio/midi");
                mimeTypes.Add("mif", "application/x-frame");
                mimeTypes.Add("mime", "message/rfc822");
                mimeTypes.Add("mjf", "audio/x-vnd.audioexplosion.mjuicemediafile");
                mimeTypes.Add("mjpg", "video/x-motion-jpeg");
                mimeTypes.Add("mm", "application/base64");
                mimeTypes.Add("mme", "application/base64");
                mimeTypes.Add("mod", "audio/mod");
                mimeTypes.Add("moov", "video/quicktime");
                mimeTypes.Add("mov", "video/quicktime");
                mimeTypes.Add("movie", "video/x-sgi-movie");
                mimeTypes.Add("mp2", "audio/mpeg");
                mimeTypes.Add("mp3", "audio/mpeg3");
                mimeTypes.Add("mpa", "audio/mpeg");
                mimeTypes.Add("mpc", "application/x-project");
                mimeTypes.Add("mpe", "video/mpeg");
                mimeTypes.Add("mpeg", "video/mpeg");
                mimeTypes.Add("mpg", "video/mpeg");
                mimeTypes.Add("mpga", "audio/mpeg");
                mimeTypes.Add("mpp", "application/vnd.ms-project");
                mimeTypes.Add("mpt", "application/x-project");
                mimeTypes.Add("mpv", "application/x-project");
                mimeTypes.Add("mpx", "application/x-project");
                mimeTypes.Add("mrc", "application/marc");
                mimeTypes.Add("ms", "application/x-troff-ms");
                mimeTypes.Add("mv", "video/x-sgi-movie");
                mimeTypes.Add("my", "audio/make");
                mimeTypes.Add("mzz", "application/x-vnd.audioexplosion.mzz");
                mimeTypes.Add("nap", "image/naplps");
                mimeTypes.Add("naplps", "image/naplps");
                mimeTypes.Add("nc", "application/x-netcdf");
                mimeTypes.Add("ncm", "application/vnd.nokia.configuration-message");
                mimeTypes.Add("nif", "image/x-niff");
                mimeTypes.Add("niff", "image/x-niff");
                mimeTypes.Add("nix", "application/x-mix-transfer");
                mimeTypes.Add("nsc", "application/x-conference");
                mimeTypes.Add("nvd", "application/x-navidoc");
                mimeTypes.Add("o", "application/octet-stream");
                mimeTypes.Add("oda", "application/oda");
                mimeTypes.Add("omc", "application/x-omc");
                mimeTypes.Add("omcd", "application/x-omcdatamaker");
                mimeTypes.Add("omcr", "application/x-omcregerator");
                mimeTypes.Add("p", "text/x-pascal");
                mimeTypes.Add("p10", "application/pkcs10");
                mimeTypes.Add("p12", "application/pkcs-12");
                mimeTypes.Add("p7a", "application/x-pkcs7-signature");
                mimeTypes.Add("p7c", "application/pkcs7-mime");
                mimeTypes.Add("pas", "text/pascal");
                mimeTypes.Add("pbm", "image/x-portable-bitmap");
                mimeTypes.Add("pcl", "application/vnd.hp-pcl");
                mimeTypes.Add("pct", "image/x-pict");
                mimeTypes.Add("pcx", "image/x-pcx");
                mimeTypes.Add("pdf", "application/pdf");
                mimeTypes.Add("pfunk", "audio/make");
                mimeTypes.Add("pgm", "image/x-portable-graymap");
                mimeTypes.Add("pic", "image/pict");
                mimeTypes.Add("pict", "image/pict");
                mimeTypes.Add("pkg", "application/x-newton-compatible-pkg");
                mimeTypes.Add("pko", "application/vnd.ms-pki.pko");
                mimeTypes.Add("pl", "text/plain");
                mimeTypes.Add("plx", "application/x-pixclscript");
                mimeTypes.Add("pm", "image/x-xpixmap");
                mimeTypes.Add("png", "image/png");
                mimeTypes.Add("pnm", "application/x-portable-anymap");
                mimeTypes.Add("pot", "application/mspowerpoint");
                mimeTypes.Add("pov", "model/x-pov");
                mimeTypes.Add("ppa", "application/vnd.ms-powerpoint");
                mimeTypes.Add("ppm", "image/x-portable-pixmap");
                mimeTypes.Add("pps", "application/mspowerpoint");
                mimeTypes.Add("ppt", "application/mspowerpoint");
                mimeTypes.Add("ppz", "application/mspowerpoint");
                mimeTypes.Add("pre", "application/x-freelance");
                mimeTypes.Add("prt", "application/pro_eng");
                mimeTypes.Add("ps", "application/postscript");
                mimeTypes.Add("psd", "application/octet-stream");
                mimeTypes.Add("pvu", "paleovu/x-pv");
                mimeTypes.Add("pwz", "application/vnd.ms-powerpoint");
                mimeTypes.Add("py", "text/x-script.phyton");
                mimeTypes.Add("pyc", "applicaiton/x-bytecode.python");
                mimeTypes.Add("qcp", "audio/vnd.qcelp");
                mimeTypes.Add("qd3", "x-world/x-3dmf");
                mimeTypes.Add("qd3d", "x-world/x-3dmf");
                mimeTypes.Add("qif", "image/x-quicktime");
                mimeTypes.Add("qt", "video/quicktime");
                mimeTypes.Add("qtc", "video/x-qtc");
                mimeTypes.Add("qti", "image/x-quicktime");
                mimeTypes.Add("qtif", "image/x-quicktime");
                mimeTypes.Add("ra", "audio/x-pn-realaudio");
                mimeTypes.Add("ram", "audio/x-pn-realaudio");
                mimeTypes.Add("ras", "application/x-cmu-raster");
                mimeTypes.Add("rast", "image/cmu-raster");
                mimeTypes.Add("rexx", "text/x-script.rexx");
                mimeTypes.Add("rf", "image/vnd.rn-realflash");
                mimeTypes.Add("rgb", "image/x-rgb");
                mimeTypes.Add("rm", "application/vnd.rn-realmedia");
                mimeTypes.Add("rmi", "audio/mid");
                mimeTypes.Add("rmm", "audio/x-pn-realaudio");
                mimeTypes.Add("rmp", "audio/x-pn-realaudio");
                mimeTypes.Add("rng", "application/ringing-tones");
                mimeTypes.Add("rnx", "application/vnd.rn-realplayer");
                mimeTypes.Add("roff", "application/x-troff");
                mimeTypes.Add("rp", "image/vnd.rn-realpix");
                mimeTypes.Add("rpm", "audio/x-pn-realaudio-plugin");
                mimeTypes.Add("rt", "text/richtext");
                mimeTypes.Add("rtf", "text/richtext");
                mimeTypes.Add("rtx", "application/rtf");
                mimeTypes.Add("rv", "video/vnd.rn-realvideo");
                mimeTypes.Add("s", "text/x-asm");
                mimeTypes.Add("s3m", "audio/s3m");
                mimeTypes.Add("saveme", "application/octet-stream");
                mimeTypes.Add("sbk", "application/x-tbook");
                mimeTypes.Add("scm", "application/x-lotusscreencam");
                mimeTypes.Add("sdml", "text/plain");
                mimeTypes.Add("sdp", "application/sdp");
                mimeTypes.Add("sdr", "application/sounder");
                mimeTypes.Add("sea", "application/sea");
                mimeTypes.Add("set", "application/set");
                mimeTypes.Add("sgm", "text/sgml");
                mimeTypes.Add("sgml", "text/sgml");
                mimeTypes.Add("sh", "application/x-bsh");
                mimeTypes.Add("shtml", "text/html");
                mimeTypes.Add("sid", "audio/x-psid");
                mimeTypes.Add("sit", "application/x-sit");
                mimeTypes.Add("skd", "application/x-koan");
                mimeTypes.Add("skm", "application/x-koan");
                mimeTypes.Add("skp", "application/x-koan");
                mimeTypes.Add("skt", "application/x-koan");
                mimeTypes.Add("sl", "application/x-seelogo");
                mimeTypes.Add("smi", "application/smil");
                mimeTypes.Add("smil", "application/smil");
                mimeTypes.Add("snd", "audio/basic");
                mimeTypes.Add("sol", "application/solids");
                mimeTypes.Add("spc", "application/x-pkcs7-certificates");
                mimeTypes.Add("spl", "application/futuresplash");
                mimeTypes.Add("spr", "application/x-sprite");
                mimeTypes.Add("sprite", "application/x-sprite");
                mimeTypes.Add("src", "application/x-wais-source");
                mimeTypes.Add("ssi", "text/x-server-parsed-html");
                mimeTypes.Add("ssm", "application/streamingmedia");
                mimeTypes.Add("sst", "application/vnd.ms-pki.certstore");
                mimeTypes.Add("step", "application/step");
                mimeTypes.Add("stl", "application/sla");
                mimeTypes.Add("stp", "application/step");
                mimeTypes.Add("sv4cpio", "application/x-sv4cpio");
                mimeTypes.Add("sv4crc", "application/x-sv4crc");
                mimeTypes.Add("svf", "image/vnd.dwg");
                mimeTypes.Add("svr", "application/x-world");
                mimeTypes.Add("swf", "application/x-shockwave-flash");
                mimeTypes.Add("t", "application/x-troff");
                mimeTypes.Add("talk", "text/x-speech");
                mimeTypes.Add("tar", "application/x-tar");
                mimeTypes.Add("tbk", "application/toolbook");
                mimeTypes.Add("tcl", "application/x-tcl");
                mimeTypes.Add("tcsh", "text/x-script.tcsh");
                mimeTypes.Add("tex", "application/x-tex");
                mimeTypes.Add("texi", "application/x-texinfo");
                mimeTypes.Add("texinfo", "application/x-texinfo");
                mimeTypes.Add("text", "text/plain");
                mimeTypes.Add("tgz", "application/x-compressed");
                mimeTypes.Add("tif", "image/tiff");
                mimeTypes.Add("tr", "application/x-troff");
                mimeTypes.Add("tsi", "audio/tsp-audio");
                mimeTypes.Add("tsp", "audio/tsplayer");
                mimeTypes.Add("tsv", "text/tab-separated-values");
                mimeTypes.Add("turbot", "image/florian");
                mimeTypes.Add("txt", "text/plain");
                mimeTypes.Add("uil", "text/x-uil");
                mimeTypes.Add("uni", "text/uri-list");
                mimeTypes.Add("unis", "text/uri-list");
                mimeTypes.Add("unv", "application/i-deas");
                mimeTypes.Add("uri", "text/uri-list");
                mimeTypes.Add("uris", "text/uri-list");
                mimeTypes.Add("ustar", "application/x-ustar");
                mimeTypes.Add("uu", "application/octet-stream");
                mimeTypes.Add("vcd", "application/x-cdlink");
                mimeTypes.Add("vcs", "text/x-vcalendar");
                mimeTypes.Add("vda", "application/vda");
                mimeTypes.Add("vdo", "video/vdo");
                mimeTypes.Add("vew", "application/groupwise");
                mimeTypes.Add("viv", "video/vivo");
                mimeTypes.Add("vivo", "video/vivo");
                mimeTypes.Add("vmd", "application/vocaltec-media-desc");
                mimeTypes.Add("vmf", "application/vocaltec-media-file");
                mimeTypes.Add("voc", "audio/voc");
                mimeTypes.Add("vos", "video/vosaic");
                mimeTypes.Add("vox", "audio/voxware");
                mimeTypes.Add("vqe", "audio/x-twinvq-plugin");
                mimeTypes.Add("vqf", "audio/x-twinvq");
                mimeTypes.Add("vql", "audio/x-twinvq-plugin");
                mimeTypes.Add("vrml", "application/x-vrml");
                mimeTypes.Add("vrt", "x-world/x-vrt");
                mimeTypes.Add("vsd", "application/x-visio");
                mimeTypes.Add("vst", "application/x-visio");
                mimeTypes.Add("vsw", "application/x-visio");
                mimeTypes.Add("w60", "application/wordperfect6.0");
                mimeTypes.Add("w61", "application/wordperfect6.1");
                mimeTypes.Add("w6w", "application/msword");
                mimeTypes.Add("wav", "audio/wav");
                mimeTypes.Add("wb1", "application/x-qpro");
                mimeTypes.Add("wbmp", "image/vnd.wap.wbmp");
                mimeTypes.Add("web", "application/vnd.xara");
                mimeTypes.Add("wiz", "application/msword");
                mimeTypes.Add("wk1", "application/x-123");
                mimeTypes.Add("wmf", "windows/metafile");
                mimeTypes.Add("wml", "text/vnd.wap.wml");
                mimeTypes.Add("wmlc", "application/vnd.wap.wmlc");
                mimeTypes.Add("wmls", "text/vnd.wap.wmlscript");
                mimeTypes.Add("wmlsc", "application/vnd.wap.wmlscriptc");
                mimeTypes.Add("word", "application/msword");
                mimeTypes.Add("wp", "application/wordperfect");
                mimeTypes.Add("wp5", "application/wordperfect");
                mimeTypes.Add("wp6", "application/wordperfect");
                mimeTypes.Add("wpd", "application/wordperfect");
                mimeTypes.Add("wq1", "application/x-lotus");
                mimeTypes.Add("wri", "application/mswrite");
                mimeTypes.Add("wrl", "application/x-world");
                mimeTypes.Add("wrz", "model/vrml");
                mimeTypes.Add("wsc", "text/scriplet");
                mimeTypes.Add("wsrc", "application/x-wais-source");
                mimeTypes.Add("wtk", "application/x-wintalk");
                mimeTypes.Add("xbm", "image/x-xbitmap");
                mimeTypes.Add("xdr", "video/x-amt-demorun");
                mimeTypes.Add("xgz", "xgl/drawing");
                mimeTypes.Add("xif", "image/vnd.xiff");
                mimeTypes.Add("xl", "application/excel");
                mimeTypes.Add("xla", "application/excel");
                mimeTypes.Add("xlb", "application/excel");
                mimeTypes.Add("xlc", "application/excel");
                mimeTypes.Add("xld", "application/excel");
                mimeTypes.Add("xlk", "application/excel");
                mimeTypes.Add("xll", "application/excel");
                mimeTypes.Add("xlm", "application/excel");
                mimeTypes.Add("xls", "application/excel");
                mimeTypes.Add("xlsx", "application/excel");
                mimeTypes.Add("xlt", "application/excel");
                mimeTypes.Add("xlv", "application/excel");
                mimeTypes.Add("xlw", "application/excel");
                mimeTypes.Add("xm", "audio/xm");
                mimeTypes.Add("xml", "text/xml");
                mimeTypes.Add("xmz", "xgl/movie");
                mimeTypes.Add("xpix", "application/x-vnd.ls-xpix");
                mimeTypes.Add("xpm", "image/x-xpixmap");
                mimeTypes.Add("x-png", "image/png");
                mimeTypes.Add("xsr", "video/x-amt-showrun");
                mimeTypes.Add("xwd", "image/x-xwd");
                mimeTypes.Add("xyz", "chemical/x-pdb");
                mimeTypes.Add("z", "application/x-compress");
                mimeTypes.Add("zip", "application/x-compressed");
                mimeTypes.Add("zoo", "application/octet-stream");
                mimeTypes.Add("zsh", "text/x-script.zsh");
                mimeTypes.Add("docx", "Microsoft Word Documnets");
                mimeTypes.Add("xaml", "xaml file");
                mimeTypes.Add("pptx", "presentation file");
                return mimeTypes[fileExt].ToString();
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to find the the file extension " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to find extension. Please check.");
                return "Unknown -" + fileExt;
                
            }
        }
        /// <summary>
        /// This method used to save attachments documnets in database
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">CraateAttachments calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> CreateAttachments()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            var attachmentsUploadList = new List<Attachments>();
            foreach (Attachments item in gridAttachments.Items)
            {

                attachmentsUploadList.Add(new Attachments { attachmentId = item.attachmentId, attachmentName = item.attachmentName, createdBy = Convert.ToInt32(_userData.id), createdDate = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss"), description = item.description, documentId = loggerId, fileExentaion = item.fileExentaion, fileName = item.fileName, fileSize = item.fileSize, fileStream = item.fileStream });
            }

            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/loggerAttachmentMaster/AddUpdateloggerAttachment";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);

                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, attachmentsUploadList).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);


                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to create attachment " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to create attachment. Please try again.");
            }
            return result;

        }
        /// <summary>
        /// This method Used to remove attachmnets from DB and from Grid as well
        /// </summary>
        /// <param name="attachmentID"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">RemoveAttachments calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> RemoveAttachments(int attachmentID)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/loggerAttachmentMaster/DeleteloggerAttachment?loggerAttachmentId=" + attachmentID;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);

                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);


                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to remove attachment " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to remove attachment. Please try again.");
            }
            return result;

        }
        protected async Task<bool> UpdateSopLoggerStatus()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            string status = "";
            try
            {
                DateTime plannedFinishedDate = Convert.ToDateTime(txtPlannedFinishDate.Text);

                //plannedfinished > actualFinish
                if (DateTime.Compare(plannedFinishedDate, DateTime.Now) > 0)
                {
                    status = "Missed";
                }
                else
                {
                    status = "Completed";
                }


                var modal = new
                {
                    sopDocId = loggerId,
                    actualFinishDateDate = DateTime.Now,
                    complianceStatus = status,
                    status = "Completed"
                };
                using (var client = new HttpClient())
                {
                    DataSet Searchresult = new DataSet();
                    apiUrl = apiEndPoint + apiVersion + "/DocumentMaster/UpdateSoploggerStatus";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, modal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error in updating logger status " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in updating logger status");
            }
            return result;
        }
        private static DateTime calculateEstimationtime(string sopEstimation, DateTime startDate)
        {
            DateTime returnValue = startDate;
            var watch = Stopwatch.StartNew();
            double weeks = 0, addweeks = 0, days = 0, hours = 0, min = 0;
            try
            {
                string[] eList = sopEstimation.Split(' ');
                foreach (var element in eList)
                {
                    if (element.Contains("w"))
                    {
                        weeks = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        addweeks = 7 * weeks;
                        returnValue = returnValue.AddDays(addweeks);
                    }
                    if (element.Contains("d"))
                    {
                        days = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddDays(days);
                    }
                    if (element.Contains("h"))
                    {
                        hours = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddHours(hours);
                    }
                    if (element.Contains("m"))
                    {
                        min = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddMinutes(min);
                    }
                }
                Console.WriteLine(returnValue);
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error in Calculating Estimation time " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in Calculating Estimation time ");
            }
            return returnValue;
        }
        #endregion

        #region Events
        /// <summary>
        /// This event fires onclick of add attachments. and used to add the input fields data to grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnAddAttchments_Click(object sender, RoutedEventArgs e)
        {
            returnValue = false;
            var watch = Stopwatch.StartNew();
            if (String.IsNullOrEmpty(txtAttachementName.Text.Trim()))
            {
                RadWindow.Alert("Please enter attachment name");
                txtAttachementName.Focus();
                return;
            }
            if (String.IsNullOrEmpty(txtAttachedFileName.Text.Trim()))
            {
                RadWindow.Alert("Please select file");
                txtAttachedFileName.Focus();
                return;
            }

            try
            {

                Attachments attachments = new Attachments();
                attachments.attachmentId = 0;
                attachments.attachmentName = txtAttachementName.Text.Trim();
                attachments.createdByName = _userData.firstName + " " + _userData.lastName;
                attachments.fileName = txtAttachedFileName.Text.Trim();
                attachments.fileExentaion = GetFileType(Path.GetExtension(txtAttachedFileName.Text).Remove(0, 1));
                attachments.fileSize = fileSizeCount.ToString() + "KB";
                attachments.createdDate = DateTime.Now.ToString("dd-MM-yyyy hh:mm tt");
                attachments.description = txtattachDescription.Text.Trim();
                attachments.fileStream = streamReads;

                attachmentsList.Add(attachments);
                gridAttachments.Items.Add(attachments);
                gridAttachments.Visibility = Visibility.Visible;
                streamReads = "";
                fileSizeCount = 0;

                ClearInputFields();
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to add attachment " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to add attachment. Please try again.");
            }

        }

       
        private async void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var result = e.DialogResult;
            if (result == true)
            {
                returnValue = true;
            }
        }
        /// <summary>
        /// This event used to remove the data from grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void RadButton_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                dynamic item = e.OriginalSource;
                Attachments item1 = item.DataContext;
                //var reurnValue = MessageBox.Show("Are you suer to remove " + item1.attachmentName + " attachment?", "Warrning", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                //if (reurnValue == MessageBoxResult.Yes)
                RadWindow.Confirm(new DialogParameters {  CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnClosed, Content = "Are you sure to remove " + item1.attachmentName + " attachment?", Header = "Confirmation" });
                if (returnValue == true)
                {

                    busyIndicator1.IsBusy = true;

                    if (item1.attachmentId != 0)
                    {
                        await RemoveAttachments(item1.attachmentId);

                    }
                    gridAttachments.Items.Remove(item1);
                    busyIndicator1.IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to remove attachment " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to remove attachment. Please try again.");
            }
        }


        bool btnSave_cliked = false;
        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            //validation code
            btnSave_cliked = true;
            if (string.IsNullOrEmpty(txtSopNumber.Text))
            {
                txtSopNumber.Focus();
                RadWindow.Alert("Please enter Title");

                return;
            }
            if (drpCategory.SelectedIndex == -1)
            {
                drpCategory.Focus();
                RadWindow.Alert("Please select Category");
                return;
            }
            if (drpEvent.SelectedIndex == -1)
            {
                drpEvent.Focus();
                RadWindow.Alert("Please select event Type");
                return;
            }
            if (drpMachine.SelectedIndex == -1)
            {

                drpMachine.Focus();
                RadWindow.Alert("Please select Equipment Type");
                return;
            }
            if (drpAssinee.SelectedIndex == -1)
            {
                drpAssinee.Focus();
                RadWindow.Alert("Please select Event Assignee");
                return;
            }
            if (drpSOPFileName.SelectedIndex == -1)
            {
                drpSOPFileName.Focus();
                RadWindow.Alert("Please select SOP Work Instruction");
                return;
            }

            bool result = false;
            apiUrl = string.Empty;
            var watch = Stopwatch.StartNew();
            GetFileNameVM files = drpSOPFileName.SelectedItem as GetFileNameVM;
            try
            {
                //if (btnNext.Content.ToString() != "Update")
                if (btnSave.Content.ToString() != "Update")
                {
                    busyIndicator1.IsBusy = true;
                    statrDate = DateTime.Now;
                    planedFinishDate = calculateEstimationtime(originalEstimation, statrDate);
                    txtPlannedFinishDate.Text = planedFinishDate.ToString("dd MMMM yyyy h:mm tt"); // commonDeclaration.GetConvertedDate(planedFinishDate.ToString(),_userData.systemTimeZone);
                    txtStartDate.Text = statrDate.ToString("dd MMMM yyyy h:mm tt");     //commonDeclaration.GetConvertedDate(statrDate.ToString(),_userData.systemTimeZone);

                    using (var client = new HttpClient())
                    {
                        
                        apiUrl = apiEndPoint + apiVersion + "/TemplateMaster/GetDocumentByFileName?fileName=" + files.fileName;
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                        {
                            if (response.IsSuccessStatusCode)
                            {  //eventDataList
                                var responseData = response.Content.ReadAsStringAsync().Result;
                                var jo = JObject.Parse(responseData);
                                var finalList = jo["templatesData"];
                                DocumentTemplate deserializedList = JsonConvert.DeserializeObject<DocumentTemplate>(finalList.ToString());

                                if (!String.IsNullOrEmpty(deserializedList.fileStream))
                                {
                                    string base64 = deserializedList.fileStream;
                                    MemoryStream stream = new MemoryStream(Convert.FromBase64String(base64));
                                    FormatProviderSettings settings = new FormatProviderSettings(ReadingMode.OnDemand);
                                    PdfFormatProvider provider = new PdfFormatProvider(stream, settings);
                                    RadFixedDocument doc = provider.Import();
                                    this.pdfViewer.Document = doc;
                                }
                                else
                                {
                                    RadWindow.Alert("Document not found for selected items");
                                    busyIndicator1.IsBusy = false;
                                    return;
                                }
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                    //Save Sop Logger On Click of Next Button

                    if (lblStatus.Text == "Not Started")
                    {
                        await SaveSOP(statrDate, planedFinishDate);
                        RadWindow.Alert(new DialogParameters { Header = "Success", Content = "SOP Logger Created successfully." });
                        tabSopInfo.IsEnabled = true;
                        btnSave.IsEnabled = false;
                        btnNext.IsEnabled = true;
                        DisableAllFields();
                    }

                    busyIndicator1.IsBusy = false;
                }
                
                else
                {
                    btnComplete_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred when clicked on next or update button " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
            }
            if (files.fileName == "")
            {
                InstructDemo.IsSelected = false;
                InstructDemo.IsEnabled = false;
                tabCustomDocuments.IsSelected = true;
                tabCustomDocuments.IsEnabled = true;
            }
            else
            {
                InstructDemo.IsSelected = true;
                InstructDemo.IsEnabled = true;
                tabCustomDocuments.IsSelected = false;
                tabCustomDocuments.IsEnabled = false;
            }

            //btnTimer.Foreground = new SolidColorBrush(Colors.Red);
            DispatcherTimer dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }

        private void chkAgreement_Unchecked(object sender, RoutedEventArgs e)
        {
            if (btnComplete.IsEnabled)
                btnComplete.IsEnabled = false;
            else if (!btnComplete.IsEnabled)
                btnComplete.IsEnabled = true;
        }

        private void EnableDisableControls(Boolean flag)
        {
            //tabSopInfo.IsEnabled = flag;
            txtSopNumber.IsEnabled = flag;
            drpCategory.IsEnabled = flag;
            drpEvent.IsEnabled = flag;
            drpArea.IsEnabled = flag;
            drpMachine.IsEnabled = flag;
            drpAssinee.IsEnabled = flag;
            drpSOPFileName.IsEnabled = flag;
            txtComments.IsEnabled = flag;
        }
        #endregion

        #endregion


    }
    #region Properties
    /// <summary>
    /// Having all attachments properties which is required.
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>
    public class Attachments
    {
        public int attachmentId { set; get; }
        public string attachmentName { set; get; }
        public string fileName { set; get; }
        public string description { set; get; }
        public string fileExentaion { set; get; }
        public string fileSize { set; get; }
        public string createdDate { set; get; }
        public string createdByName { set; get; }
        public string fileStream { set; get; }
        public int documentId { set; get; }
        public int createdBy { set; get; }
    }
    #endregion

}
