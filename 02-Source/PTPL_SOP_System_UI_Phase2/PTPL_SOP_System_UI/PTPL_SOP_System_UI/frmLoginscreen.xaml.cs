﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Configuration;
using System.Diagnostics;
using Newtonsoft.Json;
using PTPL_SOP_System_UI.CommonFunctions;
using Telerik.Windows.Controls;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using System.IO;

namespace PTPL_SOP_System_UI
{
    /// <summary>
    /// Interaction logic for frmLoginscreen.xaml
    /// </summary>
    public partial class frmLoginscreen : Window
    {



        #region Variables
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        HttpClient client = new HttpClient();

        #endregion



        #region Methods
        /// <summary>
        /// This method is use to validate username and password entered by user to login into application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code review is pending</remarks>
        private async Task<bool> authenticateUser(string username, string password)
        {
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                var loginModel = new
                {
                    userName = radMaskedTextUserName.Text,
                    password = radMaskedTextPassword.Password
                };
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/UserMaster/Authenticate";
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, loginModel).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            TimeZone zone = TimeZone.CurrentTimeZone;  // Get the system time zone and store into session;
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var loginData = JsonConvert.DeserializeObject<UserData>(responseData);
                            loginData.systemTimeZone = zone.StandardName;
                            Application.Current.Properties["userData"] = loginData as UserData;
                            Application.Current.Properties["createSOPId"] = 0;
                            commonDeclaration.timeZone = loginData.systemTimeZone;
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occurred while reading data from app.config file.  " + ex.Message + "]");
                RadWindow.Alert("Error found : " + ex.Message);
            }
            return result;
        }
        public frmLoginscreen()
        {
            InitializeComponent();
        }
        #endregion



        #region Events
        /// <summary>
        /// This method is used load all controls in login window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code review is pending</remarks>
        private async void frmLoginScreen_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                
                //this.Title = "Login SOP Application " + ConfigurationManager.AppSettings["Environment"] + " " + ConfigurationManager.AppSettings["Release"]; //Application.Current.Properties["AppVersion"]; Application.Current.Properties["AppEnv"]
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                await BindPreferenceMasterGrid();
                logDir = Application.Current.Properties["logDir"].ToString();
                this.Title = "Welcome to " + Application.Current.Properties["AppName"] + " application/" + Application.Current.Properties["AppEnv"] + "/" + ConfigurationManager.AppSettings["Release"]; //Application.Current.Properties["AppVersion"];
                radMaskedTextUserName.Focus();
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occurred while reading data from app.config file.  " + ex.Message + "]");
            }
        }

        /// <summary>
        /// This method will invoke as soon as user click on log in button and will check all required parameter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code review is pending</remarks>
        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            busyIndicator.IsBusy = true;
            try
            {
                var watch = Stopwatch.StartNew();
                if (radMaskedTextUserName.Text == string.Empty)
                {
                    app_logger.appLogger.Info("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[INFO : Username was blank " + "]");
                    RadWindow.Alert("Please enter username.");
                    radMaskedTextUserName.Focus();
                    busyIndicator.IsBusy = false;
                    return;
                }
                if (radMaskedTextPassword.Password == string.Empty)
                {
                    app_logger.appLogger.Info("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[INFO : Password field was blank " + "]");
                    RadWindow.Alert("Please enter password.");
                    radMaskedTextPassword.Focus();
                    busyIndicator.IsBusy = false;
                    return;
                }
                
                var result = await authenticateUser(radMaskedTextUserName.Text, radMaskedTextPassword.Password);

                if (result == true)
                {
                    app_logger.CreateLogger(radMaskedTextUserName.Text);

                    watch.Stop();
					app_logger.appLogger.Error("[ERROR : No error in login ]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Trace("[TRACE : Login Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Info("[INFO : Login Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");

                    this.Hide();
                    app_logger.appLogger.Info("[INFO : After login calling MainWindow ]" + $" | {watch.ElapsedMilliseconds} ms");
                    MainWindow mainhome = new MainWindow();
                    mainhome.Show();
                }
                else
                {
                    app_logger.appLogger.Error("[ERROR : Invalid username or password ]" + $" | {watch.ElapsedMilliseconds} ms");
                    RadWindow.Alert("Login failed: Invalid username or password.");
                    busyIndicator.IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                // app_logger.appLogger.Error("Login failed due to : " + ex.Message);
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occurred while login" + ex.Message + "]");
                RadWindow.Alert("Login failed due to : " + ex.Message);
                busyIndicator.IsBusy = false;
            }
        }

        /// <summary>
        /// This Method used to Bind  preference table data
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindPreferenceMasterGrid()
        {
            bool result = false;
            apiUrl = string.Empty;
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/ConfigurePreferences/GetAllPreferenceSetting";
                    //client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["prefenceSetting"];
                            List<gridData> gridResult = JsonConvert.DeserializeObject<List<gridData>>(finalList.ToString());
                            
                            Application.Current.Properties["logDir"] = gridResult[1].preferenceValue.ToString();
                            Application.Current.Properties["AppName"] = gridResult[0].preferenceValue;
                            Application.Current.Properties["AppVersion"] = gridResult[2].preferenceValue;
                            Application.Current.Properties["AppEnv"] = gridResult[3].preferenceValue;
                            Application.Current.Properties["EmailNotification"] = gridResult[8].preferenceValue;
                            string root = gridResult[1].preferenceValue;
                            if (!Directory.Exists(root))
                            {
                                Directory.CreateDirectory(root);
                            }
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the SOp WI Grid. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
            }
            return result;
        }



        #endregion

        /// <summary>
        /// this method is for login the user if press Enter in Password boxitself.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radMaskedTextPassword_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if (e.Key != Key.Return && e.Key != Key.Enter)

            if (e.Key == Key.Enter)
            {
                btnLogin_Click(sender, e);
            }
        }

        private void frmLoginScreen_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
    }
}