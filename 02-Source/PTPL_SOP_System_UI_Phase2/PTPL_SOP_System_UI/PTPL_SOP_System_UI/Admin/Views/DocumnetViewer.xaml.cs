﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Documents.Fixed.FormatProviders;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf;
using Telerik.Windows.Documents.Fixed.Model;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    ///  Interaction Logic  DocumentViewer.xaml
    ///  Created By  :  chetan 
    ///  Created on  : 20 March 2020
    ///  -----------------------------------------------------------------------
    ///  Modified By : 
    ///  Modified on : 
    ///  Purpose     :  
    ///  -------------------------------------------------------------------------
    ///  Modified By : 
    ///  Modified on : 
    ///  Purpose     :  
    /// </summary>
    public partial class DocumnetViwer : UserControl
    {
        #region Variables
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        int i = 0;
        UserData _userData;
        HttpClient client = new HttpClient();
        int _templateMasterID;
        int sopId;
        #endregion

        #region Constructor
        public DocumnetViwer(int templateMaterId)
        {
            _templateMasterID = templateMaterId;
            InitializeComponent();
            _userData = Application.Current.Properties["userData"] as UserData;
            //app_logger.CreateLogger(_userData.username);
        }
        #endregion

        #region Events
        /// <summary>
        /// This Event handled by pdf Viwer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Void</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void tbCurrentPage_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox != null)
            {
                if (e.Key == System.Windows.Input.Key.Enter)
                {
                    textBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
                }
            }
        }
        /// <summary>
        /// This Event handled by pdf Viwer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            logDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();

            await LoadFile();
            radBusyIndicator.IsBusy = false;
        }
        /// <summary>
        /// This Event Fire on click of back button. It is used to navigate the page and set to landing page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void RadToggleButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            RadNavigationView rdn = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            rdn.Content = new TemplateMaster();
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method used to load file from server and display on PDF viwer.
        /// </summary>   
        protected async Task<bool> LoadFile()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiurl = string.Empty;
            try
            {
               
                using (var client = new HttpClient())
                {
                   
                    apiurl = apiEndPoint + apiVersion + "/TemplateMaster/getFileStremeById?Id=" + _templateMasterID;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["fileStream"];
                            //string deserializedList = JsonConvert.DeserializeObject<string>(finalList.ToString());

                            if (finalList != null && finalList.ToString()!="")
                            {
                                string base64 = finalList.ToString();
                                MemoryStream stream = new MemoryStream(Convert.FromBase64String(base64));
                                FormatProviderSettings settings = new FormatProviderSettings(ReadingMode.OnDemand);
                                PdfFormatProvider provider = new PdfFormatProvider(stream, settings);
                                RadFixedDocument doc = provider.Import();
                                this.pdfViewer.Document = doc;
                                watch.Stop();
                              
                                app_logger.appLogger.Trace("[TRACE : Document Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                                app_logger.appLogger.Info("[INFO : Document Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");

                            }
                            else
                            {
                                watch.Stop();
                                RadWindow.Alert("Document is not attached");
                                app_logger.appLogger.Info("[INFO : Documents Is Empty ]" + $" | {watch.ElapsedMilliseconds} ms");
                            }
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while loading documents in document viewer. " + ex.Message+" ]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return result;
        }
        #endregion


    }

}

