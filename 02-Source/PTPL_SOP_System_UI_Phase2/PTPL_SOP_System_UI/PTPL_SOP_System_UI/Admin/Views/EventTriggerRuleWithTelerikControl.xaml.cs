﻿using System;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    ///  Interaction Logic  CalenderView.xaml
    ///  Created By  :  Chetan 
    ///  Created on  : 02 April 2020
    ///  -----------------------------------------------------------------------
    ///  Modified By : 
    ///  Modified on : 
    ///  Purpose     :  
    /// </summary>
    public partial class EventTriggerRuleWithTelerikControl : UserControl
    {
        #region Constructor
        public EventTriggerRuleWithTelerikControl()
        {
            InitializeComponent();
        }
        #endregion
        #region Events
        /// <summary>
/// This Events use to navigate the pages 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
        private void eventRule_Click(object sender, RoutedEventArgs e)
        {
            if(Application.Current.Properties["scheduledView"].ToString() == "true")
            {
                this.Content = new frmScheduledEvents();
            }
            else
            {
                TemplateModel templateModel = new TemplateModel();
                templateModel.uid = Convert.ToInt32(Application.Current.Properties["wiSopId"]);
                this.Content = new EventTriggerRule(templateModel);
            }
      
        }
        #endregion
    }
    public class CustomReadOnlyBehavior : ReadOnlyBehavior
    {
        public override bool CanSaveAppointment(IReadOnlySettings readOnlySettings, IOccurrence occurrence)
        {
            return false;
        }

        public override bool CanEditAppointment(IReadOnlySettings readOnlySettings, IOccurrence occurrence)
        {
            return false;
        }

        public override bool CanDragAppointment(IReadOnlySettings readOnlySettings, IOccurrence occurrence)
        {
            return false;
        }

        public override bool CanResizeAppointment(IReadOnlySettings readOnlySettings, IOccurrence occurrence)
        {
            return false;
        }

        public override bool CanDeleteAppointment(IReadOnlySettings readOnlySettings, IOccurrence occurrence)
        {
            return false;
        }

        public override bool CanEditSlot(IReadOnlySettings readOnlySettings, Slot slot)
        {
            return false;
        }
    }

}
