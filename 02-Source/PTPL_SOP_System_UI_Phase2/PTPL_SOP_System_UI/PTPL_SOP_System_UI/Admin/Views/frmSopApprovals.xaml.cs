﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Admin.Views
/// <summary>
///  Interaction Logic  EventTriggerRule.xaml
///  Created By  :  Ganesh 
///  Created on  : 15 June 2020
///  -----------------------------------------------------------------------
///  Modified By : 
///  Modified on : 
///  Purpose     : 
///  -------------------------------------------------------------------------
///  Modified By : 
///  Modified on : 
///  Purpose     : 
/// </summary>
///  -------------------------------------------------------------------------
///  Modified By : 
///  Modified on : 
///  Purpose     :  
/// </summary>
/// 
{
    /// <summary>
    /// Interaction logic for frmSopApprovals.xaml
    /// </summary>
    public partial class frmSopApprovals : UserControl
    {
        #region Variables
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        int workInstructionID = 0;
        int eventTriggerId = 0;
        TemplateModel objTemplateModel = null;
        UserData _userData;
        HttpClient client = new HttpClient();
        List<ScheduleEvent> gridDataList = null;
        string sOPNumberInEvent = Application.Current.Properties["SOPWINumber"].ToString();
        #endregion
        public frmSopApprovals()
        {
            InitializeComponent();
            busyIndicator.IsBusy = false;
            _userData = Application.Current.Properties["userData"] as UserData;
        }

        #region Events

        /// <summary>
        /// This event fire when this usercontrol/form loaded first time. Its for initilizing values.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>  
        private async void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                TemplateMaster templateMaster = new TemplateMaster();
                //lblHeader.Text = "SOP Approvals - " + sOPNumberInEvent;
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                logDir = ConfigurationManager.AppSettings["LogDir"].ToString();
                await BindUsers();
                busyIndicator.IsBusy = false;

                if (objTemplateModel.sopTemplateName == "View_Mode")
                {
                    //eventRule.IsEnabled = false;
                    //gridRightContextMenu.IsEnabled = false;
                }


                approvarGrid.Visibility = Visibility.Visible;
                btnaddAprovar.Visibility = Visibility.Visible;
                btnreload.Visibility = Visibility.Visible;
                appovarForm.Visibility = Visibility.Collapsed;
                btnCancel.Visibility = Visibility.Collapsed;
                btnSave.Visibility = Visibility.Collapsed;


                watch.Stop();
                app_logger.appLogger.Info("[INFO : User control loaded successfully.]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR  : Unable to load user control " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }

        }


        private void RadToggleButton_Click(object sender, RoutedEventArgs e)
        {
            approvarGrid.Visibility = Visibility.Collapsed;
            btnaddAprovar.Visibility = Visibility.Collapsed;
            btnreload.Visibility = Visibility.Collapsed;
            appovarForm.Visibility = Visibility.Visible;
            btnCancel.Visibility = Visibility.Visible;
            btnSave.Visibility = Visibility.Visible;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            approvarGrid.Visibility = Visibility.Visible;
            btnaddAprovar.Visibility = Visibility.Visible;
            btnreload.Visibility = Visibility.Visible;
            appovarForm.Visibility = Visibility.Collapsed;
            btnCancel.Visibility = Visibility.Collapsed;
            btnSave.Visibility = Visibility.Collapsed;
        }
        #endregion

        #region Methods

        /// <summary>
        /// This Method Use to bind databse All Users data to UI Assinee combobox
        /// <return>bool</return>
        /// </summary>
        ///<remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindUsers()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            List<Assignee> deserializedList = JsonConvert.DeserializeObject<List<Assignee>>(finalList.ToString());
                            drpproductUserList.ItemsSource = deserializedList;
                            drpqualityUserList.ItemsSource = deserializedList;
                            drpProcessUserList.ItemsSource = deserializedList;
                            drpsafetyUserList.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Users Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Users Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR :  Error occurred while Binding the users in Event Trigger Rule. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        #endregion
    }
}
