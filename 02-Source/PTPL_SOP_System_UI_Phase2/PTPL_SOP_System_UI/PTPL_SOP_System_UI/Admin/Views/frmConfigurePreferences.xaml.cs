﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;


namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    /// Interaction logic for frmConfigurePreferences.xaml
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks
    public partial class frmConfigurePreferences : UserControl
    {
        #region Variables
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        UserData _userData;
        #endregion

        #region Constructor
        public frmConfigurePreferences()
        {
            InitializeComponent();
            _userData = Application.Current.Properties["userData"] as UserData;
        }
        #endregion

        #region Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                logDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();
                radBusyIndicator.IsBusy = true;
                await ReadZones();
                await PopulateFields();
                radBusyIndicator.IsBusy = false;
                watch.Stop();
                app_logger.appLogger.Info("[INFO : Configure preferences page loaded. ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR :  Unable to load user control in Configure preferences because : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                radBusyIndicator.IsBusy = true;
                bool flag = await SaveSOPTemplate();
                radBusyIndicator.IsBusy = false;
                if (flag)
                {
                    RadWindow.Alert(new DialogParameters { Content = "Preferences set successfully.", Header = "Success" });
                    watch.Stop();
                    app_logger.appLogger.Info("[INFO :  preferences set in Configure preferences ]" + $" | {watch.ElapsedMilliseconds} ms");
                    return;
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR :  Unable to set preferences in Configure preferences because : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                watch.Stop();
                app_logger.appLogger.Info("[INFO : Cancel button has been clicked in Configure preferences ]" + $" | {watch.ElapsedMilliseconds} ms");
                radNavigation.Content = new frmHomePage();
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR :  Unable to load home page from Configure preferences because : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected async Task<bool> ReadZones()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/ConfigurePreferences/GetAllTimeZonesList";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["timeZoneDate"];
                            List<TimeZoneList> deserializedList = JsonConvert.DeserializeObject<List<TimeZoneList>>(finalList.ToString());
                            drpTimeZones.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Info("[INFO : Time Zone loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while Binding the Time Zones in SOP WI " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected async Task<bool> PopulateFields()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/ConfigurePreferences/GetAllConfigurePreferences";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["configureDate"];
                            var deserializedList = JsonConvert.DeserializeObject<dynamic>(finalList.ToString());
                            drpTimeZones.Text = deserializedList.serverTimeZone;
                            chkEmailNotification.IsChecked = deserializedList.emailNotification;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Info("[INFO : Time Zone loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while Binding the Time Zones in SOP WI " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method used  to create SOP WI in database
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> SaveSOPTemplate()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            int workInstructionId = 0;
            apiurl = string.Empty;
            try
            {
                var settingPrefrences = new
                {
                    uid = 0,
                    serverTimeZone = drpTimeZones.SelectedValue,
                    emailNotification = chkEmailNotification.IsChecked,
                    createdBy = Convert.ToInt32(_userData.id),
                    createdDate = DateTime.Now,
                    modifiedBy = Convert.ToInt32(_userData.id),
                    modifiedDate = DateTime.Now,

                };

                using (var client = new HttpClient())
                {
                    apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/ConfigurePreferences/AddUpdateConfigurePreferences";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiurl, settingPrefrences).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);
                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while saving sop wi. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                result = false;

            }
            return result;

        }
        #endregion
       
    }
    public class TimeZoneList
    {
        public string timeZones { set; get; }
        public int uid { set; get; }
    }
}
