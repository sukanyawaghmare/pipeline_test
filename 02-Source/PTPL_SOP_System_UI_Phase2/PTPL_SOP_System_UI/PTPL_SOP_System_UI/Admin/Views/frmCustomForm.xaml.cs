﻿using CefSharp;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.Views;
using PTPL_SOP_System_UI.CommonFunctions;
using PTPL_SOP_System_UI.Users.Views;
using System;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using CefSharp.Wpf;

namespace PTPL_SOP_System_UI
{
    /// <summary>
    /// Interaction logic for frmCustomForm.xaml
    /// </summary>
    public partial class frmCustomForm : UserControl
    {
        #region Variables
        /// <summary>
        /// Static connection is used to connect to the API.
        /// Private static Logger is use to log error in the log files.
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
              
        UserData _userData;
        HttpClient client = new HttpClient();
        private static string _type = "";
        JsHandler jsHandler = new JsHandler();

        #endregion

        #region Events

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <exception cref=""></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public frmCustomForm(string type)
        {
            _type = type;
            _userData = Application.Current.Properties["userData"] as UserData;

            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref=""></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                busyIndicator.IsBusy = true;
                string baseUrl = ConfigurationManager.AppSettings["baseUrl"].ToString();
                string mode = Application.Current.Properties["mode"] == null ? "new" : Application.Current.Properties["mode"].ToString();

                if (_type == "Builder")
                {
                    webBrowser.Address = baseUrl + "/VisiQCFormIO/FormUI.html?equipmentId=" + Application.Current.Properties["areaID"].ToString() + "&mode=" + mode + "&sopId=" + Application.Current.Properties["SopObjectID"].ToString() + "&sopWiNumber=" + Application.Current.Properties["SOPWINumber"].ToString() + "&sopTitle=" + Application.Current.Properties["sopTitle"].ToString();
                    // webBrowser.Address = "file:///E:/Apps/BackupOfSource/LeanLogicFormIO/LeanLogicFormIO/FormUI.html?equipmentId="+ Application.Current.Properties["areaID"].ToString()+"&mode="+ mode+"&sopId="+ Application.Current.Properties["SopObjectID"].ToString() + "&sopWiNumber="+Application.Current.Properties["SOPWINumber"].ToString()+"&sopTitle=" + Application.Current.Properties["sopTitle"].ToString(); 
                    CefSharpSettings.LegacyJavascriptBindingEnabled = true;
                    if (!webBrowser.JavascriptObjectRepository.HasBoundObjects)
                        webBrowser.JavascriptObjectRepository.Register("jsHandler", jsHandler, true);

                }
                else if (_type == "Render")
                {

                    webBrowser.Address = baseUrl + "/VisiQCFormIO/FormExample.html?token=" + _userData.token + "&sopID=" + Application.Current.Properties["sopLoggerID"].ToString() + "&sopNumber=" + Application.Current.Properties["sopLoggerNumber"].ToString() + "&loggerId=" + Application.Current.Properties["logeerID"].ToString() + "&mode=" + (Application.Current.Properties["loggerEditView"] == null ? "" : Application.Current.Properties["loggerEditView"].ToString());
                    //webBrowser.Address = "file:///E:/Apps/BackupOfSource/LeanLogicFormIO/LeanLogicFormIO//FormExample.html?token=" + _userData.token+"&sopID="+ Application.Current.Properties["sopLoggerID"].ToString() + "&sopNumber="+ Application.Current.Properties["sopLoggerNumber"].ToString()+ "&loggerId=" + Application.Current.Properties["logeerID"].ToString() + "&mode="+ Application.Current.Properties["loggerEditView"].ToString();
                    CefSharpSettings.LegacyJavascriptBindingEnabled = true;
                    if (!webBrowser.JavascriptObjectRepository.HasBoundObjects)
                        webBrowser.JavascriptObjectRepository.Register("jsHandler", jsHandler, true);
                    else
                        busyIndicator.IsBusy = false;

                }

            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Identified one of attribute is Null while reading SOP details. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("ERROR : Identified one of attribute is Null while reading SOP details.");
            }
        }

        private void webBrowser_FrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                busyIndicator.IsBusy = false;
            });

        }

        #endregion
    }

    class JsHandler
    {
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        UserData _userData;
        public JsHandler()
        {
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            logDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();
            _userData = Application.Current.Properties["userData"] as UserData;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg"></param>
        /// <returns>Success-return-clean</returns>
        /// <exception cref=""></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public void showMessage(string msg)
        {
            MessageBox.Show("Message from JS:" + msg);
        }

        public string getWorkInstructionId()
        {
            return Application.Current.Properties["SopObjectID"].ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arg"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref=""></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        
        public void HandleJsCall(int arg)
        {

            var watch = Stopwatch.StartNew();
            try
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    if (arg == 1)
                    {

                        RadWindow.Alert(new DialogParameters { Header = "Success", Content = "Form saved successfully" });
                    // RadNavigationView radNaviogation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnConfirm, Content = " Do you want to Add Event Trigger Rule ?", Header = "Confirmation" });
                    //  radNaviogation.Content = new TemplateMaster();
                }
                    else if (arg == 0)
                    {
                        RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnClosed, Content = " Are you sure to cancel?", Header = "Confirmation" });
                    }
                    else if (arg == 2)
                    {
                    // RadWindow.Alert(new DialogParameters { Header = "Alert", Content = "Please drag element on Form/Wizard" });
                }
                    else if (arg == 3)
                    {

                        UpdateSopLoggerStatus();
                        RadWindow.Alert(new DialogParameters { Header = "Success", Content = "Response submitted successfully." });
                        RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                        radNavigation.SelectedIndex = 2;
                        radNavigation.Content = new frmUserLandingPage();
                    }
                    else if (arg == 4)
                    {
                        RadWindow.Alert(new DialogParameters { Header = "Warning", Content = "Custom Form not found for selected Sop WI Number." });
                    }
                    else if (arg == 5)
                    {
                        RadWindow.Alert(new DialogParameters { Header = "Warning", Content = "Templates not assigned to selected equipment." });
                    }
                    else if (arg == 6)
                    {
                        RadWindow.Alert(new DialogParameters { Header = "Warning", Content = "Unable to submit response in view mode." });
                    }
                    else if (arg == 7)
                    {
                        RadWindow.Alert(new DialogParameters { Header = "Success", Content = "Form updated successfully" });
                    }
                    else if (arg == 8)
                    {
                        RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnCancleLoggerCustom, Content = "Are you sure you want to cancel?", Header = "Confirmation" });
                    }
                });
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error in OnCancleLoggerCustom logger status " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in OnCancleLoggerCustom logger status");
            }

        }
        
        /// <summary>
        /// This method is use to confirm the popwindows selected option.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="radNavigationView = null"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void OnConfirm(object sender, WindowClosedEventArgs e)
        {

            var watch = Stopwatch.StartNew();
            try
            {

                RadNavigationView radNaviogation = Application.Current.Properties["radNavigationView"] as RadNavigationView;

                RadTabItem radTabItemCust = Application.Current.Properties["radTabItemEventTrigger"] as RadTabItem;
                var result = e.DialogResult;
                if (result == true)
                {
                    radTabItemCust.IsEnabled = true;
                    radTabItemCust.IsSelected = true;
                    TemplateModel templateModel = new TemplateModel();
                    templateModel.uid = Convert.ToInt32(Application.Current.Properties["SopObjectID"]);
                    templateModel.originalEstimateTime = Application.Current.Properties["estimation"].ToString();
                    radTabItemCust.Content = new EventTriggerRule(templateModel);

                }
                else
                {
                    radNaviogation.Content = new TemplateMaster();

                }
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error in OnCancleLoggerCustom logger status " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in OnCancleLoggerCustom logger status");
            }
        }
        
        /// <summary>
        /// This method will update the SOP logger
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="radNavigationView = null"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                var result = e.DialogResult;
                if (result == true)
                {
                    RadNavigationView radNaviogation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    radNaviogation.Content = new TemplateMaster();

                }
            }
           catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error in OnCancleLoggerCustom logger status " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in OnCancleLoggerCustom logger status");
            }
            
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void OnCancleLoggerCustom(object sender, WindowClosedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                var result = e.DialogResult;
                if (result == true)
                {
                    RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    radNavigation.SelectedIndex = 2;
                    radNavigation.Content = new frmUserLandingPage();

                }
            }catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error in OnCancleLoggerCustom logger status " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in OnCancleLoggerCustom logger status");
            }
        }
        
        /// <summary>
        /// This method will update the SOP logger
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> UpdateSopLoggerStatus()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            string status = "";
            try
            {
                DateTime plannedFinishedDate = Convert.ToDateTime(Application.Current.Properties["plannedFinishDate"].ToString());

                //plannedfinished > actualFinish
                if (DateTime.Compare(plannedFinishedDate, DateTime.Now) > 0)
                {
                    status = "Missed";
                }
                else
                {
                    status = "Completed";
                }


                var modal = new
                {
                    sopDocId = Convert.ToInt32(Application.Current.Properties["logeerID"].ToString()),
                    actualFinishDateDate = DateTime.Now,
                    complianceStatus = status,
                    status = "Completed"
                };
                using (var client = new HttpClient())
                {
                    DataSet Searchresult = new DataSet();
                    apiUrl = apiEndPoint + apiVersion + "/DocumentMaster/UpdateSoploggerStatus";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, modal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error in updating logger status " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error in updating logger status");
            }
            return result;
        }
    }
}
