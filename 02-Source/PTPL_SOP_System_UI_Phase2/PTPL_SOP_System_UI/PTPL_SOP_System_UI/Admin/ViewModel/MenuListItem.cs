﻿using System;
using System.Collections.Generic;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    public class MenuListItem
    {

        public string FontFamily { set; get; }
        public string Title { set; get; }
        public string Padding  {set;get;} 
        public string IconGlyph { set; get; }
        string[] adminMenues = { "Dashboard|&#xe023;", "Rearrange Sequance|&#xe51e;", "User Master|&#xe801;", "Product Master|&#xe911;", "Tool Master|&#xe670;", "Configure reference|&#xe50e;", "My Profile|&#xe13a;" };
        
        string[] userMenues = { "Dashboard|&#xe023", "Create SOP Document|document", "My Profile|&#xe13a" };


        public List<MenuListItem> FetchMenuList(string roleId)
        {

             var menuListForAdmin = new List<MenuListItem>();

            try
            {
                if (roleId == "1")
                {
                    foreach (string content in adminMenues)
                    {
                        var menu = new MenuListItem();
                        menu.Title = content.Split('|')[0];
                        menu.FontFamily = "Roboto";
                        menu.IconGlyph = content.Split('|')[1];
                        menu.Padding = "10px";
                        menuListForAdmin.Add(menu);

                    }

                }
                if (roleId == "2")
                {
                    foreach (string content in userMenues)
                    {
                        var menu = new MenuListItem();
                        menu.Title = content.Split('|')[0];
                        menu.FontFamily = "Roboto";
                        menu.IconGlyph = content.Split('|')[1];
                        menu.Padding = "10px";
                        menuListForAdmin.Add(menu);

                    }

                }
               
            }
            catch(Exception ex)
            {
                throw new Exception("FetchMenuList:-" + ex.Message);
            }

            return menuListForAdmin;

        }


    }
}
