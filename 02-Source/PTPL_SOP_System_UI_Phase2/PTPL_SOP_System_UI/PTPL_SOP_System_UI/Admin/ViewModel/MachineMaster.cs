﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    class MachineMaster
    {
        public int uid { set; get; }
        public string machineName { set; get; }
        public int createdBy { set; get; }
        public DateTime? creationDate { set; get; }
        public int modifiedBy { set; get; }
        public DateTime? modifiedDate { set; get; }
        //public int areaId { set; get; }
    }
    class MachineMasterNew
    {
        public int uid { set; get; }
        public string machineName { set; get; }
        public int createdBy { set; get; }
        public DateTime? creationDate { set; get; }
        public int modifiedBy { set; get; }
        public DateTime? modifiedDate { set; get; }
        public int areaId { set; get; }
    }
}
