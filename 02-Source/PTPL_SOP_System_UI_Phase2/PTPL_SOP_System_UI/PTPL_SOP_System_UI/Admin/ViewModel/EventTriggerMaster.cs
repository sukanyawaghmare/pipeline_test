﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    class EventTriggerMaster
    {
        
        public int uid { set; get; }
        public string tiggerName { set; get; }
        public string triggerStatus { set; get; }
        public int createdBy { set; get; }
        public DateTime? creationDate { set; get; }
        public int modifiedBy { set; get; }
        public DateTime? modifiedDate { set; get; }
    }
}
