﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    class GetFileNameVM
    {
        public int uid { set; get; }
        public string fileName { set; get; }
        public string templateName { set; get; }
        public string sopNumber { set; get; }
        public string originalEstimateTime { get; set; }
    }
}
