﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
   //public class DocumentViewModel
   // {
   //         public int sopDocId { get; set; }
   //         public string sopName { get; set; }
   //         public string sopDesc { get; set; }
   //         public string eventName { get; set; }
   //         public string sopCategory { get; set; }
   //         public string status { get; set; }
   //         //public int? CreatedBy { get; set; }
   //         public DateTime? createdDate { get; set; }
                             
   // }


    



public class DocumentViewModel
    {
        //
        public int sopDocId { get; set; }
        public string sopName { get; set; }
        public string sopDesc { get; set; }
        public string eventName { get; set; }
        public string sopCategory { get; set; }
        public string status { get; set; }
        public string area { get; set; }
        public string startDate { get; set; }
        public string plannedFinishedDate { get; set; }
        public string assinee { get; set; }
        public string complanceStatus { get; set; }
        public string equipment { get; set; }
        //public int? CreatedBy { get; set; }
        public string createdDate { get; set; }

        public string wINumber { get; set; }

    
      public int wiID { set; get; }


    }

    public class getByIdSOPModel
    {

        public int sopDocId { get; set; }
        public string sopName { get; set; }
        public string sopDesc { get; set; }
        public int eventType { get; set; }
        public int eventTrigger { get; set; }
        public int sopCategory { get; set; }



        public int equipmentType { get; set; }
        public int sopuid { get; set; }
        public DateTime? startTime { get; set; }
        public DateTime? dueTime { get; set; }

        public string complianceStatus { get; set; }
        public string status { get; set; }
        public int? createdBy { get; set; }
        public DateTime? createdDate { get; set; }
        public int? modifiedBy { get; set; }
        public DateTime? modifiedDate { get; set; }
        public string equipmentTypeNavigation { get; set; }
        public string eventTriggerNavigation { get; set; }
        public string eventTypeNavigation { get; set; }
        public string sopCategoryNavigation { get; set; }





        public int? areaId { get; set; }
        // public int AreaName { get; set; }
        public int? assigneeId { get; set; }
        // public string  AssigneeName { get; set; }



        public int? sopwiId { get; set; }
        public string sopwiName { get; set; }

        public DateTime? planFinishDate { get; set; }
        public DateTime? actualFinishDate { get; set; }



        public int? machineId { get; set; }



    }


    public class SopLoggerByUserDTO
    {
        public int sopDocId { get; set; }
        public string sopName { get; set; }
        public string sopDesc { get; set; }
        public string sopNumber { get; set; }
        public int? sopwiId { get; set; }
        public string sopwiName { get; set; }
        public string startTime { get; set; }
        public string planFinishDate { get; set; }
        // public DateTime? ActualFinishDate { get; set; }
        public string complianceStatus { get; set; }
        public string status { get; set; }
        public string fileName { get; set; }
        public int assigneeId { get; set; }
        public string assigneeName { get; set; }
        public string createdBy { get; set; }
        //public DateTime? CreatedDate { get; set; }
        public string sopLoggerNumber { get; set; }
        public string category { get; set; }
        public string area { get; set; }
        public string equipment { get; set; }
        public string pendingDuration { get; set; }

        public string eventType { get; set; }

        public List<string> preferenceColumn { get; set; }

        public SopLoggerByUserDTO()
        {
            preferenceColumn = new List<string> { "sopDocId", "sopName", "sopNumber" };
        }
    }


}
