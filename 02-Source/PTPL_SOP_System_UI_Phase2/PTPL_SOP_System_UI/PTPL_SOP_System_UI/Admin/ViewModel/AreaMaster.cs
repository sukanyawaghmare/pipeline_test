﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    class AreaMaster
    {
        public int uid { set; get; }
        public string areaName { set; get; }
        public int createdBy { set; get; }
        public DateTime? creationDate { set; get; }
        public int modifiedBy { set; get; }
        public DateTime? modifiedDate { set; get; }
    }
}
