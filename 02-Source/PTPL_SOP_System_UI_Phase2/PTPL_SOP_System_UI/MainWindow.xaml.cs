﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.Admin.Views;
using PTPL_SOP_System_UI.CommonFunctions;
using PTPL_SOP_System_UI.Users.Views;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI
{
    /// <summary>
    ///  Interaction logic for MainWindow.xaml
    ///  Created By :  Vishal 
    ///  Created on : ---
    ///  -----------------------------------------------------------------------
    ///  Modified By : Chetan
    ///  Modified on : 10 April 2019
    ///  Purpose: Adding New Navigation for Usermaster page & Adding comments over all methods and Events
    ///  -------------------------------------------------------------------------
    ///  ///  -----------------------------------------------------------------------
    ///  Modified By : Vishal
    ///  Modified on : 20 April 2019
    ///  Purpose: Added profile pic code and all loggers for every event 
    ///  -------------------------------------------------------------------------
    public partial class MainWindow
    {
        #region Variables
        UserData _userData;
        static string apiUrl = string.Empty;
        string version = ConfigurationManager.AppSettings["Version"];
        string environment = ConfigurationManager.AppSettings["Environment"];
        string appName = ConfigurationManager.AppSettings["AppName"];
        #endregion

        #region Constructor
        public MainWindow()
        {
            var watch = Stopwatch.StartNew();
            InitializeComponent();
            this.DataContext = new AgencyViewModel();
            _userData = Application.Current.Properties["userData"] as UserData;
            Application.Current.Properties["busyIndicator"] = busyIndicator as RadBusyIndicator;
            GetUserData(Convert.ToInt32(_userData.id));

            string path = System.AppDomain.CurrentDomain.BaseDirectory + "\\images\\" + _userData.username.ToLower() +".png";

            if (!File.Exists(path))
            {
                CreateProfilePicture();
            }

            small_icon.Source = new BitmapImage(new Uri(path, UriKind.Absolute));
            watch.Stop();
            app_logger.appLogger.Info("[INFO: Main-window Loaded. ]" + $" | {watch.ElapsedMilliseconds} ms");
        }
        #endregion

        /// <summary>
        /// This method is used for creating new user default profile pic as first letter of first name and first letter of lastname in upper case
        /// </summary>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="textColor"></param>
        /// <param name="backColor"></param>
        /// <param name="filename"></param>
        /// <returns>default image</returns>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        private System.Drawing.Image GenerateAvtarImage(String text, Font font, Color textColor, Color backColor, string filename)
        {
            var watch = Stopwatch.StartNew();
            //first, create a dummy bitmap just to get a graphics object  
            System.Drawing.Image img = new Bitmap(1, 1);
            
            try
            {
               
                Graphics drawing = Graphics.FromImage(img);

                //measure the string to see how big the image needs to be  
                SizeF textSize = drawing.MeasureString(text, font);

                //free up the dummy image and old graphics object  
                img.Dispose();
                drawing.Dispose();

                //create a new image of the right size  
                img = new Bitmap(90, 90);

                drawing = Graphics.FromImage(img);

                //paint the background  
                drawing.Clear(backColor);

                //create a brush for the text  
                Brush textBrush = new SolidBrush(textColor);

                //drawing.DrawString(text, font, textBrush, 0, 0);  
                drawing.DrawString(text, font, textBrush, new Rectangle(-2, 20, 200, 110));

                drawing.Save();

                textBrush.Dispose();
                drawing.Dispose();

                img.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\images\\" + filename.ToLower() + ".png");

                watch.Stop();
                app_logger.appLogger.Info("[INFO: Default profile is created]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Unable to create Image for user" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return img;
        }


        /// <summary>
        /// This is method is used for set first letter in bold, arial format and used to passed the first name, last , font, background color and username for creating profile pic
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        public void CreateProfilePicture()
        {
            var watch = Stopwatch.StartNew();
            try
            {
                Font font = new Font("Arial", 30.0f, System.Drawing.FontStyle.Bold);
                Color fontcolor = ColorTranslator.FromHtml("#FFF");
                Color bgcolor = ColorTranslator.FromHtml("#3F51B5");
                GenerateAvtarImage(_userData.firstName.Substring(0, 1).ToUpper() + _userData.lastName.Substring(0, 1).ToUpper(), font, fontcolor, bgcolor, _userData.username);
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Unable to send first letter or last letter" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }

        #region  events
        /// <summary>
        /// This events fires on click of naviation Menu/Icon
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void RadNavigationView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            RadNavigationViewItem selectedItem = e.AddedItems[0] as RadNavigationViewItem;
            
            switch (selectedItem.Content)
            {
                case "SOP":
                    radNavigationView.Content = new TemplateMaster();
                    app_logger.appLogger.Info("[INFO: Clicked on SOP WI navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "SOP Task":
                    int createSOPId = Convert.ToInt32(Application.Current.Properties["createSOPId"]);
                    if (createSOPId > 0)
                    {
                        radNavigationView.Content = new frmCreateSop(createSOPId);
                        Application.Current.Properties["createSOPId"] = 0;
                    }
                    else
                    {
                        radNavigationView.Content = new frmUserLandingPage();
                    }
                    app_logger.appLogger.Info("[INFO: Clicked on SOP Logger navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "Dashboard":
                    if (_userData.roleId == "1")
                        radNavigationView.Content = new AdminLandingPage();
                    else
                        radNavigationView.Content = new frmUserLandingPage();
                    app_logger.appLogger.Info("[INFO: Clicked on Dashboard navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "My Profile":
                    radNavigationView.Content = new frmUploadProfilePics();
                    app_logger.appLogger.Info("[INFO: Clicked on My Profile navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "User Master":
                    radNavigationView.Content = new frmUserMaster();
                    app_logger.appLogger.Info("[INFO: Clicked on User Management navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "Home":
                    radNavigationView.Content = new frmHomePage();
                    app_logger.appLogger.Info("[INFO: Clicked on Home navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "Preferences":
                    //radNavigationView.Content = new frmConfigurePreferences();
                    radNavigationView.Content = new frmConfigurePreferencesGridView();
                    app_logger.appLogger.Info("[INFO: Clicked on Preferences navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "Session Log":
                    radNavigationView.Content = new frmLogDetails();
                    app_logger.appLogger.Info("[INFO: Clicked on Session Log navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                case "Help":
                    radNavigationView.Content = new frmHelp();
                    app_logger.appLogger.Info("[INFO: Clicked on Help navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;
                    
               case "Scheduled Events":
                    radNavigationView.Content = new frmScheduledEvents();
                    app_logger.appLogger.Info("[INFO: Clicked on Scheduled Events navigation bar. ]" + $" | {watch.ElapsedMilliseconds} ms");
                    break;

                default:
                    break;
            }

        }

       
        protected async Task<bool> GetUserData(int uid)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;

            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/GetUserById?Id=" + uid;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);

                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;   
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            var userModal = JsonConvert.DeserializeObject<UserMasterModel>(finalList.ToString());
                           
                            small_icon.Source = ByteImageConverter.ByteToImage(userModal.profilePicture);
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling updating API because new user has been created without profile picture" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return result;
        }

        /// <summary>
        /// This events Fires on navigationn drawer closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void radNavigationView_PaneClosed(object sender, RoutedEventArgs e)
        {
            small_icon.Visibility = Visibility.Visible;
        }
        /// <summary>
        /// This event fires on when navigation drawer open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void radNavigationView_PaneOpened(object sender, RoutedEventArgs e)
        {
        }
        /// <summary>
        /// This event fires when form fully loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                this.Title = "Welcome to " + Application.Current.Properties["AppName"] + " application/" + Application.Current.Properties["AppEnv"] + "/" + ConfigurationManager.AppSettings["Release"]; //Application.Current.Properties["AppVersion"];
                Application.Current.Properties["radNavigationView"] = radNavigationView;
                radNavigationView.IsPaneOpen = false;
                radNavigationView.PaneHeader = _userData.firstName.ToUpper() + " " + _userData.lastName.Substring(0, 1).ToUpper();

                if (_userData.roleId == "1")
                {
                    radNavigationView.SelectedIndex = 1;
                }
                else
                {
                    createSOPtemp.Visibility = Visibility.Collapsed;
                    //  rearrane_seq.Visibility = Visibility.Collapsed;
                    user_master.Visibility = Visibility.Collapsed;
                    //product_matser.Visibility = Visibility.Collapsed;
                    //tool_matser.Visibility = Visibility.Collapsed;
                    configure_ref.Visibility = Visibility.Collapsed;
                    scheduledEvents.Visibility = Visibility.Collapsed;
                    radNavigationView.Content = new frmHomePage();

                }

                watch.Stop();
                app_logger.appLogger.Info("[INFO: Window loaded along all data like Application title, image, pane header etc. ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Unable to load Window." + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// This Events Fires on clcik of logout Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void logoutwindow_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                radNavigationView.Effect = new BlurEffect();
                RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnClosed, Content = " Are you sure want to exit from application?", Header = "Logout" });
                radNavigationView.Effect = null;
                watch.Stop();
                app_logger.appLogger.Info("[INFO: Clicked on Logout button. ]" + $" | {watch.ElapsedMilliseconds} ms");

                
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Uanble to logout because : " + ex.Message + ". ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// This events fires on logout confirmation box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            var result = e.DialogResult;
            if (result == true)
            {
                watch.Stop();
                app_logger.appLogger.Info("[INFO: User logout. ]" + $" | {watch.ElapsedMilliseconds} ms");

                string exefileLocation = "";
                exefileLocation = AppDomain.CurrentDomain.BaseDirectory + "PTPL_SOP_System_UI.exe";
                Process process = new Process();
                process.StartInfo = new ProcessStartInfo(exefileLocation);
                process.Start();
                Application.Current.Shutdown();
                Environment.Exit(0);

                //Application.Current.Shutdown();
                //Environment.Exit(0);// handle confirmation 


                //frmLoginscreen frmloginscreen = new frmLoginscreen();
                //frmloginscreen.Show();
            }
        }
        /// <summary>
        /// Events Fires On When Closed Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void Window_Closed(object sender, EventArgs e)
        {

            Application.Current.Shutdown();
            Environment.Exit(0);

        }
        #endregion

    }



    #region property
    public class UserMasterModel
    {
      public byte[] profilePicture { get; set; }  
    }
    #endregion
}
