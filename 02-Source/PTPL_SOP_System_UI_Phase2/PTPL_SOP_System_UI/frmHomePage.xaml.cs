﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.CommonFunctions;
using PTPL_SOP_System_UI.Users.Views;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI
{
    /// <summary>
    /// Interaction logic for frmHomePage.xaml
    ///  -----------------------------------------------------------------------
    ///  Created By : Chetan
    ///  Created on : 17 April 2019
    ///  -------------------------------------------------------------------------
    ///  Modified By : Vishal 
    ///  Modified on : 5 May 2020
    ///  Purpose: Added all event loggers  and try catch wherever is missing
    /// </summary>

    /// </summary>
    public partial class frmHomePage : UserControl
    {

        #region Variables
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        UserData _userData;
        HttpClient client = new HttpClient();
        
       
        #endregion
        public frmHomePage()
        {
            var watch = Stopwatch.StartNew();
            InitializeComponent();
            _userData = Application.Current.Properties["userData"] as UserData;
            Application.Current.Properties["Home"] = "HomePage";
            watch.Stop();
            app_logger.appLogger.Info("[INFO: Home page called ]" + $" | {watch.ElapsedMilliseconds} ms");
        }

        /// <summary>
        /// This Events Fires when user control fully loaded. & it used to initilize the value at initial levels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                logDir = Application.Current.Properties["logDir"].ToString(); //ConfigurationManager.AppSettings["LogDir"].ToString();
                busyIndicator1.IsBusy = true;
                //var result = await bindSOPDocumentData();

                await LoadHomePage();
                radGridLogger.Visibility = Visibility.Visible;
                busyIndicator1.IsBusy = false;
                watch.Stop();
                app_logger.appLogger.Info("[INFO: Home Page loaded. ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Unable to load home page because : " + ex.Message + ". ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            
        }

        public async Task<bool> LoadHomePage()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/DocumentMaster/GetSopLoggerByUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["sopLogger"];
                            //DataTable dtResult = JsonConvert.DeserializeObject<DataTable>(finalList.ToString());
                            List<SopLoggerByUserDTO> dtlist = JsonConvert.DeserializeObject<List<SopLoggerByUserDTO>>(finalList.ToString());
                           foreach (SopLoggerByUserDTO item in dtlist)
                            {

                                item.startTime = commonDeclaration.GetConvertedDate(item.startTime, _userData.systemTimeZone); //localDtstartDate.ToString("dd MMMM yyyy h:mm tt");
                                item.planFinishDate = commonDeclaration.GetConvertedDate(item.planFinishDate, _userData.systemTimeZone);
                                item.pendingDuration = GetPendingValue(item.pendingDuration);
                            }
                            radGridLogger.ItemsSource = dtlist;
                            radDataPager.Source = dtlist.ToEnumerable();
                            radDataPager.Visibility = Visibility.Visible;
                            radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = radGridLogger });
                            watch.Stop();
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop(); ;
                RadWindow.Alert("ERROR: Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Exception occurred While Loading Home page " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }

        public string GetPendingValue(string inputValue)
        {
            string returnVal = inputValue;
            string[] vals = inputValue.Split(' ');
            if(vals[0]=="0w")
            {
                returnVal = inputValue.Remove(inputValue.IndexOf("0w"), 3);

            }
            if (vals[1] == "0d")
            {
                returnVal = returnVal.Remove(returnVal.IndexOf("0d"), 3);

            }
            if (vals[2] == "0h")
            {
                returnVal = returnVal.Remove(returnVal.IndexOf("0h"), 3);

            }
            if (vals[3] == "0m")
            {
                returnVal = returnVal.Remove(returnVal.IndexOf("0m"), 2);

            }
            return returnVal;
        }
     
        private void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            dynamic item = e.OriginalSource;
            var watch = Stopwatch.StartNew();

            SopLoggerByUserDTO selectedRow = this.radGridLogger.SelectedItem as SopLoggerByUserDTO;
            try
            {
                if (selectedRow == null)
                {
                    
                    RadWindow.Alert("Please select row to view");
                    
                    app_logger.appLogger.Info("[INFO: Right click on View Homepage without selecting any row ]" + $" | {watch.ElapsedMilliseconds} ms");
                    return;
                }
                var selectedValue = Convert.ToInt32(selectedRow.sopDocId);
                var selectedStatus = selectedRow.status.ToString();
                if (item.Header == "View")
                {
                    Application.Current.Properties["loggerHeader"] = "View SOP logger";
                    watch.Stop();
                    app_logger.appLogger.Info("[INFO: Right click on View in Homepage ]" + $" | {watch.ElapsedMilliseconds} ms");

                    Application.Current.Properties["createSOPId"] = selectedValue;
                    RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    radNavigation.Content = new PTPL_SOP_System_UI.Users.Views.frmCreateSop(selectedValue);
                    Application.Current.Properties["loggerEditView"] = "View";
                    radNavigation.Content = new frmCreateSop(Convert.ToInt32(selectedValue));


                }
                else if (item.Header == "Claim")
                {
                    watch.Stop();
                    app_logger.appLogger.Info("[INFO: Right click on Edit in Homepage ]" + $" | {watch.ElapsedMilliseconds} ms");
                    if (selectedStatus.ToString() != "Completed")
                    {
                        Application.Current.Properties["loggerHeader"] = "Claim SOP logger";
                        Application.Current.Properties["loggerEditView"] = "Edit";
                        Application.Current.Properties["createSOPId"] = selectedValue;
                        RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                        radNavigation.Content = new PTPL_SOP_System_UI.Users.Views.frmCreateSop(selectedValue);
                        radNavigation.Content = new frmCreateSop(Convert.ToInt32(selectedValue));

                    }
                    else
                    {
                        RadWindow.Alert("Selected SOP Logger is already completed.\nPlease Contact Administrator.");
                    }

                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable trigger the event RadMenuItem_Click" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again.");
            }

        }

        private async void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            await LoadHomePage();
            busyIndicator1.IsBusy = false;

        }
    }

}
