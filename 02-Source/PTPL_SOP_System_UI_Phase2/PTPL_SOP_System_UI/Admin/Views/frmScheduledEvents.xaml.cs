﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Effects;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    /// Interaction logic for frmScheduledEvents.xaml
    /// </summary>
    public partial class frmScheduledEvents : UserControl
    {
        #region Variables
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        int workInstructionID = 0;
        int eventTriggerId = 0;
        TemplateModel objTemplateModel = null;
        UserData _userData;
        HttpClient client = new HttpClient();
        List<ScheduleEvent> gridDataList = null;
        List<GetFileNameVM> listFinal = new List<GetFileNameVM>();
        //GenericMethods genericMethods = new GenericMethods();
        #endregion

        public frmScheduledEvents()
        {
            InitializeComponent();
           
            ClearInputFields();
            _userData = Application.Current.Properties["userData"] as UserData;
            
        }


        #region Methods

        //protected async Task<bool> BindEquipmentdropdown()
        //{
        //    var watch = Stopwatch.StartNew();
        //    bool result = false;
        //    apiUrl = string.Empty;
        //    try
        //    {
        //        apiUrl = apiEndPoint + apiVersion + "/MachineMaster/GetAllMachines?Id=";
        //        var list = await genericMethods.BinddropdownList(apiUrl, "machinesData");
        //        if (list.Count > 0)
        //        {
        //            result = true;
        //            drpAssinee.ItemsSource = list.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        watch.Stop();
        //        RadWindow.Alert("Something went wrong. Please contact your administrator.");
        //        app_logger.appLogger.Error("[ERROR :  Error occured while Binding the machinesData in Event Trigger Rule. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
        //        result = false;
        //    }
        //    return result;

        //}

        /// <summary>
        /// This Method Use to bind databse Machine/Equpment data to UI Equiment combobox
        /// <return> bool</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindMachine()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/MachineMaster/GetAllMachines?Id=";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["machinesData"];
                            List<MachineMaster> deserializedList = JsonConvert.DeserializeObject<List<MachineMaster>>(finalList.ToString());
                            drpEquipment.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Machine Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Machine Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");

                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occured while Binding Machine Data in event trigger rule. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }


        //protected async Task<bool> BindUsersdropdown()
        //{
        //    var watch = Stopwatch.StartNew();
        //    bool result = false;
        //    apiUrl = string.Empty;
        //    try
        //    {
        //        apiUrl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
        //        var list = await genericMethods.BinddropdownList(apiUrl, "userData");
        //        if (list.Count > 0)
        //        {
        //            result = true;
        //            drpAssinee.ItemsSource = list.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        watch.Stop();
        //        RadWindow.Alert("Something went wrong. Please contact your administrator.");
        //        app_logger.appLogger.Error("[ERROR :  Error occured while Binding the users in Event Trigger Rule. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
        //        result = false;
        //    }
        //    return result;

        //}

        /// <summary>
        /// This Method Use to bind databse All Users data to UI Assinee combobox
        /// <return>bool</return>
        /// </summary>
        ///<remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindUsers()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            List<Assignee> deserializedList = JsonConvert.DeserializeObject<List<Assignee>>(finalList.ToString());
                            drpAssinee.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Users Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Users Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR :  Error occured while Binding the users in Event Trigger Rule. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
    //END
        /// <summary>
        /// This Method Use to bind databse created events on UI Grid ande calenderView
        /// <return>bool</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> BindEventsGrid()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTriggerSchedule/GetAllEventTriggerSchedule";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventTriggerScheduleData"];
                            gridDataList = JsonConvert.DeserializeObject<List<ScheduleEvent>>(finalList.ToString());
                            List<ScheduleEvent> listSchduleEvents = new List<ScheduleEvent>();
                            foreach (ScheduleEvent item in gridDataList)
                            {
                                DateTime localDt = Convert.ToDateTime(item.timeValue);
                                item.timeValue = localDt.ToString("dd MMMM yyyy h:mm tt");
                                listSchduleEvents.Add(item);
                            }
                            if (gridDataList.Count > 0)
                            {
                                watch.Stop();
                                gridEventMaster.ItemsSource = gridDataList;
                                radDataPager.Source = gridDataList.ToEnumerable();
                                radDataPager.Visibility = Visibility.Visible;
                                radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = gridEventMaster });
                                app_logger.appLogger.Trace("[TRACE : Events Grid Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                                app_logger.appLogger.Info("[INFO : Events Grid Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");

                            }
                            else
                            {
                                gridEventMaster.ItemsSource = null;
                                watch.Stop();
                                app_logger.appLogger.Info("[INFO :  Not a single event attached. ]" + $" | {watch.ElapsedMilliseconds} ms");
                            }

                            result = true;
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while Binding the event rule grid " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return result;
        }
        /// <summary>
        /// This Method Use to bind databse Occurences values on UI  Occurences Combobox
        /// <return>bool</return>
        /// </summary>
        /// 
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> BindOccurences()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventOccurrence/GetAllEventOccurrence";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventOccurrenceData"];
                            List<Occurences> deserializedList = JsonConvert.DeserializeObject<List<Occurences>>(finalList.ToString());
                            drpOcuurences.ItemsSource = deserializedList;
                            drpOcuurences.SelectedIndex = 5;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Occurenced  Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Occurenced  Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");


                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while Binding the Occurances. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return result;
        }
        /// <summary>
        /// This Method Use to bind create Events in database
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> AddEvents(int sopWinID)
        {
            bool result = false;
            DateTime normalDateTimeFormat;
            if (drpEventType.Text == "Time Based")
                normalDateTimeFormat = Convert.ToDateTime(txtTimeValue.SelectedValue.ToString());
            else
                normalDateTimeFormat = DateTime.Now;
            //18-04-2020 08:00:00
            //inspectionLogDTO.PipeNo = !string.IsNullOrEmpty(txtPipenumber.Text.Trim()) ? txtPipenumber.Text.Replace("\"", "''").Trim() : "";
            int userSelectedSopWIId = Convert.ToInt32(drpSOPWI.SelectedValue);

            //ScheduleEvent orignalEstimation = gridDataList.Where(p => p.wiId == userSelectedSopWIId).FirstOrDefault();

            GetFileNameVM orignalEstimation = listFinal.Where(p => p.uid == userSelectedSopWIId).SingleOrDefault();

            DateTime endDateTime = commonDeclaration.calculateEstimationtime(normalDateTimeFormat, orignalEstimation.originalEstimateTime);

            var watch = Stopwatch.StartNew();
            var modal = new
            {
                eventTriggerId = eventTriggerId,
                wiSopId = sopWinID,
                eventType = drpEventType.SelectedValue,
                title = !string.IsNullOrEmpty(txtTitle.Text.Trim()) ? txtTitle.Text.Replace("\"", "''").Trim() : "",
                startTime = TimeZoneInfo.ConvertTimeToUtc(normalDateTimeFormat, TimeZoneInfo.Local),

                endTime = TimeZoneInfo.ConvertTimeToUtc(endDateTime, TimeZoneInfo.Local), //"2020-04-03T08:15:58.805Z",
                occurance = Convert.ToInt32(drpOcuurences.SelectedValue == null ? 6 : drpOcuurences.SelectedValue),
                machineId = drpEquipment.SelectedValue,
                assignee = drpAssinee.SelectedValue,
                condition = !string.IsNullOrEmpty(txtConditon.Text.Trim()) ? txtConditon.Text.Replace("\"", "''").Trim() : "",
                externalEvents = "Test",
                timeValue = normalDateTimeFormat,
                status = chkActive.IsChecked
            };
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTriggerSchedule/AddUpdateEventTriggerSchedule";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, modal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData1 = response.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            result = true;
                            watch.Stop();
                            string displayMsg = string.Empty;
                            if (btnUpdate.Content.ToString() != "Update")
                            {
                                displayMsg = "Event Rules and Triggers Added Successfully. ";
                            }
                            else
                            {
                                displayMsg = "Event Rules and Triggers Updated Successfully. ";
                            }

                            RadWindow.Alert(new DialogParameters { Header = "Success", Content = displayMsg });
                            app_logger.appLogger.Trace("[TRACE : Event added successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : "+ displayMsg  + "  ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  :Error occured while saving the events. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }


        /// <summary>
        /// This Method Use to bind databse EventType data to UI EventType combobox
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindEventType()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTypes/GetAllEventTypes";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventDataList"];
                            List<EventTypeMaster> deserializedList = JsonConvert.DeserializeObject<List<EventTypeMaster>>(finalList.ToString());
                            drpEventType.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : EventType loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : EventType loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  :Error occured while binding the event type.  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method Use to delete single event from DB 
        /// <paramref name="scheduleID"/>
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> DeleteEventSchedule(int scheduleID)
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTriggerSchedule/DeleteEventTriggerSchedule?id=" + scheduleID;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Event deleted successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Event deleted successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  :Error occured while deleting the event.  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;

        }
        /// <summary>
        /// This methods use to clear the selection of input fields
        /// <return>void</return>
        /// </summary>
        protected void ClearInputFields()
        {
            txtTitle.Text = "";
            txtConditon.Text = "";
            txtTimeValue.SelectedValue = null;
            txtConditon.Text = "";
            drpAssinee.SelectedIndex = -1;
            drpEquipment.SelectedIndex = -1;
            drpEventType.SelectedIndex = -1;
            drpOcuurences.SelectedIndex = -1;
            chkActive.IsChecked = false;
            drpSOPWI.SelectedIndex = -1;

        }
        /// <summary>
        /// This methods use to Check the selected time slot should not match with others event time
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected bool CheckAvilabiltyOfTimeSlots()
        {

            bool result = false;
            try
            {
                DateTime userSelectedDateTime = Convert.ToDateTime(txtTimeValue.SelectedValue);
                var matches = gridDataList.Where(p => p.timeValue == userSelectedDateTime.ToString("dd MMMM yyyy h:mm tt"));
                if (matches.Count() > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;

        }

        protected async void AddEvent_SaveUpdate()
        {
            var watch = Stopwatch.StartNew();
            try
            {
                if (String.IsNullOrEmpty(txtTitle.Text))
                {
                    RadWindow.Alert("Please enter title for event");
                    txtTitle.Focus();
                    return;
                }
                if (drpEventType.SelectedIndex == -1)
                {
                    RadWindow.Alert("Please select event type");
                    drpEventType.Focus();
                    return;
                }
                if (drpEventType.Text == "Time Based")
                {
                    if (String.IsNullOrEmpty(txtTimeValue.SelectedValue.ToString()))
                    {
                        RadWindow.Alert("Please select event Date & Time");
                        txtTimeValue.Focus();
                        return;
                    }
                    if (btnUpdate.Content.ToString() != "Update")
                    {
                        if (CheckAvilabiltyOfTimeSlots())
                        {
                            RadWindow.Alert("Event is already scheduled on the selected time, Kindly select different time slot.");
                            txtTimeValue.Focus();
                            return;
                        }
                    }
                }
                if (drpEquipment.SelectedIndex == -1)
                {
                    RadWindow.Alert("Please select equipment");
                    drpEquipment.Focus();
                    return;
                }
                if (drpAssinee.SelectedIndex == -1)
                {
                    RadWindow.Alert("Please select assignee");
                    drpAssinee.Focus();
                    return;
                }
                if(chkActive.IsChecked == false)
                {
                    RadWindow.Alert("Please Checked Active Status.");
                    drpAssinee.Focus();
                    return;
                }
                busyIndicator.IsBusy = true;
                int sopWorkInstructionID = 0;
                
                if (btnUpdate.Content.ToString() != "Update")
                {
                    sopWorkInstructionID = Convert.ToInt32(drpSOPWI.SelectedValue.ToString());
                }
                else
                {
                    sopWorkInstructionID = workInstructionID;// Get the Id From selected rows;
                }
                
                await AddEvents(sopWorkInstructionID);
                await BindEventsGrid();
                
                busyIndicator.IsBusy = false;
                frmGrid.Visibility = Visibility.Visible;
                frmUI.Visibility = Visibility.Hidden;
                btnAddEvent.Visibility = Visibility.Visible;
                btnRefresh.Visibility = Visibility.Visible;
                lblHeader.Visibility = Visibility.Visible;
                lblHeader.Text = "Event Rules and Triggers History";
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong in calendar view. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Unable to calendar view because of : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// This event fire when this usercontrol/form loaded first time. Its for initilizing values.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>  
        private async void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            logDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();
            await bindSOPNo_TitleNames();
            await BindEventType();
            await BindMachine();
            //await BindEquipmentdropdown(); // replacing BindMachine method
            await BindUsers();
            //await BindUsersdropdown();
            await BindEventsGrid();
            await BindOccurences();
            busyIndicator.IsBusy = false;
            busyIndicatorGrid.IsBusy = false;
            gridEventMaster.Visibility = Visibility.Visible;
            txtTimeValue.SelectableDateStart = DateTime.Now;
            
        }
       
        /// <summary>
        /// This event fires on click of cancel button. this event changing the UI screen from Input Control Form to Grid View.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            frmGrid.Visibility = Visibility.Visible;
            frmUI.Visibility = Visibility.Hidden;
            btnRefresh.Visibility = Visibility.Visible;
            btnAddEvent.Visibility = Visibility.Visible;
            lblHeader.Visibility = Visibility.Visible;
           // eventRule.Content = "Add Event Rule";
            lblHeader.Text = "Event Trigger Rule History";
            // btnCalenderView.Visibility = Visibility.Visible;
            // btnBacktoWI.Visibility = Visibility.Visible;
            // eventRule.Visibility = Visibility.Visible;
            //btnAddEvent.IsEnabled = true;
            ClearInputFields();

        }
       
        /// <summary>
        /// This event fires on right click grid. this event used to Edit /Remove Event from Grid
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            btnUpdate.Content = "Update";
            var watch = Stopwatch.StartNew();
            try
            {
                dynamic item = e.OriginalSource;
                var selectedRow = this.gridEventMaster.SelectedItem as ScheduleEvent;
                if (selectedRow == null)
                {
                    RadWindow.Alert("Please select event");
                    return;
                }
                //if (item.Header == "Add Event")
                //{
                //    frmGrid.Visibility = Visibility.Hidden;
                //    frmUI.Visibility = Visibility.Visible;
                //    lblHeader.Text = "Add Event Rules and Triggers";

                //}
                if (item.Header == "Edit Event")
                {
                   // eventRule.Visibility = Visibility.Hidden;
                   drpSOPWI.SelectedValue = selectedRow.wiId;
                    eventTriggerId = Convert.ToInt32(selectedRow.eventTriggerId);
                    txtTitle.Text = selectedRow.title;
                    workInstructionID = selectedRow.wiId;
                    txtConditon.Text = selectedRow.condition;
                    txtTimeValue.SelectedValue = Convert.ToDateTime(selectedRow.timeValue);
                    drpEquipment.Text = selectedRow.machine;
                    drpEventType.Text = selectedRow.eventType;
                    drpOcuurences.Text = selectedRow.occurance;
                    drpAssinee.Text = selectedRow.assignee;
                    chkActive.IsChecked = selectedRow.status;
                    frmGrid.Visibility = Visibility.Hidden;
                    frmUI.Visibility = Visibility.Visible;
                    lblHeader.Text = "Update Event Rules and Triggers";
                    btnAddEvent.IsEnabled = true;
                 //   eventRule.Content = "Back";
                  
                   // btnCalenderView.Visibility = Visibility.Hidden;
                   // btnBacktoWI.Visibility = Visibility.Hidden;
                    watch.Stop();
                    app_logger.appLogger.Trace("[TRACE : Event in Edit Mode]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Info("[INFO : Event in Edit Mode ]" + $" | {watch.ElapsedMilliseconds} ms");
                }
                else if (item.Header == "Remove Event")
                {
                    this.Effect = new BlurEffect();
                    eventTriggerId = Convert.ToInt32(selectedRow.eventTriggerId);
                    RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = onRemove, Content = " Are you sure to remove " + selectedRow.title + " event?", Header = "Remove Event" });
                    this.Effect = null;
                    watch.Stop();
                    app_logger.appLogger.Trace("[TRACE : Event in Remove Mode]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Info("[INFO : Event in Remove Mode ]" + $" | {watch.ElapsedMilliseconds} ms");
                }
                if (item.Header == "Calendar View")
                {
                    workInstructionID = selectedRow.wiId;
                    /// this object hold a single variable selection to caleneder view
                    Application.Current.Properties["calenderViewSM"] = "single";
                    btnCalenderView_Click();
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while right click on scheduled event  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }


        }
        /// <summary>
        /// Remove Events Called On click of Ok Popup Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void onRemove(object sender, WindowClosedEventArgs e)
        {
            var result = e.DialogResult;
            if (result == true)
            {
                busyIndicatorGrid.IsBusy = true;
                await DeleteEventSchedule(Convert.ToInt32(eventTriggerId));
                await BindEventsGrid();
                eventTriggerId = 0;
                busyIndicatorGrid.IsBusy = false;
            }
        }

        /// <summary>
        /// his Method used to Bind  databse table data with SOP WI Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">bindSOPFileName calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> bindSOPNo_TitleNames()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/TemplateMaster/GetAllMachineTemplate";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templatesData"];

                            List<GetFileNameVM> deserializedList = JsonConvert.DeserializeObject<List<GetFileNameVM>>(finalList.ToString());
                            //List<GetFileNameVM> listFinal = new List<GetFileNameVM>();
                            foreach (var item in deserializedList)
                            {
                                GetFileNameVM getFileNameVM = new GetFileNameVM();
                                getFileNameVM.uid = item.uid;
                                getFileNameVM.templateName = item.sopNumber + " - " + item.templateName;
                                getFileNameVM.sopNumber = item.sopNumber;
                                getFileNameVM.fileName = item.fileName;
                                getFileNameVM.originalEstimateTime = item.originalEstimateTime;
                                listFinal.Add(getFileNameVM);
                            }
                            
                            drpSOPWI.SelectedValuePath = "uid";
                            drpSOPWI.DisplayMemberPath = "templateName";
                            drpSOPWI.ItemsSource = listFinal;// deserializedList;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : SOP File name binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to select SOP file. Please try again or check with administrator.  " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// This event fires on selection changed of DateTime Picker. this event used to validate Schedule 
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void txtTimeValue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CheckAvilabiltyOfTimeSlots() && eventTriggerId == 0)
            {

                RadWindow.Alert("Events already schedule on this timming");
                txtTimeValue.SelectedValue = null;
                txtTimeValue.Focus();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            AddEvent_SaveUpdate();
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void drpEventType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (drpEventType.Text == "Time Based")
            {
                drpOcuurences.IsEnabled = true;
                txtTimeValue.IsEnabled = true;
                txtConditon.IsEnabled = false;
            }
            else
            {
                drpOcuurences.IsEnabled = false;
                txtTimeValue.IsEnabled = false;
                txtConditon.IsEnabled = true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            busyIndicator.IsBusy = true;
            await BindEventsGrid();
            busyIndicator.IsBusy = false;

        }

        /// <summary>
        /// 
        /// </summary>
        private void btnCalenderView_Click()
        {
            var watch = Stopwatch.StartNew();
            try
            {
                bool statusFlag = false;
                foreach (var item in gridDataList)
                {
                    if (item.occurance == "Yearly")
                    {
                        statusFlag = true;
                        break;
                    }
                }

                if (statusFlag)
                {
                    RadWindow.Alert(new DialogParameters { Header = "Info", Content = "Calendar view does not support for Yearly view." });

                }
                Application.Current.Properties["scheduledView"] = "true";
                Application.Current.Properties["wiSopId"] = workInstructionID;// objTemplateModel.uid;
                this.Content = new EventTriggerRuleWithTelerikControl();
            }
            catch (Exception ex)
            {

                watch.Stop();
                RadWindow.Alert("Something went wrong in calendar view. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Unable to calendar view because of : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddEvent_Click(object sender, RoutedEventArgs e)
        {

            ClearInputFields();
            btnAddEvent.Visibility = Visibility.Collapsed;
            btnRefresh.Visibility = Visibility.Collapsed;
            frmGrid.Visibility = Visibility.Hidden;
            frmUI.Visibility = Visibility.Visible;
            lblHeader.Text = "Add Event Rules and Triggers";
            btnUpdate.Content = "Create/Save";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCalender_Click(object sender, RoutedEventArgs e)
        {
            /// this object hold a multi SOP WI selection to caleneder view
            Application.Current.Properties["calenderViewSM"] = "multi";
            btnCalenderView_Click();
        }
        #endregion

    }
   
}
