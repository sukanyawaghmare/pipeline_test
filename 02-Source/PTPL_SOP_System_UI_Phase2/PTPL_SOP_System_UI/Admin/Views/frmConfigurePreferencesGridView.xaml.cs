﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    /// Interaction logic for frmConfigurePreferencesGridView.xaml
    /// </summary>
    /// 
    

    public partial class frmConfigurePreferencesGridView : UserControl
    {

        #region Variables
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        UserData _userData;
        
        ObservableCollection<gridData> _gridDataCollection = new ObservableCollection<gridData>();
        public frmConfigurePreferencesGridView()
        {
            var watch = Stopwatch.StartNew();
            try
            {
                InitializeComponent();
                
                _userData = Application.Current.Properties["userData"] as UserData;
                configurePreferencegrid.ItemsSource = _gridDataCollection;
                defaultTemplate.Visibility = Visibility.Visible;
                editPreferencegrid.Visibility = Visibility.Collapsed;
                addPreference.Visibility = Visibility.Visible;
                watch.Stop();
                app_logger.appLogger.Info("[INFO : Configure preferences initialized ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch(Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Configure preferences initialization failed : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Configure preferences initialization failed.");
            }
        }

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                GetPreferenceByUser();
                busyIndicator1.IsBusy = true;
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                await BindPreferenceMasterGrid();
                await BindPreferenceStorage();
                configurePreferencegrid.Visibility = Visibility.Visible;
                busyIndicator1.IsBusy = false;
                watch.Stop();
                app_logger.appLogger.Info("[INFO : Configure preferences grid loaded ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Configure preferences grid load failed : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Configure preferences grid load failed.");
            }
        }
        private async void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                border_List.Visibility = Visibility.Collapsed;
                drpValuesArea.Visibility = Visibility.Collapsed;
                listGrid.Visibility = Visibility.Collapsed;
                listGrid.Visibility = Visibility.Collapsed;
                editButton.Visibility = Visibility.Collapsed;
                addButtonlist.Visibility = Visibility.Collapsed;
                removeButton.Visibility = Visibility.Collapsed;

                dynamic item = e.OriginalSource;
                var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
                Application.Current.Properties["selectedDataRow"] = selectedRow;
                var selectedItem = selectedRow;

                txt_addColButton.Visibility = Visibility.Collapsed;
                btnAdd.Visibility = Visibility.Collapsed;

                Application.Current.Properties["datatype"] = selectedItem.storageType;

                if (selectedRow == null)
                {
                    RadWindow.Alert("Please select row from grid.");
                    return;
                }

                if (item.Header == "Edit")
                {
                    busyIndicator1.IsBusy = true;

                    //await GetStorageByPId(selectedItem.preferenceId);

                    if (selectedItem.preferenceName == "Equipment")
                    {
                        radListBox.ItemsSource = "";
                        drpValuesArea.Visibility = Visibility.Visible;
                    }



                    await DropDownBinding(selectedItem.preferenceName);
                    RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    defaultTemplate.Visibility = Visibility.Collapsed;
                    editPreferencegrid.Visibility = Visibility.Visible;

                    addPreference.Visibility = Visibility.Collapsed;
                    txtblockName.Text = "Edit preference setting";
                    txtPrefernceName.Text = selectedItem.preferenceName;


                    txtStorage.Text = selectedItem.storageType;
                    txtPrefernceType.Text = selectedItem.storageType;

                    if (txtStorage.Text == "Text")
                    {
                        txtValues.Visibility = Visibility.Visible;
                        chkValues.Visibility = Visibility.Collapsed;
                        txtValues.Text = selectedItem.preferenceValue;

                        if (selectedItem.preferenceName == "Log Directory")
                        {
                            txtValues.IsEnabled = false;
                        }
                        else
                        {
                            txtValues.IsEnabled = true;
                        }
                    }
                    else if (txtStorage.Text == "Bool")
                    {
                        //drpValues.Visibility = Visibility.Collapsed;
                        txtValues.Visibility = Visibility.Collapsed;
                        chkValues.Visibility = Visibility.Visible;
                        if (selectedItem.preferenceValue == "True")
                        {
                            chkValues.IsChecked = true;
                        }
                        else
                        {
                            chkValues.IsChecked = false;
                        }
                    }
                    else
                    {

                        busyIndicator1.IsBusy = true;
                        await BindArea();
                        busyIndicator1.IsBusy = false;
                        txtValues.Visibility = Visibility.Collapsed;
                        chkValues.Visibility = Visibility.Collapsed;
                        txtPrefernceType.Text = "List";
                        border_List.Visibility = Visibility.Visible;
                        listGrid.Visibility = Visibility.Visible;
                        editButton.Visibility = Visibility.Visible;
                        addButtonlist.Visibility = Visibility.Visible;
                        removeButton.Visibility = Visibility.Visible;

                        foreach (string selectedValue in selectedItem.preferenceValue.Split(','))
                        {
                            drpValues.Text += selectedValue + ",";
                        }

                        drpValues.Text = drpValues.Text.Remove(drpValues.Text.LastIndexOf(','), 1);
                    }

                    txtScope.Text = selectedItem.scope;
                    txtCategory.Text = selectedItem.category;
                    txtDesc.Text = selectedItem.description;
                    busyIndicator1.IsBusy = false;
                    watch.Stop();
                    app_logger.appLogger.Info("[INFO : Right clicked on grid and form loaded ]" + $" | {watch.ElapsedMilliseconds} ms");
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Right clicked on configure preference grid and form load failed : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to load the form. Please try again.");
            }

        }
        private void RadMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Material;component/Themes/System.Windows.xaml", UriKind.RelativeOrAbsolute)
            });
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Material;component/Themes/Telerik.Windows.Controls.xaml", UriKind.RelativeOrAbsolute)
            });
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Material;component/Themes/Telerik.Windows.Controls.Input.xaml", UriKind.RelativeOrAbsolute)
            });

            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Material;component/Themes/Telerik.Windows.Controls.GridView.xaml", UriKind.RelativeOrAbsolute)
            });

            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Material;component/Themes/Telerik.Windows.Controls.Navigation.xaml", UriKind.RelativeOrAbsolute)
            });
        }

        private void RadMenuItem_Click_1(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Office2016Touch;component/Themes/System.Windows.xaml", UriKind.RelativeOrAbsolute)
            });
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Office2016Touch;component/Themes/Telerik.Windows.Controls.xaml", UriKind.RelativeOrAbsolute)
            });
            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Office2016Touch;component/Themes/Telerik.Windows.Controls.Input.xaml", UriKind.RelativeOrAbsolute)
            });

            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Office2016Touch;component/Themes/Telerik.Windows.Controls.GridView.xaml", UriKind.RelativeOrAbsolute)
            });

            Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
            {
                Source = new Uri("/Telerik.Windows.Themes.Office2016Touch;component/Themes/Telerik.Windows.Controls.Navigation.xaml", UriKind.RelativeOrAbsolute)
            });
        }

        private void RadButton_Click_cancel(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                editPreferencegrid.Visibility = Visibility.Hidden;
                defaultTemplate.Visibility = Visibility.Visible;
                app_logger.appLogger.Info("[INFO : Cancel button clicked on the preference grid]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Cancel clicked failed : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Cancel clicked failed. Please try again.");
            }
        }


        /// <summary>
        /// This is will execute as soon as Admin will create new user and once admin fill all mandatory fields then we are calling AddUpdateUser API 
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void RadButton_Click_update(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                dynamic item = e.OriginalSource;
                var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
                var selectedItem = selectedRow;
                //var listItem = JsonConvert.DeserializeObject<List<GridNewColumn>>(radListBox.ItemsSource);
                var listItem = JsonConvert.SerializeObject(radListBox.ItemsSource);
                await UpdatePreference(selectedRow.preferenceId);
                //await UpdatePreference(selectedRow.preferenceId, listItem);
                RadWindow.Alert(new DialogParameters { Header = "Success",  Content = "Application preference updated successfully. \nPlease login back to reflect the changes." });

                string exefileLocation = "";
                exefileLocation = AppDomain.CurrentDomain.BaseDirectory + "PTPL_SOP_System_UI.exe";
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo(exefileLocation);
                process.Start();
                Application.Current.Shutdown();
                Environment.Exit(0);
                app_logger.appLogger.Info("[INFO : Updated button clicked on the preference grid]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Application preference update failed : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Application preference update failed.");
            }
        }


        /// <summary>
        /// This Method used to Bind  databse table data with Template Master Grid
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindPreferenceMasterGrid()
        {
            bool result = false;
            apiUrl = string.Empty;
            var watch = Stopwatch.StartNew();
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/ConfigurePreferences/GetAllPreferenceSetting";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["prefenceSetting"];
                            List<gridData> gridResult = JsonConvert.DeserializeObject<List<gridData>>(finalList.ToString());

                            configurePreferencegrid.ItemsSource = gridResult;

                            

                            radDataPager.Source = configurePreferencegrid.ToEnumerable();
                            radDataPager.Visibility = Visibility.Visible;
                            radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = configurePreferencegrid });
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the SOp WI Grid. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
            }
            return result;
        }



        /// <summary>
        /// This is Update User data method in which once invoke as soon as admin want to update user information which accepting uid i.e. user id as parameter.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">API not found</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> DropDownBinding(string preferenceType)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    if (preferenceType == "Category")
                    {
                        apiUrl = apiEndPoint + apiVersion + "/CategoryMaster/GetAllCategorys";
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var responseData = response.Content.ReadAsStringAsync().Result;
                                var jo = JObject.Parse(responseData);
                                var finalList = jo["categorysData"];
                                List<CategoryMaster> deserializedList = JsonConvert.DeserializeObject<List<CategoryMaster>>(finalList.ToString());
                                radListBox.ItemsSource = deserializedList;
                                radListBox.DisplayMemberPath = "categoryName";
                                radListBox.SelectedValuePath = "uid";
                                result = true;
                                watch.Stop();
                                app_logger.appLogger.Info("[INFO : Category loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }


                    if (preferenceType == "Area")
                    {
                        apiUrl = apiEndPoint + apiVersion + "/AreaMaster/GetAllAreas";
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                        {
                            if (response.IsSuccessStatusCode)
                            {  //eventDataList
                                var responseData = response.Content.ReadAsStringAsync().Result;
                                var jo = JObject.Parse(responseData);
                                var finalList = jo["areasData"];
                                List<AreaMaster> deserializedList = JsonConvert.DeserializeObject<List<AreaMaster>>(finalList.ToString());
                                radListBox.ItemsSource = deserializedList;
                                radListBox.DisplayMemberPath = "areaName";
                                radListBox.SelectedValuePath = "uid";
                                result = true;
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }


                    if (preferenceType == "Event")
                    {
                        apiUrl = apiEndPoint + apiVersion + "/EventTypes/GetAllEventTypes";
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var responseData = response.Content.ReadAsStringAsync().Result;
                                var jo = JObject.Parse(responseData);
                                var finalList = jo["eventDataList"];
                                List<EventTypeMaster> deserializedList = JsonConvert.DeserializeObject<List<EventTypeMaster>>(finalList.ToString());
                                radListBox.ItemsSource = deserializedList;
                                radListBox.DisplayMemberPath = "eventType";
                                radListBox.SelectedValuePath = "uid";
                                result = true;
                                app_logger.appLogger.Trace("[TRACE : Event Type loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                                app_logger.appLogger.Info("[INFO : Event Type loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }

                    if (preferenceType == "Equipement")
                    {
                        apiUrl = apiEndPoint + apiVersion + "/MachineMaster/GetMachineByAreaId?Id=" + drpValuesArea.SelectedValue;
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var responseData = response.Content.ReadAsStringAsync().Result;
                                var jo = JObject.Parse(responseData);
                                var finalList = jo["machinesData"];
                                List<MachineMaster> deserializedList = JsonConvert.DeserializeObject<List<MachineMaster>>(finalList.ToString());
                                result = true;
                                radListBox.ItemsSource = deserializedList;
                                radListBox.DisplayMemberPath = "machineName";
                                radListBox.SelectedValuePath = "uid"; 

                                app_logger.appLogger.Trace("[TRACE : Equipment loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                               

                            }
                            else
                            {
                                result = false;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling updating API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                //RadWindow.Alert("Error occurred while updating user. Please try again");
            }
            return result;
        }

        /// <summary>
        /// This is Update User data method in which once invoke as soon as admin want to update user information which accepting uid i.e. user id as parameter.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">API not found</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> GetStorageByPId(int preferenceId)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/ConfigurePreferences/GetStorageTypeByPId?Id=" + preferenceId;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            //var userModal = JsonConvert.DeserializeObject<storage>(responseData);
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["storageType"];
                            List<GridNewColumn> deserializedList = JsonConvert.DeserializeObject<List<GridNewColumn>>(finalList.ToString());

                            //List<GridNewColumn> cList = new List<GridNewColumn>();

                            //foreach (var item in deserializedList)
                            //{

                            //    GridNewColumn gridNew = new GridNewColumn();

                            //    gridNew.cName = item.cName;


                            //}

                           // drpValues.ItemsSource = cName;
                            drpValues.ItemsSource = deserializedList;
                            radListBox.ItemsSource = deserializedList;
                            radListBox.DisplayMemberPath = "cName";
                            drpValues.DisplayMemberPath = "cName";

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling updating API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                //RadWindow.Alert("Error occurred while updating user. Please try again");
            }
            return result;
        }


        /// <summary>
        /// This is Update User data method in which once invoke as soon as admin want to update user information which accepting uid i.e. user id as parameter.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">API not found</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> GetPreferenceByUser()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/DocumentMaster/GetPreferenceByUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            //var userModal = JsonConvert.DeserializeObject<storage>(responseData);
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["storageType"];
                            List<PreferenceDTO> prefdeserializedList = JsonConvert.DeserializeObject<List<PreferenceDTO>>(finalList.ToString());

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while calling updating API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                //RadWindow.Alert("Error occurred while updating user. Please try again");
            }
            return result;
        }


        /// <summary>
        /// This Method used to Bind  databse table data with Template Master Grid
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindPreferenceStorage()
        {
            bool result = false;
            apiUrl = string.Empty;
            var watch = Stopwatch.StartNew();
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/ConfigurePreferences/GetAllPreferenceSetting";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["prefenceSetting"];
                            List<storage> storageResult = JsonConvert.DeserializeObject<List<storage>>(finalList.ToString());
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the SOp WI Grid. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
            }
            return result;
        }

       

        /// <summary>
        /// This is will execute as soon as Admin will create new user and once admin fill all mandatory fields then we are calling AddUpdateUser API 
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> UpdatePreference(int preferenceId, string storagetype)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                gridData gridDataDTO = new gridData();
                
                gridDataDTO.storageType = storagetype;
                gridDataDTO.preferenceId = preferenceId;
                gridDataDTO.preferenceName = txtPrefernceName.Text;

                if (Application.Current.Properties["datatype"].ToString() == "Text")
                {
                    gridDataDTO.preferenceValue = txtValues.Text;
                }
                else if (Application.Current.Properties["datatype"].ToString() == "Bool")
                {
                    if (chkValues.IsChecked == true)
                    {
                        gridDataDTO.preferenceValue = "True";
                    }
                    else
                    {
                        gridDataDTO.preferenceValue = "False";
                    }
                }
                else
                { 
                    gridDataDTO.preferenceValue = drpValues.Text;
                }

                gridDataDTO.scope = txtScope.Text;
                gridDataDTO.category = txtCategory.Text;
                gridDataDTO.description = txtDesc.Text;

                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/ConfigurePreferences/AddUpdatePreferenceSetting";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    // using (HttpResponseMessage response = client.PostAsJsonAsync(apiUrl, userModal).Result)
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, gridDataDTO).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);

                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error occurred " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return result;
        }


        /// <summary>
        /// This is will execute as soon as Admin will create new user and once admin fill all mandatory fields then we are calling AddUpdateUser API 
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> UpdatePreference(int preferenceId)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                gridData gridDataDTO = new gridData();

                gridDataDTO.storageType = txtStorage.Text;
                gridDataDTO.preferenceId = preferenceId;
                gridDataDTO.preferenceName = txtPrefernceName.Text;

                if (Application.Current.Properties["datatype"].ToString() == "Text")
                {
                    gridDataDTO.preferenceValue = txtValues.Text;
                }
                else if (Application.Current.Properties["datatype"].ToString() == "Bool")
                {
                    if (chkValues.IsChecked == true)
                    {
                        gridDataDTO.preferenceValue = "True";
                    }
                    else
                    {
                        gridDataDTO.preferenceValue = "False";
                    }
                }
                else
                {
                    gridDataDTO.preferenceValue = drpValues.Text;
                }

                gridDataDTO.scope = txtScope.Text;
                gridDataDTO.category = txtCategory.Text;
                gridDataDTO.description = txtDesc.Text;
                

                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/ConfigurePreferences/AddUpdatePreferenceSetting";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    // using (HttpResponseMessage response = client.PostAsJsonAsync(apiUrl, userModal).Result)
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, gridDataDTO).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);

                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error occurred" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return result;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                txt_addColButton.Visibility = Visibility.Visible;
                btnAdd.Visibility = Visibility.Visible;
                txt_addColButton.Text = "";
                txt_addColButton.Focus();
                btnAdd.Content = "Add";
                app_logger.appLogger.Info("[INFO : Add button clicked on the preference grid]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Add button clicked failed : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Add button clicked failed.");
            }
        }


        /// <summary>
        /// This is will execute as soon as click on remove button
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void removeButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedvalue = radListBox.SelectedValue;
            var selecteditem = radListBox.SelectedItem;
            var selectedindex = radListBox.SelectedIndex;
            int selecteduid = Convert.ToInt32(radListBox.SelectedValue);
            dynamic item = e.OriginalSource;
            var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
            var selectedItem = selectedRow;
           // busyIndicator1.IsBusy = true;

            var watch = Stopwatch.StartNew();
            try
            {

                if (selectedindex == -1)
                {
                    RadWindow.Alert("Please select value.");
                }
                else
                {
                    //List<GridNewColumn> newColumns = new List<GridNewColumn>();
                    //newColumns.AddRange(radListBox.ItemsSource);
                    //newColumns.RemoveAt(selectedindex);
                    //radListBox.ItemsSource = newColumns;
                    //radListBox.DisplayMemberPath = "cName";
                    //dynamic item = e.OriginalSource;
                    //var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
                    //var selectedItem = selectedRow;
                    ////var listItem = JsonConvert.DeserializeObject<List<GridNewColumn>>(radListBox.ItemsSource);
                    //var listItem = JsonConvert.SerializeObject(radListBox.ItemsSource);
                    //busyIndicator1.IsBusy = true;

                    //await UpdatePreference(selectedRow.preferenceId, listItem);
                    //await GetStorageByPId(selectedItem.preferenceId);

                    //busyIndicator1.IsBusy = false;

                    RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnClosed, Content = "Do you want to remove ?", Header = "Confirm" });
                   
                }
                app_logger.appLogger.Info("[INFO : Selected value has been removed from preference list]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Selected value has been removed from preference list : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Failed to removed from preference list.");
            }
        }

        private async void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
            var selectedItem = selectedRow;
            int selecteduid = Convert.ToInt32(radListBox.SelectedValue);
            var watch = Stopwatch.StartNew();
            var result = e.DialogResult;
            if (result == true)
            {
                busyIndicator1.IsBusy = true;
                await DeleteAllDropDownValue(selectedItem.preferenceName, selecteduid);
                await DropDownBinding(selectedItem.preferenceName);
                txt_addColButton.Text = "";
                busyIndicator1.IsBusy = false;
                watch.Stop();
                app_logger.appLogger.Info("[INFO: Values has been removed.]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Selected value has been removed.");
            }
        }


        /// <summary>
        /// This is will execute as soon as click on add button
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            int selecteduid = Convert.ToInt32(radListBox.SelectedValue);
            var selectedindex = radListBox.SelectedIndex;
            try
            {
                if (btnAdd.Content.ToString() == "Add")
                {

                    txt_addColButton.Focus();

                    if (txt_addColButton.Text == "")
                    {
                        RadWindow.Alert("Please enter value to add");
                        return;
                    }
                    else
                    {

                        //List<CategoryMaster> newColumns = new List<CategoryMaster>();
                        //newColumns.AddRange(radListBox.ItemsSource);
                        //CategoryMaster gridNewColumn = new CategoryMaster();
                        //gridNewColumn.cName = txt_addColButton.Text;
                        //gridNewColumn.status = "True";
                        //newColumns.Add(gridNewColumn);
                        //radListBox.ItemsSource = newColumns;
                        //radListBox.DisplayMemberPath = "cName";

                        //await UpdateAllDropDown(selectedItem.preferenceName);
                    }
                }

                if (btnAdd.Content.ToString() == "Update")
                {
                    //txt_addColButton.Focus();
                    //List<GridNewColumn> newColumns = new List<GridNewColumn>();
                    //newColumns.AddRange(radListBox.ItemsSource);
                    //newColumns.RemoveAt(selectedindex);
                    //GridNewColumn gridNewColumn = new GridNewColumn();
                    //gridNewColumn.cName = txt_addColButton.Text;
                    //gridNewColumn.status = "True";
                    //newColumns.Add(gridNewColumn);
                    //radListBox.ItemsSource = newColumns;
                    //radListBox.DisplayMemberPath = "cName";
                }


                dynamic item = e.OriginalSource;
                var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
                var selectedItem = selectedRow;
                //var listItem = JsonConvert.DeserializeObject<List<GridNewColumn>>(radListBox.ItemsSource);
                var listItem = JsonConvert.SerializeObject(radListBox.ItemsSource);
                busyIndicator1.IsBusy = true;

                //await UpdatePreference(selectedRow.preferenceId, listItem);
                //await GetStorageByPId(selectedItem.preferenceId);

                await UpdateAllDropDown(selectedItem.preferenceName, selecteduid, btnAdd.Content.ToString());

                await DropDownBinding(selectedItem.preferenceName);


                busyIndicator1.IsBusy = false;
                txt_addColButton.Text = "";
                txt_addColButton.Visibility = Visibility.Hidden;
                btnAdd.Visibility = Visibility.Collapsed;
                app_logger.appLogger.Info("[INFO : Value has been added to preference list]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Value has been failed to add in preference list: " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Value has been failed to add in preference list.");
            }

        }


        /// <summary>
        /// This is will execute as soon as click on Edit button
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                int selecteduid = Convert.ToInt32(radListBox.SelectedValue);
                dynamic item = e.OriginalSource;
                var selectedRow = this.configurePreferencegrid.SelectedItem as gridData;
                var selectedItem = selectedRow;

                txt_addColButton.Visibility = Visibility.Visible;
                txt_addColButton.Focus();
                btnAdd.Visibility = Visibility.Visible;
                var selectedindex = radListBox.SelectedIndex;

                btnAdd.Content = "Update";
                if (selectedindex == -1)
                {
                    RadWindow.Alert("Please select value.");
                    txt_addColButton.Visibility = Visibility.Collapsed;
                    btnAdd.Visibility = Visibility.Collapsed;
                    return;
                }
                if (selectedItem.preferenceName == "Category")
                {
                    txt_addColButton.Text = (radListBox.SelectedItem as CategoryMaster).categoryName;
                }
                if (selectedItem.preferenceName == "Area")
                {
                    txt_addColButton.Text = (radListBox.SelectedItem as AreaMaster).areaName;
                }
                if (selectedItem.preferenceName == "Event")
                {
                    txt_addColButton.Text = (radListBox.SelectedItem as EventTypeMaster).eventType;
                }

                if (selectedItem.preferenceName == "Equipement")
                {
                    txt_addColButton.Text = (radListBox.SelectedItem as MachineMaster).machineName;
                }
                busyIndicator1.IsBusy = false;

                app_logger.appLogger.Info("[INFO : Edit button clicked on preference list]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Edit button clicked failed on preference list: " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Edit button clicked failed ");
            }
        }

        /// <summary>
        /// This is will execute as soon as Admin will create new user and once admin fill all mandatory fields then we are calling AddUpdateUser API 
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        int uid = 0;
        protected async Task<bool> UpdateAllDropDown(string preferenceName, int selectedUId, string actionTypes)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                if(actionTypes == "Add")
                {
                    selectedUId = 0;
                }
                
                if (preferenceName == "Event")
                {

                    List<dropDownEventDTO> dropDownEventDTOList = new List<dropDownEventDTO>();
                    dropDownEventDTO dropDownEventDTO = new dropDownEventDTO();

                    dropDownEventDTO.uid = selectedUId;
                    dropDownEventDTO.eventType = txt_addColButton.Text;
                    dropDownEventDTO.eventStatus = 1;
                    dropDownEventDTO.createdBy = 1;
                    dropDownEventDTO.creationDate = DateTime.Now;
                    dropDownEventDTO.modifiedBy = 1;
                    dropDownEventDTO.modifiedDate = DateTime.Now;

                    if (uid == 0)
                    {
                        dropDownEventDTOList.Add(dropDownEventDTO);
                    }
                    else
                    {
                        
                    }
                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/EventTypes/AddUpdateEventType";
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        var data = JsonConvert.SerializeObject(dropDownEventDTOList);
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.PostAsJsonAsync(apiUrl, dropDownEventDTOList).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }

                else if (preferenceName == "Area")
                {
                    dropDownAreaDTO dropDownNewAreaDTO = new dropDownAreaDTO();
                    dropDownNewAreaDTO.uid = selectedUId;
                    dropDownNewAreaDTO.areaName = txt_addColButton.Text;
                    dropDownNewAreaDTO.createdBy = 1;
                    dropDownNewAreaDTO.creationDate = DateTime.Now;
                    dropDownNewAreaDTO.modifiedBy = 1;
                    dropDownNewAreaDTO.modifiedDate = DateTime.Now;

                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/AreaMaster/AddUpdateArea";
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        var data = JsonConvert.SerializeObject(dropDownNewAreaDTO);
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.PostAsJsonAsync(apiUrl, dropDownNewAreaDTO).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }

                else if (preferenceName == "Category")
                {
                    dropDownCategoryDTO dropDownCategoryDTO = new dropDownCategoryDTO();
                    dropDownCategoryDTO.uid = selectedUId;
                    dropDownCategoryDTO.categoryName = txt_addColButton.Text;
                    dropDownCategoryDTO.createdBy = 1;
                    dropDownCategoryDTO.creationDate = DateTime.Now;
                    dropDownCategoryDTO.modifiedBy = 1;
                    dropDownCategoryDTO.modifiedDate = DateTime.Now;

                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/CategoryMaster/AddUpdateCategory";
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        var data = JsonConvert.SerializeObject(dropDownCategoryDTO);
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.PostAsJsonAsync(apiUrl, dropDownCategoryDTO).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }

                else if (preferenceName == "Equipement")
                {
                    MachineMasterNew dropDownCategoryDTO = new MachineMasterNew();

                    if (drpValuesArea.SelectedValue == null)
                    {
                        RadWindow.Alert("Please select Area first.");
                        return false;
                    }
                    dropDownCategoryDTO.areaId = (int)drpValuesArea.SelectedValue;
                    dropDownCategoryDTO.uid = selectedUId;
                    dropDownCategoryDTO.machineName = txt_addColButton.Text;
                    dropDownCategoryDTO.createdBy = 1;
                    dropDownCategoryDTO.creationDate = DateTime.Now;
                    dropDownCategoryDTO.modifiedBy = 1;
                    dropDownCategoryDTO.modifiedDate = DateTime.Now;

                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/MachineMaster/AddUpdateMachine";
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        var data = JsonConvert.SerializeObject(dropDownCategoryDTO);
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.PostAsJsonAsync(apiUrl, dropDownCategoryDTO).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }
                        }
                    }

                    return true;
                }

            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error occurred" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return result;
        }


        /// <summary>
        /// This is will execute as soon as Admin will create new user and once admin fill all mandatory fields then we are calling AddUpdateUser API 
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        
        protected async Task<bool> DeleteAllDropDownValue(string preferenceName, int selectedUId)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
              
                if (preferenceName == "Category")
                {
                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/CategoryMaster/DeleteCategory?CategoryId=" + selectedUId;
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        //using (HttpResponseMessage response1 = await Task.Run(() => client1.DeleteAsync(apiUrl).Result))
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.GetAsync(apiUrl).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }

                if (preferenceName == "Area")
                {
                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/AreaMaster/DeleteArea?AreaId=" + selectedUId;
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        //using (HttpResponseMessage response1 = await Task.Run(() => client1.DeleteAsync(apiUrl).Result))
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.GetAsync(apiUrl).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }

                if (preferenceName == "Event")
                {
                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/EventTypes/DeleteEvent?EventId=" + selectedUId;
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        //using (HttpResponseMessage response1 = await Task.Run(() => client1.DeleteAsync(apiUrl).Result))
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.GetAsync(apiUrl).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }

                if (preferenceName == "Equipement")
                {
                    using (var client1 = new HttpClient())
                    {
                        apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/MachineMaster/DeleteMachine?MachineId=" + selectedUId;
                        client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        //using (HttpResponseMessage response1 = await Task.Run(() => client1.DeleteAsync(apiUrl).Result))
                        using (HttpResponseMessage response1 = await Task.Run(() => client1.GetAsync(apiUrl).Result))
                        {
                            if (response1.IsSuccessStatusCode)
                            {
                                var responseData1 = response1.Content.ReadAsStringAsync().Result;
                                var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            }

                        }
                    }

                    return true;
                }


            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error occurred " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return result;
        }


        /// <summary>
        /// This Method used to Bind  databse table data with Area Dropdownlist
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindArea()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/AreaMaster/GetAllAreas";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["areasData"];
                            List<AreaMaster> deserializedList = JsonConvert.DeserializeObject<List<AreaMaster>>(finalList.ToString());
                            drpValuesArea.ItemsSource = deserializedList;
                            drpValuesArea.DisplayMemberPath = "areaName";
                            drpValuesArea.SelectedValuePath = "uid";
                            result = true;

                            app_logger.appLogger.Trace("[TRACE :  Area loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO :  Area loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the Area in SOP WI. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");

                result = false;
            }
            return result;
        }

        /// <summary>
        /// This Method used to Bind  databse table data with Machine Dropdownlist
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">BindMachine calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindMachine()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/MachineMaster/GetMachineByAreaId?Id=" + drpValuesArea.SelectedValue;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["machinesData"];
                            List<MachineMaster> deserializedList = JsonConvert.DeserializeObject<List<MachineMaster>>(finalList.ToString());
                            radListBox.ItemsSource = deserializedList;
                            radListBox.DisplayMemberPath = "machineName";
                            radListBox.SelectedValuePath = "uid";
                            result = true;
                            
                            radListBox.SelectedValue = Convert.ToInt32(Application.Current.Properties["selectedMachineID"]);
                            

                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Machine binding failed due to " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Unable to bind Machine or equipement. Please try again or check with administrator.");
                result = false;
            }
            return result;
        }

        private async void drpValuesArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            await BindMachine();
            busyIndicator1.IsBusy = false;
        }
    }

    public class gridData
    {
        public int preferenceId { get; set; }
        public string preferenceName { get; set; }
        public string preferenceValue { get; set; }
        public string scope { get; set; }
        public string category { get; set; }
        public string description { get; set; }
        public string storageType { get; set; }
    }


    public class dropDownEventDTO
    {
        public int uid { get; set; }
        public string eventType { get; set; }
        public int? eventStatus { get; set; }
        public int? createdBy { get; set; }

        public DateTime? creationDate { get; set; }
        public int? modifiedBy { get; set; }

        public DateTime? modifiedDate { get; set; }
    }

    public class dropDownAreaDTO
    {
        public int uid { get; set; }
        public string areaName { get; set; }
        
        public int? createdBy { get; set; }

        public DateTime? creationDate { get; set; }
        public int? modifiedBy { get; set; }

        public DateTime? modifiedDate { get; set; }
    }


    public class dropDownCategoryDTO
    {
        public int uid { get; set; }
        public string categoryName { get; set; }

        public int? createdBy { get; set; }

        public DateTime? creationDate { get; set; }
        public int? modifiedBy { get; set; }

        public DateTime? modifiedDate { get; set; }
    }

    

    public class storage
    {
        public string storageType { get; set; }
    }

    public class storageValue
    {
        public Dictionary<string, bool> gridColumn { get; set; }

        public storageValue()
        {
            gridColumn = new Dictionary<string, bool>();
        }

    }

    public class GridNewColumn
    {
        public string cName { get; set; }

        public string status { get; set; }

    }

    public class PreferenceDTO
    {
        public int uid { get; set; }

        public string logPath { get; set; }

        public int? createdBy { get; set; }

        public int? preferenceId { get; set; }

        
    }

}

#endregion