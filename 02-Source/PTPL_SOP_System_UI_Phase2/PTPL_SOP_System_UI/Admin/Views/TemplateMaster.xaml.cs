﻿using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Admin.Views
{


    /// <summary>
    ///  Interaction Logic  TemplateMaster.xaml
    ///  Created By  :  Vishal 
    ///  Created on  : 10 March 2020
    ///  -----------------------------------------------------------------------
    ///  Modified By :  Chetan 
    ///  Modified on : 08 April 2020
    ///  Purpose     :  Removed the dropdwon List  Event Type as Deenu suggested  in last call 
    /// </summary>
    ///  -------------------------------------------------------------------------
    ///  Modified By : 
    ///  Modified on : 
    ///  Purpose     :  
    /// </summary>

    public partial class TemplateMaster : UserControl
    {
        #region Variables
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        private static readonly Regex _regex = new Regex(@"^(?:\d{1,2})?(?:\.\d{1,2})?[Ww]\s+ *(?:\d{1,2})?(?:\.\d{1,2})?[Dd]\s+ *(?:\d{1,2})?(?:\.\d{1,2})?[Hh]\s+ *(?:\d{1,2})?(?:\.\d{1,2})?(?!.*(.)\1)[Mm]");//(Float+ integer)
        int i = 0;
        int sopId = 0;
        string streamReads = "";
        int saveAsEventType = 0;
        UserData _userData;
        HttpClient client = new HttpClient();
        RadBusyIndicator busyIndicator = null;
        int workInstructionId = 0;
        bool updateSopWi = false;
        

        #endregion

        #region Constructor
        public TemplateMaster()
        {
            InitializeComponent();
            _userData = Application.Current.Properties["userData"] as UserData;
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method used to Read File selected from Ui & COnvert the file into file stream.
        /// <paramref name="filePath"/>
        /// <return>Byte[]</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;

            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }
        /// <summary>
        /// This Method used  to Bind  databse table data with Catgeory Dropdownlist
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindCategory()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/CategoryMaster/GetAllCategorys";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["categorysData"];
                            List<CategoryMaster> deserializedList = JsonConvert.DeserializeObject<List<CategoryMaster>>(finalList.ToString());
                            drpSopCategory.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Info("[INFO : Category loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while Binding the category in SOP WI " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;

        }
        /// <summary>
        /// This Method used  to copy custom form of old sop & create New Onew
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> SaveAsCustomForm(int oldSopId,int newSopId)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint +  "CustomeForm/SaveAsCustomForm?oldSopId="+oldSopId+ "&newSopId="+ newSopId;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Info("[INFO : Save as custom from created successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while Binding the category in SOP WI " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;

        }
        /// <summary>
        /// This Method used to Genrate Unique Sop Wi Number 
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<string> display_SopNumber()
        {
            var watch = Stopwatch.StartNew();
            string resultData = string.Empty;

            try
            {
                apiurl = string.Empty;
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/TemplateMaster/GetLatestSopNo";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string sopNumberVal1 = "SOP-WI" + "-";
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            int val = Convert.ToInt32(responseData);
                            responseData = val.ToString();
                            if (responseData == string.Empty)
                                responseData = "0";
                            int resultVal = Convert.ToInt32(val) + 1;
                            
                            if (responseData.Length.Equals(1))
                            {
                                resultData = "0000" + resultVal;
                            }
                            if (responseData.Length.Equals(2))
                            {
                                resultData = "000" + resultVal;
                            }
                            if (responseData.Length.Equals(3))
                            {
                                resultData = "00" + resultVal;
                            }
                            if (responseData.Length.Equals(4))
                            {
                                resultData = "0" + resultVal;
                            }
                            resultData = sopNumberVal1 + resultData;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Login Successfully " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");

            }
            return resultData;
        }
        /// <summary>
        /// This Method used to  Bind  databse table data with Event Type Dropdownlist
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindEventType()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/EventTypes/GetAllEventTypes";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventDataList"];
                            List<EventTypeMaster> deserializedList = JsonConvert.DeserializeObject<List<EventTypeMaster>>(finalList.ToString());
                            drpEventType.ItemsSource = deserializedList;
                            result = true;

                            app_logger.appLogger.Trace("[TRACE : Event Type loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Event Type loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while Binding the Event Type in SOP WI " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;

        }
        /// <summary>
        /// This Method used to Bind  databse table data with Machine/Equipment Dropdownlist
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindMachine()
        {
            var watch = Stopwatch.StartNew();
            drpMachine.Text = "";
            bool result = false;
            apiurl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/MachineMaster/GetMachineByAreaId?Id=" + drpArea.SelectedValue;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["machinesData"];
                            List<MachineMaster> deserializedList = JsonConvert.DeserializeObject<List<MachineMaster>>(finalList.ToString());
                            result = true;
                            drpMachine.ItemsSource = deserializedList;

                            app_logger.appLogger.Trace("[TRACE : Equipment loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            var selectedRow = Application.Current.Properties["selectedDataRow"] as TemplateMasterGrid;//DataRow;
                            if (selectedRow != null)
                            {
                                //foreach (string selectedMachine in BindMachineContext(selectedRow).Split(','))
                                //{

                                //    drpMachine.Text += selectedMachine + ",";

                                //}
                                //// drpMachine.Text = drpMachine.Text.Remove(drpMachine.Text.LastIndexOf(','), 1);
                                ///
                                drpMachine.Text = selectedRow.equipmentType;
                                Application.Current.Properties["selectedMachine"] = drpMachine.Text;


                            }
                            else
                            {
                                drpMachine.ItemsSource = deserializedList;
                            }

                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");

                app_logger.appLogger.Error("[ERROR : Error occurred while loading the equipment in SOP WI. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");

                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method used to Bind  databse table data with Area Dropdownlist
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindArea()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/AreaMaster/GetAllAreas";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  //eventDataList
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["areasData"];
                            List<AreaMaster> deserializedList = JsonConvert.DeserializeObject<List<AreaMaster>>(finalList.ToString());
                            drpArea.ItemsSource = deserializedList;
                            result = true;

                            app_logger.appLogger.Trace("[TRACE :  Area loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO :  Area loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the Area in SOP WI. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");

                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method used to  Bind  databse table data with Users List Dropdownlist
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindUsers()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            List<Assignee> deserializedList = JsonConvert.DeserializeObject<List<Assignee>>(finalList.ToString());
                            drpAssinee.ItemsSource = deserializedList;
                            result = true;

                            app_logger.appLogger.Trace("[TRACE : Users loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Users loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the Users in SOP WI. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");

                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method used  to shows selected machine on Multisect Dropdown list in Update Mode
        /// <return>bool</return>
        /// <paramref name="dr"/>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private string BindMachineContext(DataRow dr)
        {
            string bindvalue = "";
            try
            {
                DataTable dt = new DataTable();
                DataView dv = new DataView(dr.Table);
                dv.RowFilter = "uid=" + dr[0].ToString();
                dt = dv.ToTable();
               

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Rows[0][i].ToString() == "True")
                    {
                        bindvalue += dt.Columns[i].ToString() + ",";
                    }
                }
                bindvalue = bindvalue.TrimEnd(',');
                Application.Current.Properties["selectedMachine"] = bindvalue;
               
            }
            catch(Exception ex)
            {
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the Users in SOP WI. " + ex.Message + " ]" + $" | 10 ms");
            }
            return bindvalue;

        }
        /// <summary>
        /// This Method used to Bind  databse table data with Template Master Grid
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindTemplateMasterGrid()
        {
            bool result = false;
            apiurl = string.Empty;
            var watch = Stopwatch.StartNew();
            try
            {

                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/TemplateMaster/GetAllMachineTemplate";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templatesData"];
                            List<TemplateMasterGrid> gridResult = JsonConvert.DeserializeObject<List<TemplateMasterGrid>>(finalList.ToString());
                            if (gridResult.Count > 0)
                            {
                                var fixedColumnList = new List<ColumnData>();
                                //fixedColumnList = gridResult[0].machineDTOs;
                                //Telerik.Windows.Controls.GridViewColumnCollection columns = new Telerik.Windows.Controls.GridViewColumnCollection();
                                //GridViewDataColumn gridViewDataColumn = new GridViewDataColumn();
                                //DataTable dtMaster = new DataTable();
                                //dtMaster.Columns.Add("uid");
                                //dtMaster.Columns.Add("areaName");
                                //dtMaster.Columns.Add("templateName");
                                //dtMaster.Columns.Add("fileName");
                                //dtMaster.Columns.Add("SopNumber");
                                //dtMaster.Columns.Add("Category");
                                //dtMaster.Columns.Add("originalEstimateTime");
                                //dtMaster.Columns.Add("assigneeName");
                                //dtMaster.Columns.Add("eventType");
                                //dtMaster.Columns.Add("sopTemplateDesc");


                                //DataTemplate template;
                                //foreach (ColumnData cln in fixedColumnList)
                                //{
                                //    dtMaster.Columns.Add(cln.machineName);
                                //}
                                //gridTemplateMaster.Columns.AddRange(columns);
                                //for (int i = 0; i < gridResult.Count; i++)
                                //{
                                //    DataRow dr = dtMaster.NewRow();
                                //    dr["uid"] = gridResult[i].uid;
                                //    dr["areaName"] = gridResult[i].areaName;
                                //    dr["templateName"] = gridResult[i].templateName;
                                //    dr["fileName"] = gridResult[i].fileName;
                                //    dr["sopNumber"] = gridResult[i].sopNumber;
                                //    dr["category"] = gridResult[i].category;
                                //    dr["originalEstimateTime"] = gridResult[i].originalEstimateTime;
                                //    dr["assigneeName"] = gridResult[i].assigneeName;
                                //    dr["eventType"] = gridResult[i].eventType;
                                //    dr["sopTemplateDesc"] = gridResult[i].sopTemplateDesc;
                                //    for (int j = 0; j < gridResult[i].machineDTOs.Count; j++)
                                //    {
                                //        dr[gridResult[i].machineDTOs[j].machineName] = gridResult[i].machineDTOs[j].isUsed;
                                //    }
                                //    dtMaster.Rows.Add(dr);
                                //}
                                gridTemplateMaster.ItemsSource = gridResult;

                                gridTemplateMaster.Visibility = Visibility.Visible;

                                //gridTemplateMaster.Columns[3].IsVisible = false;
                                //gridTemplateMaster.Columns[4].IsVisible = false;
                                //gridTemplateMaster.Columns[5].IsVisible = false;
                                //gridTemplateMaster.Columns[6].IsVisible = false;
                                //gridTemplateMaster.Columns[7].IsVisible = false;
                                //gridTemplateMaster.Columns[8].IsVisible = false;
                                //gridTemplateMaster.Columns[9].IsVisible = false;
                                //gridTemplateMaster.Columns[10].IsVisible = false;
                                //gridTemplateMaster.Columns[11].IsVisible = false;


                                radDataPager.Source = gridResult.ToEnumerable();
                                radDataPager.Visibility = Visibility.Visible;
                                radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = gridTemplateMaster });

                            }
                            else
                            {
                                gridTemplateMaster.Visibility = Visibility.Hidden;
                                gridTemplateMaster.ItemsSource = null;
                                //gridTemplateMaster.AutoGenerateColumns = false;

                                app_logger.appLogger.Info("[INFO : Data Not Found. ]" + $" | {watch.ElapsedMilliseconds} ms");
                                RadWindow.Alert("No Data Found");

                            }
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the SOp WI Grid. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
            }
            return result;

        }
        /// <summary>
        /// This Method used to  updates Template Master UI Fields value into database
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> UdpateSOPTemplate()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiurl = string.Empty;
            try
            {

                List<int> selectedMachineIds = new List<int>();
                foreach (dynamic item in drpMachine.SelectedItems)
                {
                    selectedMachineIds.Add(item.uid);
                }
                using (var client = new HttpClient())
                {
                    apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/TemplateMaster/GetTemplatesById?Id=" + sopId;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templateData"];
                            var deserializedList = JsonConvert.DeserializeObject<dynamic>(finalList.ToString());
                            TemplateModel sopTemplateModal = new TemplateModel();
                            sopTemplateModal.uid = sopId;
                            sopTemplateModal.sopNumber = !string.IsNullOrEmpty(txtSopID.Text.Trim()) ? txtSopID.Text.Replace("\"", "''").Trim() : "";
                            sopTemplateModal.sopTemplateName = !string.IsNullOrEmpty(txtSopTemplateName.Text.Trim()) ? txtSopTemplateName.Text.Replace("\"", "''").Trim() : "";
                            sopTemplateModal.sopTemplateDesc = !string.IsNullOrEmpty(txtTemplateDecription.Text.Trim()) ? txtTemplateDecription.Text.Replace("\"", "''").Trim() : "";
                            sopTemplateModal.sopCategory = (int)drpSopCategory.SelectedValue;
                            sopTemplateModal.areaId = (int)drpArea.SelectedValue;
                            sopTemplateModal.originalEstimateTime = !string.IsNullOrEmpty(txtEstimation.Text.Trim()) ? txtEstimation.Text.Replace("\"", "''").Trim() : "";
                            sopTemplateModal.assigneeId = (int)drpAssinee.SelectedValue;
                            sopTemplateModal.filePath = "";//fileUpload.FilePath,
                            sopTemplateModal.fileSize = "";//fileUpload.RenderSize,
                            sopTemplateModal.fileVersion = "";
                            sopTemplateModal.machineId = Convert.ToInt32(drpMachine.SelectedValue);//selectedMachineIds;
                            sopTemplateModal.createdBy = Convert.ToInt32(_userData.id);
                            sopTemplateModal.fileStream = streamReads == "" ? deserializedList.fileStream : streamReads;
                            sopTemplateModal.eventType = deserializedList.eventType;
                            sopTemplateModal.fileName = txtFilenmame.Text;
                            await PostTemplateData(sopTemplateModal);
                            result = true;
                        }
                        else
                        {
                            result = false;

                        }


                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while loading the selected SOP WI while updating. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");


            }
            return result;

        }
        /// <summary>
        /// This Method used  to fetch the value from database for selected SOP WI
        /// <paramref name="sopTemplateModal"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> PostTemplateData(TemplateModel sopTemplateModal)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client1 = new HttpClient())
                {
                    apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/TemplateMaster/AddUpdateTemplate";
                    client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    var data = JsonConvert.SerializeObject(sopTemplateModal);
                    using (HttpResponseMessage response1 = await Task.Run(() => client1.PostAsJsonAsync(apiurl, sopTemplateModal).Result))
                    {
                        if (response1.IsSuccessStatusCode)
                        {
                            var responseData1 = response1.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            result = true;
                        }

                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Data updated unsuccessfull " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                return false;
            }
        }

        /// <summary>
        /// This Method used  to create SOP WI in database
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<int> SaveSOPTemplate()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            
            apiurl = string.Empty;
            try
            {
                List<int> selectedMachineIds = new List<int>();
                foreach (dynamic item in drpMachine.SelectedItems)
                {
                    selectedMachineIds.Add(item.uid);
                }

                if (updateSopWi == true)
                {
                    busyIndicator1.IsBusy = true;
                    var flag = await UdpateSOPTemplate();
                    if (flag)
                    {

                        RadWindow.Alert(new DialogParameters { Header = "Success", Content = "SOP WI item updated successfully" });
                        busyIndicator1.IsBusy = true;
                        await BindTemplateMasterGrid();
                        //sp_AddTemplate.Visibility = Visibility.Hidden;
                        //tmpRadControl.Visibility = Visibility.Hidden;
                        //templateGrid.Visibility = Visibility.Visible;
                        busyIndicator1.IsBusy = false;
                    }
                    else
                    {
                        RadWindow.Alert("ERROR: Update failed to save SOP WI.");
                    }
                    busyIndicator1.IsBusy = false;
                }
                else
                {

                    var sopTemplateModal = new
                    {
                        uid = 0,
                        sopNumber = !string.IsNullOrEmpty(txtSopID.Text.Trim()) ? txtSopID.Text.Replace("\"", "''").Trim() : "",
                        sopTemplateName = !string.IsNullOrEmpty(txtSopTemplateName.Text.Trim()) ? txtSopTemplateName.Text.Replace("\"", "''").Trim() : "",
                        sopTemplateDesc = !string.IsNullOrEmpty(txtTemplateDecription.Text.Trim()) ? txtTemplateDecription.Text.Replace("\"", "''").Trim() : "",
                        sopCategory = drpSopCategory.SelectedValue,
                        eventType = 1,
                        filePath = "",//fileUpload.FilePath,
                        fileSize = "",//fileUpload.RenderSize,
                        fileVersion = "",
                        originalEstimateTime = !string.IsNullOrEmpty(txtEstimation.Text.Trim()) ? txtEstimation.Text.Replace("\"", "''").Trim() : "",
                        //machineId = new int[] { Convert.ToInt32(drpMachine.SelectedValue)},//selectedMachineIds,
                        machineId = drpMachine.SelectedValue,
                        createdBy = Convert.ToInt32(_userData.id),
                        assigneeId = drpAssinee.SelectedValue,
                        fileStream = streamReads,
                        fileName = txtFilenmame.Text,
                        areaId = drpArea.SelectedValue


                        //uid = 0,
                        //sopNumber = "Test June 23 New1",
                        //sopTemplateName = "Test Temp",
                        //originalEstimateTime = "5h",
                        //areaId = 4,
                        //sopTemplateDesc = "Test Desc",
                        //sopCategory = 2,
                        //equipmentType =  4,
                        //eventType = 1,
                        //eventTrigger = 1,
                        //assigneeId = 1,
                        //fileStream = "",
                        //filePath = "",
                        //fileSize = "",
                        //fileVersion = "",
                        //createdBy = 1,
                        //machineId = new int[] { 1 },
                        //fileName = "",
                        //wiFlag = true

                    };
                    using (var client = new HttpClient())
                    {
                        apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/TemplateMaster/AddUpdateTemplate";
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                        using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiurl, sopTemplateModal).Result))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                var responseData = response.Content.ReadAsStringAsync().Result;
                                var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);
                                if (resultData.id == "Sop WI Number already exist")
                                {
                                    RadWindow.Alert("Sop WI Number already exist. ");
                                    result = false;
                                    this.Dispatcher.Invoke(() =>
                                    {
                                        txtSopID.Focus();
                                    });

                                    return 0;

                                }
                                else
                                {
                                    updateSopWi = true;
                                    Application.Current.Properties["SopObjectID"] = sopId = workInstructionId = resultData.id;
                                    Application.Current.Properties["estimation"] = txtEstimation.Text.Trim();
                                    result = true;
                                    app_logger.appLogger.Trace("[TRACE : SOP WI Item saved successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                                    app_logger.appLogger.Info("[INFO : SOP WI Item saved successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                                    RadWindow.Alert(new DialogParameters { Header = "Success", Content = "SOP WI Item saved successfully" });
                                    Application.Current.Properties["SOPWINumber"] = resultData.sOPWINumber;
                                    Application.Current.Properties["areaID"] = drpMachine.SelectedValue; //drpArea.SelectedValue
                                    Application.Current.Properties["sopTitle"] = txtSopTemplateName.Text;

                                }

                            }
                            else
                            {
                                result = false;
                                app_logger.appLogger.Error("[ERROR : API return with an error : " + response + "]" + $" | {watch.ElapsedMilliseconds} ms");
                                RadWindow.Alert("Something went wrong. Please contact your administrator");

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occurred while saving sop wi. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please contact your administrator.");


            }
            return workInstructionId;

        }
        /// <summary>
        /// This Method used  check validation for Original Estimation
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected void CheckValidationForEstimationTime()
        {
            if (_regex.IsMatch(txtEstimation.Text))
            {
                IsTextAllowed(txtEstimation.Text);
                string estimation = txtEstimation.Text;
            }
            else
            {
                RadWindow.Alert("Your original estimate must be in the format of \n5w 5d 5h 5m");
                //MessageBox.Show("Your original estimate must be in the format 5w 5d 5h 5m");
                txtEstimation.Text = string.Empty;
                // txtEstimation.Focus();
                return;

            }
        }
        /// <summary>
        /// This method used to validate the textbox string .
        /// </summary>
        /// <param name="text"></param>
        /// <returns>bool</returns>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }
        /// <summary>
        /// This Method use to fetch the events related to WI Item
        /// <return>List<ScheduleEvent></return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<List<ScheduleEvent>> FetchEventOfWI(int localSopId)
        {
            List<ScheduleEvent> listSchduleEvent = null;

            var watch = Stopwatch.StartNew();
            try
            {

                using (var client = new HttpClient())
                {

                    apiurl = apiEndPoint + apiVersion + "/EventTriggerSchedule/getAllEventTriggerScheduleWithoutPropertyNameByWIId?id=" + localSopId;

                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventTriggerScheduleData"];
                            listSchduleEvent = JsonConvert.DeserializeObject<List<ScheduleEvent>>(finalList.ToString());
                        }
                        else
                        {
                            listSchduleEvent = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while  fetching events of SOp WI. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");

            }
            return listSchduleEvent;
        }
        /// <summary>
        /// This Method Use to bind create Events in database
        /// <paramref name="scheduleEvent"/>
        /// <paramref name="sopWinID"/>
        /// <return>bool</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> AddEvents(int sopWinID, ScheduleEvent scheduleEvent)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            var modal = new
            {
                eventTriggerId = 0,
                wiSopId = sopWinID,
                eventType = Convert.ToInt32(scheduleEvent.eventType),
                title = scheduleEvent.title,
                startTime = "2020-04-03T08:15:58.805Z",
                endTime = "2020-04-03T08:15:58.805Z",
                occurance = Convert.ToInt32(scheduleEvent.occurance),
                machineId = Convert.ToInt32(scheduleEvent.machine),
                assignee = Convert.ToInt32(scheduleEvent.assignee),
                condition = scheduleEvent.condition,
                externalEvents = scheduleEvent.externalEvents,
                timeValue = scheduleEvent.timeValue,
                status = scheduleEvent.status

            };

            try
            {

                using (var client = new HttpClient())
                {

                    apiurl = apiEndPoint + apiVersion + "/EventTriggerSchedule/AddUpdateEventTriggerSchedule";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiurl, modal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData1 = response.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            result = true;

                            app_logger.appLogger.Trace("[TRACE : Event added successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Event added successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while saving sop wi Events. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// Validation For Estimation
        /// </summary>
        /// <returns>bool</returns>
        protected bool CheckValidationForEstimationTimeModified()
        {
            bool status = false;
            var watch = Stopwatch.StartNew();
            Regex _week = new Regex(@"^(?:\d{1,2})?(?:\.\d{1,2})?[Ww]");
            Regex _days = new Regex(@"^(?:\d{1,2})?(?:\.\d{1,2})?[Dd]");
            Regex _hrs = new Regex(@"^(?:\d{1,2})?(?:\.\d{1,2})?[Hh]");
            Regex _minuts = new Regex(@"^(?:\d{1,2})?(?:\.\d{1,2})?[Mm]");

            try
            {
                string[] arr = txtEstimation.Text.Split(' ');
                int countOfArr = arr.Length;
                if (countOfArr <= 4 && countOfArr > 0)
                {
                    if (countOfArr == 4)
                    {
                        if (_regex.IsMatch(txtEstimation.Text))
                        {
                            status = true;
                        }
                        else
                        {
                            status = false;
                        }
                    }
                    else if (countOfArr == 3)
                    {
                        if (_days.IsMatch(arr[0]))
                        {
                            if (_hrs.IsMatch(arr[1]))
                            {
                                if (_minuts.IsMatch(arr[2]))
                                {
                                    status = true;//allow
                                }
                                else
                                {
                                    status = false;
                                }
                            }
                            else
                            {
                                status = false;
                            }
                        }
                        else
                        {
                            status = false;
                        }
                    }
                    else if (countOfArr == 2)
                    {
                        if (_hrs.IsMatch(arr[0]))
                        {
                            if (_minuts.IsMatch(arr[1]))
                            {
                                status = true; //allow
                            }
                            else
                            {
                                status = false;
                            }
                        }
                        else
                        {
                            status = false;
                        }
                    }
                    else if (countOfArr == 1)
                    {
                        if (_minuts.IsMatch(arr[0]) || _hrs.IsMatch(arr[0]) || _days.IsMatch(arr[0]) || _week.IsMatch(arr[0]))
                        {
                            status = true;
                        }
                        else
                        {
                            status = false;
                        }



                    }
                }
                else
                {
                    status = false;
                }



            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error in CheckValidationForEstimationTimeModified " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                status = false;
            }
            return status;
        }
        /// <summary>
        /// Clearing the input fields.
        /// </summary>
        protected void ClearInputFields()
        {
            txtEstimation.Text = "";
            txtFilenmame.Text = "";
            txtSopID.Text = "";
            txtSopTemplateName.Text = "";
            drpSopCategory.SelectedIndex = -1;
            drpArea.SelectedIndex = -1;
            drpMachine.SelectedIndex = -1;
            drpAssinee.SelectedIndex = -1;
            txtTemplateDecription.Text = "";
        }
        #endregion

        #region Events
        /// <summary>
        /// This Events Fires when user control fully loaded. & it used to initilize the value at initial levels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            logDir = Application.Current.Properties["logDir"].ToString();//ConfigurationManager.AppSettings["LogDir"].ToString();
            await BindCategory();
            await BindEventType();
            await BindArea();
            await BindUsers();
            await BindTemplateMasterGrid();
            gridTemplateMaster.Visibility = Visibility.Visible;
            busyIndicator1.IsBusy = false;
            Application.Current.Properties["radTabItemCustomForm"] = customeForm;
            Application.Current.Properties["radTabItemEventTrigger"] = EventInstruction;
        }
        /// <summary>
        /// This Event use to navigate the page from Grid Page to Create Item Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void AddTemplate_Click(object sender, RoutedEventArgs e)
        {
            
            Application.Current.Properties["selectedDataRow"] = null;
            ClearEnableProperty();
            ClearInputFields();
            txtSopID.Text = "";
            busyIndicator1.IsBusy = true;
           // txtSopID.Text = await display_SopNumber();
            busyIndicator1.IsBusy = false;
            txtblockAddSOPWI.Text = "Add SOP Work Instructions";
            templateGrid.Visibility = Visibility.Collapsed;
            sp_AddTemplate.Visibility = Visibility.Visible;
            tmpRadControl.Visibility = Visibility.Visible;
            txtEstimation.Text = "";
            txtFilenmame.Text = "";
            txtSopTemplateName.Text = "";
            txtTemplateDecription.Text = "";
            drpEventType.SelectedIndex = -1;
            drpMachine.SelectedIndex = -1;
            drpSopCategory.SelectedIndex = -1;
            btnUpdate.Visibility = Visibility.Collapsed;
            btnSave.Visibility = Visibility.Visible;
            btnSave.Content = "Create/Save";
            btnSave.IsEnabled = true;
            btnNext.Visibility = Visibility.Visible;
            btnUpdate.Visibility = Visibility.Collapsed;
            btnNext.IsEnabled = false;
            drpAssinee.IsEnabled = true;
            customeForm.IsEnabled = false;
            EventInstruction.IsEnabled = false;
            tb_addSOPApprovals.IsEnabled = false;
            sopId = 0;
            streamReads = "";

            //Application.Current.Properties["SOPWINumber"] = txtSopID.Text;
        }
        /// <summary>
        /// This event fires on click of cancle button. and it is used to navigate the page from create item to Grid page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async void RadButton_Cancel_Click(object sender, RoutedEventArgs e)
        {

            RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnClosed, Content = " Do you want to cancel ?", Header = "Confirmation" });

        }

        private async void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var result = e.DialogResult;
            if (result == true)
            {
                sp_AddTemplate.Visibility = Visibility.Collapsed;
                templateGrid.Visibility = Visibility.Visible;
                tmpRadControl.Visibility = Visibility.Hidden;
                busyIndicator1.IsBusy = true;
                await BindTemplateMasterGrid();
                busyIndicator1.IsBusy = false;

            }
        }

        /// <summary>
        /// This Event Fires onclick of create SOP WI. And it is used to create SOP WI in databse.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void btnNext_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Properties["areaID"] = drpMachine.SelectedValue;
            Application.Current.Properties["sopTitle"] = txtSopTemplateName.Text;
            Application.Current.Properties["SOPWINumber"] = txtSopID.Text;

            int sopIdForevent = sopId;
            if (Application.Current.Properties["selectedDataRow"] != null && btnNext.Content.ToString() == "Next")
            {
                TemplateModel templateModel = new TemplateModel();
                templateModel.uid = sopId;
                if (btnSave.IsEnabled == true)
                {
                    templateModel.sopTemplateName = "Edit_Mode";
                }
                else if (btnUpdate.IsVisible == true)
                {
                    templateModel.sopTemplateName = "Edit_Mode";
                }
                else
                {
                    templateModel.sopTemplateName = "View_Mode";
                }

                if (txtFilenmame.Text == "")
                {
                    customeForm.IsEnabled = true;
                    customeForm.IsSelected = true;
                }
                else
                {
                    EventInstruction.IsEnabled = true;
                    tb_addSOPApprovals.IsEnabled = true;
                    EventInstruction.IsSelected = true;
                    tb_addSOPApprovals.IsEnabled = true;
                    EventInstruction.Content = new EventTriggerRule(templateModel);
                    tb_addSOPApprovals.Content = new frmSopApprovals();
                }

            }

            else if (sopId != 0 && btnNext.Content.ToString() == "Next")
            {
                if (txtFilenmame.Text == "")
                {
                    customeForm.IsEnabled = true;
                    customeForm.IsSelected = true;
                }
                else
                {
                    TemplateModel templateModel = new TemplateModel();
                    templateModel.uid = sopId;
                    templateModel.sopTemplateName = "EVENT";
                    EventInstruction.IsEnabled = true;
                    EventInstruction.IsSelected = true;
                    tb_addSOPApprovals.IsEnabled = true;
                    EventInstruction.Content = new EventTriggerRule(templateModel);
                    tb_addSOPApprovals.Content = new frmSopApprovals();
                }
            }
            
            busyIndicator1.IsBusy = false; ;
        }
        /// <summary>
        /// This Events fires onclik of Browse button. and it allow to select file using openFile Dilaog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                OpenFileDialog fileDialog = new OpenFileDialog();
                fileDialog.DefaultExt = ".pdf"; // Required file extension 
                fileDialog.Filter = "Text documents (.pdf)|*.pdf"; // Optional file extensions

                fileDialog.ShowDialog();
                txtFilenmame.Text = fileDialog.SafeFileName;
                if (!string.IsNullOrEmpty(txtFilenmame.Text))
                {
                    byte[] stremeBuffer = ReadFile(fileDialog.FileName);
                    streamReads = Convert.ToBase64String(stremeBuffer);
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error in browse button " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again.");
            }

        }
        /// <summary>
        /// This Events FIres when new rows are binding with Grid. It is used to change the value of Grid at the time of grid rendering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void gridTemplateMaster_AutoGeneratingColumn(object sender, GridViewAutoGeneratingColumnEventArgs e)
        {
            //var watch = Stopwatch.StartNew();
            //try
            //{
            //    var dataColumn = e.Column as GridViewDataColumn;
            //    if (dataColumn != null)
            //    {
            //        if (dataColumn.UniqueName == "uid" || dataColumn.UniqueName == "areaName" || dataColumn.UniqueName == "Category" || dataColumn.UniqueName == "eventType" || dataColumn.UniqueName == "SL1" || dataColumn.UniqueName == "SL4" || dataColumn.UniqueName == "SL3" || dataColumn.UniqueName == "Press 1" || dataColumn.UniqueName == "Press 2" || dataColumn.UniqueName == "Press 3" || dataColumn.UniqueName == "Biax 1" || dataColumn.UniqueName == "Biax 2" || dataColumn.UniqueName == "MD1" || dataColumn.UniqueName == "Tool Installation" || dataColumn.UniqueName == "Tool Removal" || dataColumn.UniqueName == "Tool Setup" || dataColumn.UniqueName == "Employee Onboarding" || dataColumn.UniqueName == "Appraisal" || dataColumn.UniqueName == "New Client Setup" || dataColumn.UniqueName == "Daily & Weekly Task" || dataColumn.UniqueName == "Candidate Screening" || dataColumn.UniqueName == "Invoice Generation" || dataColumn.UniqueName == "IT Help Desk Support" || dataColumn.UniqueName == "Setup Cloud Infrastructure" || dataColumn.UniqueName == "New Eq"  || dataColumn.UniqueName == "fileName" || dataColumn.UniqueName == "originalEstimateTime" || dataColumn.UniqueName == "assigneeName"  || dataColumn.UniqueName == "sopTemplateDesc" || dataColumn.UniqueName == "templateName" || dataColumn.UniqueName == "SopNumber")
            //        {
            //            GridViewDataColumn newColumn = new GridViewDataColumn();
            //            newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
            //            newColumn.IsVisible = false;
            //            newColumn.UniqueName = dataColumn.UniqueName;
            //            e.Column = newColumn;
            //        }
            //        else
            //        {
            //            GridViewCheckBoxColumn newColumn = new GridViewCheckBoxColumn();
            //            newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
            //            newColumn.Header = dataColumn.Header;
            //            newColumn.UniqueName = dataColumn.UniqueName;
            //            e.Column = newColumn;
            //        }

            //    }
            //}
            //catch (Exception ex)
            //{
            //    watch.Stop();
            //    app_logger.appLogger.Error("[ERROR : Error in AutoGeneratingColumn " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            //    RadWindow.Alert("Something went wrong. Please try again.");
            //}
        }
        /// <summary>
        /// This Event Fires when Right click context menu click on grid. Its Use to perform diffrent operations on Grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                ClearInputFields();
                dynamic item = e.OriginalSource;
                var selectedRow = this.gridTemplateMaster.SelectedItem as TemplateMasterGrid;
                Application.Current.Properties["selectedDataRow"] = selectedRow;
                if (selectedRow == null)
                {
                    RadWindow.Alert("Please select SOP Work Instruction");
                    return;
                }
                //var selectedItem = selectedRow.ItemArray;
                var itm = item.DataContext;
                Application.Current.Properties["SopObjectID"] = selectedRow.uid;//selectedItem[0];
                workInstructionId = Convert.ToInt32(selectedRow.uid);//(selectedItem[0]);
                if (item.Header == "View Document")
                {

                    int selectedId = Convert.ToInt32(selectedRow.uid);//(selectedItem[0]);
                    RadNavigationView rdn = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    rdn.Content = new PTPL_SOP_System_UI.Admin.Views.DocumnetViwer(selectedId);
                }
                else if (item.Header == "View")
                {
                    Application.Current.Properties["mode"] = "view";
                    btnSave.Content = "Create/Save";
                    txtblockAddSOPWI.Text = "SOP Work Instructions";
                    btnNext.Visibility = Visibility.Visible;
                    btnUpdate.Visibility = Visibility.Hidden;
                    sopId = Convert.ToInt32(selectedRow.uid);//Convert.ToInt32(selectedItem[0]);
                    txtSopTemplateName.Text = selectedRow.templateName;//selectedItem[2].ToString();
                    txtSopTemplateName.IsReadOnly = true;
                    txtSopID.Text = selectedRow.sopNumber;//selectedItem[4].ToString();
                    Application.Current.Properties["SOPWINumber"] = txtSopID.Text;
                    Application.Current.Properties["sopTitle"] = txtSopTemplateName.Text;

                    txtSopID.IsReadOnly = true;
                    txtFilenmame.Text = selectedRow.fileName;//selectedItem[3].ToString();
                    txtTemplateDecription.Text = selectedRow.sopTemplateDesc;//selectedItem[9] == null ? "" : selectedItem[9].ToString();
                    txtTemplateDecription.IsReadOnly = true;
                    drpSopCategory.Text = selectedRow.category;//selectedItem[5].ToString();
                    drpSopCategory.IsEnabled = false;
                    drpArea.IsEnabled = false;
                    drpArea.Text = selectedRow.areaName;//selectedItem[1].ToString();
                    drpMachine.IsEnabled = false;
                    drpAssinee.Text = selectedRow.assigneeName;//selectedItem[7].ToString();
                    drpAssinee.IsEnabled = false;
                    txtEstimation.Text = selectedRow.originalEstimateTime;//selectedItem[6].ToString();
                    txtEstimation.IsReadOnly = true;
                    btnNext.IsEnabled = true;
                    sp_AddTemplate.Visibility = Visibility.Visible;
                    btnNext.Content = "Next";
                    btnSave.Visibility = Visibility.Visible;
                    btnSave.IsEnabled = false;
                    tmpRadControl.Visibility = Visibility.Visible;
                    templateGrid.Visibility = Visibility.Hidden;
                    EventInstruction.IsEnabled = true;
                    customeForm.IsEnabled = true;
                    btnBrowse.IsEnabled = false;
                }
                else if (item.Header == "Edit")
                {
                    Application.Current.Properties["mode"] = "edit";
                    EventInstruction.IsEnabled = true;
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        txtSopTemplateName.Focus();
                    }), System.Windows.Threading.DispatcherPriority.Render);
                    txtblockAddSOPWI.Text = "Edit SOP Work Instructions";

                    sopId = Convert.ToInt32(selectedRow.uid);//Convert.ToInt32(selectedItem[0]);
                    txtSopTemplateName.Text = selectedRow.templateName;//selectedItem[2].ToString();
                    txtSopID.Text = selectedRow.sopNumber;//selectedItem[4].ToString();
                    Application.Current.Properties["SOPWINumber"] = txtSopID.Text;
                    Application.Current.Properties["sopTitle"] = txtSopTemplateName.Text;
                    txtFilenmame.Text = selectedRow.fileName;//selectedItem[3].ToString();
                    drpSopCategory.Text = selectedRow.category;//selectedItem[5].ToString();
                    drpArea.Text = selectedRow.areaName;//selectedItem[1].ToString();
                    // Application.Current.Properties["areaID"] = drpMachine.SelectedValue;//drpArea.SelectedValue;
                    //drpMachine.Text = selectedRow.equipmentType;
                    drpEventType.Text = selectedRow.eventType;//Convert.ToInt32(selectedItem[0]);  //selectedItem[0].ToString();
                    txtTemplateDecription.Text = selectedRow.sopTemplateDesc;//selectedItem[9] == null ? "" : selectedItem[9].ToString();
                    drpAssinee.Text = selectedRow.assigneeName;//selectedItem[7].ToString();
                    txtEstimation.Text = selectedRow.originalEstimateTime;//selectedItem[6].ToString();

                    sp_AddTemplate.Visibility = Visibility.Visible;
                    tmpRadControl.Visibility = Visibility.Visible;
                    templateGrid.Visibility = Visibility.Hidden;
                    btnSave.Visibility = Visibility.Hidden;
                    btnNext.Visibility = Visibility.Visible;
                    btnUpdate.Visibility = Visibility.Visible;
                    ClearEnableProperty();
                }
                else if (item.Header == "Save As")
                {
                    Application.Current.Properties["mode"] = "saveas";
                    // Application.Current.Properties["areaID"] = drpMachine.SelectedValue;
                    btnSave.IsEnabled = true;
                    EventInstruction.IsEnabled = true;
                    tb_addSOPApprovals.IsEnabled = true;
                   
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        txtSopTemplateName.Focus();
                    }), System.Windows.Threading.DispatcherPriority.Render);
                    txtblockAddSOPWI.Text = "Save As SOP Work Instructions";
                    sopId = Convert.ToInt32(selectedRow.uid);//Convert.ToInt32(selectedItem[0]);
                    txtSopTemplateName.Text = selectedRow.templateName;//selectedItem[2].ToString();
                    busyIndicator1.IsBusy = true;
                    txtSopID.Text = await display_SopNumber();
                    Application.Current.Properties["SOPWINumber"] = txtSopID.Text;
                    Application.Current.Properties["sopTitle"] = txtSopTemplateName.Text;
                    busyIndicator1.IsBusy = false;
                    drpAssinee.Text = selectedRow.assigneeName;//selectedItem[7].ToString();
                    txtEstimation.Text = selectedRow.originalEstimateTime;//selectedItem[6].ToString();
                    txtFilenmame.Text = selectedRow.fileName;//selectedItem[3].ToString();
                    // drpMachine.Text = BindMachineContext(selectedRow);
                    drpSopCategory.Text = selectedRow.category;//selectedItem[5].ToString();
                    drpArea.Text = selectedRow.areaName;//selectedItem[1].ToString();
                    txtTemplateDecription.Text = selectedRow.sopTemplateDesc;//selectedItem[9] == null ? "" : selectedItem[9].ToString();
                    drpEventType.Text = selectedRow.eventType;//Convert.ToInt32(selectedItem[0]);
                    drpAssinee.Text = selectedRow.assigneeName;//selectedItem[0].ToString();

                    btnNext.Visibility = Visibility.Visible;
                    btnUpdate.Visibility = Visibility.Hidden;
                    btnSave.Visibility = Visibility.Visible;
                    btnSave.Content = "Save As";
                    btnNext.IsEnabled = true;
                    sp_AddTemplate.Visibility = Visibility.Visible;
                    tmpRadControl.Visibility = Visibility.Visible;
                    templateGrid.Visibility = Visibility.Hidden;
                    ClearEnableProperty();


                    
                }

                Application.Current.Properties["estimation"] = txtEstimation.Text.Trim();
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occurred while saving SOP WI events. " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }

        }
        /// <summary>
        /// This Events fires when Area selection changed. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void drpArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            await BindMachine();
            Application.Current.Properties["areaID"] = drpMachine.SelectedValue;
            TemplateModel templateModel = new TemplateModel();
            templateModel.uid = sopId;
            busyIndicator1.IsBusy = false;
        }
        protected void ClearEnableProperty()
        {
            txtSopTemplateName.IsReadOnly = false;
            txtSopID.IsReadOnly = false;
            txtTemplateDecription.IsReadOnly = false;
            txtEstimation.IsReadOnly = false;
            drpSopCategory.IsEnabled = true;
            drpArea.IsEnabled = true;
            drpAssinee.IsEnabled = true;
            btnBrowse.IsEnabled = true;
            drpMachine.IsEnabled = true;
        }
        /// <summary>
        /// This Events Fires When Event Type selection changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void drpEventType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        /// <summary>
        /// This Events fire onclick of update button. Its used to perform update operations on SOP WI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            var flag = await UdpateSOPTemplate();
            if (flag)
            {
               
                RadWindow.Alert(new DialogParameters { Header = "Success", Content = "SOP WI item updated successfully" });
                busyIndicator1.IsBusy = true;
                await BindTemplateMasterGrid();
                //sp_AddTemplate.Visibility = Visibility.Hidden;
                //tmpRadControl.Visibility = Visibility.Hidden;
                //templateGrid.Visibility = Visibility.Visible;
                busyIndicator1.IsBusy = false;
            }
            else
            {
                RadWindow.Alert("ERROR: Update failed to save SOP WI.");
            }
            busyIndicator1.IsBusy = false;
        }
        /// <summary>
        /// This Events Fire when tabs changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void tmpRadControl_SelectionChanged(object sender, RadSelectionChangedEventArgs e)
        {
            if (workInstructionId > 0)
            {
                TemplateModel templateModel = new TemplateModel();
                templateModel.uid = sopId;
                string mode = Application.Current.Properties["mode"] == null ? "" : Application.Current.Properties["mode"].ToString();
                if (mode == "view")
                {
                    templateModel.sopTemplateName = "View_Mode";
                }
                RadSelectionChangedEventArgs selectionArgs = (RadSelectionChangedEventArgs)e;
                if (((RadTabItem)selectionArgs.AddedItems[0]).Header.ToString() == "SOP Scheduler")
                {
                    EventInstruction.Content = new EventTriggerRule(templateModel);
                }
                else if (((RadTabItem)selectionArgs.AddedItems[0]).Header.ToString() == "SOP Form Designer")
                {
                    customeForm.Content = new frmCustomForm("Builder");
                }
                else if (((RadTabItem)selectionArgs.AddedItems[0]).Header.ToString() == "SOP Approvals")
                {
                    tb_addSOPApprovals.Content = new frmSopApprovals();
                }
            }

        }
        /// <summary>
        /// This Events FIres When focus is lost from textbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void txtEstimation_LostFocus(object sender, RoutedEventArgs e)
        {
            // CheckValidationForEstimationTime();
            if (CheckValidationForEstimationTimeModified())
            {
                //  string estimation = txtEstimation.Text;
            }
            else
            {
                RadWindow.Alert("INFO: Your original estimate must be in the format 5w 5d 5h 5m");
                //MessageBox.Show("Your original estimate must be in the format 5w 5d 5h 5m");
                txtEstimation.Text = string.Empty;
                // txtEstimation.Focus();
                return;

            }
        }
        #endregion

        private async void btnSave_Click(object sender, RoutedEventArgs e)
        {
            int sopIdForevent = sopId;
            if (string.IsNullOrEmpty(txtSopID.Text))
            {
                RadWindow.Alert("Please enter WI number");
                txtSopID.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtSopTemplateName.Text))
            {
                RadWindow.Alert("Please enter SOP title name");
                txtSopTemplateName.Focus();
                return;
            }
            if (drpSopCategory.SelectedIndex == -1)
            {

                RadWindow.Alert("Please select SOP category");
                drpSopCategory.Focus();
                return;
            }
            if (drpArea.SelectedIndex == -1)
            {
                RadWindow.Alert("Please select area");
                drpArea.Focus();
                return;
            }
            if (drpMachine.SelectedIndex == -1)
            {
                RadWindow.Alert("Please select equipment type");
                drpMachine.Focus();
                return;
            }
            if (drpAssinee.SelectedIndex == -1)
            {
                RadWindow.Alert("Please select Assignee");
                drpAssinee.Focus();
                return;
            }
            if (string.IsNullOrEmpty(txtEstimation.Text))
            {
                RadWindow.Alert("Please enter original estimation");
                txtEstimation.Focus();
                return;
            }
            //if (string.IsNullOrEmpty(txtFilenmame.Text))
            //{
            //    RadWindow.Alert("Please select file");
            //    txtEstimation.Focus();
            //    return;
            //}

            Application.Current.Properties["selectedMachine"] = drpMachine.Text;


            if (btnSave.Content.ToString() == "Save As")
            {
                int savasId = sopId;
                busyIndicator1.IsBusy = true;

                using (var client = new HttpClient())
                {
                    apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/TemplateMaster/GetTemplatesById?Id=" + sopId;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["templateData"];
                            var deserializedList = JsonConvert.DeserializeObject<dynamic>(finalList.ToString());
                            streamReads = deserializedList.fileStream;
                            saveAsEventType = deserializedList.eventType;
                        }
                    }
                }

                var saveasWiId = await SaveSOPTemplate();
                //fetch all events  related to WI of Save as item
                var listOfEvents = await FetchEventOfWI(sopIdForevent);
                if (listOfEvents != null)
                {

                    foreach (var item in listOfEvents)
                    {
                        await AddEvents(saveasWiId, item);
                    }
                }
                //dublicate the custom form
                 await  SaveAsCustomForm(savasId, saveasWiId);
                
                await BindTemplateMasterGrid();
                busyIndicator1.IsBusy = false;
                if (btnSave.Content != "Save As")
                {
                    sp_AddTemplate.Visibility = Visibility.Hidden;
                    tmpRadControl.Visibility = Visibility.Hidden;
                    templateGrid.Visibility = Visibility.Visible;
                    

                }
                
            }
            else
            {

                List<int> selectedMachineIds = new List<int>();
                foreach (dynamic item in drpMachine.SelectedItems)
                {
                    selectedMachineIds.Add(item.uid);
                }

                TemplateModel templateModel = new TemplateModel();
                templateModel.uid = 0;
                templateModel.sopNumber = !string.IsNullOrEmpty(txtSopID.Text.Trim()) ? txtSopID.Text.Replace("\"", "''").Trim() : "";
                templateModel.sopTemplateName = !string.IsNullOrEmpty(txtSopTemplateName.Text.Trim()) ? txtSopTemplateName.Text.Replace("\"", "''").Trim() : "";
                templateModel.sopTemplateDesc = !string.IsNullOrEmpty(txtTemplateDecription.Text.Trim()) ? txtTemplateDecription.Text.Replace("\"", "''").Trim() : "";
                templateModel.sopCategory = Convert.ToInt32(drpSopCategory.SelectedValue);
                templateModel.equipmentType = 1;//drpEquipmentType.SelectedValue,
                templateModel.eventType = Convert.ToInt32(drpEventType.SelectedValue);
                templateModel.eventTrigger = 1;//,//drpEventTrigger.SelectedValue,
                templateModel.filePath = "";//fileUpload.FilePath,
                templateModel.fileSize = "";//fileUpload.RenderSize,
                templateModel.fileVersion = "";
                templateModel.machineId = Convert.ToInt32(drpMachine.SelectedValue);
                templateModel.createdBy = Convert.ToInt32(_userData.id);
                templateModel.assigneeId = Convert.ToInt32(drpAssinee.SelectedValue);
                templateModel.fileStream = streamReads;
                templateModel.fileName = txtSopID.Text.Trim() + "  -" + txtSopTemplateName.Text.Trim();
                templateModel.areaId = Convert.ToInt32(drpArea.SelectedValue);
                templateModel.originalEstimateTime = !string.IsNullOrEmpty(txtEstimation.Text.Trim()) ? txtEstimation.Text.Replace("\"", "''").Trim() : "";
                busyIndicator1.IsBusy = true;
                await SaveSOPTemplate();
                busyIndicator1.IsBusy = false;
                if (workInstructionId > 0)
                {
                    //btnSave.IsEnabled = false;
                    btnNext.IsEnabled = true;
                    txtSopID.IsEnabled = false;
                    //DisableAllFields();

                    OnConfirm(sender, null);
                }
            }
        }

        protected void DisableAllFields()
        {

            drpMachine.IsEnabled = false;
            btnBrowse.IsEnabled = false;
            txtEstimation.IsEnabled = false;
            txtTemplateDecription.IsEnabled = false;
            txtSopTemplateName.IsEnabled = false;
            drpSopCategory.IsEnabled = false;
            drpArea.IsEnabled = false;
            drpMachine.IsEnabled = false;
            drpAssinee.IsEnabled = false;
            
        }
        protected void EnableAllFields()
        {
            drpMachine.IsEnabled = true;
            btnBrowse.IsEnabled = true;
            txtEstimation.IsEnabled = true;
            txtTemplateDecription.IsEnabled = true;
            txtSopTemplateName.IsEnabled = true;
            drpSopCategory.IsEnabled = true;
            drpArea.IsEnabled = true;
            drpMachine.IsEnabled = true;
            drpAssinee.IsEnabled = true;

        }
        private async void OnConfirm(object sender, WindowClosedEventArgs e)
        {
            //var result = e.DialogResult;
            //if (result == true)
            //{
            //    EventInstruction.IsEnabled = true;
            //    EventInstruction.IsSelected = true;
            //    TemplateModel templateModel = new TemplateModel();
            //    templateModel.uid = sopId;
            //    EventInstruction.Content = new EventTriggerRule(templateModel);


            //}
            //else
            //{
            //    await BindTemplateMasterGrid();
            //    sp_AddTemplate.Visibility = Visibility.Hidden;
            //    tmpRadControl.Visibility = Visibility.Hidden;
            //    templateGrid.Visibility = Visibility.Visible;

            //}

            customeForm.IsEnabled = true;
            customeForm.IsSelected = true;
            Application.Current.Properties["mode"] = "new";
            EventInstruction.IsEnabled = true;
            tb_addSOPApprovals.IsEnabled = true;

               // TemplateModel templateModel = new TemplateModel();
               //templateModel.uid = sopId;
               //EventInstruction.Content = new EventTriggerRule(templateModel);


        }

        private async void OnSave(object sender, WindowClosedEventArgs e)
        {
            var result = e.DialogResult;
            if (result == true)
            {

                busyIndicator1.IsBusy = true;
                await SaveSOPTemplate();
                busyIndicator1.IsBusy = false;
            }
        }

        private async void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            busyIndicator1.IsBusy = true;
            await BindTemplateMasterGrid();
            busyIndicator1.IsBusy = false;

        }
    }
    #region Properties
    public class TemplateMasterGrid
    {
        public int uid { set; get; }
        public string templateName { set; get; }
        public string sopTemplateDesc { set; get; }
        public string areaName { set; get; }
        public string fileName { set; get; }
        public string sopNumber { set; get; }
        public string category { set; get; }
        public string originalEstimateTime { set; get; }
        public string assigneeName { set; get; }
        public string eventType { set; get; }
        //public List<ColumnData> machineDTOs { set; get; }

        public string equipmentType { get; set; }

    }
    public class ColumnData
    {
        public int machineId { set; get; }
        public string machineName { set; get; }
        public bool isUsed { set; get; }

    }
    public class Assignee
    {
        public int uid { set; get; }
        public string userName { set; get; }
        public string firstName { set; get; }
        public string lastName { set; get; }
    }
    public class TemplateModel
    {


        public int uid { get; set; }
        public string sopNumber { get; set; }
        public string sopTemplateName { get; set; }
        public string sopTemplateDesc { get; set; }
        public int? sopCategory { get; set; }
        public int? equipmentType { get; set; }
        public int? eventType { get; set; }
        public int? eventTrigger { get; set; }
        public int? assigneeId { get; set; }
        public string fileStream { get; set; }
        public string filePath { get; set; }
        public string fileSize { get; set; }
        public string fileVersion { get; set; }
        public int? createdBy { get; set; }
        //public List<int> machineId { get; set; }
        public int machineId { get; set; }
        public string fileName { get; set; }
        public int? areaId { set; get; }
        public string originalEstimateTime { set; get; }
    }
    #endregion

}
