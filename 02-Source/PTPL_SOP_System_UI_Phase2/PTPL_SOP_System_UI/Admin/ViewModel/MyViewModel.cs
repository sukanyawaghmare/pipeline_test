﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Net.Http;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ScheduleView;


namespace PTPL_SOP_System_UI.Admin.Views
{

    public class MyViewModel
    {
        UserData _userData;

        private ObservableCollection<Appointment> appointments;

        public ObservableCollection<Appointment> Appointments
        {
            get
            {
                if (this.appointments == null)
                {
                    this.appointments = this.CreateAppointments();
                }
                return this.appointments;
            }
        }

        private ObservableCollection<Appointment> CreateAppointments()
        {
            _userData = Application.Current.Properties["userData"] as UserData;
            ObservableCollection<Appointment> apps = new ObservableCollection<Appointment>();
            try
            {

                List<ScheduleEvent> list = BindEventsGrid(Convert.ToInt32(Application.Current.Properties["wiSopId"]));
                if (list != null)
                {
                    foreach (ScheduleEvent item in list)
                    {


                        var startDt = Convert.ToDateTime(item.startTime);
                        var endTime = Convert.ToDateTime(item.endTime);//startDt.AddHours(2);
                        string startDateTime = commonDeclaration.GetConvertedDate(startDt.ToString(), commonDeclaration.timeZone);
                        string endDateTime = commonDeclaration.GetConvertedDate(endTime.ToString(), commonDeclaration.timeZone);

                        Appointment obj = new Appointment();
                        obj.Subject = "Event : " + item.title + "\n Assigned To: " + item.assignee + "\n Start Time:" + Convert.ToDateTime(startDateTime).ToString("h:mm tt") + "\n End Time: " + Convert.ToDateTime(endDateTime).ToString("h:mm tt") + "\n Occurrence: " + item.occurance;
                        obj.Start = Convert.ToDateTime(startDateTime);
                        obj.End = Convert.ToDateTime(endDateTime);

                        if (item.occurance != "None")
                        {
                            var pattern = new RecurrencePattern();
                            if (item.occurance == "Daily")
                                pattern.Frequency = RecurrenceFrequency.Daily;
                            else if (item.occurance == "Weekly")
                                pattern.Frequency = RecurrenceFrequency.Weekly;
                            else if (item.occurance == "Monthly")
                                pattern.Frequency = RecurrenceFrequency.Monthly;
                            else if (item.occurance == "Yearly")
                            {


                            }
                            else if (item.occurance == "Hourly")
                                pattern.Frequency = RecurrenceFrequency.Hourly;



                            obj.RecurrenceRule = new RecurrenceRule(pattern);
                        }
                        apps.Add(obj);


                    }
                }
                else
                {
                    RadWindow.Alert("No Events Scheduled");
                }
            }
            catch (Exception ex)
            {

            }
            return apps;
        }


        private List<ScheduleEvent> BindEventsGrid(int sopId)
        {
            bool result = false;

            string apiurl = string.Empty;
            string apiEndPoint = string.Empty;
            string apiVersion = string.Empty;
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            try
            {

                using (var client = new HttpClient())
                {

                    if(Application.Current.Properties["calenderViewSM"].ToString() == "single")
                    {
                        apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/EventTriggerSchedule/GetAllEventTriggerScheduleByWIId?Id=" + sopId;
                    }
                    else if (Application.Current.Properties["calenderViewSM"].ToString() == "multi")
                    {
                        apiurl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/EventTriggerSchedule/GetAllEventTriggerSchedule";
                    }

                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = client.GetAsync(apiurl).Result)
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventTriggerScheduleData"];
                            //var resultData = JsonConvert.DeserializeObject<DocumentViewModel>(responseData);
                            List<ScheduleEvent> docdata = JsonConvert.DeserializeObject<List<ScheduleEvent>>(finalList.ToString());


                            return docdata;

                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show("Error found : " + ex.Message, "Error", MessageBoxButton.OK);
                return null;
            }

        }
    }
}
