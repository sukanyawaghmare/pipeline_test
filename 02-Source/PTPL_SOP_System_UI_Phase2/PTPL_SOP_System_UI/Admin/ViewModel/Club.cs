﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    public class Club : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string equipment;
        private string sopNumber;
        private string sopTitle;
        private string fileName;

        private bool sl1Machine;
        private bool sl2Machine;
        private bool sl3Machine;
        private bool sl4Machine;

        private bool press1;
        private bool press2;
        private bool press3;

        private bool biax1;
        private bool biax2;

        private bool mdo;

        private string trigger;

        public string Equipment
        {
            get { return this.equipment; }
            set
            {
                if (value != this.equipment)
                {
                    this.equipment = value;
                    this.OnPropertyChanged("Equipment");
                }
            }
        }

        public string SopNumber
        {
            get { return this.sopNumber; }
            set
            {
                if (value != this.sopNumber)
                {
                    this.sopNumber = value;
                    this.OnPropertyChanged("sopNumber");
                }
            }
        }

        public string SopTitle
        {
            get { return this.sopTitle; }
            set
            {
                if (value != this.sopTitle)
                {
                    this.sopTitle = value;
                    this.OnPropertyChanged("SopTitle");
                }
            }
        }

        public string FileName
        {
            get { return this.fileName; }
            set
            {
                if (value != this.fileName)
                {
                    this.fileName = value;
                    this.OnPropertyChanged("FileName");
                }
            }
        }

        public bool SL1
        {
            get { return this.sl1Machine; }
            set
            {
                if (value != this.sl1Machine)
                {
                    this.sl1Machine = value;
                    this.OnPropertyChanged("SL1");
                }
            }
        }

        public bool SL2
        {
            get { return this.sl2Machine; }
            set
            {
                if (value != this.sl2Machine)
                {
                    this.sl2Machine= value;
                    this.OnPropertyChanged("SL2");
                }
            }
        }


        public bool SL3
        {
            get { return this.sl3Machine; }
            set
            {
                if (value != this.sl3Machine)
                {
                    this.sl3Machine = value;
                    this.OnPropertyChanged("SL3");
                }
            }
        }


        public bool SL4
        {
            get { return this.sl4Machine; }
            set
            {
                if (value != this.sl4Machine)
                {
                    this.sl4Machine = value;
                    this.OnPropertyChanged("SL4");
                }
            }
        }

        public bool Press1
        {
            get { return this.press1; }
            set
            {
                if (value != this.press1)
                {
                    this.press1 = value;
                    this.OnPropertyChanged("Press1");
                }
            }
        }

        public bool Press2
        {
            get { return this.press2; }
            set
            {
                if (value != this.press2)
                {
                    this.press2 = value;
                    this.OnPropertyChanged("Press2");
                }
            }
        }

        public bool Press3
        {
            get { return this.press3; }
            set
            {
                if (value != this.press3)
                {
                    this.press3 = value;
                    this.OnPropertyChanged("Press3");
                }
            }
        }


        public bool Biax1
        {
            get { return this.biax1; }
            set
            {
                if (value != this.biax1)
                {
                    this.biax1 = value;
                    this.OnPropertyChanged("Biax1");
                }
            }
        }

        public bool Biax2
        {
            get { return this.biax2; }
            set
            {
                if (value != this.biax2)
                {
                    this.biax2 = value;
                    this.OnPropertyChanged("Biax2");
                }
            }
        }

        public bool MDO
        {
            get { return this.mdo; }
            set
            {
                if (value != this.mdo)
                {
                    this.mdo = value;
                    this.OnPropertyChanged("Biax2");
                }
            }
        }

        public string Trigger
        {
            get { return this.trigger; }
            set
            {
                if (value != this.trigger)
                {
                    this.trigger = value;
                    this.OnPropertyChanged("Trigger");
                }
            }
        }

        public Club(string equipment, string sopNumber, string sopTitle, string fileName, bool sl1Machine, bool sl2Machine, bool sl3Machine, bool sl4Machine, bool press1, bool press2, bool press3, bool biax1, bool biax2, bool mdo, string trigger)
        {
            this.equipment = equipment;
            this.sopNumber = sopNumber;
            this.sopTitle = sopTitle;
            this.fileName = fileName;
            this.sl1Machine = sl1Machine;
            this.sl2Machine = sl2Machine;
            this.sl3Machine = sl3Machine;
            this.sl4Machine = sl4Machine;
            this.press1 = press1;
            this.press2 = press2;
            this.press3 = press3;
            this.biax1 = biax1;
            this.biax2 = biax2;
            this.mdo = mdo;
            this.trigger = trigger;
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }
    }
}
