﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    class DocumentTemplate
    {
        public string fileName { set; get; }
        
        public string fileSize { set; get; }
        public string fileStream { set; get; }

    }
}
