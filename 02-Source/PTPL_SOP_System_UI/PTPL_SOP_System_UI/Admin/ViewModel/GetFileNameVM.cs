﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.Admin.ViewModel
{
    class GetFileNameVM
    {

        public int id { set; get; }
        public string fileName { set; get; }
    }
}
