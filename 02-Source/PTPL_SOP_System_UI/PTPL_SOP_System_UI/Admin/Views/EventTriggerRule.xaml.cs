﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Effects;
using Telerik.Windows.Controls;


namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    ///  Interaction Logic  EventTriggerRule.xaml
    ///  Created By  :  Chetan 
    ///  Created on  : 02 April 2020
    ///  -----------------------------------------------------------------------
    ///  Modified By : Chetan 
    ///  Modified on : 03 April  2020
    ///  Purpose     : Additition of Edit/Remove Functionality & validations as well
    ///  -------------------------------------------------------------------------
    ///  Modified By :  Chetan 
    ///  Modified on : 07 April 2020
    ///  Purpose     :  Added Back Button TO redirect to home page
    /// </summary>
    ///  -------------------------------------------------------------------------
    ///  Modified By : 
    ///  Modified on : 
    ///  Purpose     :  
    /// </summary>
    public partial class EventTriggerRule : UserControl
    {
        #region Variables
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        int workInstructionID = 0;
        int eventTriggerId = 0;
        TemplateModel objTemplateModel = null;
        UserData _userData;
        HttpClient client = new HttpClient();
        List<ScheduleEvent> gridDataList = null;
        string sOPNumberInEvent = Application.Current.Properties["SOPWINumber"].ToString();
        #endregion

        #region Constructor
        public EventTriggerRule(TemplateModel _objTemplateModel)
        {

            objTemplateModel = _objTemplateModel;
            InitializeComponent();
            ClearInputFields();
            _userData = Application.Current.Properties["userData"] as UserData;
           
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Method Use to bind databse Machine/Equpment data to UI Equiment combobox
        /// <return> bool</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindMachine()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/MachineMaster/GetAllMachines?Id=";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["machinesData"];
                            List<MachineMaster> deserializedList = JsonConvert.DeserializeObject<List<MachineMaster>>(finalList.ToString());
                            drpEquipment.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Machine Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Machine Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");

                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : Error occured while Binding Machine Data in event trigger rule. "+ex.Message+"]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method Use to bind databse All Users data to UI Assinee combobox
        /// <return>bool</return>
        /// </summary>
        ///<remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindUsers()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
               using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["userData"];
                            List<Assignee> deserializedList = JsonConvert.DeserializeObject<List<Assignee>>(finalList.ToString());
                            drpAssinee.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Users Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Users Data Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR :  Error occured while Binding the users in Event Trigger Rule. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                 result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method Use to bind databse created events on UI Grid ande calenderView
        /// <return>bool</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> BindEventsGrid()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTriggerSchedule/GetAllEventTriggerScheduleByWIId?id=" + objTemplateModel.uid;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventTriggerScheduleData"];
                            gridDataList = JsonConvert.DeserializeObject<List<ScheduleEvent>>(finalList.ToString());
                            List<ScheduleEvent> listSchduleEvents = new List<ScheduleEvent>();
                            foreach (ScheduleEvent item in gridDataList)
                            {
                                DateTime localDt = Convert.ToDateTime(item.timeValue);
                                item.timeValue = localDt.ToString("dd MMMM yyyy h:mm tt");
                                item.machine = Application.Current.Properties["selectedMachine"].ToString();
                                listSchduleEvents.Add(item);
                            }
                            if (gridDataList.Count > 0)
                            {
                                watch.Stop();
                                gridEventMaster.ItemsSource = gridDataList;
                                radDataPager.Source = gridDataList.ToEnumerable();
                                radDataPager.Visibility = Visibility.Visible;
                                radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = gridEventMaster });
                                app_logger.appLogger.Trace("[TRACE : Events Grid Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                                app_logger.appLogger.Info("[INFO : Events Grid Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                               
                            }
                            else
                            {
                                gridEventMaster.ItemsSource = null;
                                watch.Stop();
                                app_logger.appLogger.Info("[INFO :  Not a single event attached. ]" + $" | {watch.ElapsedMilliseconds} ms");
                             }

                            result = true;
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while Binding the event rule grid " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
               
            }
            return result;
        }
        /// <summary>
        /// This Method Use to bind databse Occurences values on UI  Occurences Combobox
        /// <return>bool</return>
        /// </summary>
        /// <exception cref="API">Unable to reach to API</exception>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> BindOccurences()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventOccurrence/GetAllEventOccurrence";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventOccurrenceData"];
                            List<Occurences> deserializedList = JsonConvert.DeserializeObject<List<Occurences>>(finalList.ToString());
                            drpOcuurences.ItemsSource = deserializedList;
                            drpOcuurences.SelectedIndex = 5;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Occurenced  Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Occurenced  Loaded Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");


                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while Binding the Occurances. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return result;
        }
        /// <summary>
        /// This Method Use to bind create Events in database
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> AddEvents(int sopWinID)
        {
            bool result = false;
            DateTime normalDateTimeFormat;
            if (drpEventType.Text == "Time Based")
                normalDateTimeFormat = Convert.ToDateTime(txtTimeValue.SelectedValue.ToString());
            else
                normalDateTimeFormat = DateTime.Now;
            var watch = Stopwatch.StartNew();
            var modal = new
            {
                eventTriggerId =eventTriggerId,
                wiSopId = sopWinID,
                eventType = drpEventType.SelectedValue,
                title = !string.IsNullOrEmpty(txtTitle.Text.Trim()) ? txtTitle.Text.Replace("\"", "''").Trim() : "",
                startTime = TimeZoneInfo.ConvertTimeToUtc(normalDateTimeFormat, TimeZoneInfo.Local),

                endTime = "2020-04-03T08:15:58.805Z",
                occurance = Convert.ToInt32(drpOcuurences.SelectedValue==null? 1: drpOcuurences.SelectedValue),
                machineId = 1,//drpEquipment.SelectedValue,
                assignee = drpAssinee.SelectedValue,
                condition = !string.IsNullOrEmpty(txtConditon.Text.Trim()) ? txtConditon.Text.Replace("\"", "''").Trim() : "",
                externalEvents = "Test",
                timeValue = normalDateTimeFormat,
                status = chkActive.IsChecked
            };
            try
            {
                using (var client = new HttpClient())
                {
                     apiUrl = apiEndPoint + apiVersion + "/EventTriggerSchedule/AddUpdateEventTriggerSchedule";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, modal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData1 = response.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            result = true;
                            watch.Stop();
                            if (eventTriggerId != 0) { RadWindow.Alert("Your event updated successfully"); }
                            app_logger.appLogger.Trace("[TRACE : Event added successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Event added successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
               
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  :Error occured while saving the events. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }


     
        /// <summary>
        /// This Method Use to bind databse EventType data to UI EventType combobox
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> BindEventType()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTypes/GetAllEventTypes";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["eventDataList"];
                            List<EventTypeMaster> deserializedList = JsonConvert.DeserializeObject<List<EventTypeMaster>>(finalList.ToString());
                            drpEventType.ItemsSource = deserializedList;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : EventType loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : EventType loaded successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                          }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  :Error occured while binding the event type.  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return result;
        }
        /// <summary>
        /// This Method Use to delete single event from DB 
        /// <paramref name="scheduleID"/>
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected async Task<bool> DeleteEventSchedule(int scheduleID)
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/EventTriggerSchedule/DeleteEventTriggerSchedule?id=" + scheduleID;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Event deleted successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : Event deleted successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                         }
                        else
                        {
                            watch.Stop();
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  :Error occured while deleting the event.  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
             }
            return result;

        }
        /// <summary>
        /// This methods use to clear the selection of input fields
        /// <return>void</return>
        /// </summary>ClearInputFields
        protected void ClearInputFields()
        {
            txtTitle.Text = "";
            txtConditon.Text = "";
            txtTimeValue.SelectedValue = null;
            txtConditon.Text = "";
            drpAssinee.SelectedIndex = -1;
            drpEquipment.Visibility = Visibility.Collapsed;
            drpEquipment1.Text = Application.Current.Properties["selectedMachine"].ToString();
            //drpEquipment.SelectedIndex = -1;
            drpEventType.SelectedIndex = -1;
            drpOcuurences.SelectedIndex = -1;
            chkActive.IsChecked = false;

        }
        /// <summary>
        /// This methods use to create SOP Work Instruction Item
        /// <return>int </return>
        /// </summary>
        ///<remarks>Code documentation and Review is pending</remarks>
        protected async Task<int> SaveSOPTemplate()
        {
            var watch = Stopwatch.StartNew();
            bool result = false;
            int workInstructionId = 0;
            apiUrl = string.Empty;
            try
            {
                var sopTemplateModal = new
                {
                    uid = objTemplateModel.uid,
                    sopNumber = objTemplateModel.sopNumber,
                    sopTemplateName = objTemplateModel.sopTemplateName,
                    sopTemplateDesc = objTemplateModel.sopTemplateDesc,
                    sopCategory = objTemplateModel.sopCategory,
                    equipmentType = objTemplateModel.equipmentType,
                    eventType =  Convert.ToInt32(drpEventType.SelectedValue),
                    eventTrigger = objTemplateModel.eventTrigger,
                    filePath = objTemplateModel.filePath,
                    fileSize = objTemplateModel.fileSize,
                    fileVersion = objTemplateModel.fileVersion,
                    machinIds = objTemplateModel.machinIds,
                    createdBy = Convert.ToInt32(_userData.id),
                    assigneeId = objTemplateModel.assigneeId,
                    fileStream = objTemplateModel.fileStream,
                    fileName = objTemplateModel.fileName,
                    areaId = objTemplateModel.areaId,
                    originalEstimateTime = objTemplateModel.originalEstimateTime
                };
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/TemplateMaster/AddUpdateTemplate";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, sopTemplateModal).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);
                            workInstructionId = resultData.id;
                            result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : SOP WI Item saved successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            app_logger.appLogger.Info("[INFO : SOP WI Item saved successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                            RadWindow.Alert("SOP WI Item saved successfully");

                        }
                        else
                        {

                            watch.Stop();
                            app_logger.appLogger.Error("[ERROR : API return with an error : " + response + "]" + $" | {watch.ElapsedMilliseconds} ms");
                            RadWindow.Alert("API return with status code : " + response.StatusCode);
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while saving the SOP WI.  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                result = false;
            }
            return workInstructionId;

        }
        /// <summary>
        /// This methods use to Check the selected time slot should not match with others event time
        /// <return>bool</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        protected bool CheckAvilabiltyOfTimeSlots()
        {

            var watch = Stopwatch.StartNew();
            bool result = false;
            try
            {
                DateTime userSelectedDateTime = Convert.ToDateTime(txtTimeValue.SelectedValue);
                var matches = gridDataList.Where(p => p.timeValue == userSelectedDateTime.ToString("dd MMMM yyyy h:mm tt"));
                if (matches.Count() > 0)
                {
                    result = true;
                }
                watch.Stop();
                app_logger.appLogger.Info("[INFO : Checking the time slot . ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch(Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR  : Error in check time slot.  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return result;

        }
        #endregion

        #region Events
        /// <summary>
        /// This event fire when this usercontrol/form loaded first time. Its for initilizing values.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>  
        private async void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                TemplateMaster templateMaster = new TemplateMaster();
                lblHeader.Text = "Event Rules and Triggers History - " + sOPNumberInEvent;
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                logDir = ConfigurationManager.AppSettings["LogDir"].ToString();
                await BindEventType();
                await BindMachine();
                await BindUsers();
                await BindEventsGrid();
                await BindOccurences();
                busyIndicator.IsBusy = false;
                busyIndicatorGrid.IsBusy = false;
                gridEventMaster.Visibility = Visibility.Visible;
                txtTimeValue.SelectableDateStart = DateTime.Now;


                if (objTemplateModel.sopTemplateName == "View_Mode")
                {
                    eventRule.IsEnabled = false;
                    gridRightContextMenu.IsEnabled = false;
                }

                watch.Stop();
                app_logger.appLogger.Info("[INFO : User control loaded successfully.]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch(Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR  : Unable to load user control " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }

        }
        /// <summary>
        /// This event fires on click of addEvent button. this event changing the UI screen from Grid to Input Control Form.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void eventRule_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            ClearInputFields();
            var trigger = e.OriginalSource as RadToggleButton;
            try
            {
                if (trigger.Content.ToString() != "Back")
                {
                    frmGrid.Visibility = Visibility.Hidden;
                    frmUI.Visibility = Visibility.Visible;
                    lblHeader.Text = "Add Event Trigger Rule - " + sOPNumberInEvent;
                    eventRule.Content = "Back";
                    eventRule.Visibility = Visibility.Hidden;
                    btnCalenderView.Visibility = Visibility.Hidden;
                    btnCreate.Content = "Create Event";
                    eventTriggerId = 0;
                    btnBacktoWI.Visibility = Visibility.Hidden;

                }
                else
                {
                    btnCancel_Click(sender, e);
                }

                watch.Stop();
                app_logger.appLogger.Info("[INFO : Clicked on event rule.]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR  : Unable to clicked on Event Rule " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        /// <summary>
        /// This event fires on click of cancel button. this event changing the UI screen from Input Control Form to Grid View.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                watch.Stop();
                app_logger.appLogger.Info("[INFO: Cancel button clicked on in Event and Trigger rule. ]" + $" | {watch.ElapsedMilliseconds} ms");
                frmGrid.Visibility = Visibility.Visible;
                frmUI.Visibility = Visibility.Hidden;
                lblHeader.Visibility = Visibility.Visible;
                eventRule.Content = "Add Event Rule";
                lblHeader.Text = "Event Trigger Rule History - " + sOPNumberInEvent;
                btnCalenderView.Visibility = Visibility.Visible;
                btnBacktoWI.Visibility = Visibility.Visible;
                eventRule.Visibility = Visibility.Visible;
               
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR  : Unable to clicked on Cancel click. Please try again." + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
                
        }
        /// <summary>
        /// This event fires on click of Create/Update Button. this event used to call Add/Update function.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                if (String.IsNullOrEmpty(txtTitle.Text))
                {
                    RadWindow.Alert("Please enter title for event");
                    txtTitle.Focus();
                    return;
                }
                if (drpEventType.SelectedIndex == -1)
                {
                    RadWindow.Alert("Please select event type");
                    drpEventType.Focus();
                    return;
                }
                if (drpEventType.Text == "Time Based")
                {
                    if (String.IsNullOrEmpty(txtTimeValue.SelectedValue.ToString()))
                    {
                        RadWindow.Alert("Please select event start Date & Time");
                        txtTimeValue.Focus();
                        return;
                    }
                    if (CheckAvilabiltyOfTimeSlots() && eventTriggerId == 0)
                    {
                        RadWindow.Alert("Events already schedule on this timing");
                        txtTimeValue.Focus();
                        return;
                    }
                }


                if (drpAssinee.SelectedIndex == -1)
                {
                    RadWindow.Alert("Please select assignee");
                    drpAssinee.Focus();
                    return;
                }
                busyIndicator.IsBusy = true;
                int sopWorkInstructionID = 0;
                if (objTemplateModel.uid == 0)

                {
                    // sopWorkInstructionID = await SaveSOPTemplate();
                    objTemplateModel.uid = sopWorkInstructionID;
                }
                else
                {
                    sopWorkInstructionID = objTemplateModel.uid;
                }

                
                await AddEvents(sopWorkInstructionID);
                await BindEventsGrid();
                busyIndicator.IsBusy = false;
                if (eventTriggerId == 0)
                {

                }
                else
                {

                }

                watch.Stop();
                app_logger.appLogger.Info("[INFO : Event adn trigger rule added. ]" + $" | {watch.ElapsedMilliseconds} ms");


                btnCancel_Click(sender, e);
            }

            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR  : Error while creating the event. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
            
        
        /// <summary>
        /// This event fires on click of Calender View Button. this event used to switch the Grid view to calender view.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void btnCalenderView_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                bool statusFlag = false;
                foreach (var item in gridDataList)
                {
                    if (item.occurance == "Yearly")
                    {
                        statusFlag = true;
                        break;
                    }
                }

                if (statusFlag)
                {
                    RadWindow.Alert(new DialogParameters { Header = "Info", Content = "Calender view does not support for Yearly view." });

                }

                Application.Current.Properties["wiSopId"] = objTemplateModel.uid;
                this.Content = new EventTriggerRuleWithTelerikControl();
            }
            catch (Exception ex)
            {
             
                watch.Stop();
                RadWindow.Alert("Something went wrong in Calender view. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Unable to calender view because of : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        
        }
        /// <summary>
        /// This event fires on right click grid. this event used to Edit /Remove Event from Grid
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private async void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                dynamic item = e.OriginalSource;
                var selectedRow = this.gridEventMaster.SelectedItem as ScheduleEvent;
                if (selectedRow == null)
                {
                    RadWindow.Alert("Please select event");
                    return;
                }
                if (item.Header == "Edit Event")
                {
                    eventRule.Visibility = Visibility.Hidden;
                    eventTriggerId = Convert.ToInt32(selectedRow.eventTriggerId);
                    txtTitle.Text = selectedRow.title;
                    txtConditon.Text = selectedRow.condition;
                    txtTimeValue.SelectedValue = Convert.ToDateTime(selectedRow.timeValue);
                    //drpEquipment.Text = selectedRow.machine;
                    drpEquipment1.Text = Application.Current.Properties["selectedMachine"].ToString();
                    drpEventType.Text = selectedRow.eventType;
                    drpOcuurences.Text = selectedRow.occurance;
                    drpAssinee.Text = selectedRow.assignee;
                    chkActive.IsChecked = selectedRow.status;
                    frmGrid.Visibility = Visibility.Hidden;
                    frmUI.Visibility = Visibility.Visible;
                    lblHeader.Text = "Update Event Trigger Rule - " + sOPNumberInEvent;
                    eventRule.Content = "Back";
                    btnCreate.Content = "Update";
                    btnCalenderView.Visibility = Visibility.Hidden;
                    btnBacktoWI.Visibility = Visibility.Hidden;
                    watch.Stop();
                    app_logger.appLogger.Trace("[TRACE : Event in Edit Mode]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Info("[INFO : Event in Edit Mode ]" + $" | {watch.ElapsedMilliseconds} ms");
                }
                else if (item.Header == "Remove Event")
                {
                    this.Effect = new BlurEffect();
                    eventTriggerId = Convert.ToInt32(selectedRow.eventTriggerId);
                    RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = onRemove, Content = " Are you sure to remove " + selectedRow.title + " event?", Header = "Remove Event" });
                    this.Effect = null;
                    watch.Stop();
                    app_logger.appLogger.Trace("[TRACE : Event in Remove Mode]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Info("[INFO : Event in Remove Mode ]" + $" | {watch.ElapsedMilliseconds} ms");
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured while right click on scheduled event  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
        }
        /// <summary>
        /// Remove Events Called On click of Ok Popup Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void onRemove(object sender, WindowClosedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                var result = e.DialogResult;
                if (result == true)
                {
                    busyIndicatorGrid.IsBusy = true;
                    await DeleteEventSchedule(Convert.ToInt32(eventTriggerId));
                    await BindEventsGrid();
                    eventTriggerId = 0;
                    busyIndicatorGrid.IsBusy = false;
                }
            }

            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Unable to remove event because :  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
        }
        /// <summary>
        /// This event fires on selection changed of DateTime Picker. this event used to validate Schedule 
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void txtTimeValue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                if (CheckAvilabiltyOfTimeSlots() && eventTriggerId == 0)
                {

                    RadWindow.Alert("Events already schedule on this timing");
                    txtTimeValue.SelectedValue = null;
                    txtTimeValue.Focus();
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured checking event is available ot not :  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }
        }
        /// <summary>
        /// This event fires on button click of back button . this event used to redirect the user on Home Screen.
        /// <paramref name="e"/>
        /// <paramref name="sender"/>
        /// <return>void</return>
        /// </summary>
        ///  <remarks>Code documentation and Review is pending</remarks>
        private void btnBacktoWI_Click(object sender, RoutedEventArgs e)
        {

            RadNavigationView radNaviogation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            radNaviogation.Content = new TemplateMaster();
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void drpEventType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                if (drpEventType.Text == "Time Based")
                {
                    drpOcuurences.IsEnabled = true;
                    txtTimeValue.IsEnabled = true;
                    txtConditon.IsEnabled = false;
                }
                else
                {
                    drpOcuurences.IsEnabled = false;
                    txtTimeValue.IsEnabled = false;
                    txtConditon.IsEnabled = true;
                }

                app_logger.appLogger.Info("[INFO : Event type selected from dropdown. ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong. Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR  : Error occured in EventType SelectionChanged :  " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
    }

    #region Properties
    public class WorkInstrionModel
    {
        public string uid { set; get; }
        public string templateName { set; get; }
    }
    public class Occurences
    {
        public string occuranceId { set; get; }
        public string occurrenceName { set; get; }
    }
    public class UsersList
    {
        public int uid { set; get; }
        public string userName { set; get; }
    }
    public class ScheduleEvent
    {
        public string eventTriggerId { get; set; }
        public string wiSopId { get; set; }
        public string eventType { get; set; }
        public string title { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string occurance { get; set; }
        public string machine { get; set; }
        public string assignee { get; set; }
        public string  condition { get; set; }
        public string externalEvents { get; set; }
        public bool status { set; get; }
        public string timeValue { get; set; }
        public int wiId { set; get; }

        public string sopNumber { get; set; }



    }
    #endregion
}

