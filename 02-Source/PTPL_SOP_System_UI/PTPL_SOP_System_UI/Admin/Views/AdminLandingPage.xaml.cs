﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    ///  Interaction Logic  AdminLanddingPage.xaml
    ///  Created By  :  Chetan 
    ///  Created on  : 10 March 2020
    ///  -----------------------------------------------------------------------
    ///  Modified By :  
    ///  Modified on : 
    ///  Purpose     : 
    ///  -------------------------------------------------------------------------
    ///  Modified By : 
    ///  Modified on : 
    ///  Purpose     :  
    /// </summary>

    #region Properties
    public class StringWrapper
        {
            public string SOP { get; set; }
            public string TemplateName { get; set; }
            public string EventName { get; set; }
            public string CreatedOn { get; set; }
            public string CreatedBy { get; set; }
            public string Status { get; set; }
            public string Action { get; set; }
        }
      #endregion

    public partial class AdminLandingPage : UserControl
    {

        #region Constructor
        public AdminLandingPage()
        {
            InitializeComponent();


            var wrappedData = from str in new List<string>() { "SOP-001", "Template-1", "E-001", "12/01/2020", "Completed", "Action" }
             select new StringWrapper { SOP = "SOP-1", TemplateName = "Template-1", EventName = "E-001", CreatedOn = "12/01/2020", CreatedBy = "Admin", Status = "Completed", Action="Action"};
             this.RadGridView_CreateTemplate.ItemsSource = wrappedData;
        }
        #endregion

        #region Methods
        /// <summary>
        /// THis Method used to Navigate the user on to admin landing page.
        /// </summary>
        /// <param></param>
        /// <returns>void</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void Create_Template(object sender, RoutedEventArgs e)
        {
            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            radNavigation.SelectedIndex = 2;
        }
        #endregion
    }
}
