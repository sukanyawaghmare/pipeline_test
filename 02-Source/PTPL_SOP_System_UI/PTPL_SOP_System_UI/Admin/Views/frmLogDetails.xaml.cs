﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Data;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using System.Collections.Generic;
using Telerik.Windows.Controls;
using System.Diagnostics;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    /// This frmLogDetails class file is having all the Logs related properties and methods/functions
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>   
    public partial class frmLogDetails : UserControl
    {

        public frmLogDetails()
        {
            InitializeComponent();
        }

        #region event
        /// <summary>
        /// This timer_Tick methodwhich will get invoked in every second will check logs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>   
        private void timer_Tick(object sender, EventArgs e)
        {
            var watch = Stopwatch.StartNew();
            List<LogDetails> logDetails = new List<LogDetails>();
            try
            {
               
                string directoryPath = ConfigurationManager.AppSettings["LogDir"].ToString();

                var directory = new DirectoryInfo(directoryPath);

                //var myFile = (from f in directory.GetFiles()
                //              orderby f.LastWriteTime descending
                //              select f).First();

                List<string> chkfile = new List<string>();
                var myFile = (from f in directory.GetFiles()
                              orderby f.LastWriteTime descending
                              select f).Distinct().Take(3).ToList();
              

                var createTime = myFile[0].CreationTime;

                foreach (var file in myFile)
                {
                    //if (file.CreationTime == createTime)
                    //{
                    //chkfile.Add(file.Name);

                    //if(chkfile.Contains())

                    //var recentFile = directory + "\\" + myFile;
                    var recentFile = directory + "\\" + file.Name;
                    using (var sr = File.OpenText(recentFile))
                    {
                        string line;

                        while ((line = sr.ReadLine()) != null)
                        {
                            //DataRow insertRows1 = insertDt1.NewRow();

                            var fields = line.Split('|');
                            if (fields.Length > 0)
                            {
                                logDetails.Add(new LogDetails
                                {
                                    date = fields[0].Trim(),
                                    log = fields[1].Trim(),
                                    loggerName = fields[2].Trim(),
                                    logMessage = fields[3].Trim(),
                                    executionTime = fields[4].Trim()

                                });
                            }
                        }
                    }
                }

                watch.Stop();
            }
            catch(Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Something went wrong.Please contact your administrator.");
                app_logger.appLogger.Error("[ERROR : " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");

            }


            try
            {
                LogDetails.ItemsSource = logDetails;
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : "  + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
        #endregion

        /// <summary>
        /// This UserControl_Loaded method which will invoked as soon as controls are loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {

                DispatcherTimer dispatcherTimer = new DispatcherTimer();
                dispatcherTimer.Tick += new EventHandler(timer_Tick);
                dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
                dispatcherTimer.Start();

            }
            
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Login Successfully and ]" + $" | {watch.ElapsedMilliseconds} ms" );
            }
        }

        /// <summary>
        /// This LogDetails_AutoGeneratingColumn which is generating automatic columns and accepting column name as property name
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code documentation and Review is pending</remarks>  
        private void LogDetails_AutoGeneratingColumn(object sender, Telerik.Windows.Controls.GridViewAutoGeneratingColumnEventArgs e)
        {
            var dataColumn = e.Column as GridViewDataColumn;
            var watch = Stopwatch.StartNew();
            try
            {

                if (dataColumn != null)
                {

                    if (dataColumn.UniqueName == "date" || dataColumn.UniqueName == "log" )
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = dataColumn.Header.ToString() == "date" ? "Date" : "Logs";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }
                    if (dataColumn.UniqueName == "loggerName" || dataColumn.UniqueName == "logMessage")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = dataColumn.Header.ToString() == "loggerName" ? "Logger Name" : "Log Message";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    if (dataColumn.UniqueName == "executionTime" )
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "Execution Time";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }
                }

                watch.Stop();
            }
            
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR :" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }
    }

    #region prop
    public class LogDetails
    {
        public string date { get; set; }
        public string log { get; set; }

        public string  loggerName { get; set; }

        public string  logMessage { get; set; }

        public string executionTime { get; set; }

    }
    #endregion
}
