﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace PTPL_SOP_System_UI.Admin.Views
{
    /// <summary>
    /// Interaction logic for frmUserMaster.xaml
    /// </summary>
    ///    
    public partial class frmUserMaster : UserControl
    {
        
        static string apiUrl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        UserData _userData;
       


        /// <summary>
        /// This is user master method which will initialized all components
        /// </summary>
        public frmUserMaster()
        {
            var watch = Stopwatch.StartNew();
            InitializeComponent();
            _userData = Application.Current.Properties["userData"] as UserData;
            watch.Stop();
            app_logger.appLogger.Info("[INFO : User page initialized]" + $" | {watch.ElapsedMilliseconds} ms");

        }
        #region Events
        /// <summary>
        /// This is method will execute when user clicked on add new user button on Page
        /// </summary>
        /// <param name="sender"></param>
        /// Sender that contains a reference to the control/object that raised the event.
        /// <param name="e"></param>
        /// Trigger an event when it occurs
        /// <exception cref="is required box is not updated"></exception>
        /// <remarks>code documentation and review is pending</remarks>
        private void AddUser_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                txtUserName.Text = "";
                txtFirstName.Text = "";
                txtLastName.Text = "";
                drpStatus.SelectedIndex = -1;
                drpRole.SelectedIndex = -1;
                txtEmail.Text = "";
                txtContact.Text = "";
                txtLocation.Text = "";
                txtPassword.Password = "";
                sp_UserGrid.Visibility = Visibility.Collapsed;
                sp_CreateUser.Visibility = Visibility.Visible;
                sp_CreateUser1.Visibility = Visibility.Visible;
                btnCreate.Content = "Create";
            }
            catch (Exception ex)
            {
                watch.Stop();
                //app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occured while loading the Add User page " + ex.Message + "]");
                app_logger.appLogger.Error("[ERROR : Error occured while loading the Add User page " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }

        /// <summary>
        /// This this is cancel button functionality where if cancel clicked then we redirecting back to master grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref=""></exception>
        /// <remarks>code documentation and review is pending</remarks>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            sp_CreateUser.Visibility = Visibility.Collapsed;
            sp_CreateUser1.Visibility = Visibility.Collapsed;
            sp_UserGrid.Visibility = Visibility.Visible;

            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            radNavigation.Content = new frmUserMaster();
        }
        /// <summary>
        /// This method will execute while creating new user or updating new user. We are checking if it is Edit or Create.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Required fields"></exception>
        /// <exception cref="save API connection"></exception>
        /// <remarks>Code documentation and review is pending</remarks>
        private async void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                watch.Start();
                if (btnCreate.Content.ToString() == "Update")
                {

                    if (string.IsNullOrEmpty(txtUserName.Text))
                    {
                        RadWindow.Alert("User Name should not be empty");
                        txtUserName.Focus();
                        return;
                    }
                    else if (!Regex.IsMatch(txtUserName.Text, @"^[^\s]+$"))
                    {
                        txtUserName_block1.Text = "Spaces are not allowed!";
                        txtUserName.Focus();
                        return;
                    }
                    else
                    {
                        txtUserName_block1.Text = "";
                    }

                    if (string.IsNullOrEmpty(txtFirstName.Text))
                    {
                        RadWindow.Alert("First Name should not be empty");
                        txtFirstName.Focus();
                        return;
                    }


                    if (string.IsNullOrEmpty(txtLastName.Text))
                    {
                        RadWindow.Alert("Last Name should not be empty");
                        txtLastName.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtEmail.Text))
                    {
                        RadWindow.Alert("Email ID should not be empty");
                        txtEmail.Focus();
                        return;
                    }
                    else if (!Regex.IsMatch(txtEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
                    {
                        txtEmail_block1.Text = "Please Enter Valid Email Id";
                        txtEmail.Select(0, txtEmail.Text.Length);
                        txtEmail.Focus();
                        return;
                    }


                    dynamic item = e.OriginalSource;
                    var selectedRow = this.gridUserMaster.SelectedItem as DataRow;
                    var selectedItem = selectedRow.ItemArray;
                    radBusyIndicator.IsBusy = true;
                    bool flag = await UdpateUserData(Convert.ToInt32(selectedItem[0].ToString()));
                    if (flag)
                    {
                        watch.Stop();
                        app_logger.appLogger.Info("[INFO : User updated Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
						RadWindow.Alert(new DialogParameters { Header = "Success", Content = "User updated Successfully" });
                        await BindUserMasterGrid();
                        sp_CreateUser.Visibility = Visibility.Collapsed;
                        sp_CreateUser1.Visibility = Visibility.Collapsed;
                        sp_UserGrid.Visibility = Visibility.Visible;

                    }
                    else
                    {
                        watch.Stop();
                        app_logger.appLogger.Error("[ERROR : Failed to add user]" + $" | {watch.ElapsedMilliseconds} ms");
                        RadWindow.Alert("Failed to add user");
                    }
                    radBusyIndicator.IsBusy = false;
                }

                else if (btnCreate.Content.ToString() == "Create")
                {
                    watch.Start();
                    if (string.IsNullOrEmpty(txtUserName.Text))
                    {
                        RadWindow.Alert("Please Enter User Name");
                        txtUserName.Focus();
                        return;
                    }
                    else if (!Regex.IsMatch(txtUserName.Text, @"^[^\s]+$"))
                    {
                        txtUserName_block1.Text = "Spaces are not allowed!";
                        txtUserName.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtFirstName.Text))
                    {
                        RadWindow.Alert("Please Enter First Name");
                        txtFirstName.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtLastName.Text))
                    {
                        RadWindow.Alert("Please Enter Last Name");
                        txtContact.Focus();
                        return;
                    }

                    if (drpStatus.SelectedIndex == -1)
                    {
                        RadWindow.Alert("Please select Status");
                        drpStatus.Focus();
                        return;
                    }

                    if (drpRole.SelectedIndex == -1)
                    {
                        RadWindow.Alert("Please Select Role");
                        drpRole.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtEmail.Text))
                    {
                        RadWindow.Alert("Please Enter Email");
                        txtEmail.Focus();
                        return;
                    }
                    else if (!Regex.IsMatch(txtEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
                    {
                        txtEmail_block1.Text = "Please Enter Valid Email Id";
                        txtEmail.Select(0, txtEmail.Text.Length);
                        txtEmail.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(txtPassword.Password))
                    {
                        RadWindow.Alert("Please set the password for first time");
                        txtContact.Focus();
                        return;
                    }
                    radBusyIndicator.IsBusy = true;

                    bool flag = await SaveUser();

                    if (flag)
                    {

                       CreateProfilePicture();
                        watch.Stop();
                        app_logger.appLogger.Info("[INFO : User Added Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
						RadWindow.Alert(new DialogParameters { Header = "Success", Content = "User Added Successfully" });
                        await BindUserMasterGrid();
                        sp_CreateUser.Visibility = Visibility.Collapsed;
                        sp_CreateUser1.Visibility = Visibility.Collapsed;
                        sp_UserGrid.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        watch.Stop();
                        app_logger.appLogger.Error("[ERROR : " + txtUserName.Text + "user name already exist ]" + $" | {watch.ElapsedMilliseconds} ms");
                        RadWindow.Alert(txtUserName.Text + " user name already exist");
                    }
                    radBusyIndicator.IsBusy = false; ;
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                RadWindow.Alert("Error occured while updating or creating the user. Please check with Administrator");
                app_logger.appLogger.Error("[ERROR : Error occured while updating or creating the user" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// This is will execute as soon as Admin will create new user and once admin fill all mandatory fields then we are calling AddUpdateUser API 
        /// </summary>
        /// <param name="xxxxxxxDTO"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">failed to call API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> SaveUser()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                UserMasterModel userMasterModel = new UserMasterModel();

                userMasterModel.uid= 0;
                userMasterModel.userName = txtUserName.Text;
                userMasterModel.firstName = txtFirstName.Text;
                userMasterModel.lastName = txtLastName.Text;
                userMasterModel.role = (int)drpRole.SelectedIndex+1;
                //userMasterModel.role = 2;
                userMasterModel.email = txtEmail.Text;
                userMasterModel.contactNumber = txtContact.Text;
                userMasterModel.location = txtLocation.Text;
                //userMasterModel.isActive = (bool)drpStatus.SelectedValue;
                userMasterModel.isActive = ((System.Windows.Controls.ContentControl)drpStatus.SelectedItem).Content.ToString() == "Active";
                userMasterModel.profilePicture = null;
                //userMasterModel.password = RandomPassword(10,true);
                userMasterModel.password = txtPassword.Password;
                //userMasterModel.password = "Welcome@123";


                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/AddUpdateUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    //                    using (HttpResponseMessage response = client.PostAsJsonAsync(apiUrl, userModal).Result)
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiUrl, userMasterModel).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var resultData = JsonConvert.DeserializeObject<dynamic>(responseData);

                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[ERROR : Error occured" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                
            }
            return result;
        }


        /// <summary>
        /// This method using for Binding User Grid with help of GetAllUsers API. 
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">Failed to called GetAllUsers API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool>  BindUserMasterGrid()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {

                using (var client = new HttpClient())
                {
                    apiUrl = apiEndPoint + apiVersion + "/UserMaster/GetAllUsers";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {  
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo["usersData"];
                            List<UserMasterModel> gridResult = JsonConvert.DeserializeObject<List<UserMasterModel>>(finalList.ToString());
                            var fixedColumnList = new List<ColumnData>();
                            Telerik.Windows.Controls.GridViewColumnCollection columns = new Telerik.Windows.Controls.GridViewColumnCollection();
                            GridViewDataColumn gridViewDataColumn = new GridViewDataColumn();
                            DataTable dtMaster = new DataTable();
                            dtMaster.Columns.Add("uid");
                            dtMaster.Columns.Add("userName");
                            dtMaster.Columns.Add("password");
                            dtMaster.Columns.Add("firstName");
                            dtMaster.Columns.Add("lastName");
                            dtMaster.Columns.Add("role");
                            dtMaster.Columns.Add("email");
                            dtMaster.Columns.Add("contactNumber");
                            dtMaster.Columns.Add("location");
                            dtMaster.Columns.Add("isActive");
                            DataTemplate template;
                            gridUserMaster.Columns.AddRange(columns);

                            for (int i = 0; i < gridResult.Count; i++)
                            {
                                DataRow dr = dtMaster.NewRow();
                                dr["uid"] = gridResult[i].uid;
                                dr["userName"] = gridResult[i].userName;
                                dr["password"] = gridResult[i].password;
                                dr["firstName"] = gridResult[i].firstName;
                                dr["lastName"] = gridResult[i].lastName;
                                dr["email"] = gridResult[i].email;
                                dr["contactNumber"] = gridResult[i].contactNumber;
                                dr["location"] = gridResult[i].location;


                                if (gridResult[i].role.ToString() == "1")
                                {
                                    dr["role"] = "Admin";
                                }
                                else if (gridResult[i].role.ToString() == "2")
                                {
                                    dr["role"] = "Supervisor";
                                }
                                else if (gridResult[i].role.ToString() == "3")
                                {
                                    dr["role"] = "Operator";
                                }

                                if (gridResult[i].isActive)
                                {
                                    dr["isActive"] = "Active";
                                }
                                else
                                {
                                    dr["isActive"] = "Inactive";
                                }
                                dtMaster.Rows.Add(dr);
                            }
                            gridUserMaster.ItemsSource = dtMaster;
                            radDataPager.Source = dtMaster.ToEnumerable();
                            radDataPager.Visibility = Visibility.Visible;
                            radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = gridUserMaster });
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured loading the grid" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error occured loading the grid, Please try again");
                result = false;
            }
            return result;
        }

        /// <summary>
        /// this is user control loaded method will execute as soon as control is load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Error">Failed to load user control</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            radBusyIndicator.IsBusy = true;
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            await BindUserMasterGrid();
            radBusyIndicator.IsBusy = false; ;
        }

        /// <summary>
        /// This is Update User data method in which once invoke as soon as admin want to update user information which accepting uid i.e. user id as parameter.
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">API not found</exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        protected async Task<bool> UdpateUserData(int uid)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            apiUrl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/GetUserById?Id=" + uid;
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);


                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiUrl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var userModal = JsonConvert.DeserializeObject<UserMasterModel>(responseData);

                            userModal.uid = uid;
                            userModal.userName = txtUserName.Text;
                            userModal.firstName = txtFirstName.Text;
                            userModal.lastName = txtLastName.Text;
                            userModal.email = txtEmail.Text;
                            userModal.contactNumber = txtContact.Text;
                            userModal.location = txtLocation.Text;
                            //userModal.password = RandomPassword(10,true);
                            
                            userModal.role = (int)drpRole.SelectedIndex + 1;
                            userModal.isActive = ((System.Windows.Controls.ContentControl)drpStatus.SelectedItem).Content.ToString() == "Active";

                            PostUserData(userModal);
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured while calling updating API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Error occured while updating user. Please try again");
            }
            return result;

        }

        /// <summary>
        /// This is Random Password method, will invoke as soon as new user will create new user then we are sending Random password.
        /// </summary>
        /// <param name="size"></param>
        /// <param name="lowerCase"></param>
        /// <returns></returns
        /// <remarks>Code documentation and Review is pending</remarks>
        public string RandomPassword(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        /// <summary>
        /// This Post method for updating user information which is accepting as userModal  as parameter  accepting userModal as parameter
        /// </summary>
        /// <param name="userModal"></param>
        ///<returns>Success-StatusCode-200</returns>
        /// <exception cref="400">API calling failed</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void PostUserData(UserMasterModel userModal)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client1 = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/AddUpdateUser";
                    client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    var data = JsonConvert.SerializeObject(userModal);
                    using (HttpResponseMessage response1 = client1.PostAsJsonAsync(apiUrl, userModal).Result)
                    {
                        if (response1.IsSuccessStatusCode)
                        {
                            var responseData1 = response1.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured while calling AddUpdateUser API" + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong while updating user information.");
            }
        }


        /// <summary>
        /// This is delete user method, will invoke when admin right click on user and will say inactive then we are inactiviting user with help help of this method which is accepting uid
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">API calling failed</exception>
        /// <exception cref="uid not found"></exception>
        /// /// <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> DeleteUserData(int uid)
        {
            bool result = false;
            var watch = Stopwatch.StartNew();
            try
            {
                using (var client1 = new HttpClient())
                {
                    apiUrl = commonDeclaration.apiEndPoint + commonDeclaration.apiVersion + "/UserMaster/DeleteUser?userId=" + uid;
                    client1.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    //var data = JsonConvert.SerializeObject(userModal);
                    //using (HttpResponseMessage response1 = client1.PostAsJsonAsync(apiUrl, userModal).Result)
                    using (HttpResponseMessage response1 = await Task.Run(() => client1.GetAsync(apiUrl).Result))
                    {
                        if (response1.IsSuccessStatusCode)
                        {
                            var responseData1 = response1.Content.ReadAsStringAsync().Result;
                            var resultData1 = JsonConvert.DeserializeObject<dynamic>(responseData1);
                            result = true;
                        }
                        else
                        {
                            result = false;

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured while calling DeleteUserData API" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
                result = false;

            }
            return result;
        }


        /// <summary>
        /// This is method is used for binding user status. Is user active or Inactive
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        private string BindStatus(DataRow dr)
        {
            var selectedItem = dr.ItemArray;
            string bindvalue = null;
            var watch = Stopwatch.StartNew();
            try
            {
                if (selectedItem[9].ToString() == "Active")
                {
                    bindvalue += "Active";

                }
                else
                {
                    bindvalue += "Inactive";
                }
            }
            catch(Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured in BindStatus"+ ex.Message+" ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
            }

            return bindvalue;
        }
        /// <summary>
        /// This for binding the user role while clicked on edit.
        /// </summary>
        /// <param name="dr"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="status binding"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private string BindRole(DataRow dr)
        {
            var watch = Stopwatch.StartNew();
            var selectedItem = dr.ItemArray;

            string bindvalue = null;
            try
            {
                if (selectedItem[5].ToString() == "0")
                {
                    bindvalue += "Admin";

                }
                else if (selectedItem[5].ToString() == "1")
                {
                    bindvalue += "Supervisor";
                }
                else if (selectedItem[5].ToString() == "2")
                {
                    bindvalue += "Operator";
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured while binding the role" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");                
                RadWindow.Alert("Unable to bind the role. Please try again");
                
            }

            return bindvalue;
        }


        /// <summary>
        /// This method is will invoke when Admin right clicked on user. We are checking if user has clicked on Edit or Inactive.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        private void RadContextMenu_ItemClick(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            dynamic item = e.OriginalSource;
            var selectedRow = this.gridUserMaster.SelectedItem as DataRow;

            try
            {
                if (selectedRow == null)
                {
                    RadWindow.Alert("Please select user first");
                    return;
                }
                var selectedItem = selectedRow.ItemArray;

                if (item.Header == "Edit")
                {
                    HeaderText.Text = "Update Existing User";
                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        txtUserName.Focus();
                    }), System.Windows.Threading.DispatcherPriority.Render);

                    txtUserName.Text = selectedItem[1].ToString();
                    txtFirstName.Text = selectedItem[3].ToString();
                    txtLastName.Text = selectedItem[4].ToString();
                    drpStatus.Text = BindStatus(selectedRow);
                    drpRole.Text = selectedItem[5].ToString();
                    txtEmail.Text = selectedItem[6].ToString();
                    txtContact.Text = selectedItem[7].ToString();
                    txtLocation.Text = selectedItem[8].ToString();

                    sp_UserGrid.Visibility = Visibility.Collapsed;
                    txtPasswordPanel.Visibility = Visibility.Collapsed;
                    sp_CreateUser.Visibility = Visibility.Visible;
                    sp_CreateUser1.Visibility = Visibility.Visible;
                    btnCreate.Content = "Update";
                }
                else if (item.Header == "Inactive")
                {

                    if (BindStatus(selectedRow) == "Inactive")
                    {
                        RadWindow.Alert("User is already Inactive");
                    }
                    else
                    {
                        RadWindow.Confirm(new DialogParameters { CancelButtonContent = "No", OkButtonContent = "Yes", Closed = OnClosed, Content = "Do you want to Inactive user?", Header = "Inactive User" });
                    }
                }
            }

            catch(Exception ex)
            {
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occured " + ex.Message + "]");
                RadWindow.Alert("Unable to right click. Please check");
                
            }
            
        }
        /// <summary>
        /// This is OnClosed method which will invoke when admin right click on any Inactive user.
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="400"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>

        private async void OnClosed(object sender, WindowClosedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                var selectedRow = this.gridUserMaster.SelectedItem as DataRow;
                var selectedItem = selectedRow.ItemArray;
                var result = e.DialogResult;
                if (result == true)
                {
                    radBusyIndicator.IsBusy = true;
                    await DeleteUserData(Convert.ToInt32(selectedItem[0].ToString()));
                    await BindUserMasterGrid();
                    radBusyIndicator.IsBusy = false; ;
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured while inactiviting user" + ex.Message +" ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again.");
            }
        }

        /// <summary>
        /// This method is for hiding or showing or renaming header for autogenerat column.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <remarks>Code documentation and Review is pending</remarks>

        private void gridUserMaster_AutoGeneratingColumn(object sender, GridViewAutoGeneratingColumnEventArgs e)
        {
            var dataColumn = e.Column as GridViewDataColumn;
            var watch = Stopwatch.StartNew();
            try
            {

                if (dataColumn != null)
                {
                    if (dataColumn.UniqueName == "uid" || dataColumn.UniqueName == "password" || dataColumn.UniqueName == "location")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.IsVisible = false;
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    else if (dataColumn.UniqueName == "userName")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "User Name";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }
                    else if (dataColumn.UniqueName == "firstName")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "First Name";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    else if (dataColumn.UniqueName == "lastName")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "Last Name";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    else if (dataColumn.UniqueName == "email")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "Email Id";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    else if (dataColumn.UniqueName == "contactNumber")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "Contact Number";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    else if (dataColumn.UniqueName == "role")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "Role";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }

                    else if (dataColumn.UniqueName == "isActive")
                    {
                        GridViewDataColumn newColumn = new GridViewDataColumn();
                        newColumn.DataMemberBinding = dataColumn.DataMemberBinding;
                        newColumn.Header = "Status";
                        newColumn.UniqueName = dataColumn.UniqueName;
                        e.Column = newColumn;
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to set the column header" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }


        /// <summary>
        /// This method is used for creating new user default profile pic as first letter of first name and first letter of lastname in upper case
        /// </summary>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="textColor"></param>
        /// <param name="backColor"></param>
        /// <param name="filename"></param>
        /// <returns>default image</returns>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private System.Drawing.Image GenerateAvtarImage(String text, Font font, Color textColor, Color backColor, string filename)
        {
            var watch = Stopwatch.StartNew();
            //first, create a dummy bitmap just to get a graphics object  
            System.Drawing.Image img = new Bitmap(1, 1);
            try
            {
                Graphics drawing = Graphics.FromImage(img);

                //measure the string to see how big the image needs to be  
                SizeF textSize = drawing.MeasureString(text, font);

                //free up the dummy image and old graphics object  
                img.Dispose();
                drawing.Dispose();

                //create a new image of the right size  
                img = new Bitmap(90, 90);

                drawing = Graphics.FromImage(img);

                //paint the background  
                drawing.Clear(backColor);

                //create a brush for the text  
                Brush textBrush = new SolidBrush(textColor);

                //drawing.DrawString(text, font, textBrush, 0, 0);  
                drawing.DrawString(text, font, textBrush, new Rectangle(-2, 20, 200, 110));

                drawing.Save();

                textBrush.Dispose();
                drawing.Dispose();

                //img.Save(@"pack://application:,,,/Assests/Images/" + filename + ".png");

                //img.Save("D:/VisiSOP/Dev/LeanLogic - VisiSOP\02 - Source/PTPL_SOP_System_UI/PTPL_SOP_System_UI/Assests/Images/" + filename + ".png");
                img.Save(System.AppDomain.CurrentDomain.BaseDirectory + "\\images\\" + filename.ToLower() + ".png");

            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Unable to create Image for user" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return img;

        }
        /// <summary>
        /// This is method is used for set first letter in bold, arial format and used to passed the first name, last , font, background color and username for creating profile pic
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        public void CreateProfilePicture()
        {
            var watch = Stopwatch.StartNew();
            try
            {
                Font font = new Font("Arial", 30.0f, System.Drawing.FontStyle.Bold);
                Color fontcolor = ColorTranslator.FromHtml("#FFF");
                Color bgcolor = ColorTranslator.FromHtml("#3F51B5");
                GenerateAvtarImage(txtFirstName.Text.Substring(0, 1).ToUpper() + txtLastName.Text.Substring(0, 1).ToUpper(), font, fontcolor, bgcolor, txtUserName.Text);
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR: Unable to send first letter or last letter" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
            }
        }

        /// <summary>
        /// Checking if focus is lost from Email textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if (Regex.IsMatch(txtEmail.Text, @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"))
            {
                txtEmail_block1.Text = "";
            }
           
        }

    }
    #endregion

    #region Properties
    /// <summary>
    /// This UserMasterModel class file is having all the  properties  whcih required to get the data
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>
    public class UserMasterModel
    {
        public int uid { get; set; }
        
        public string userName { get; set; }
        
        public string password { get; set; }

        public string firstName { get; set; }
        
        public int? role { get; set; }
        public string lastName { get; set; }
        
        public byte[] profilePicture { get; set; }

        public bool isActive { get; set; }

        public string email { get; set; }

        public string contactNumber { get; set; }

        public string location { get; set; }
            }

    #endregion
}


