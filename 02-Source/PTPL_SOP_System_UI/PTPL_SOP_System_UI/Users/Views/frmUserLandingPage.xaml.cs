﻿using Newtonsoft.Json;
using PTPL_SOP_System_UI.Admin.ViewModel;
using PTPL_SOP_System_UI.Admin.Views;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Data;

namespace PTPL_SOP_System_UI.Users.Views
{
    /// <summary>
    ///  Interaction logic for frmUserLandingPage.xaml
    ///  Created By :  Chetan 
    ///  Created on : 12 March 2019
    ///  --------------------------------------------------------------------
    ///  Modified By : Chetan
    ///  Modified on : 1 April 2019
    ///  Purpose: Adding Validataions & Grid Header Changes as per requirements
    ///  --------------------------------------------------------------------------
    ///   Modified By : 
    ///   Modified on : 
    ///   Purpose:
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>   

    public partial class frmUserLandingPage : UserControl
    {
        #region Variables
        /// <summary>
        ///  data table is use to hold result of the Business class return type.
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>

        public string SOP { get; set; }
        public string TemplateName { get; set; }
        public string EventName { get; set; }
        public string CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public string Action { get; set; }
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        UserData _userData;
        #endregion

        #region Constructor
        public frmUserLandingPage()
        {
            Application.Current.Properties["Home"] = "";
            _userData = Application.Current.Properties["userData"] as UserData;
            InitializeComponent();
        }
        #endregion

        #region Methods
        /// <summary>
        /// This Methods used to bind the DB data to grid
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400">Uanble to call the API</exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async Task<bool> bindSOPDocumentData()
        {
            bool result = false;
            var watch = Stopwatch.StartNew();

            try
            {

                using (var client = new HttpClient())
                {

                    apiurl = apiEndPoint + apiVersion + "/DocumentMaster/GetAllDocDetails";
                    // apiurl = "http://localhost:55661/api/v1.0/DocumentMaster/GetAllDocDetails";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiurl).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var doc = new DocumentViewModel();

                            //var resultData = JsonConvert.DeserializeObject<DocumentViewModel>(responseData);
                            List<DocumentViewModel> docdata = JsonConvert.DeserializeObject<List<DocumentViewModel>>(responseData);
                            RadGridView_CreateTemplate.ItemsSource = docdata;
                            //RadGridView_CreateTemplate.ItemsSource = new { Binding = docdata, ElementName = "radDataPager" };
                            radDataPager.Source = docdata.ToEnumerable();
                            radDataPager.Visibility = Visibility.Visible;
                            radDataPager.SetBinding(RadDataPager.SourceProperty, new Binding("Items") { Source = RadGridView_CreateTemplate });

                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Error occured in bindSOPDocumentData " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
            }
            return result;
        }
        #endregion

        #region 
        /// <summary>
        /// This event fires when the user control fully loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Connection problem"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private async void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var watch = Stopwatch.StartNew();
            try
            {
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                logDir = ConfigurationManager.AppSettings["LogDir"].ToString();
                var result = await bindSOPDocumentData();
                RadGridView_CreateTemplate.Visibility = Visibility.Visible;
                busyIndicator1.IsBusy = false;
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable to load the grid " + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again");
            }

        }
        /// <summary>
        /// This Event fires onclick of add logger button. and used to navigate the pages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Connection problem"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void RadToggleButton_Click(object sender, RoutedEventArgs e)
        {
            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
            
            radNavigation.Content = new frmCreateSop(0);
        }
        /// <summary>
        /// This event fires right click on grid. And used to perform grid operation 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <exception cref="Connection problem"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void RadMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

            dynamic item = e.OriginalSource;
            var watch = Stopwatch.StartNew();
            DocumentViewModel selectedRow = this.RadGridView_CreateTemplate.SelectedItem as DocumentViewModel;
            Application.Current.Properties["Home"] = "";
            try
            {
                if (selectedRow == null)
                {
                    RadWindow.Alert("Please select row to view");
                    return;
                }
                var selectedValue = Convert.ToInt32(selectedRow.sopDocId);
                var selectedStatus = selectedRow.status.ToString();
                if (item.Header == "View")
                {
                    Application.Current.Properties["createSOPId"] = selectedValue;
                    RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                    radNavigation.Content = new PTPL_SOP_System_UI.Users.Views.frmCreateSop(selectedValue);
                    Application.Current.Properties["loggerEditView"] = "View";
                   
                    radNavigation.Content = new frmCreateSop(Convert.ToInt32(selectedValue));
                    

                }
                else if (item.Header == "Edit")
                {
                    if (selectedStatus.ToString() != "Completed")
                    {
                        Application.Current.Properties["loggerEditView"] = "Edit";
                        if (_userData.roleId == "1")
                        {
                            Application.Current.Properties["createSOPId"] = selectedValue;
                           
                            RadNavigationView radNavigation = Application.Current.Properties["radNavigationView"] as RadNavigationView;
                            radNavigation.Content = new PTPL_SOP_System_UI.Users.Views.frmCreateSop(selectedValue);
                            radNavigation.Content = new frmCreateSop(Convert.ToInt32(selectedValue));
                        }
                        else
                        {
                            RadWindow.Alert("Please contact Administrator.");
                        }
                    }
                    else
                    {
                        RadWindow.Alert("Selected SOP Logger is already completed.,Contact Administrator.");
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR : Unable trigger the event RadMenuItem_Click" + ex.Message + " ]" + $" | {watch.ElapsedMilliseconds} ms");
                RadWindow.Alert("Something went wrong. Please try again.");
            }


        }

        #endregion

        private void RadGridView_CreateTemplate_RowLoaded(object sender, RowLoadedEventArgs e)
        {
            if (e.Row.DataContext is DocumentViewModel)
            {
                e.Row.Background = (e.Row.DataContext as DocumentViewModel).status== "In-progress" ? new SolidColorBrush(Colors.White) : (SolidColorBrush)(new BrushConverter().ConvertFrom("#dbf3fa")); ;

            }

        }
    }
}
