﻿using System.Windows.Controls;

namespace PTPL_SOP_System_UI.Users.Views
{

    /// <summary>
    /// Interaction logic for frmLoggerAttachments.xaml
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>    

    public partial class frmLoggerAttachments : UserControl
    {

        #region Constructor
        /// <summary>
        /// This is Logger attachment form.
        /// </summary>
        /// <remarks>Code documentation and Review is pending</remarks>
        public frmLoggerAttachments()
        {
            InitializeComponent();
        }
        #endregion
    }
}
