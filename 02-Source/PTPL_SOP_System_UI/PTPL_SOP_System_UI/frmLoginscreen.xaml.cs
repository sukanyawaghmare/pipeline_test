﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Configuration;
using System.Diagnostics;
using Newtonsoft.Json;
using PTPL_SOP_System_UI.CommonFunctions;
using Telerik.Windows.Controls;
using System.Windows.Input;

namespace PTPL_SOP_System_UI
{
    /// <summary>
    /// Interaction logic for frmLoginscreen.xaml
    /// </summary>
    public partial class frmLoginscreen : Window
    {



        #region Variables
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        HttpClient client = new HttpClient();

        #endregion



        #region Methods
        /// <summary>
        /// This method is use to validate username and password entered by user to login into application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code review is pending</remarks>
        private async Task<bool> authenticateUser(string username, string password)
        {
            bool result = false;
            apiurl = string.Empty;
            try
            {
                var loginModel = new
                {
                    userName = radMaskedTextUserName.Text,
                    password = radMaskedTextPassword.Password
                };
                using (var client = new HttpClient())
                {
                    apiurl = apiEndPoint + apiVersion + "/UserMaster/Authenticate";
                    using (HttpResponseMessage response = await Task.Run(() => client.PostAsJsonAsync(apiurl, loginModel).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var loginData = JsonConvert.DeserializeObject<UserData>(responseData);
                            Application.Current.Properties["userData"] = loginData as UserData;
                            Application.Current.Properties["createSOPId"] = 0;
                            result = true;
                        }
                        else
                        {
                            result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occured while reading data from app.config file.  " + ex.Message + "]");
                RadWindow.Alert("Error found : " + ex.Message);
            }
            return result;
        }
        public frmLoginscreen()
        {
            InitializeComponent();
        }
        #endregion



        #region Events
        /// <summary>
        /// This method is used load all controls in login window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code review is pending</remarks>
        private void frmLoginScreen_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
                apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
                logDir = ConfigurationManager.AppSettings["LogDir"].ToString();
                radMaskedTextUserName.Focus();
            }
            catch (Exception ex)
            {
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occured while reading data from app.config file.  " + ex.Message + "]");
            }
        }

        /// <summary>
        /// This method will invoke as soon as user click on log in button and will check all required parameter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>Code review is pending</remarks>
        private async void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            busyIndicator.IsBusy = true;
            try
            {
                var watch = Stopwatch.StartNew();
                if (radMaskedTextUserName.Text == string.Empty)
                {
                    app_logger.appLogger.Info("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[INFO : Username was blank " + "]");
                    RadWindow.Alert("Please enter username.");
                    radMaskedTextUserName.Focus();
                    busyIndicator.IsBusy = false;
                    return;
                }
                if (radMaskedTextPassword.Password == string.Empty)
                {
                    app_logger.appLogger.Info("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[INFO : Password field was blank " + "]");
                    RadWindow.Alert("Please enter password.");
                    radMaskedTextPassword.Focus();
                    busyIndicator.IsBusy = false;
                    return;
                }

                var result = await authenticateUser(radMaskedTextUserName.Text, radMaskedTextPassword.Password);

                if (result == true)
                {
                    app_logger.CreateLogger(radMaskedTextUserName.Text);

                    watch.Stop();
					app_logger.appLogger.Error("[ERROR : No error in login ]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Trace("[TRACE : Login Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");
                    app_logger.appLogger.Info("[INFO : Login Successfully ]" + $" | {watch.ElapsedMilliseconds} ms");

                    this.Hide();
                    app_logger.appLogger.Info("[INFO : After login calling MainWindow ]" + $" | {watch.ElapsedMilliseconds} ms");
                    MainWindow mainhome = new MainWindow();
                    mainhome.Show();
                }
                else
                {
                    app_logger.appLogger.Error("[ERROR : Invalid username or password ]" + $" | {watch.ElapsedMilliseconds} ms");
                    RadWindow.Alert("Login failed: Invalid username or password.");
                    busyIndicator.IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                // app_logger.appLogger.Error("Login failed due to : " + ex.Message);
                app_logger.appLogger.Error("[" + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") + "] " + "[ERROR : Error occured while login" + ex.Message + "]");
                RadWindow.Alert("Login failed due to : " + ex.Message);
                busyIndicator.IsBusy = false;
            }
        }





        #endregion

        /// <summary>
        /// this method is for login the user if press Enter in Password boxitself.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radMaskedTextPassword_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if (e.Key != Key.Return && e.Key != Key.Enter)

            if (e.Key == Key.Enter)
            {
                btnLogin_Click(sender, e);
            }
        }
    }
}