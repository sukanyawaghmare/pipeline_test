﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL_SOP_System_UI.CommonFunctions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PTPL_SOP_System_UI.CommonFunctions
{
    public class GenericMethods
    {

        #region variables
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        static string logDir = string.Empty;
        static string LogDirTrace = string.Empty;
        UserData _userData;
        #endregion

        public GenericMethods()
        {
            apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
            apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
            logDir = Application.Current.Properties["logDir"].ToString();
            _userData = Application.Current.Properties["userData"] as UserData;
        }
        /// <summary>
        /// This Method Use to bind databse All Users data to UI Assinee combobox
        /// <return>bool</return>
        /// </summary>
        ///<remarks>Code documentation and Review is pending</remarks>
        public async Task<List<dropdownList>> BinddropdownList(string apiURL, string resultType)
        {
            var watch = Stopwatch.StartNew();
            //bool result = false;
            List<dropdownList> dropdownList = new List<dropdownList>();
            //apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    //apiurl = apiEndPoint + apiVersion + "/UserMaster/GetAssigneeUser";
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + _userData.token);
                    using (HttpResponseMessage response = await Task.Run(() => client.GetAsync(apiURL).Result))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            var finalList = jo[resultType];
                            dropdownList  = JsonConvert.DeserializeObject<List<dropdownList>>(finalList.ToString());
                            //drpAssinee.ItemsSource = deserializedList;
                            //result = true;
                            watch.Stop();
                            app_logger.appLogger.Trace("[TRACE : Data retrived and converted Successfully for : " + resultType + "]" + $" | {watch.ElapsedMilliseconds} ms");
                        }
                        else
                        {
                            watch.Stop();
                            //result = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                watch.Stop();
                app_logger.appLogger.Error("[ERROR :Error occured while Binding data into dropdownlist. " + ex.Message + "]" + $" | {watch.ElapsedMilliseconds} ms");
            }
            return await Task.Run(() => dropdownList);
        }
    }




    public class dropdownList
    {
        public int valueMember { set; get; }
        public string displayMember { set; get; }
    }
}
