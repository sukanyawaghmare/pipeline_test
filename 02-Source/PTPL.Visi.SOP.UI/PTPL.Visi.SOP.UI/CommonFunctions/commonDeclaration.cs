﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI
{
  public static  class commonDeclaration
    {
        /// <summary>
        ///  class for sharing methods with common methods
        ///  Created By  :  Chetan 
        ///  Created on  : 10 March 2020
        ///  -----------------------------------------------------------------------
        ///  Modified By :  
        ///  Modified on : 
        ///  Purpose     : 
        ///  -------------------------------------------------------------------------
        ///  Modified By : 
        ///  Modified on : 
        ///  Purpose     :  
        /// </summary>

        #region variables
        public static string apiEndPoint = ConfigurationManager.AppSettings["apiEndPoint"].ToString();
        public static string apiVersion = ConfigurationManager.AppSettings["apiVersion"].ToString();
        public static string timeZone = string.Empty;
        public static string applicationStartupPath = string.Empty;
        #endregion
        #region Methods
        /// <summary>
        /// THis Method used to convet date from UTC to local date
        /// </summary>
        /// <param></param>
        /// <returns>void</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        public static string GetConvertedDate(string inputDate,string timeZone)
        {
            string convertedDate = "";
            try
            {
               if(inputDate=="")
                {
                    return "";
                }
                DateTime convertedDT = Convert.ToDateTime(inputDate);
                DateTime loclTimeZoneDate = TimeZoneInfo.ConvertTimeFromUtc(convertedDT, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
                convertedDate= loclTimeZoneDate.ToString("dd MMMM yyyy h:mm tt");
              
            }
            catch(Exception ex)
            {
                throw new Exception("Error occuerd in common files."); 
            }
            return convertedDate;

        }
        /// <summary>
        /// THis Method used to convert estimation time to end date
        /// </summary>
        /// <param></param>
        /// <returns>void</returns>
        /// <remarks>Code documentation and Review is pending</remarks>
        public static DateTime calculateEstimationtime(DateTime startDate, string estimate)
        {
            DateTime returnValue = startDate;
            double weeks = 0, addweeks = 0, days = 0, hours = 0, min = 0;
            try
            {
                string[] eList = estimate.Split(' ');
                foreach (var element in eList)
                {
                    if (element.Contains("w"))
                    {
                        weeks = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        addweeks = 7 * weeks;
                        returnValue = returnValue.AddDays(addweeks);
                    }
                    if (element.Contains("d"))
                    {
                        days = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddDays(days);
                    }
                    if (element.Contains("h"))
                    {
                        hours = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddHours(hours);
                    }
                    if (element.Contains("m"))
                    {
                        min = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddMinutes(min);
                    }
                }



                Console.WriteLine(returnValue);



            }
            catch (Exception ex)
            {
                // log you error message here
            }
            return returnValue;
        }
        #endregion
    }
}
