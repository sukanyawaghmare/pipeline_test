﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL_SOP_System_UI.CommonFunctions
{
    public class UserData
    {

        public string id { set; get; }
        public string username { set; get; }
        public string firstName { set; get; }
        public string lastName { set; get; }
        public string roleId { set; get; }
        public string token { set; get; }
        public string systemTimeZone { set; get; }
    
    }
}
