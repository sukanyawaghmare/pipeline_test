﻿using PTPL_SOP_System_UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace PTPL.Visi.SOP.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    
    public partial class MainWindow : Window
    {

     

        public MainWindow()
        {
            InitializeComponent();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                commonDeclaration.applicationStartupPath = System.IO.Directory.GetCurrentDirectory();
                //imgLoginImage.ImageSource = Path.Combine(outPutDirectory, "Images\\logo.png");
                string filePath = commonDeclaration.applicationStartupPath + "\\Images\\logo.png";
                BitmapImage bi = new BitmapImage(new Uri(filePath));
                img_headerLogo.Source = bi;
            }
            catch (Exception ex)
            {
                RadWindow.Alert(ex.Message);
            }
        }

        private void RadMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            try
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Fluent;component/Themes/System.Windows.xaml", UriKind.RelativeOrAbsolute)
                });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Fluent;component/Themes/Telerik.Windows.Controls.xaml", UriKind.RelativeOrAbsolute)
                });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Fluent;component/Themes/Telerik.Windows.Controls.Input.xaml", UriKind.RelativeOrAbsolute)
                });

                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Fluent;component/Themes/Telerik.Windows.Controls.GridView.xaml", UriKind.RelativeOrAbsolute)
                });

                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Fluent;component/Themes/Telerik.Windows.Controls.Navigation.xaml", UriKind.RelativeOrAbsolute)
                });
            }
            catch (Exception ex)
            {
                RadWindow.Alert(ex.Message);
            }
        }

        private void RadMenuItem_Click_1(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            Application.Current.Resources.MergedDictionaries.Clear();
            try
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Crystal;component/Themes/System.Windows.xaml", UriKind.RelativeOrAbsolute)
                });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Crystal;component/Themes/Telerik.Windows.Controls.xaml", UriKind.RelativeOrAbsolute)
                });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Crystal;component/Themes/Telerik.Windows.Controls.Input.xaml", UriKind.RelativeOrAbsolute)
                });

                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Crystal;component/Themes/Telerik.Windows.Controls.GridView.xaml", UriKind.RelativeOrAbsolute)
                });

                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary()
                {
                    Source = new Uri("/Telerik.Windows.Themes.Crystal;component/Themes/Telerik.Windows.Controls.Navigation.xaml", UriKind.RelativeOrAbsolute)
                });
            }
            catch (Exception ex)
            {
                RadWindow.Alert(ex.Message);
            }
        }

        private void btnTheme_Click(object sender, RoutedEventArgs e)
        {
            if (btnTheme.IsChecked == false)
            {

                FluentPalette.LoadPreset(FluentPalette.ColorVariation.Light);
                CrystalPalette.LoadPreset(CrystalPalette.ColorVariation.Light);
                btnTheme.Foreground = Brushes.Black;

            }
            if (btnTheme.IsChecked == true)
            {
                FluentPalette.LoadPreset(FluentPalette.ColorVariation.Dark);
                CrystalPalette.LoadPreset(CrystalPalette.ColorVariation.Dark);
                btnTheme.Foreground = Brushes.Black;
            }
        }
    }
}
