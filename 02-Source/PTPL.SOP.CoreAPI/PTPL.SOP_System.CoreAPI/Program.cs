using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Reflection;

namespace PTPL.SOP_System.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
           WebHost.CreateDefaultBuilder(args)
               .UseStartup<Startup>();
    }
}

    //public static IHostBuilder CreateHostBuilder(string[] args) =>
    //    Host.CreateDefaultBuilder(args)
    //        .ConfigureWebHostDefaults(webBuilder =>
    //        {
    //            webBuilder.ConfigureKestrel(serverOptions =>
    //            {
    //                // Set properties and call methods on options
    //            })
    //            .UseStartup<Startup>();
    //        });

    //public static IHostBuilder CreateHostBuilder(string[] args) =>
    //        Host.CreateDefaultBuilder(args)
    //          .UseServiceProviderFactory(new AutofacServiceProviderFactory())
    //          .ConfigureWebHostDefaults(webBuilder =>
    //          {
    //              webBuilder.UseStartup<Startup>();
    //          });


