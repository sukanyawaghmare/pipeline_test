﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the EventTriggerSchedule master (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>

    // [EnableCors("VisiPolicy")]
    // [MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/EventTriggerSchedule/")]
    [ApiController]
    //[Authorize]
    public class EventTriggerScheduleController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IEventTriggerScheduleServices _iEventTriggerScheduleService;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        int userId;
        int roleId;
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventTriggerScheduleServices" /> class.
        /// </summary>
        /// <param name="iEventTriggerScheduleService">The iEventTriggerScheduleService<see cref="IEventTriggerScheduleService" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public EventTriggerScheduleController(IEventTriggerScheduleServices iEventTriggerScheduleService, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            //userId = int.Parse(httpContextAccessor.HttpContext.User.Identity.Name);
            // roleId = int.Parse((new List<System.Security.Claims.Claim>(httpContextAccessor.HttpContext.User.Claims))[1].Value);
            _iEventTriggerScheduleService = iEventTriggerScheduleService;
            _mapper = mapper;
        }
        #endregion constructor

        #region Method
        /// <summary>
        /// This API ActionMethod use to insert or update the EventTriggerSchedule master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateEventTriggerSchedule")]

        public async Task<IActionResult> AddUpdateEventTriggerSchedule(EventTriggerScheduleDTO eventTriggerSchedule)
        {
            try
            {
                if (!ModelState.IsValid || eventTriggerSchedule == null)
                {
                    return BadRequest(ModelState);
                }
                var eventTSchedule = _mapper.Map<PtplEventTriggerSchedule>(eventTriggerSchedule);
                var result = _iEventTriggerScheduleService.addupdateEventTriggerSchedule(eventTSchedule);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get All EventTriggerSchedule List
        /// </summary>
        /// <returns>Returns List of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllEventTriggerSchedule")]

        public async Task<IActionResult> GetAllEventTriggerSchedule()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iEventTriggerScheduleService.getAllEventTriggerSchedule();
                var modal = new
                {
                    EventTriggerScheduleData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get All EventTriggerSchedule List
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Returns List of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllEventTriggerScheduleByWIId")]

        public async Task<IActionResult> GetAllEventTriggerScheduleByWIId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iEventTriggerScheduleService.getAllEventTriggerScheduleByWIId(Id);
                var modal = new
                {
                    EventTriggerScheduleData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }


        /// <summary>
        /// This API ActionMethod use to Get All EventTriggerSchedule List
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Returns List of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("getAllEventTriggerScheduleWithoutPropertyNameByWIId")]

        public async Task<IActionResult> getAllEventTriggerScheduleWithoutPropertyNameByWIId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iEventTriggerScheduleService.getAllEventTriggerScheduleWithoutPropertyNameByWIId(Id);
                var modal = new
                {
                    EventTriggerScheduleData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete EventTriggerSchedule master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteEventTriggerSchedule")]
        public async Task<IActionResult> DeleteEventTriggerSchedule(int Id)
        {
            try
            {
                if (!ModelState.IsValid || Id == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _iEventTriggerScheduleService.deleteEventTriggerSchedule(Id);
                errorInfo.Id = result.Id;
                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All EventTriggerSchedule and Template List
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Returns List of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetEventTriScheTemplateByWIId")]

        public async Task<IActionResult> GetEventTriScheTemplateByWIId(int WId,int EventId)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iEventTriggerScheduleService.getEventTriScheTemplateByWIId(WId,EventId);
                var modal = new
                {
                    EventTriggerScheduleData = result.Result,
                    message = "Data inserted successfully",
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Method
    }
}