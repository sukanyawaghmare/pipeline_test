﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the Document details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>

    // [EnableCors("VisiPolicy")]
    // [MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/DocumentMaster/")]
    [ApiController]
    
    [Authorize]
    public class DocumentMasterController : ControllerBase
    {

        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IDocumentServices _idocumentService;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        int userId;
        int roleId;
        private object httpContextAccessor;
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="DocumentMasterController" /> class.
        /// </summary>
        /// <param name="idocumentServices">The idocumentServices<see cref="IDocumentServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public DocumentMasterController(IDocumentServices idocumentServices, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            userId =  int.Parse(httpContextAccessor.HttpContext.User.Identity.Name);
            roleId =  int.Parse((new List<System.Security.Claims.Claim>(httpContextAccessor.HttpContext.User.Claims))[1].Value);
            _idocumentService = idocumentServices;
            _mapper = mapper;
        }
        #endregion constructor

        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert or update the Document master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateDocument")]
        

        public async Task<IActionResult> AddUpdateDocument([FromBody] PtplSopLogger documentMaster)
        {
            try
            {
                if (!ModelState.IsValid || documentMaster == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _idocumentService.addupdateDocument(documentMaster);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }
       

        /// <summary>
        /// This API ActionMethod use to Get All Document Details
        /// </summary>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllDocDetails")]


        public async Task<IActionResult> GetAllDocDetails()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _idocumentService.getAllDocDetails(userId,roleId);
                var modal = new
                {
                    documentsData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((result.Result)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }




        /// <summary>
        /// This API ActionMethod use to Delete Document master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteDocument")]
        public async Task<IActionResult> DeleteDocument(int DocumentId)
        {
            try
            {
                if (!ModelState.IsValid || DocumentId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _idocumentService.deleteDocument(DocumentId);
                errorInfo.Id = result.Id;
                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All Document List
        /// </summary> 
        /// <returns>Returns List of documents</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllDocuments")]

        public async Task<IActionResult> GetAllDocuments()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _idocumentService.getAllDocuments();
                var modal = new
                {
                    documentsData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get Document by Id
        /// </summary>
        /// <returns>Returns document by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetDocumentById")]

        public async Task<IActionResult> GetDocumentById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _idocumentService.getDocumentById(Id);
                var modal = new
                {
                    documentsData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }


        /// <summary>
        /// This API ActionMethod use Update Sop logger Status
        /// </summary>
        /// <returns>Returns document by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("UpdateSoploggerStatus")]

        public async Task<IActionResult> UpdateSoploggerStatus(LoggerUpdateStatus loggerUpdateStatus)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
               
                var modal = new
                {
                  sopDocId = _idocumentService.updateSoploggerStatus(loggerUpdateStatus).Result,
                  statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use Get Sop logger By userId and Role
        /// </summary>
        /// <returns>Returns List of logger</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetSopLoggerByUser")]

        public async Task<IActionResult> GetSopLoggerByUser()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var modal = new
                {
                    sopLogger = _idocumentService.getSopLoggerByUser(userId,roleId).Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }


        #endregion Methods
    }
}