﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the LoggerAttachment (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>

    // [EnableCors("VisiPolicy")]
    // [MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/loggerAttachmentMaster/")]
    [ApiController]
    //[Authorize]
    public class LoggerAttachmentController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly ILoggerAttachmentService _iloggerAttachmentService;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        int userId;
        int roleId;
        #endregion Fields
        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="" /> class.
        /// </summary>
        /// <param name="iloggerAttachmentService">The iloggerAttachmentService<see cref="LoggerAttachment" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public LoggerAttachmentController(ILoggerAttachmentService iloggerAttachmentService, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            //userId = int.Parse(httpContextAccessor.HttpContext.User.Identity.Name);
            // roleId = int.Parse((new List<System.Security.Claims.Claim>(httpContextAccessor.HttpContext.User.Claims))[1].Value);
            _iloggerAttachmentService = iloggerAttachmentService;
            _mapper = mapper;
        }
        #endregion constructor

        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert or update the loggerAttachment master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateloggerAttachment")]

        public async Task<IActionResult> AddUpdateloggerAttachment([FromBody] List<PtplLoggerAttachment> ptplLoggerAttachments)
        {
            try
            {
                if (!ModelState.IsValid || ptplLoggerAttachments.Count()==0)
                {
                    return BadRequest(ModelState);
                }
                //var LoggerAttachments = _mapper.Map<PtplSoploggerAttachmentMaster>(loggerAttachmentMachine);
               // loggerAttachmentMaster.CreationDate = DateTime.Now;
                var result = _iloggerAttachmentService.addupdateLoggerAttachment(ptplLoggerAttachments);
                var modal = new
                {
                    attachementId = result.Result,
                    msg = "Data inserted successfully",
                    statusCode = HttpStatusCode.OK
                };
               

                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get All loggerAttachment List
        /// </summary>
        /// <returns>Returns List of loggerAttachment</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllloggerAttachments")]

        public async Task<IActionResult> GetAllloggerAttachments()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iloggerAttachmentService.getAllloggerAttachments();
                var modal = new
                {
                    loggerAttachmentsData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete loggerAttachment master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteloggerAttachment")]
        public async Task<IActionResult> DeleteloggerAttachment(int loggerAttachmentId)
        {
            try
            {
                if (!ModelState.IsValid || loggerAttachmentId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _iloggerAttachmentService.deleteloggerAttachment(loggerAttachmentId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }


        /// <summary> /// <summary>
        /// This API ActionMethod use to Delete loggerAttachment By Documentt Id
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteloggerAttachmentByDocId")]
        public async Task<IActionResult> DeleteloggerAttachmentByDocId(int Id)
        {
            try
            {
                if (!ModelState.IsValid || Id == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _iloggerAttachmentService.deleteloggerAttachmentByDocId(Id);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to get loggerAttachment by Id
        /// </summary>
        /// <returns>Returns loggerAttachment by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetloggerAttachmentById")]

        public async Task<IActionResult> GetloggerAttachmentById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _iloggerAttachmentService.getloggerAttachmentById(Id);
                var modal = new
                {
                    loggerAttachment = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to get loggerAttachment by Document Id
        /// </summary>
        /// <returns>Returns loggerAttachment by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetloggerAttachmentByDocId")]

        public async Task<IActionResult> GetloggerAttachmentByDocId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _iloggerAttachmentService.getloggerAttachmentByDocId(Id);
                var modal = new
                {
                    loggerAttachment = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Method
    }
}