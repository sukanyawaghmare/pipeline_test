﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the Event Trigger Master details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>


    // [MapToApiVersion("1.0")]
   // [EnableCors("VisiPolicy")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/EventTriggerMaster/")]
    [ApiController]
    // [Authorize]
    public class EventTriggerMasterController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IEventTriggerServices _ieventTriggerServices;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventTriggerMaster" /> class.
        /// </summary>
        /// <param name="ieventTriggerServices">The ieventTriggerServices<see cref="IEventTriggerServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>
        public EventTriggerMasterController(IEventTriggerServices ieventTriggerServices, IMapper mapper)
        {
            _ieventTriggerServices = ieventTriggerServices;
            _mapper = mapper;
        }
        #endregion constructor
        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert or update the EventTrigger master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateEventTrigger")]

        public async Task<IActionResult> AddUpdateEventTrigger([FromBody] PtplEventTriggerMaster EventTriggerMaster)
        {
            try
            {
                if (!ModelState.IsValid || EventTriggerMaster == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _ieventTriggerServices.addupdateEventTrigger(EventTriggerMaster);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete EventTrigger master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteEventTrigger")]
        public async Task<IActionResult> DeleteEventTrigger(int EventTriggerId)
        {
            try
            {
                if (!ModelState.IsValid || EventTriggerId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _ieventTriggerServices.deleteEventTrigger(EventTriggerId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All EventTrigger List
        /// </summary> 
        /// <returns>Returns List of event trigger</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllEventTriggers")]

        public async Task<IActionResult> GetAllEventTriggers()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _ieventTriggerServices.getAllEventTrigger();
                var modal = new
                {
                    eventTriggersData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get EventTrigger by Id
        /// </summary>
        /// <returns>Returns event trigger by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetEventTriggerById")]

        public async Task<IActionResult> GetEventTriggerById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _ieventTriggerServices.getEventTriggerById(Id);
                var modal = new
                {
                    eventTriggerData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Methods
    }
}