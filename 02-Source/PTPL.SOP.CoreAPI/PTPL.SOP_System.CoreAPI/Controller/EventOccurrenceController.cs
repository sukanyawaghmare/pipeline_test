﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the EventOccurrence master (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>

    // [EnableCors("VisiPolicy")]
    // [MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/EventOccurrence/")]
    [ApiController]
    //[Authorize]
    public class EventOccurrenceController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IEventOccurrenceServices _iEventOccurrenceService;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        int userId;
        int roleId;
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventOccurrenceServices" /> class.
        /// </summary>
        /// <param name="iEventOccurrenceService">The iEventOccurrenceService<see cref="IEventOccurrenceService" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public EventOccurrenceController(IEventOccurrenceServices iEventOccurrenceService, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            //userId = int.Parse(httpContextAccessor.HttpContext.User.Identity.Name);
            // roleId = int.Parse((new List<System.Security.Claims.Claim>(httpContextAccessor.HttpContext.User.Claims))[1].Value);
            _iEventOccurrenceService = iEventOccurrenceService;
            _mapper = mapper;
        }
        #endregion constructor

        #region Methods
        /// <summary>
        /// This API ActionMethod use to Get All EventOccurrence List
        /// </summary>
        /// <returns>Returns List of Template</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllEventOccurrence")]

        public async Task<IActionResult> GetAllEventOccurrence()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iEventOccurrenceService.getAllEventOccurrence();
                var modal = new
                {
                    eventOccurrenceData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Method
    }
}