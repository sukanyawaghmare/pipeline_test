﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the Equipment details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>


    // [MapToApiVersion("1.0")]
    //[EnableCors("VisiPolicy")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/EquipmentMaster/")]
    [ApiController]
    // [Authorize]
    public class EquipmentMasterController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IEquipmentServices _iequipmentServices;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="EquipmentMaster" /> class.
        /// </summary>
        /// <param name="iequipmentServices">The iequipmentServices<see cref="IEquipmentServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>
        public EquipmentMasterController(IEquipmentServices ieuipmentServices, IMapper mapper)
        {
            _iequipmentServices = ieuipmentServices;
            _mapper = mapper;
        }
        #endregion constructor
        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert or update the Equipment master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateEquipment")]

        public async Task<IActionResult> AddUpdateEquipment([FromBody] PtplEquipmentMaster EquipmentMaster)
        {
            try
            {
                if (!ModelState.IsValid || EquipmentMaster == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _iequipmentServices.addupdateEquipment(EquipmentMaster);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete Equipment master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteEquipment")]
        public async Task<IActionResult> DeleteEquipment(int EquipmentId)
        {
            try
            {
                if (!ModelState.IsValid || EquipmentId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _iequipmentServices.deleteEquipment(EquipmentId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All Equipment List
        /// </summary> 
        /// <returns>Returns List of equipments</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllEquipments")]

        public async Task<IActionResult> GetAllEquipments()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iequipmentServices.getAllEquipments();
                var modal = new
                {
                    equipmentsData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get Equipment by Id
        /// </summary>
        /// <returns>Returns equipment by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetEquipmentById")]

        public async Task<IActionResult> GetEquipmentById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iequipmentServices.getEquipmentById(Id);
                var modal = new
                {
                    euipmentData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Methods
    }
}