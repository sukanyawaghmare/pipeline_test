﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the template master (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>

    // [EnableCors("VisiPolicy")]
    // [MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/TemplateMaster/")]
    [ApiController]
     //[Authorize]
    public class TemplateMasterController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly ITemplateService _itemplateService;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        int userId;
        int roleId;
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="TemplateMaster" /> class.
        /// </summary>
        /// <param name="itemplateService">The itemplateService<see cref="ITemplateService" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public TemplateMasterController(ITemplateService itemplateService, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            //userId = int.Parse(httpContextAccessor.HttpContext.User.Identity.Name);
           // roleId = int.Parse((new List<System.Security.Claims.Claim>(httpContextAccessor.HttpContext.User.Claims))[1].Value);
            _itemplateService = itemplateService;
            _mapper = mapper;
        }
        #endregion constructor
        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert or update the template master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateTemplate")]

        public async Task<IActionResult> AddUpdateTemplate([FromBody] TemplateMachineDTO templateMachine)
        {
            try
            {
                if (!ModelState.IsValid || templateMachine == null)
                {
                    return BadRequest(ModelState);
                }
                var templateMaster = _mapper.Map<PtplSopWorkInstruction>(templateMachine);
                templateMaster.CreationDate = DateTime.Now;
                var result = _itemplateService.addupdateTemplate(templateMaster, templateMachine.machinIds);
                var modal = new
                {
                    Id=result.Result[0],
                    sOPWINumber=result.Result[1],
                    Message = "Data inserted successfully",
                statusCode = HttpStatusCode.OK,
                };
                
               
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;
               
                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete template master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteTemplate")]
        public async Task<IActionResult> DeleteTemplate(int TemplateId)
        {
            try
            {
                if (!ModelState.IsValid || TemplateId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _itemplateService.deleteTemplate(TemplateId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All Template List
        /// </summary>
        /// <returns>Returns List of Template</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllTemplates")]

        public async Task<IActionResult> GetAllTemplates()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _itemplateService.getAllTemplates();
                var modal = new
                {
                    TemplatesData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All Template with Machine List
        /// </summary>
        /// <returns>Returns all machine templates</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllMachineTemplate")]

        public async Task<IActionResult> GetAllMachineTemplate()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _itemplateService.getAllMachineTemplate();
                var modal = new
                {
                    templatesData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get sop number
        /// </summary>
        /// <returns>Returns sop number</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetDocumentByFileName")]

        public async Task<IActionResult> GetDocumentByFileName(string fileName)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getDocumentByFileName(fileName);
                var modal = new
                {
                    templatesData = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get All File Details
        /// </summary>
        /// <returns>Returns document by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetSopNoByEquipmentId")]

        public async Task<IActionResult> GetSopNoByEquipmentId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getSopNoByEquipmentId(Id);
                var modal = new
                {
                    equipmentName = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }


        
        /// <summary>
        /// This API ActionMethod use to Get All File Name
        /// </summary>
        /// <returns>Returns file name</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("getFileNameByEquipmentId")]

        public async Task<IActionResult> getFileNameByEquipmentId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getFileNameByEquipmentId(Id);
                var modal = new
                {
                    fileName = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        [HttpGet, Route("getFileStremeById")]

        public async Task<IActionResult> getFileStremeById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getFileStemeById(Id);
              
                return await Task.Run(() => Ok((result)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        [HttpGet, Route("GetLatestSopNo")]

        public async Task<IActionResult> GetLatestSopNo()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getLatestSopNo();

                return await Task.Run(() => Ok((result)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get All tamplate data
        /// </summary>
        /// <returns>Returns file name</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetTemplatesById")]

        public async Task<IActionResult> GetTemplatesById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getTemplatesById(Id);
                var modal = new
                {
                    templateData = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }


        /// <summary>
        /// This API ActionMethod use to Get All WI Flag
        /// </summary>
        /// <returns>Returns file name</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetWIFlag")]

        public async Task<IActionResult> GetWIFlag()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getWIFlag();
                var modal = new
                {
                    templateData = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        [HttpPost, Route("SaveCustomFormJson")]

        public async Task<IActionResult> SaveCustomFormJson([FromBody] PtplDepartmentMaster templateMachine)
        {
            try
            {
                if (!ModelState.IsValid || templateMachine == null)
                {
                    return BadRequest(ModelState);
                }
                
               
                var result = _itemplateService.saveFormJson(templateMachine);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }
        [HttpGet, Route("GetJsonFormData")]

        public async Task<IActionResult> GetJsonFormData()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = await _itemplateService.getJsonFormData();
                var modal = new
                {
                    templateData = result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Methods
    }
}