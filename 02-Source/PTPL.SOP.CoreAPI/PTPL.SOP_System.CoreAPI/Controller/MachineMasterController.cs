﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{

    /// <summary>
    /// This API controller use to  managing the Machine details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code Machineation and Review is pending </remarks>


    // [MapToApiVersion("1.0")]
    //[EnableCors("VisiPolicy")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/MachineMaster/")]
    [ApiController]
    //[Authorize]
    public class MachineMasterController : ControllerBase
    {

        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IMachineServices _imachineServices;

        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="MachineMaster" /> class.
        /// </summary>
        /// <param name="imachineServices">The imachineServices<see cref="IMachineServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public MachineMasterController(IMachineServices imachineServices, IMapper mapper)
        {
            _imachineServices = imachineServices;
            _mapper = mapper;
        }
        #endregion constructor
        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert or update the Machine master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code Machineation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateMachine")]

        public async Task<IActionResult> AddUpdateMachine([FromBody] PtplMachineMaster MachineMaster)
        {
            try
            {
                if (!ModelState.IsValid || MachineMaster == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _imachineServices.addupdateMachine(MachineMaster);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete Machine master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code Machineation and Review is pending </remarks>
        [HttpGet, Route("DeleteMachine")]
        public async Task<IActionResult> DeleteMachine(int MachineId)
        {
            try
            {
                if (!ModelState.IsValid || MachineId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _imachineServices.deleteMachine(MachineId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All Machine List
        /// </summary> 
        /// <returns>Returns List of machine</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code Machineation and Review is pending </remarks>
        [HttpGet, Route("GetAllMachines")]

        public async Task<IActionResult> GetAllMachines()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _imachineServices.getAllMachines();
                var modal = new
                {
                    machinesData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get Machine by Id
        /// </summary>
        /// <returns>Returns machine by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code Machineation and Review is pending </remarks>
        [HttpGet, Route("GetMachineById")]

        public async Task<IActionResult> GetMachineById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _imachineServices.getMachineById(Id);
                var modal = new
                {
                    machinesData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get Machine by Id
        /// </summary>
        /// <returns>Returns machine by areaid</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code Machineation and Review is pending </remarks>
        [HttpGet, Route("GetMachineByAreaId")]

        public async Task<IActionResult> GetMachineByAreaId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _imachineServices.getMachineByAreaId(Id);
                var modal = new
                {
                    machinesData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Methods
    }
}