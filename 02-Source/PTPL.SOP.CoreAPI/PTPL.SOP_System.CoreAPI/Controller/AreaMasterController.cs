﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the area details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>


    // [MapToApiVersion("1.0")]
    //[EnableCors("VisiPolicy")]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/AreaMaster/")]
    [ApiController]
    //[Authorize]
    public class AreaMasterController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _iAreaServices
        /// </summary>
        private readonly IAreaServices _iAreaServices;
       
        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="AreaMaster" /> class.
        /// </summary>
        /// <param name="iAreaServices">The iAreaServices<see cref="IAreaServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public AreaMasterController(IAreaServices iAreaServices)
        {
            _iAreaServices = iAreaServices;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// This API ActionMethod use to insert or update the Area master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateArea")]

        public async Task<IActionResult> AddUpdateArea([FromBody] PtplAreaMaster AreaMaster)
        {
            try
            {
                if (!ModelState.IsValid || AreaMaster == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _iAreaServices.addupdateArea(AreaMaster);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete Area master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpDelete, Route("DeleteArea")]
        public async Task<IActionResult> DeleteArea(int AreaId)
        {
            try
            {
                if (!ModelState.IsValid || AreaId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _iAreaServices.deleteArea(AreaId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All Area List
        /// </summary>
        /// <returns>Returns List of categories</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllAreas")]

        public async Task<IActionResult> GetAllCategories()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iAreaServices.getAllAreas();
                var modal = new
                {
                    AreasData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get Area by Id
        /// </summary>
        /// <returns>Returns Area by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAreaById")]

        public async Task<IActionResult> GetAreaById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iAreaServices.getAreaById(Id);
                var modal = new
                {
                    AreaData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Methods

    }
}