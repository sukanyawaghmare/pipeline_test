﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PTPL.SOP_System.Common;

namespace PTPL.SOP_System.CoreAPI.Controller
{

    /// <summary>
    /// This API controller use to  managing the Custom Form (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>


    // [MapToApiVersion("1.0")]
    // [EnableCors("VisiPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CustomeFormController : ControllerBase
    {
       static MongoClient client = new MongoClient();
       static IMongoDatabase db = client.GetDatabase("PTPL_SOP");
       static IMongoCollection<ptplSopWi> ptplSopWiCollection = db.GetCollection<ptplSopWi>("ptplSopWi");
       private ErrorInfo errorInfo = new ErrorInfo();
        /// <summary>
        /// This API ActionMethod use to insert or update the EventTrigger master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateDesignedForm")]

        public async Task<IActionResult> AddUpdateDesignedForm([FromBody] CustomFormDto customFormDto)
        {
            try
            {
                //if (!ModelState.IsValid || customFormDto == null)
                //{
                //    return BadRequest(ModelState);
                //}
                ptplSopWi ptplSopWi = new ptplSopWi(customFormDto.designedFormJson.ToString(),Convert.ToInt32(customFormDto.sopWorkInstructionID));
                ptplSopWiCollection.InsertOne(ptplSopWi);
                errorInfo.Message = "Data inserted successfully with ID:-" + ptplSopWi.formId;
                errorInfo.StatusCode = HttpStatusCode.OK;
                return await Task.Run(() => Ok((errorInfo)));

            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }


        /// <summary>
        /// This API ActionMethod use to insert or update the EventTrigger master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetDesignedFormBySopNumber")]

        public async Task<IActionResult> GetDesignedFormBySopNumber(int sopId)
        {
            try
            {
                List<ptplSopWi> allCollectionList = ptplSopWiCollection.AsQueryable().ToList<ptplSopWi>();
                var data = (from a in allCollectionList where a.sopWorkInstructionID == sopId select a).SingleOrDefault();
                var modal = new
                {
                    templateData = data,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }


        /// <summary>
        /// This API ActionMethod use to insert or update the EventTrigger master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("PostFilledLoggerData")]

        public async Task<IActionResult> PostFilledLoggerData(LoggerDataDto loggerDataDto)
        {
            try
            {

               // List<ptplSopWi> allCollectionList = ptplSopWiCollection.AsQueryable().ToList<ptplSopWi>();
               // var data = (from a in allCollectionList where a.sopWorkInstructionID == sopId select a).SingleOrDefault();
                var modal = new
                {
                    templateData ="ok",
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }


    }


    public class ptplSopWi
    {
        [BsonId]
        public ObjectId formId { get; set; }
        [BsonElement("jsonForm")]
        public string jsonDesignedForm { set; get; }
        [BsonElement("sopWorkInstructionID")]
        public int sopWorkInstructionID { set; get; }
        public ptplSopWi(string _jsonDesignedForm,int _sopWorkInstructionID)
        {
            jsonDesignedForm = _jsonDesignedForm;
            sopWorkInstructionID = _sopWorkInstructionID;
        }
    }


    public class CustomFormDto
    {
        public string designedFormJson { set; get; }
        public string sopWorkInstructionID { set; get; }
    }

    public class LoggerDataDto
    {
        public string formId { set; get; }
        public int loggerId { set; get; }
        public object filledData { set; get; }
    }


}