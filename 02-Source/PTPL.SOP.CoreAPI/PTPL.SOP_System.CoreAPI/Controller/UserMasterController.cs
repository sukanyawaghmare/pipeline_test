﻿using AutoMapper;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataModels;
using PTPL.SOP_System.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PTPL.SOP_System.DataEntities;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace PTPL.SOP_System.API.Core.Controller
{
    /// <summary>
    /// This API controller use to  managing the user details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>

    // [EnableCors("VisiPolicy")]
    // [MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/UserMaster/")]
    [ApiController]
    // [Authorize]
    public class UserMasterController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IUserService _iuserService;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        // int userId=0;
        // int roleId=0;
        private object httpContextAccessor;
        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="UserMaster" /> class.
        /// </summary>
        /// <param name="iuserService">The iuserService<see cref="IUserService" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public UserMasterController(IUserService iuserService, IMapper mapper)
        {
            //userId = int.Parse(httpContextAccessor.HttpContext.User.Identity.Name);
            // roleId = int.Parse((new List<System.Security.Claims.Claim>(httpContextAccessor.HttpContext.User.Claims))[1].Value);
            _iuserService = iuserService;
            _mapper = mapper;

            //   _appSetting = appSettings.Value;
        }
        #endregion constructor

        #region Methods
        [HttpPost, Route("AddUpdateUser")]

        public async Task<IActionResult> AddUpdateUser([FromBody] UserMasterDTO userMaster)
        {
            try
            {
                byte[] passHash, passSalt;
                if (!ModelState.IsValid || userMaster == null)
                {
                    return BadRequest(ModelState);
                }
                var usermasterobj = new PtplUserMaster();
                var user = _mapper.Map<PtplUserMaster>(userMaster);
                if (userMaster.Uid == 0)
                {
                    UserService.CreatePasswordHash(userMaster.Password, out passHash, out passSalt);

                    user.PasswordHash = passHash;
                    user.PasswordSalt = passSalt;
                    user.CreatedBy = 1;
                    user.IsActive = true;
                    user.CreationDate = DateTime.Now;
                }



                var result = _iuserService.addUpdateUser(user);
                if (result.Result == 0)

                    return await Task.Run(() => Ok(("User name already exist")));
                var modal = new
                {
                    userId = result.Result,

                    msg = "Data inserted successfully",
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Edit User
        /// </summary>
        /// <returns>Returns Id of recently updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("EditUser")]
        public async Task<IActionResult> EditUser([FromBody] PtplUserMaster userMaster)
        {
            try
            {
                if (!ModelState.IsValid || userMaster == null)
                {
                    return BadRequest(ModelState);
                }
                var result = _iuserService.editUser(userMaster);
                var modal = new
                {
                    userId = result.Result,
                    msg = "Data updated successfully",
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;
                return await Task.Run(() => BadRequest(errorInfo));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Delete User
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteUser")]
        public async Task<IActionResult> DeleteUser(int userId)
        {
            try
            {
                if (!ModelState.IsValid || userId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _iuserService.deleteUser(userId);
                var modal = new
                {
                    msg = result.Result,
                    statusCode = HttpStatusCode.OK

                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get All User List
        /// </summary>
        /// <returns>Returns List of user</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllUsers")]

        public async Task<IActionResult> GetAllUsers()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iuserService.getAllUsers();
                var modal = new
                {
                    usersData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Validate User Login
        /// </summary>
        /// <remarks>Code documentation and Review is pending </remarks>

        [HttpPost, Route("Authenticate")]
        [AllowAnonymous]
        public async Task<IActionResult> Authenticate([FromBody]UserCredentialsDTO userCredentialsDTO)
        {
            try
            {
                if (!ModelState.IsValid || userCredentialsDTO == null)
                {
                    return BadRequest(ModelState);
                }
                var user = _iuserService.Authenticate(userCredentialsDTO.UserName, userCredentialsDTO.Password);
                if (user.Result == null)
                    return Unauthorized();

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(CommonDeclarations.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.Result.Uid.ToString()),
                    new Claim(ClaimTypes.Role, user.Result.Role.ToString()),

                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);



                // return basic user info (without password) and token to store client side
                return await Task.Run(() => Ok(new
                {
                    Id = user.Result.Uid,
                    Username = user.Result.UserName,
                    FirstName = user.Result.FirstName,
                    LastName = user.Result.LastName,
                    ProfilePicture = user.Result.ProfilePicture,
                    RoleId = user.Result.Role,
                    Token = tokenString
                }));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get User by Id
        /// </summary>
        /// <returns>Returns user by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetUserById")]

        public async Task<IActionResult> GetUserById(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iuserService.getUserById(Id);
                var modal = new
                {
                    userData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to Get User by Id
        /// </summary>
        /// <returns>Returns user by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAssigneeUser")]

        public async Task<IActionResult> GetAssigneeUser()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iuserService.getAssigneeUser();
                var modal = new
                {
                    userData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Send mail to user by Id
        /// </summary>
        /// <returns>Returns user by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("SentMailToUserByUId")]
        public async Task<IActionResult> SentMailToUserByUId(int Id)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iuserService.sentMailToUserByUId(Id);
                var modal = new
                {
                    userData = result.Result,
                    message = "mail send successfully",
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        /// <summary>
        /// This API ActionMethod use to change password of user by Id
        /// </summary>
        /// <returns>Returns user by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("ChangeUserPassword")]
        public async Task<IActionResult> ChangeUserPassword([FromBody]UserChangePassDTO userPass)
        {
            try
            {
                // 2. Get user from db and match current password hash with password from db
                byte[] passHash, passSalt;

                UserService.CreatePasswordHash(userPass.NewPassword, out passHash, out passSalt);

                var userPassword = _iuserService.GetUserPassword(userPass.Uid);
                if (UserService.VerifyPasswordHash(userPass.Password, userPassword.Result.PasswordHash, userPassword.Result.PasswordSalt))
                {
                    // 3. Set Hash password with user in db
                    var result = _iuserService.ChangeUserPass((new PtplUserMaster { Uid = userPass.Uid, Password = userPass.NewPassword, PasswordHash = passHash, PasswordSalt = passSalt }));
                    return await Task.Run(() => Ok((result.Result)));
                }
                else
                {
                    return new UnauthorizedResult();
                }
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion Methods

    }
}