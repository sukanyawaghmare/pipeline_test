﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace PTPL.SOP_System.CoreAPI.Controller
{

    /// <summary>
    // This API controller use to managing the Event master details(ADD/EDIT/DELETE)
   /// </summary>
   /// <remarks>Code documentation and Review is pending</remarks>

     //[EnableCors("VisiPolicy")]
     //[MapToApiVersion("1.0")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/EventTypes/")]
    [ApiController]
    [Authorize]
    public class EventTypesController : ControllerBase
    {

        #region Fields

        /// <summary>
        ///     Defines the _idocumentService
        /// </summary>
        private readonly IEventTypeServices _ieventTypeServices;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;
        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields

        #region constructor
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventTypeMaster" /> class.
        /// </summary>
        /// <param name="ieventTypeServices">The ieventTypeServices<see cref="IEventTypeServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public EventTypesController(IEventTypeServices ieventTypeServices, IMapper mapper)
        {
            _ieventTypeServices = ieventTypeServices;
            _mapper = mapper;

            //   _appSetting = appSettings.Value;
        }
        #endregion constructor
        #region Methods
        /// <summary>
        /// This API ActionMethod use to insert event type data
        /// </summary>
        /// <returns>Returns Id of recently added record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddEventType")]

        public async Task<IActionResult> AddEventType([FromBody] List<PtplEventMaster> eventTypes)
        {
            try
            {
                if (ModelState.IsValid || eventTypes == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _ieventTypeServices.addEvents(eventTypes);
                var modal = new
                {
                    eventTypeId = result.Result,
                    msg = "Data inserted successfully",
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Delete Event master
        /// </summary>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("DeleteEvent")]
        public async Task<IActionResult> DeleteEvent(int EventId)
        {
            try
            {
                if (!ModelState.IsValid || EventId == 0)
                {
                    return BadRequest(ModelState);
                }
                var result = _ieventTypeServices.deleteEvent(EventId);

                errorInfo.Message = result.Result;
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Fetch All EvevntTypes
        /// </summary> 
        /// <returns>Returns List of event types</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllEventTypes")]
        public async Task<IActionResult> GetAllEventTypes()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var result = _ieventTypeServices.getAllEventTypes();
                var modal = new
                {
                    eventDataList = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Fetch All EvevntTypes
        /// </summary>
        /// <returns>Returns event by id</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetEventTypeById")]
        public async Task<IActionResult> GetEventTypeById(int eventID)
        {
            try
            {
                if (ModelState.IsValid || eventID == 0)
                {
                    return BadRequest(ModelState);
                }

                var result = _ieventTypeServices.getEventTypesById(eventID);
                var modal = new
                {
                    eventTypeData = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }

        #endregion Methods
    }
}