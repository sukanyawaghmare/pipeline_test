﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;

namespace PTPL.SOP_System.CoreAPI.Controller
{
    /// <summary>
    /// This API controller use to  managing the Category details (ADD/EDIT/DELETE)
    /// </summary>
    /// <remarks>Code documentation and Review is pending </remarks>


    // [MapToApiVersion("1.0")]
    //[EnableCors("VisiPolicy")]

    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/ConfigurePreferences/")]
    [ApiController]
    public class ConfigurePreferencesController : ControllerBase
    {
        #region Fields

        /// <summary>
        ///     Defines the _icategoryServices
        /// </summary>
        private readonly IConfigurePreferencesServices _iconfigurePreferencesServices;
        /// <summary>
        ///     Defines the _mapper
        /// </summary>
        private IMapper _mapper;

        private ErrorInfo errorInfo = new ErrorInfo();
        #endregion Fields


        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="ConfigurePreferences" /> class.
        /// </summary>
        /// <param name="iconfigurePreferencesServices">The icategoryServices<see cref="IConfigurePreferencesServices" /></param>
        /// <param name="mapper">The mapper<see cref="IMapper" /></param>

        public ConfigurePreferencesController(IConfigurePreferencesServices iconfigurePreferencesServices, IMapper mapper)
        {
            _iconfigurePreferencesServices = iconfigurePreferencesServices;
            _mapper = mapper;
        }
        #endregion Constructors


        #region Methods

        /// <summary>
        /// This API ActionMethod use to insert or update the Category master
        /// </summary>
        /// <returns>Returns Id of recently added or updated record</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpPost, Route("AddUpdateConfigurePreferences")]

        public async Task<IActionResult> AddUpdateConfigurePreferences([FromBody] PtplConfigurePreferences configurePreferences)
        {
            try
            {
                if (!ModelState.IsValid || configurePreferences == null)
                {
                    return BadRequest(ModelState);
                }

                var result = _iconfigurePreferencesServices.addupdateConfigurePreferences(configurePreferences);
                errorInfo.Id = result.Result;
                errorInfo.Message = "Data inserted successfully";
                errorInfo.StatusCode = HttpStatusCode.OK;

                return await Task.Run(() => Ok((errorInfo)));
            }
            catch (Exception ex)
            {

                errorInfo.StatusCode = HttpStatusCode.BadRequest;
                errorInfo.Message = ex.Message;

                return await Task.Run(() => BadRequest(errorInfo));
            }
        }


        /// <summary>
        /// This API ActionMethod use to Get All Category List
        /// </summary>
        /// <returns>Returns List of categories</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllConfigurePreferences")]

        public async Task<IActionResult> GetAllConfigurePreferences()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iconfigurePreferencesServices.getAllConfigurePreferences();
                var modal = new
                {
                    configureDate = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }

        /// <summary>
        /// This API ActionMethod use to Get All Category List
        /// </summary>
        /// <returns>Returns List of categories</returns>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <remarks>Code documentation and Review is pending </remarks>
        [HttpGet, Route("GetAllTimeZonesList")]

        public async Task<IActionResult> GetAllTimeZonesList()
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _iconfigurePreferencesServices.getAllTimeZoneList();
                var modal = new
                {
                    timeZoneDate = result.Result,
                    statusCode = HttpStatusCode.OK
                };
                return await Task.Run(() => Ok((modal)));
            }
            catch (Exception ex)
            {
                var modal = new
                {
                    statusCode = HttpStatusCode.BadRequest,
                    errorString = ex.Message
                };
                return await Task.Run(() => BadRequest(modal));
            }
        }
        #endregion
    }
}