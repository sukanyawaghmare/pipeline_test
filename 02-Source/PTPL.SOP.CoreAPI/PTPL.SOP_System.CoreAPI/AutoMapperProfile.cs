﻿using AutoMapper;
using  PTPL.SOP_System.DataModels;
using PTPL.SOP_System.DataEntities;

namespace PTPL.SOP_System.Core
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserMasterDTO, PtplUserMaster > ();
            CreateMap<TemplateMachineDTO, PtplSopWorkInstruction>();
            CreateMap<EventTriggerScheduleDTO, PtplEventTriggerSchedule>();
        }

    }
}
