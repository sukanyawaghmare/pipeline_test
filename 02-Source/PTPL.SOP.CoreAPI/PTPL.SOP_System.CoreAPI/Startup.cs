using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using PTPL.SOP_System.Common;
using PTPL.SOP_System.Common.Logger;
using PTPL.SOP_System.Core;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.Services;
using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PTPL.SOP_System.Core
{
    public class Startup
    {
        /// <summary>
        /// Create a object of swaggerUI layer
        /// </summary>

        #region Variable
        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }
        SwaggerUI.SwaggerUI _objSwaggerUI;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _objSwaggerUI = new SwaggerUI.SwaggerUI("VisiSOP Core API", "VisiSOP Core API", 
                Path.Combine(AppContext.BaseDirectory, "LeanLogic.Core.xml"), "v1");
            //Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
            //Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
            .WriteTo.Map(
                evt => evt.Level,
                (level, wt) => wt.RollingFile(Configuration["LogPath"] + level + "-{Date}.log"))
            .CreateLogger();
            var config = new MapperConfiguration(cfg => {

                cfg.AddProfile<AutoMapperProfile>();
            });
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddAutoMapper(typeof(Startup));

            //This line is use to get Connection string from config file.
            //This line is use to get Connection string from config file.
            CommonDeclarations.ConnectionString = Configuration["ConnectionStrings:DefaultConnection"];
            CommonDeclarations.Secret = Configuration["AppSettings:Secret"];
            //// configure jwt authentication
            //var appSettings = appSettingsSection.Get<AppSettings>();
            
            
            var key = Encoding.ASCII.GetBytes(CommonDeclarations.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                //x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddCors(o => o.AddPolicy("VisiPolicy", builder => builder.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader()));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
           //services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            //services.AddAutoMapper();
            //Add versioning 
            services.AddApiVersioning(option =>
            {
                option.ReportApiVersions = true;
                option.DefaultApiVersion = new ApiVersion(1, 0);
                option.AssumeDefaultVersionWhenUnspecified = true;
            });
    //        services.AddMvcCore().AddNewtonsoftJson(
    //options => options.SerializerSettings.ReferenceLoopHandling =
    //    Newtonsoft.Json.ReferenceLoopHandling.Ignore);
            services.AddControllers();
            
                
            _objSwaggerUI.ConfigureServices(services);
            services.AddResponseCompression();
            //services.AddTransient<IUserService, UserService>();
            var builder = new ContainerBuilder();
            builder.Populate(services);
            // Registered services to ContainerBuilder
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<EventTypeServices>().As<IEventTypeServices>();
            builder.RegisterType<TemplateService>().As<ITemplateService>();
            builder.RegisterType<DocumentServices>().As<IDocumentServices>();
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>();
            builder.RegisterType<CategoryServices>().As<ICategoryServices>();
            builder.RegisterType<MachineServices>().As<IMachineServices>();
            builder.RegisterType<AreaServices>().As<IAreaServices>();
            builder.RegisterType<EventOccurrenceServices>().As<IEventOccurrenceServices>();
            builder.RegisterType<EventTriggerScheduleServices>().As<IEventTriggerScheduleServices>();
            builder.RegisterType<LoggerAttachmentService>().As<ILoggerAttachmentService>();
            builder.RegisterType<ConfigurePreferencesServices>().As<IConfigurePreferencesServices>();
            // Registered dbConetext (data connection string)
            builder.RegisterType<PTPL_SOPContext>();
            services.AddMvc().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            this.ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            
            //This line of code will use to save logs into Database table as [EventLog].
            loggerFactory.AddContext(LogLevel.Error, Configuration.GetConnectionString("DefaultConnection"));
            //This line will add Serilog to the project for Client Side logging.
            loggerFactory.AddSerilog();
            _objSwaggerUI.Configure(app, "");
            app.UseResponseCompression();
            app.UseCors("VisiPolicy");
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
