﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using PTPL.SOP_System.Common;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PTPL_SOPContext : DbContext
    {
        public PTPL_SOPContext()
        {
        }

        public PTPL_SOPContext(DbContextOptions<PTPL_SOPContext> options)
            : base(options)
        {
        }

        public virtual DbSet<PtplAreaMaster> PtplAreaMaster { get; set; }
        public virtual DbSet<PtplCategoryMaster> PtplCategoryMaster { get; set; }
        public virtual DbSet<PtplConfigureEmail> PtplConfigureEmail { get; set; }
        public virtual DbSet<PtplConfigurePreferences> PtplConfigurePreferences { get; set; }
        public virtual DbSet<PtplDepartmentMaster> PtplDepartmentMaster { get; set; }
        public virtual DbSet<PtplEventMaster> PtplEventMaster { get; set; }
        public virtual DbSet<PtplEventOccurrence> PtplEventOccurrence { get; set; }
        public virtual DbSet<PtplEventTriggerSchedule> PtplEventTriggerSchedule { get; set; }
        public virtual DbSet<PtplLoggerAttachment> PtplLoggerAttachment { get; set; }
        public virtual DbSet<PtplMachineMaster> PtplMachineMaster { get; set; }
        public virtual DbSet<PtplSopLogger> PtplSopLogger { get; set; }
        public virtual DbSet<PtplSopStatus> PtplSopStatus { get; set; }
        public virtual DbSet<PtplSopWimachine> PtplSopWimachine { get; set; }
        public virtual DbSet<PtplSopWorkInstruction> PtplSopWorkInstruction { get; set; }
        public virtual DbSet<PtplTimeZoneMaster> PtplTimeZoneMaster { get; set; }
        public virtual DbSet<PtplUserMaster> PtplUserMaster { get; set; }
        public virtual DbSet<PtplUserRoles> PtplUserRoles { get; set; }
        public virtual DbSet<SopEventLog> SopEventLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(CommonDeclarations.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PtplAreaMaster>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_areaMaster");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.AreaName)
                    .IsRequired()
                    .HasColumnName("areaName");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PtplCategoryMaster>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_categoryMaster");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.CategoryName)
                    .IsRequired()
                    .HasColumnName("categoryName");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PtplConfigureEmail>(entity =>
            {
                entity.HasKey(e => e.EmailId);

                entity.ToTable("ptpl_ConfigureEmail");

                entity.Property(e => e.Bcc)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Body)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Cc)
                    .HasColumnName("CC")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.From)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HostName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subject)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.To)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PtplConfigurePreferences>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_ConfigurePreferences");

                entity.Property(e => e.Uid)
                    .HasColumnName("uid")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmailNotification).HasColumnName("emailNotification");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ServerTimeZone).HasColumnName("serverTimeZone");

                entity.HasOne(d => d.U)
                    .WithOne(p => p.PtplConfigurePreferences)
                    .HasForeignKey<PtplConfigurePreferences>(d => d.Uid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ptpl_ConfigurePreferences_ptpl_timeZoneMaster");
            });

            modelBuilder.Entity<PtplDepartmentMaster>(entity =>
            {
                entity.HasKey(e => e.DeptId);

                entity.ToTable("ptpl_departmentMaster");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.DeptName).IsRequired();

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<PtplEventMaster>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_eventMaster");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventStatus).HasColumnName("eventStatus");

                entity.Property(e => e.EventType)
                    .IsRequired()
                    .HasColumnName("eventType")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PtplEventOccurrence>(entity =>
            {
                entity.HasKey(e => e.OccuranceId);

                entity.ToTable("ptpl_eventOccurrence");

                entity.Property(e => e.OccuranceId).HasColumnName("occuranceId");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.OccurrenceName)
                    .IsRequired()
                    .HasColumnName("occurrenceName");
            });

            modelBuilder.Entity<PtplEventTriggerSchedule>(entity =>
            {
                entity.HasKey(e => e.EventTriggerId);

                entity.ToTable("ptpl_eventTriggerSchedule");

                entity.Property(e => e.EventTriggerId).HasColumnName("eventTriggerId");

                entity.Property(e => e.Assignee).HasColumnName("assignee");

                entity.Property(e => e.Condition).HasColumnName("condition");

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EndTime)
                    .HasColumnName("endTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventDateTime)
                    .HasColumnName("eventDateTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventType).HasColumnName("eventType");

                entity.Property(e => e.ExternalEvents).HasColumnName("externalEvents");

                entity.Property(e => e.MachineId).HasColumnName("machineId");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Occurance).HasColumnName("occurance");

                entity.Property(e => e.StartTime)
                    .HasColumnName("startTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.Property(e => e.TimeValue).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title");

                entity.Property(e => e.WiSopId).HasColumnName("wiSopId");
            });

            modelBuilder.Entity<PtplLoggerAttachment>(entity =>
            {
                entity.HasKey(e => e.AttachmentId);

                entity.ToTable("ptpl_loggerAttachment");

                entity.Property(e => e.AttachmentId).HasColumnName("attachmentId");

                entity.Property(e => e.AttachmentName)
                    .HasColumnName("attachmentName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false);

                entity.Property(e => e.DocumentId).HasColumnName("documentId");

                entity.Property(e => e.FileExentaion)
                    .HasColumnName("fileExentaion")
                    .HasMaxLength(50);

                entity.Property(e => e.FileName)
                    .HasColumnName("fileName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize)
                    .HasColumnName("fileSize")
                    .HasMaxLength(20);

                entity.Property(e => e.FileStream).HasColumnName("fileStream");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Document)
                    .WithMany(p => p.PtplLoggerAttachment)
                    .HasForeignKey(d => d.DocumentId)
                    .HasConstraintName("FK_ptpl_loggerAttachment_ptpl_sopDocumentMaster");
            });

            modelBuilder.Entity<PtplMachineMaster>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_machineMaster");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.AreaId).HasColumnName("areaId");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.MachineName)
                    .HasColumnName("machineName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.PtplMachineMaster)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_ptpl_machineMaster_ptpl_areaMaster");
            });

            modelBuilder.Entity<PtplSopLogger>(entity =>
            {
                entity.HasKey(e => e.SopDocId)
                    .HasName("PK_ptpl_sopDocumentMaster");

                entity.ToTable("ptpl_sopLogger");

                entity.Property(e => e.SopDocId).HasColumnName("sopDocId");

                entity.Property(e => e.ActualFinishDate)
                    .HasColumnName("actualFinishDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.AreaId).HasColumnName("areaId");

                entity.Property(e => e.Assignee).HasColumnName("assignee");

                entity.Property(e => e.ComplianceStatus)
                    .HasColumnName("complianceStatus")
                    .HasMaxLength(20);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.EventType).HasColumnName("eventType");

                entity.Property(e => e.MachineId).HasColumnName("machineId");

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PlanFinishDate)
                    .HasColumnName("planFinishDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SopCategory).HasColumnName("sopCategory");

                entity.Property(e => e.SopDesc)
                    .IsRequired()
                    .HasColumnName("sopDesc");

                entity.Property(e => e.SopLoggerNumber)
                    .HasColumnName("sopLoggerNumber")
                    .IsUnicode(false);

                entity.Property(e => e.SopName)
                    .IsRequired()
                    .HasColumnName("sopName");

                entity.Property(e => e.SopwiId).HasColumnName("sopwiId");

                entity.Property(e => e.StartTime)
                    .HasColumnName("startTime")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.PtplSopLogger)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_ptpl_sopLogger_ptpl_areaMaster");

                entity.HasOne(d => d.AssigneeNavigation)
                    .WithMany(p => p.PtplSopLogger)
                    .HasForeignKey(d => d.Assignee)
                    .HasConstraintName("FK_ptpl_sopLogger_ptpl_userMaster");

                entity.HasOne(d => d.EventTypeNavigation)
                    .WithMany(p => p.PtplSopLogger)
                    .HasForeignKey(d => d.EventType)
                    .HasConstraintName("FK_DocumentMaster_eventMaster");

                entity.HasOne(d => d.Machine)
                    .WithMany(p => p.PtplSopLogger)
                    .HasForeignKey(d => d.MachineId)
                    .HasConstraintName("FK_ptpl_sopLogger_ptpl_machineMaster");

                entity.HasOne(d => d.SopCategoryNavigation)
                    .WithMany(p => p.PtplSopLogger)
                    .HasForeignKey(d => d.SopCategory)
                    .HasConstraintName("FK_DocumentMaster_categoryMaster");

                entity.HasOne(d => d.Sopwi)
                    .WithMany(p => p.PtplSopLogger)
                    .HasForeignKey(d => d.SopwiId)
                    .HasConstraintName("FK_ptpl_sopLogger_ptpl_sopWorkInstruction");
            });

            modelBuilder.Entity<PtplSopStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK_ptpl_SOP_Status");

                entity.ToTable("ptpl_sopStatus");

                entity.Property(e => e.StatusId).HasColumnName("statusId");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("createdBy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("createdDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.StatusName)
                    .HasColumnName("statusName")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PtplSopWimachine>(entity =>
            {
                entity.ToTable("ptpl_sopWIMachine");

                entity.Property(e => e.MachineId).HasColumnName("machineId");

                entity.Property(e => e.WiId).HasColumnName("wiId");

                entity.HasOne(d => d.Wi)
                    .WithMany(p => p.PtplSopWimachine)
                    .HasForeignKey(d => d.WiId)
                    .HasConstraintName("FK_templateMachine_template");
            });

            modelBuilder.Entity<PtplSopWorkInstruction>(entity =>
            {
                entity.HasKey(e => e.Uid)
                    .HasName("PK_ptpl_sopTemplateMaster");

                entity.ToTable("ptpl_sopWorkInstruction");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.AreaId).HasColumnName("areaId");

                entity.Property(e => e.AssigneeId).HasColumnName("assigneeId");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.EventType).HasColumnName("eventType");

                entity.Property(e => e.FileName)
                    .HasColumnName("fileName")
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .IsRequired()
                    .HasColumnName("filePath")
                    .HasMaxLength(256);

                entity.Property(e => e.FileSize)
                    .HasColumnName("fileSize")
                    .HasMaxLength(16);

                entity.Property(e => e.FileStream).HasColumnName("fileStream");

                entity.Property(e => e.FileVersion)
                    .HasColumnName("fileVersion")
                    .HasMaxLength(8);

                entity.Property(e => e.MachineId).HasColumnName("machineId");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.OriginalEstimateTime)
                    .HasColumnName("originalEstimateTime")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SopCategory).HasColumnName("sopCategory");

                entity.Property(e => e.SopNumber)
                    .HasColumnName("sopNumber")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SopTemplateDesc).HasColumnName("sopTemplateDesc");

                entity.Property(e => e.SopTemplateName)
                    .IsRequired()
                    .HasColumnName("sopTemplateName");

                entity.Property(e => e.WiFlag).HasColumnName("wiFlag");

                entity.HasOne(d => d.Area)
                    .WithMany(p => p.PtplSopWorkInstruction)
                    .HasForeignKey(d => d.AreaId)
                    .HasConstraintName("FK_ptpl_sopTemplateMaster_ptpl_areaMaster");

                entity.HasOne(d => d.Assignee)
                    .WithMany(p => p.PtplSopWorkInstruction)
                    .HasForeignKey(d => d.AssigneeId)
                    .HasConstraintName("FK_ptpl_sopTemplateMaster_ptpl_userMaster");

                entity.HasOne(d => d.EventTypeNavigation)
                    .WithMany(p => p.PtplSopWorkInstruction)
                    .HasForeignKey(d => d.EventType)
                    .HasConstraintName("FK_TemplateMaster_eventMaster");
            });

            modelBuilder.Entity<PtplTimeZoneMaster>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_timeZoneMaster");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeZones)
                    .IsRequired()
                    .HasColumnName("timeZones")
                    .HasMaxLength(350)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PtplUserMaster>(entity =>
            {
                entity.HasKey(e => e.Uid);

                entity.ToTable("ptpl_userMaster");

                entity.Property(e => e.Uid).HasColumnName("uid");

                entity.Property(e => e.ContactNumber)
                    .HasColumnName("contactNumber")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasColumnName("createdBy");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("creationDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeleteFlag).HasColumnName("deleteFlag");

                entity.Property(e => e.DepartmentId).HasColumnName("departmentId");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("firstName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsActive).HasColumnName("isActive");

                entity.Property(e => e.IsBlocked).HasColumnName("isBlocked");

                entity.Property(e => e.LastName)
                    .HasColumnName("lastName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy).HasColumnName("modifiedBy");

                entity.Property(e => e.ModifiedDate)
                    .HasColumnName("modifiedDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasColumnName("passwordHash")
                    .HasMaxLength(250);

                entity.Property(e => e.PasswordSalt)
                    .IsRequired()
                    .HasColumnName("passwordSalt")
                    .HasMaxLength(250);

                entity.Property(e => e.PhotoPath)
                    .HasColumnName("photoPath")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProfilePicture).HasColumnName("profilePicture");

                entity.Property(e => e.Role).HasColumnName("role");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.PtplUserMaster)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("FK_ptpl_user_ptpl_dept");

                entity.HasOne(d => d.RoleNavigation)
                    .WithMany(p => p.PtplUserMaster)
                    .HasForeignKey(d => d.Role)
                    .HasConstraintName("FK_ptpl_user_ptpl_role");
            });

            modelBuilder.Entity<PtplUserRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("ptpl_userRoles");

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SopEventLog>(entity =>
            {
                entity.ToTable("sop_EventLog");

                entity.Property(e => e.ApplicationName).HasMaxLength(100);

                entity.Property(e => e.CreatedTime).HasColumnType("datetime");

                entity.Property(e => e.LogLevel).HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
