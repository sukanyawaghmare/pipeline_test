﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class AreaServices:IAreaServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<AreaServices> _logger;
        #endregion Fiels
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AreaServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{AreaServices}" /></param>
        public AreaServices(PTPL_SOPContext context, ILogger<AreaServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion  Constructors
        #region Methods
        /// <summary>
        /// Service Method used to insert or update the Area master
        /// </summary>
        /// <param name="AreaMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateArea(PtplAreaMaster AreaMaster)
        {

            try
            {
                if (AreaMaster.Uid == 0)
                {
                    AreaMaster.CreatedBy = 1;
                    AreaMaster.CreationDate = DateTime.Now;
                    _context.PtplAreaMaster.Add(AreaMaster);
                    _logger.LogInformation("Record Inserted : " + AreaMaster.Uid);
                }
                else
                {
                    _context.PtplAreaMaster.Update(AreaMaster);
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + AreaMaster.Uid);
                }
                _context.SaveChanges();
                return await Task.Run(() => AreaMaster.Uid);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete Area 
        /// </summary>
        /// <param name="AreaId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteArea(int AreaId)
        {
            try
            {

                var Area = _context.PtplAreaMaster.Find(AreaId);
                if (Area != null)
                    _context.PtplAreaMaster.Remove(Area);
                else
                {
                    _logger.LogInformation("Services: deleteArea : Area Not Found");
                    return await Task.Run(() => "Area Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + AreaId);
                return await Task.Run(() => "Area Deleted Successfully" + AreaId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Area data
        /// </summary>
        /// <returns>Returns cotegory master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplAreaMaster>> getAllAreas()
        {
            try
            {
                return await Task.Run(() => _context.PtplAreaMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Area data by id
        /// </summary>
        /// <param name="AreaId"></param>
        /// <returns>Returns Area master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplAreaMaster> getAreaById(int AreaId)
        {
            try
            {
                return await Task.Run(() => _context.PtplAreaMaster.Find(AreaId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Methods
    }
}
