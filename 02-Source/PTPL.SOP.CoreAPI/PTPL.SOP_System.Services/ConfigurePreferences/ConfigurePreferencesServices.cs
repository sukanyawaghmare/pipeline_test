﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;

namespace PTPL.SOP_System.Services
{
    public class ConfigurePreferencesServices : IConfigurePreferencesServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<ConfigurePreferencesServices> _logger;
        #endregion Fiels
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{CategoryServices}" /></param>
        public ConfigurePreferencesServices(PTPL_SOPContext context, ILogger<ConfigurePreferencesServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion  Constructors

        #region Methods

        /// <summary>
        /// Service Method used to insert or update the Category master
        /// </summary>
        /// <param name="categoryMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateConfigurePreferences(PtplConfigurePreferences configurePreferences)
        {

            try
            {
                if (configurePreferences.Uid == 0)
                {
                    configurePreferences.CreatedBy = 1;
                    configurePreferences.CreatedDate = DateTime.Now;
                    _context.PtplConfigurePreferences.Add(configurePreferences);
                    _logger.LogInformation("Record Inserted : " + configurePreferences.Uid);
                }
                else
                {
                    _context.PtplConfigurePreferences.Update(configurePreferences);
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + configurePreferences.Uid);
                }
                _context.SaveChanges();
                return await Task.Run(() => configurePreferences.Uid);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Category data
        /// </summary>
        /// <returns>Returns cotegory master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplTimeZoneMaster>> getAllTimeZoneList()
        {
            try
            {

                return await Task.Run(() => _context.PtplTimeZoneMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// Service Method used to fetch Category data
        /// </summary>
        /// <returns>Returns cotegory master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<ConfigureDTO> getAllConfigurePreferences()
        {
            try
            {
                var result = from conf in _context.PtplConfigurePreferences
                             join zone in _context.PtplTimeZoneMaster on conf.Uid equals zone.Uid
                             select new ConfigureDTO
                             {
                                 Uid = conf.Uid,
                                 ServerTimeZone = zone.TimeZones,
                                 EmailNotification = conf.EmailNotification,
                                 CreatedBy = conf.CreatedBy,
                                 CreatedDate = conf.CreatedDate,
                                 ModifiedBy = conf.ModifiedBy,
                                 ModifiedDate = conf.ModifiedDate
                             };
                return await Task.Run(() => result.FirstOrDefault());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }


        #endregion
    }
}
