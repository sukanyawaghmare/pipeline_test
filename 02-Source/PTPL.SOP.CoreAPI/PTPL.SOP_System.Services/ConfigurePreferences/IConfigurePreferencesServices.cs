﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PTPL.SOP_System.DataModels;

namespace PTPL.SOP_System.Services
{
   public interface IConfigurePreferencesServices
    {
        Task<int> addupdateConfigurePreferences(PtplConfigurePreferences configurePreferences);
        
        Task<ConfigureDTO> getAllConfigurePreferences();
        
        Task<List<PtplTimeZoneMaster>> getAllTimeZoneList();


    }
}
