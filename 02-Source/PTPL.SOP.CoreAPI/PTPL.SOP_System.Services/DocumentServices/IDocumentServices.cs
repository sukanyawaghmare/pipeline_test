﻿using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IDocumentServices
    {
        Task<int> addupdateDocument(PtplSopLogger documentMaster);
        Task<string> deleteDocument(int userId);
        Task<List<PtplSopLogger>> getAllDocuments();
        Task<List<DocumentDTO>> getAllDocDetails(int userId,int roleId);
        Task<SopLoggerDTO> getDocumentById(int machindocumentId);
        Task<int> updateSoploggerStatus(LoggerUpdateStatus loggerUpdateStatus);
        Task<List<SopLoggerByUserDTO>> getSopLoggerByUser(int userId, int roleId);

    }
}
