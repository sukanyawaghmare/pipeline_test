﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class DocumentServices : IDocumentServices
    {
        #region Fiels
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<DocumentServices> _logger;
        #endregion Fields
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DocumentServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{DocumentServices}" /></param>
        public DocumentServices(PTPL_SOPContext context, ILogger<DocumentServices> logger)
        {
            _context = context;
            _logger = logger;
            //DbContextTransaction transaction = _context.Database.BeginTransaction();
        }
        #endregion Constructors
        #region Methods
        /// <summary>
        /// Service Method used to insert or update the Document master
        /// </summary>
        /// <param name="documentMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateDocument(PtplSopLogger documentMaster)
        {

            try
            {
                if (documentMaster.SopDocId == 0)
                {
                    documentMaster.CreatedBy = 1;
                    documentMaster.CreatedDate = DateTime.Now;
                    documentMaster.StartTime = TimeZoneInfo.ConvertTimeToUtc(documentMaster.StartTime, TimeZoneInfo.Local);
                    string sopNumberVal1 = "SOP-LOG" + "-";
                    string responseData, resultData = "";
                    var val = _context.PtplSopLogger.Count()+1;
                    responseData = val.ToString();
                    
                    if (responseData.Length.Equals(1))
                    {
                        resultData = "0000" + responseData;
                    }
                    if (responseData.Length.Equals(2))
                    {
                        resultData = "000" + responseData;
                    }
                    if (responseData.Length.Equals(3))
                    {
                        resultData = "00" + responseData;
                    }
                    if (responseData.Length.Equals(4))
                    {
                        resultData = "0" + responseData;
                    }
                    documentMaster.SopLoggerNumber = sopNumberVal1 + resultData;
                    _context.PtplSopLogger.Add(documentMaster);
                    _logger.LogInformation("Record Inserted : " + documentMaster.SopDocId);
                }
                else
                {
                    _context.PtplSopLogger.Update(documentMaster);
                    _context.Entry(documentMaster).Property(x => x.SopLoggerNumber).IsModified = false;
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + documentMaster.SopDocId);
                }
                _context.SaveChanges();
                return await Task.Run(() => documentMaster.SopDocId);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to Delete Document 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteDocument(int DocumentId)
        {
            try
            {

                var Document = _context.PtplSopLogger.Find(DocumentId);
                if (Document != null)
                    _context.PtplSopLogger.Remove(Document);
                else
                {
                    _logger.LogInformation("Services: deleteDocument : Document Not Found");
                    return await Task.Run(() => "Document Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + DocumentId);
                return await Task.Run(() => "Document Deleted Successfully" + DocumentId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Document data
        /// </summary>
        /// <returns>Returns list of document</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplSopLogger>> getAllDocuments()
        {
            try
            {
                return await Task.Run(() => _context.PtplSopLogger.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Document data
        /// </summary>
        /// <returns>Returns list document</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        //public async Task<List<DocumentDTO>> getAllDocDetails()
        //{
        //    try
        //    {
        //        var result = from doc in _context.PtplSopLogger
        //                     join Eve in _context.PtplEventMaster on doc.EventType equals Eve.Uid
        //                     join cat in _context.PtplCategoryMaster on doc.SopCategory equals cat.Uid
        //                     select new DocumentDTO
        //                     {
        //                         SopDocId = doc.SopDocId,
        //                         SopName = doc.SopName,
        //                         SopCategory = cat.CategoryName,
        //                         SopDesc = doc.SopDesc,
        //                         EventName = Eve.EventType,
        //                         CreatedDate = doc.CreatedDate,
        //                         Status = doc.Status
        //                     };
        //        return await Task.Run(() => result.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"It broke due to {ex.Message}");
        //        throw;
        //    }

        //}
        public async Task<List<DocumentDTO>> getAllDocDetails(int userId, int roleId)
        {
            try
            {
                if (roleId == 1)
                {


                    var result = from doc in _context.PtplSopLogger
                                 join Eve in _context.PtplEventMaster on doc.EventType equals Eve.Uid
                                 join cat in _context.PtplCategoryMaster on doc.SopCategory equals cat.Uid
                                 select new DocumentDTO
                                 {
                                     //startdate,plannedfinishedate,assinee,complancestatus,area,equipment
                                     SopDocId = doc.SopDocId,
                                     SopName = doc.SopName,
                                     SopCategory = cat.CategoryName,
                                     SopDesc = doc.SopDesc,
                                     EventName = Eve.EventType,
                                     CreatedDate = doc.CreatedDate,
                                     WINumber = (from wi in _context.PtplSopWorkInstruction where wi.Uid == doc.SopwiId select wi.SopNumber).FirstOrDefault(),
                                     Status = doc.Status,
                                     StartDate = doc.StartTime,
                                     PlannedFinishedDate = doc.PlanFinishDate,
                                     Assinee = (from us in _context.PtplUserMaster where us.Uid == doc.Assignee select us.UserName).FirstOrDefault(),
                                     ComplanceStatus = doc.ComplianceStatus,
                                     Area = (from us in _context.PtplAreaMaster where us.Uid == doc.AreaId select us.AreaName).FirstOrDefault(),
                                     Equipment = (from us in _context.PtplMachineMaster where us.Uid == doc.AreaId select us.MachineName).FirstOrDefault(),
                                     WiID = doc.SopwiId
                                 };

                    return await Task.Run(() => result.OrderByDescending(x => x.SopDocId).ToList());
                }
                else
                {
                    var result = from doc in _context.PtplSopLogger
                                 join Eve in _context.PtplEventMaster on doc.EventType equals Eve.Uid
                                 join cat in _context.PtplCategoryMaster on doc.SopCategory equals cat.Uid
                                 where doc.Assignee==userId
                                 select new DocumentDTO
                                 {
                                     //startdate,plannedfinishedate,assinee,complancestatus,area,equipment
                                     SopDocId = doc.SopDocId,
                                     SopName = doc.SopName,
                                     SopCategory = cat.CategoryName,
                                     SopDesc = doc.SopDesc,
                                     EventName = Eve.EventType,
                                     CreatedDate = doc.CreatedDate,
                                     WINumber = (from wi in _context.PtplSopWorkInstruction where wi.Uid == doc.SopwiId select wi.SopNumber).FirstOrDefault(),
                                     Status = doc.Status,
                                     StartDate = doc.StartTime,
                                     PlannedFinishedDate = doc.PlanFinishDate,
                                     Assinee = (from us in _context.PtplUserMaster where us.Uid == doc.Assignee select us.UserName).FirstOrDefault(),
                                     ComplanceStatus = doc.ComplianceStatus,
                                     Area = (from us in _context.PtplAreaMaster where us.Uid == doc.AreaId select us.AreaName).FirstOrDefault(),
                                     Equipment = (from us in _context.PtplMachineMaster where us.Uid == doc.AreaId select us.MachineName).FirstOrDefault(),
                                     WiID = doc.SopwiId
                                 };
                    return await Task.Run(() => result.OrderByDescending(x => x.SopDocId).ToList());
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }



        }
        /// <summary>
        /// Service Method used to fetch Document data by id
        /// </summary>
        /// <param name="documenId"></param>
        /// <returns>Returns document master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<SopLoggerDTO> getDocumentById(int DocumentId)
        {
            try
            {
                var result = (from slog in _context.PtplSopLogger
                              join wi in _context.PtplSopWorkInstruction
                              on slog.SopwiId equals wi.Uid
                              where slog.SopDocId == DocumentId
                              select new SopLoggerDTO
                              {
                                  SopDocId = slog.SopDocId,
                                  SopName = slog.SopName,
                                  SopDesc = slog.SopDesc,
                                  EventType = slog.EventType,
                                  SopCategory = slog.SopCategory,
                                  MachineId = slog.MachineId,
                                  SopwiId = slog.SopwiId,
                                  SopwiName = wi.SopTemplateName,
                                  StartTime = slog.StartTime,
                                  PlanFinishDate = slog.PlanFinishDate,
                                  ActualFinishDate = slog.ActualFinishDate,
                                  ComplianceStatus = slog.ComplianceStatus,
                                  AreaId=slog.AreaId,
                                  AssigneeId=slog.Assignee,
                                  CreatedBy=slog.CreatedBy,
                                  CreatedDate=slog.CreatedDate,
                                  Status = slog.Status,
                              }).FirstOrDefault();
                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to fetch Document data by id
        /// </summary>
        /// <param name="documenId"></param>
        /// <returns>Returns document master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> updateSoploggerStatus(LoggerUpdateStatus loggerUpdateStatus)
        {
            try
            {
                var result = _context.PtplSopLogger.Find(loggerUpdateStatus.sopDocId);
                result.ActualFinishDate = loggerUpdateStatus.ActualFinishDateDate;
                result.ComplianceStatus = loggerUpdateStatus.complianceStatus;
                result.Status = loggerUpdateStatus.status;


                //result.SopLoggerNumber = result.SopLoggerNumber;
                _context.PtplSopLogger.Update(result);
                _context.SaveChanges();
                return loggerUpdateStatus.sopDocId;
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }


        /// <summary>
        /// Service Method used to fetch Document data by id
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="roleId"></param>
        /// <returns>Returns document master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        //public async Task<List<SopLoggerByUserDTO>> getSopLoggerByUser(int userId, int roleId)
        //{
        //    try
        //    {


        //        if (roleId == 1)
        //        {


        //            var result = (from wi in _context.PtplSopWorkInstruction
        //                          join slog in _context.PtplSopLogger
        //                          on wi.Uid equals slog.SopwiId
        //                          join et in _context.PtplEventTriggerSchedule
        //                          on wi.Uid equals et.WiSopId
        //                          select new SopLoggerByUserDTO
        //                          {
        //                              SopDocId = slog.SopDocId,
        //                              SopwiId = slog.SopwiId,
        //                              SopwiName = wi.SopTemplateName,
        //                              SopName = slog.SopName,
        //                              SopDesc = slog.SopDesc,
        //                              FileName = wi.FileName,
        //                              SopNumber = wi.SopNumber + "-" + wi.SopTemplateName,
        //                              StartTime = slog.StartTime,
        //                              PlanFinishDate = slog.PlanFinishDate,
        //                              ActualFinishDate = slog.ActualFinishDate,
        //                              ComplianceStatus = slog.ComplianceStatus,
        //                              Status = slog.Status,
        //                              CreatedBy = (from us in _context.PtplUserMaster where us.Uid == slog.CreatedBy select us.UserName).FirstOrDefault(),
        //                              CreatedDate = slog.CreatedDate,
        //                              AssigneeId = et.Assignee,
        //                              AssigneeName = (from us in _context.PtplUserMaster where us.Uid == et.Assignee select us.UserName).FirstOrDefault()


        //                          }).ToList();

        //            return await Task.Run(() => result);
        //        }
        //        else
        //        {
        //            var result = (from wi in _context.PtplSopWorkInstruction
        //                          join slog in _context.PtplSopLogger
        //                          on wi.Uid equals slog.SopwiId
        //                          join et in _context.PtplEventTriggerSchedule
        //                          on wi.Uid equals et.WiSopId
        //                          where wi.CreatedBy == userId
        //                          select new SopLoggerByUserDTO
        //                          {
        //                              SopDocId = slog.SopDocId,
        //                              SopwiId = slog.SopwiId,
        //                              SopwiName = wi.SopTemplateName,
        //                              SopName = slog.SopName,
        //                              SopDesc = slog.SopDesc,
        //                              FileName = wi.FileName,
        //                              SopNumber = wi.SopNumber + "-" + wi.SopTemplateName,
        //                              StartTime = slog.StartTime,
        //                              PlanFinishDate = slog.PlanFinishDate,
        //                              ActualFinishDate = slog.ActualFinishDate,
        //                              ComplianceStatus = slog.ComplianceStatus,
        //                              Status = slog.Status,
        //                              CreatedBy = (from us in _context.PtplUserMaster where us.Uid == slog.CreatedBy select us.UserName).FirstOrDefault(),
        //                              CreatedDate = slog.CreatedDate,
        //                              AssigneeId = et.Assignee,
        //                              AssigneeName = (from us in _context.PtplUserMaster where us.Uid == et.Assignee select us.UserName).FirstOrDefault()


        //                          }).ToList();
        //            return await Task.Run(() => result);
        //        }




        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"It broke due to {ex.Message}");
        //        throw;
        //    }
        //}

        public async Task<List<SopLoggerByUserDTO>> getSopLoggerByUser(int userId, int roleId)
        {
            try
            {

               
                
                    var result = (from wi in _context.PtplSopWorkInstruction
                                  join slog in _context.PtplSopLogger
                                  on wi.Uid equals slog.SopwiId
                                  join et in _context.PtplEventTriggerSchedule
                                  on wi.Uid equals et.WiSopId
                                  where et.Assignee == userId && slog.Status != "Completed"
                                  select new SopLoggerByUserDTO
                                  {
                                      SopDocId = slog.SopDocId,
                                      SopwiId = slog.SopwiId,
                                      SopwiName = wi.SopTemplateName,
                                      SopName = slog.SopName,
                                      SopDesc = slog.SopDesc,
                                      FileName = wi.FileName,
                                      SopNumber = wi.SopNumber,
                                      StartTime = slog.StartTime,
                                      PlanFinishDate = slog.PlanFinishDate,
                                      EventType = (from ev in _context.PtplEventMaster where ev.Uid == et.EventType select ev.EventType).FirstOrDefault(),
                                      //ActualFinishDate = slog.ActualFinishDate,
                                      SopLoggerNumber = slog.SopLoggerNumber,
                                      ComplianceStatus = slog.ComplianceStatus,
                                      Status = slog.Status,
                                      CreatedBy = (from us in _context.PtplUserMaster where us.Uid == slog.CreatedBy select us.UserName).FirstOrDefault(),
                                      //CreatedDate = slog.CreatedDate,
                                      AssigneeId = et.Assignee,
                                      AssigneeName = (from us in _context.PtplUserMaster where us.Uid == et.Assignee select us.UserName).FirstOrDefault(),
                                      Category = (from us in _context.PtplCategoryMaster where us.Uid == wi.SopCategory select us.CategoryName).FirstOrDefault(),
                                      Area = (from us in _context.PtplAreaMaster where us.Uid == wi.AreaId select us.AreaName).FirstOrDefault(),
                                      Equipment = (from us in _context.PtplMachineMaster where us.Uid == slog.MachineId select us.MachineName).FirstOrDefault(),
                                      PendingDuration = CalculatePendingDurat(slog.StartTime, slog.PlanFinishDate)

                                  }).OrderByDescending(x => x.SopDocId).ToList();
                    return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }



        public static string CalculatePendingDurat(DateTime startDate, DateTime? planedFinishDate)
        {
            string result = "";
            try
            {
                //0d 
                var data = planedFinishDate - startDate;
                result = Math.Round(Convert.ToDecimal(data.Value.Days / 7)) + "w " + data.Value.Days + "d " + data.Value.Hours + "h " + data.Value.Minutes + "m";




            }
            catch (Exception ex)
            {
                //  _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
            return result;
        }

        #endregion Methods
    }
}
