﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;

namespace PTPL.SOP_System.Services
{
    public class TemplateService : ITemplateService
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<TemplateService> _logger;
         
        private static int count = 01;

        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="TemplateService" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{TemplateService}" /></param>

        public TemplateService(PTPL_SOPContext context, ILogger<TemplateService> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Service Method used to insert or update the template master
        /// </summary>
        /// <param name="templateMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<string>> addupdateTemplate(PtplSopWorkInstruction templateMaster,int[] machinIds)
        {
            int tempId=0;
            List<string> ids = new List<string>();
            try
            {
                //var PtplSopWimachine = new  PtplSopWimachine();
               
               
                if (templateMaster.Uid == 0)
                {
                    templateMaster.CreatedBy = 1;
                    templateMaster.CreationDate = DateTime.Now;
                    templateMaster.WiFlag = false;
                    string sopNumberVal1 = "SOP-WI" + "-";
                    string responseData, resultData = "";
                    var val = _context.PtplSopWorkInstruction.Count() + 1;
                    responseData = val.ToString();

                    if (responseData.Length.Equals(1))
                    {
                        resultData = "0000" + responseData;
                    }
                    if (responseData.Length.Equals(2))
                    {
                        resultData = "000" + responseData;
                    }
                    if (responseData.Length.Equals(3))
                    {
                        resultData = "00" + responseData;
                    }
                    if (responseData.Length.Equals(4))
                    {
                        resultData = "0" + responseData;
                    }
                    templateMaster.SopNumber = sopNumberVal1 + resultData;
                    _context.PtplSopWorkInstruction.Add(templateMaster);
                    _context.SaveChanges();
                     tempId = templateMaster.Uid;
                   
                    foreach (var id in machinIds)
                    {
                        var PtplSopWimachine = new PtplSopWimachine();
                        PtplSopWimachine.WiId = tempId;
                        PtplSopWimachine.MachineId = id;
                        _context.PtplSopWimachine.Add(PtplSopWimachine);
                        _context.SaveChanges();
                    }
                   
                    _logger.LogInformation("Record Inserted : " + templateMaster.Uid);
                }
                else
                {
                    tempId = templateMaster.Uid;
                    _context.PtplSopWorkInstruction.Update(templateMaster);
                    var oldmachine = _context.PtplSopWimachine.Where(x => x.WiId == tempId);
                    _context.PtplSopWimachine.RemoveRange(oldmachine);
                    _context.SaveChanges();

                    foreach (var id in machinIds)
                    {
                        var PtplSopWimachine = new PtplSopWimachine();
                        PtplSopWimachine.WiId = tempId;
                        PtplSopWimachine.MachineId = id;
                        _context.PtplSopWimachine.Update(PtplSopWimachine);
                        _context.SaveChanges();
                    }

                    _logger.LogInformation("Record Updated : " + templateMaster.Uid);
                }
                _context.SaveChanges();
                ids.Add(tempId.ToString());
                ids.Add(templateMaster.SopNumber);
                return await Task.Run(() => ids);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete Template 
        /// </summary>
        /// <param name="TemplateId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteTemplate(int TemplateId)
        {
            try
            {

                var Template = _context.PtplSopWorkInstruction.Find(TemplateId);
                if (Template != null)
                    _context.PtplSopWorkInstruction.Remove(Template);
                else
                {
                    _logger.LogInformation("Services: deleteTemplate : Template Not Found");
                    return await Task.Run(() => "Template Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + TemplateId);
                return await Task.Run(() => "Template Deleted Successfully" + TemplateId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Template data
        /// </summary>
        /// <returns>Returns list of template</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplSopWorkInstruction>> getAllTemplates()
        {
            try
            {
                return await Task.Run(() => _context.PtplSopWorkInstruction.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Template and Machine data
        /// </summary>
        /// <returns>Returns templateDTO</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<TemplateDTO>> getAllMachineTemplate()
        {
            try
            {
                var TempMachine = (from tp in _context.PtplSopWorkInstruction
                                   //join eq in _context.PtplEquipmentMaster
                                   //on tp.EquipmentType equals eq.Uid
                                   join Tm in _context.PtplSopWimachine
                                   on tp.Uid equals Tm.WiId
                                   join ct in _context.PtplCategoryMaster
                                   on tp.SopCategory equals ct.Uid
                                   join etype in _context.PtplEventMaster
                                   on tp.EventType equals etype.Uid
                                   //As per new requirement
                                   //join etri in _context.PtplEventTriggerMaster
                                   //on tp.EventTrigger equals etri.Uid
                                   join usr in _context.PtplUserMaster
                                   on tp.AssigneeId equals usr.Uid
                                   join ar in _context.PtplAreaMaster
                                   on tp.AreaId equals ar.Uid
                                   select new TemplateDTO
                                   {
                                       //EquipmentType = eq.EquipmentName,
                                       Uid = tp.Uid,
                                       TemplateName = tp.SopTemplateName,
                                       SopTemplateDesc=tp.SopTemplateDesc,
                                       FileName = tp.FileName,
                                       SopNumber = tp.SopNumber,
                                       Category = ct.CategoryName,
                                       OriginalEstimateTime=tp.OriginalEstimateTime,
                                       EventTypeId=tp.EventType,
                                       EventType=etype.EventType,
                                       AssigneeId=tp.AssigneeId,
                                       AssigneeName=usr.UserName,
                                       AreaId=tp.AreaId,
                                       AreaName=ar.AreaName,
                                      // FileStream=tp.FileStream,
                                       FilePath=tp.FilePath,
                                       FileSize=tp.FileSize,
                                       FileVersion=tp.FileVersion,
                                       MachineDTOs = (from mm in _context.PtplMachineMaster
                                                      select new MachineDTO
                                                      {
                                                          MachineId = mm.Uid,
                                                          MachineName = mm.MachineName,
                                                          IsUsed = (from tm in _context.PtplSopWimachine
                                                                    where tm.WiId == tp.Uid && tm.MachineId == mm.Uid
                                                                    select true).FirstOrDefault()

                                                      }).ToList()

                                   }).Distinct().ToList();

                return await Task.Run(() => TempMachine);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to fetch File Document
        /// </summary>
        /// <param name="sopInfo"></param>
        /// <returns>Returns object of document</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<object> getDocumentByFileName(string fileName)
        {
            try
            {
                var fileData = (
                               from tm in _context.PtplSopWorkInstruction
                               where tm.FileName == fileName
                                   
                               select
                                  new
                                  { 
                                      FileName=tm.FileName,
                                      FileSize=tm.FileSize,
                                      FileStream=Convert.ToBase64String(tm.FileStream==null?new byte[0]: tm.FileStream) }).FirstOrDefault();
                return await Task.Run(() => fileData);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }

        }

        /// <summary>
        /// Service Method used get sopNumber
        /// </summary>
        /// <param name="equipmentId"></param>
        /// <returns>Returns sopNumber</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> getSopNoByEquipmentId(int machineId)
        {
            try
            {
                string result = "";
                if (_context.PtplSopWorkInstruction.Where(x => x.MachineId == machineId) == null)
                {
                    result = _context.PtplSopWorkInstruction.Where(x => x.MachineId == machineId).OrderByDescending(x => x.Uid).FirstOrDefault().SopNumber;
                }
                else
                {
                    result = (from a in _context.PtplMachineMaster where a.Uid == machineId select a.MachineName).SingleOrDefault();
                    if (result == "Streching")
                    {
                        result = "STR-1";
                    }
                    else if (result == "Press")
                    {
                        result = "PR-1";
                    }
                    else
                        result = result + "-1";
                }
                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used get file name
        /// NOT IN USE
        /// </summary>
        /// <param name="equipmentId"></param>
        /// <returns>Returns file name</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<GetFileNameDTO>> getFileNameByEquipmentId(int equipmentId)
        {
            try
            {
                var result = (from tm in _context.PtplSopWorkInstruction
                             // where tm.EquipmentType == equipmentId
                              select new GetFileNameDTO
                              {
                                  Id = tm.Uid,
                                  fileName = tm.FileName
                              }).ToList();

                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }

        }



        public async Task<object> getFileStemeById(int templateMasterId)
        {
            try
            {
                var result = (from tm in _context.PtplSopWorkInstruction
                              where tm.Uid == templateMasterId
                              select new { FileStream = Convert.ToBase64String(tm.FileStream == null ? new byte[0] : tm.FileStream) }).FirstOrDefault();
            
                            

                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }

        }
        /// <summary>
        /// Service Method used get sopNumber
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns>Returns sopNumber</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> getLatestSopNo()
        {
            try
            {
                var result = (from temp in _context.PtplSopWorkInstruction
                              //as per new requirement by chetan
                              //where temp.EventType == eventId
                              orderby temp.Uid descending
                              select temp.SopNumber).FirstOrDefault();
                if (result != null)
                {
                    string[] str = result.Split("-");
                    return await Task.Run(() => str[2]);
                }
                else
                {
                    return await Task.Run(() => "00000");
                }
               
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used get sopNumber
        /// </summary>
        /// <param name="uid"></param>
        /// <returns>Returns sopNumber</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplSopWorkInstruction> getTemplatesById(int uid)
        {
            try
            {
                    return await Task.Run(() =>_context.PtplSopWorkInstruction.Find(uid));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }


        /// <summary>
        /// Service Method used get sopNumber
        /// </summary>
        /// <returns>Returns sopNumber</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<bool> getWIFlag()
        {
            try
            {
                var result= _context.PtplSopWorkInstruction.Where(x => x.WiFlag == false).FirstOrDefault();
                
                if (result == null)
                    return await Task.Run(() => true);
                else
                    return await Task.Run(() => false);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used get sopNumber
        /// </summary>
        /// <returns>Returns sopNumber</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> saveFormJson(PtplDepartmentMaster ptplDepartmentMaster)
        {
            try
            {
                // PtplDepartmentMaster obj = new PtplDepartmentMaster();
                if (ptplDepartmentMaster.DeptId == 0)
                {
                    ptplDepartmentMaster.CreatedBy = 1;
                    ptplDepartmentMaster.CreatedDate = DateTime.Now;
                    ptplDepartmentMaster.ModifiedBy = 1;
                    ptplDepartmentMaster.ModifiedDate = DateTime.Now;
                    var result = _context.PtplDepartmentMaster.Add(ptplDepartmentMaster);
                    return _context.SaveChanges(); 
                  
                }
                else
                {
                    var result = _context.PtplDepartmentMaster.Update(ptplDepartmentMaster);
                    return _context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        public async Task<PtplDepartmentMaster> getJsonFormData()
        {
            try
            {
                // PtplDepartmentMaster obj = new PtplDepartmentMaster();
                return await Task.Run(() => _context.PtplDepartmentMaster.Find(1));
            

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Methods
    }
}
