﻿using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
   public interface ITemplateService
    {
        Task<List<string>> addupdateTemplate(PtplSopWorkInstruction templateMaster,int[] machinIds);
        Task<string> deleteTemplate(int userId);
        Task<List<PtplSopWorkInstruction>> getAllTemplates();
        Task<List<TemplateDTO>> getAllMachineTemplate();
        Task<object> getDocumentByFileName(string fileName);
        Task<string> getSopNoByEquipmentId(int equipmentId);
        Task<List<GetFileNameDTO>> getFileNameByEquipmentId(int equipmentId);
        Task<object> getFileStemeById(int templateMasterId);
        Task<string> getLatestSopNo();
        Task<PtplSopWorkInstruction> getTemplatesById(int uid);
        Task<bool> getWIFlag();
        Task<int> saveFormJson(PtplDepartmentMaster ptplDepartmentMaster);
        Task<PtplDepartmentMaster> getJsonFormData();


    }
}
