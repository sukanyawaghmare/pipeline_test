﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class EquipmentServices:IEquipmentServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<EquipmentServices> _logger;
        #endregion Fields
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EquipmentServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{EquipmentServices}" /></param>

        public EquipmentServices(PTPL_SOPContext context, ILogger<EquipmentServices> logger)
        {
            _context = context;
            _logger = logger;
        }

        #endregion Constructors
        #region Methods
        /// <summary>
        /// Service Method used to insert or update the Equipment master
        /// </summary>
        /// <param name="EquipmentMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateEquipment(PtplEquipmentMaster EquipmentMaster)
        {

            try
            {
                if (EquipmentMaster.Uid == 0)
                {
                    EquipmentMaster.CreatedBy = 1;
                    EquipmentMaster.CreatedDate = DateTime.Now;
                    _context.PtplEquipmentMaster.Add(EquipmentMaster);
                    _logger.LogInformation("Record Inserted : " + EquipmentMaster.Uid);
                }
                else
                {
                    _context.PtplEquipmentMaster.Update(EquipmentMaster);
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + EquipmentMaster.Uid);
                }
                _context.SaveChanges();
                return await Task.Run(() => EquipmentMaster.Uid);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete Equipment 
        /// </summary>
        /// <param name="EquipmentId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteEquipment(int EquipmentId)
        {
            try
            {

                var Equipment = _context.PtplEquipmentMaster.Find(EquipmentId);
                if (Equipment != null)
                    _context.PtplEquipmentMaster.Remove(Equipment);
                else
                {
                    _logger.LogInformation("Services: deleteEquipment : Equipment Not Found");
                    return await Task.Run(() => "Equipment Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + EquipmentId);
                return await Task.Run(() => "Equipment Deleted Successfully" + EquipmentId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Equipment data
        /// </summary>
        /// <returns>Returns list of equipment master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplEquipmentMaster>> getAllEquipments()
        {
            try
            {
                return await Task.Run(() => _context.PtplEquipmentMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Equipment data by id
        /// </summary>
        /// <param name="EquipmentId"></param>
        /// <returns>Returns equipment master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplEquipmentMaster> getEquipmentById(int EquipmentId)
        {
            try
            {
                return await Task.Run(() => _context.PtplEquipmentMaster.Find(EquipmentId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Methods
    }
}
