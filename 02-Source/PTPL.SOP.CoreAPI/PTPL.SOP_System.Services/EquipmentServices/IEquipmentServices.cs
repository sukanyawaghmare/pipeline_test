﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IEquipmentServices
    {
        Task<int> addupdateEquipment(PtplEquipmentMaster equipmentMaster);
        Task<string> deleteEquipment(int userId);
        Task<List<PtplEquipmentMaster>> getAllEquipments();
        Task<PtplEquipmentMaster> getEquipmentById(int machineId);
    }
}
