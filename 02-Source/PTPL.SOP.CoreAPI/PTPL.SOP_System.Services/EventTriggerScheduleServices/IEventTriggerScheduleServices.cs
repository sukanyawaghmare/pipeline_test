﻿using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IEventTriggerScheduleServices
    {
        Task<int> addupdateEventTriggerSchedule(PtplEventTriggerSchedule eventTriggerSchedule);
        Task<List<GetEventTriggerScheduleDTO>> getAllEventTriggerSchedule();
        Task<List<GetEventTriggerScheduleDTO>> getAllEventTriggerScheduleByWIId(int WiSopId);

        Task<List<GetEventTriggerScheduleDTO>> getAllEventTriggerScheduleWithoutPropertyNameByWIId(int WiSopId);
        Task<int?> getEventTriScheTemplateByWIId(int WiSopId,int EventId);
        
        Task<string> deleteEventTriggerSchedule(int eventTriggerScheduleId);
        
    }
}
