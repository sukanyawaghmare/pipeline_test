﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class EventTriggerScheduleServices : IEventTriggerScheduleServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<EventTriggerScheduleServices> _logger;

        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventTriggerScheduleServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{EventTriggerScheduleServices}" /></param>

        public EventTriggerScheduleServices(PTPL_SOPContext context, ILogger<EventTriggerScheduleServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Service Method used to insert or update the eventTriggerSchedule
        /// </summary>
        /// <param name="eventTriggerSchedule"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateEventTriggerSchedule(PtplEventTriggerSchedule eventTriggerSchedule)
        {
            try
            {
                //var ptplEventTriggerScheduleMachine = new  PtplEventTriggerScheduleMachine();


                if (eventTriggerSchedule.EventTriggerId == 0)
                {
                    eventTriggerSchedule.CreatedBy = 1;
                    eventTriggerSchedule.CreatedDate = DateTime.Now;
                    eventTriggerSchedule.EventDateTime = eventTriggerSchedule.StartTime;
                    // eventTriggerSchedule.StartTime = TimeZoneInfo.ConvertTimeToUtc(eventTriggerSchedule.StartTime, TimeZoneInfo.Local);

                    _context.PtplEventTriggerSchedule.Add(eventTriggerSchedule);
                    _context.SaveChanges();

                    _logger.LogInformation("Record Inserted : " + eventTriggerSchedule.EventTriggerId);
                }
                else
                {
                    eventTriggerSchedule.CreatedBy = 1;
                    eventTriggerSchedule.CreatedDate = DateTime.Now;
                    eventTriggerSchedule.EventDateTime = eventTriggerSchedule.StartTime;

                    _context.PtplEventTriggerSchedule.Update(eventTriggerSchedule);
                    _context.SaveChanges();

                    _logger.LogInformation("Record Updated : " + eventTriggerSchedule.EventTriggerId);
                }

                return await Task.Run(() => eventTriggerSchedule.EventTriggerId);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to fetch EventTriggerSchedule data by sopWI Id
        /// </summary>
        /// <returns>Returns list of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<GetEventTriggerScheduleDTO>> getAllEventTriggerSchedule()
        {
            try
            {
                var result = (from ev in _context.PtplEventTriggerSchedule
                              join usr in _context.PtplUserMaster
                              on ev.Assignee equals usr.Uid
                              join et in _context.PtplEventMaster
                              on ev.EventType equals et.Uid
                              join mm in _context.PtplMachineMaster
                              on ev.MachineId equals mm.Uid
                              join occ in _context.PtplEventOccurrence
                              on ev.Occurance equals occ.OccuranceId
                              join tm in _context.PtplSopWorkInstruction
                              on ev.WiSopId equals tm.Uid
                              select new GetEventTriggerScheduleDTO
                              {
                                  EventTriggerId = ev.EventTriggerId,
                                  WiId=tm.Uid,
                                  WiSopId = tm.SopTemplateName,
                                  SopNumber=tm.SopNumber,
                                  EventType = et.EventType,
                                  Title = ev.Title,
                                  StartTime = ev.StartTime,
                                  EndTime = ev.EndTime,
                                  Occurance = occ.OccurrenceName,
                                  Machine = mm.MachineName,
                                  Assignee = usr.UserName ,
                                  Condition = ev.Condition,
                                  ExternalEvents = ev.ExternalEvents,
                                  TimeValue = ev.TimeValue,
                                  Status = ev.Status
                              }).ToList();
                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to fetch EventTriggerSchedule data by sopWI Id
        /// </summary>
        /// <param name="sopWIId"></param>
        /// <returns>Returns list of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<GetEventTriggerScheduleDTO>> getAllEventTriggerScheduleByWIId(int WiSopId)
        {
            try
            {
                var result = (from ev in _context.PtplEventTriggerSchedule
                              join usr in _context.PtplUserMaster
                              on ev.Assignee equals usr.Uid
                              join et in _context.PtplEventMaster
                              on ev.EventType equals et.Uid
                              join mm in _context.PtplMachineMaster
                              on ev.MachineId equals mm.Uid
                              join occ in _context.PtplEventOccurrence
                              on ev.Occurance equals occ.OccuranceId
                              join tm in _context.PtplSopWorkInstruction
                              on ev.WiSopId equals tm.Uid
                              where ev.WiSopId == WiSopId
                              select new GetEventTriggerScheduleDTO
                              {
                                  EventTriggerId = ev.EventTriggerId,
                                  WiSopId = tm.SopTemplateName,
                                  EventType = et.EventType,
                                  Title = ev.Title,
                                  StartTime = ev.StartTime,
                                  EndTime = ev.EndTime,
                                  Occurance = occ.OccurrenceName,
                                  Machine = mm.MachineName,
                                  Assignee = usr.UserName,
                                  Condition = ev.Condition,
                                  ExternalEvents = ev.ExternalEvents,
                                  TimeValue = ev.TimeValue,
                                  Status = ev.Status
                              }).ToList();
                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete EventTriggerSchedule 
        /// </summary>
        /// <param name="EventTriggerScheduleId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteEventTriggerSchedule(int EventTriggerScheduleId)
        {
            try
            {

                var EventTriggerSchedule = _context.PtplEventTriggerSchedule.Find(EventTriggerScheduleId);
                if (EventTriggerSchedule != null)
                    _context.PtplEventTriggerSchedule.Remove(EventTriggerSchedule);
                else
                {
                    _logger.LogInformation("Services: deleteEventTriggerSchedule : EventTriggerSchedule Not Found");
                    return await Task.Run(() => "EventTriggerSchedule Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + EventTriggerScheduleId);
                return await Task.Run(() => "EventTriggerSchedule Deleted Successfully " + EventTriggerScheduleId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch EventTriggerSchedule data by sopWI Id
        /// </summary>
        /// <param name="sopWIId"></param>
        /// <returns>Returns list of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int?> getEventTriScheTemplateByWIId(int WiSopId, int EventId)
        {
            try
            {

                var result = (from tm in _context.PtplSopWorkInstruction
                              join ev in _context.PtplEventTriggerSchedule
                              on tm.Uid equals ev.WiSopId
                              where tm.Uid == WiSopId && ev.EventTriggerId == EventId
                              select new PtplSopLogger
                              {
                                  SopDocId = 0,
                                  SopName = ev.Title,
                                  SopDesc = "Auto generated event trigger rule",
                                  EventType = tm.EventType,
                                  SopCategory = tm.SopCategory,
                                  MachineId = ev.MachineId,
                                  SopwiId = tm.Uid,
                                  StartTime = DateTime.Now,
                                  PlanFinishDate = calculateEstimationtime(tm.OriginalEstimateTime),
                                  AreaId = (from mm in _context.PtplMachineMaster where mm.Uid == ev.MachineId select mm.AreaId).FirstOrDefault(),
                                  Assignee = ev.Assignee,
                                  //ActualFinishDate = DateTime.Now,
                                  ComplianceStatus = "In-progress",
                                  Status = "In-progress",
                                  CreatedBy = 5,
                                  CreatedDate = DateTime.Now

                              }).FirstOrDefault();
                string sopNumberVal1 = "SOP-LOG" + "-";
                string responseData, resultData = "";
                var val = _context.PtplSopLogger.Count() + 1;
                responseData = val.ToString();

                if (responseData.Length.Equals(1))
                {
                    resultData = "0000" + responseData;
                }
                if (responseData.Length.Equals(2))
                {
                    resultData = "000" + responseData;
                }
                if (responseData.Length.Equals(3))
                {
                    resultData = "00" + responseData;
                }
                if (responseData.Length.Equals(4))
                {
                    resultData = "0" + responseData;
                }
                result.SopLoggerNumber = sopNumberVal1 + resultData;

                _context.PtplSopLogger.Add(result);
                _context.SaveChanges();
                // This lines of code is use to send an email notification to asigned user after creating an SOP logger object.
                var EmailId = _context.PtplUserMaster.Find(result.Assignee).Email;
                var mailconfig = _context.PtplConfigureEmail.FirstOrDefault();

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(mailconfig.HostName);

                mail.From = new MailAddress(mailconfig.From);
                mail.To.Add(EmailId);
                //New task - ID: 298 - SL2 - Task to complete SOP for Biax 2 Pre-heat
                mail.Subject = "New task - ID: " + result.SopDocId + " - " + _context.PtplMachineMaster.Find(result.MachineId).MachineName + " – Task to complete SOP for " + _context.PtplSopWorkInstruction.Find(result.SopwiId).SopTemplateName;
                var dueDate = result.PlanFinishDate;
                mail.IsBodyHtml = true;

                StringBuilder strHTMLBody = new StringBuilder();
                strHTMLBody.Append("<html>");
                strHTMLBody.Append("    <body>");
                strHTMLBody.Append("        <table style='text-align:left; font-family:calibri'>");
                strHTMLBody.Append("            <tr>");
                strHTMLBody.Append("                <th>");
                strHTMLBody.Append("                    Current Task");
                strHTMLBody.Append("                </th>");
                strHTMLBody.Append("                <th>:</th>");
                strHTMLBody.Append("                <td>New task to complete SOP</td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("            <tr>"); 
                strHTMLBody.Append("                <th>SOP Logger</th>");
                strHTMLBody.Append("                <th>:</th>");
                strHTMLBody.Append("                <td>"+result.SopDocId+"</td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("            <tr>");
                strHTMLBody.Append("            <th>SOP WI</th>");
                strHTMLBody.Append("                <th>:</th>");
                strHTMLBody.Append("                <td>" + _context.PtplSopWorkInstruction.Find(result.SopwiId).SopTemplateName + "</td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("            <tr>");
                strHTMLBody.Append("                <th>Description</th>");
                strHTMLBody.Append("                <th>:</th>");
                strHTMLBody.Append("                <td>" + result.SopDesc + "</td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("            <tr>");
                strHTMLBody.Append("                <th>Due Date</th>");
                strHTMLBody.Append("                <th>:</th>");
                strHTMLBody.Append("                <td>" + dueDate.ToString("dd MMMM yyyy h:mm tt") + "</td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("            <tr>");
                strHTMLBody.Append("                 <td style='height:50'></td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("            <tr>");
                strHTMLBody.Append("                <td colspan='3'>This email is auto generated and sent from VisiSOP Application</td>");
                strHTMLBody.Append("            </tr>");
                strHTMLBody.Append("          </table>");
                strHTMLBody.Append("      </body>");
                strHTMLBody.Append(" </html>");

                //mail.Body = "<div> <b> Current Task:</b> New task to complete SOP</br > " +
                //    "<b>SOP Logger:</b> " + result.SopDocId + "</br>" +
                //    "<b>SOP WI:</b> " + _context.PtplSopWorkInstruction.Find(result.SopwiId).SopTemplateName + "</br>" +
                //    "<b>Description:</b> " + result.SopDesc + "</br>" +
                //     "<b>Due Date:</b> " + dueDate.ToString("dd MMMM yyyy h:mm tt") + "</br>" +
                //     "</br></br></br>This email is auto generated and sent from VisiSOP Application" +
                //         "</div>";
                //SmtpServer.Port = mailconfig.Port;
                mail.Body = strHTMLBody.ToString();
                SmtpServer.Credentials = new System.Net.NetworkCredential(mailconfig.UserName, mailconfig.Password);
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

                var eventData = _context.PtplEventTriggerSchedule.Find(EventId);
                if (eventData != null)
                {
                    int occurence = eventData.Occurance;

                    switch (occurence)
                    {
                        case 1:
                            eventData.EventDateTime = eventData.EventDateTime;
                            break;
                        case 2:
                            eventData.EventDateTime = eventData.EventDateTime.AddHours(1);
                            break;
                        case 3:
                            eventData.EventDateTime = eventData.EventDateTime.AddDays(1);
                            break;
                        case 4:
                            eventData.EventDateTime = eventData.EventDateTime.AddDays(7);
                            break;
                        case 5:
                            eventData.EventDateTime = eventData.EventDateTime.AddMonths(1);
                            break;
                        case 6:
                            eventData.EventDateTime = eventData.EventDateTime.AddYears(1);
                            break;
                        default:
                            Console.WriteLine("Default case");
                            break;
                    }
                    _context.PtplEventTriggerSchedule.Update(eventData);
                    _context.SaveChanges();

                }

                return await Task.Run(() => result.SopDocId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }



        private static DateTime calculateEstimationtime(string estimate)
        {
            DateTime returnValue = DateTime.Now;
            double weeks = 0, addweeks = 0, days = 0, hours = 0, min = 0;
            try
            {
                string[] eList = estimate.Split(" ");
                foreach (var element in eList)
                {
                    if (element.Contains("w"))
                    {
                        weeks = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        addweeks = 7 * weeks;
                        returnValue = returnValue.AddDays(addweeks);
                    }
                    if (element.Contains("d"))
                    {
                        days = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddDays(days);
                    }
                    if (element.Contains("h"))
                    {
                        hours = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddHours(hours);
                    }
                    if (element.Contains("m"))
                    {
                        min = Convert.ToDouble(Regex.Replace(element, "[^0-9]+", string.Empty));
                        returnValue = returnValue.AddMinutes(min);
                    }
                }



                Console.WriteLine(returnValue);



            }
            catch (Exception ex)
            {
                // log you error message here
            }
            return returnValue;

        }

        /// <summary>
        /// Service Method used to fetch EventTriggerSchedule data without joining table by sopWI Id
        /// </summary>
        /// <param name="sopWIId"></param>
        /// <returns>Returns list of EventTriggerSchedule</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<GetEventTriggerScheduleDTO>> getAllEventTriggerScheduleWithoutPropertyNameByWIId(int WiSopId)
        {
            try
            {
                var result = (from a in _context.PtplEventTriggerSchedule
                              where a.WiSopId == WiSopId
                              select new GetEventTriggerScheduleDTO
                              {
                                  EventTriggerId = a.EventTriggerId,
                                  WiSopId = a.WiSopId.ToString(),
                                  EventType = a.EventType.ToString(),
                                  Title = a.Title,
                                  StartTime = a.StartTime,
                                  EndTime = a.EndTime,
                                  Occurance = a.Occurance.ToString(),
                                  Machine = a.MachineId.ToString(),
                                  Assignee = a.Assignee.ToString(),
                                  Condition = a.Condition,
                                  ExternalEvents = a.ExternalEvents,
                                  TimeValue = a.TimeValue,
                                  Status = a.Status
                              }).ToList();
                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }


        #endregion Method
    }
}

