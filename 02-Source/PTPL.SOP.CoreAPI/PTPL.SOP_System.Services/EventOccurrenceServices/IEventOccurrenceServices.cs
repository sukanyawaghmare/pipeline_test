﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IEventOccurrenceServices
    {
        Task<List<PtplEventOccurrence>> getAllEventOccurrence();
    }
}
