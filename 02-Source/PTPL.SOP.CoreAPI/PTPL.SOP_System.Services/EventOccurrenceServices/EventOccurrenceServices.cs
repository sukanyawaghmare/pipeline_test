﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class EventOccurrenceServices:IEventOccurrenceServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<EventOccurrenceServices> _logger;


        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventOccurrenceServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{EventOccurrenceServices}" /></param>

        public EventOccurrenceServices(PTPL_SOPContext context, ILogger<EventOccurrenceServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors

        /// <summary>
        /// Service Method used to fetch EventOccurrence data
        /// </summary>
        /// <returns>Returns list of EventOccurrence</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplEventOccurrence>> getAllEventOccurrence()
        {
            try
            {
                return await Task.Run(() => _context.PtplEventOccurrence.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
    }
}
