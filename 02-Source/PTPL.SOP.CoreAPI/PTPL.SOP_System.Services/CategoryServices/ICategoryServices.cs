﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
   public interface ICategoryServices
    {
        Task<int> addupdateCategory(PtplCategoryMaster categoryMaster);
        Task<string> deleteCategory(int userId);
        Task<List<PtplCategoryMaster>> getAllCategories();
        Task<PtplCategoryMaster> getCategoryById(int machineId);
    }
}
