﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;

namespace PTPL.SOP_System.Services
{
    public class CategoryServices : ICategoryServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<CategoryServices> _logger;
        #endregion Fiels
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{CategoryServices}" /></param>
        public CategoryServices(PTPL_SOPContext context, ILogger<CategoryServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion  Constructors
        #region Methods
        /// <summary>
        /// Service Method used to insert or update the Category master
        /// </summary>
        /// <param name="categoryMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateCategory(PtplCategoryMaster categoryMaster)
        {

            try
            {
                if (categoryMaster.Uid == 0)
                {
                    categoryMaster.CreatedBy = 1;
                    categoryMaster.CreationDate = DateTime.Now;
                    _context.PtplCategoryMaster.Add(categoryMaster);
                    _logger.LogInformation("Record Inserted : " + categoryMaster.Uid);
                }
                else
                {
                    _context.PtplCategoryMaster.Update(categoryMaster);
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + categoryMaster.Uid);
                }
                _context.SaveChanges();
                return await Task.Run(() => categoryMaster.Uid);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete Category 
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteCategory(int CategoryId)
        {
            try
            {

                var Category = _context.PtplCategoryMaster.Find(CategoryId);
                if (Category != null)
                    _context.PtplCategoryMaster.Remove(Category);
                else
                {
                    _logger.LogInformation("Services: deleteCategory : Category Not Found");
                    return await Task.Run(() => "Category Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + CategoryId);
                return await Task.Run(() => "Category Deleted Successfully" + CategoryId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Category data
        /// </summary>
        /// <returns>Returns cotegory master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplCategoryMaster>> getAllCategories()
        {
            try
            {
                return await Task.Run(() => _context.PtplCategoryMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Category data by id
        /// </summary>
        /// <param name="CategoryId"></param>
        /// <returns>Returns category master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplCategoryMaster> getCategoryById(int CategoryId)
        {
            try
            {
                return await Task.Run(() => _context.PtplCategoryMaster.Find(CategoryId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Methods
    }
}
