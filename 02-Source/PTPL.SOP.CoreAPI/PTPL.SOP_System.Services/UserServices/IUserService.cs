﻿using PTPL.SOP_System.DataEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    /// <summary>
    /// This BusinessService Interface class file is having all the User Management related Declarations
    /// </summary>
    /// <remarks>Code documentation and Review is pending</remarks>
    public interface IUserService
    {
         Task<int> addUpdateUser(PtplUserMaster userMasterDTO);
         Task<int> editUser(PtplUserMaster userMasterDTO);
         Task<string> deleteUser(int userId);
         Task<List<PtplUserMaster>> getAllUsers();
         Task<PtplUserMaster> Authenticate(string username,string password);
        Task<PtplUserMaster> getUserById(int userId);
        Task<List<PtplUserMaster>> getAssigneeUser();
        Task<int> sentMailToUserByUId(int userId);
        Task<PtplUserMaster> GetUserPassword(int userid);
        Task<string> ChangeUserPass(PtplUserMaster userInfo);
    }
}
