﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using PTPL.SOP_System.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{

    public class UserService : IUserService
    {
        #region Fielda
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<UserService> _logger;

        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="UserService" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{UserService}" /></param>

        public UserService(PTPL_SOPContext context, ILogger<UserService> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors

        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");



            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
        /// <summary>
        /// Service Method used to insert user data
        /// </summary>
        /// <param name="userMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        /// 


        public async Task<int> addUpdateUser(PtplUserMaster userMaster)
        {
            try
            {
                var userData = _context.PtplUserMaster.Where(x => x.UserName == userMaster.UserName).FirstOrDefault();
                if (userMaster.Uid == 0)
                {
                    if (userData == null)
                        _context.PtplUserMaster.Add(userMaster);
                    else
                        return 0;
                }
                else
                {
                    var user = _context.PtplUserMaster.Find(userMaster.Uid);
                    user.UserName = userMaster.UserName;
                    user.FirstName = userMaster.FirstName;
                    user.LastName = userMaster.LastName;
                    user.Email = userMaster.Email;
                    user.ContactNumber = userMaster.ContactNumber;
                    user.ProfilePicture = userMaster.ProfilePicture;
                    user.IsActive = userMaster.IsActive;
                    user.Location = userMaster.Location;
                    user.Role = userMaster.Role;
                    //user.Password = userMaster.Password;
                    user.ModifiedDate = DateTime.Now;
                    _context.PtplUserMaster.Update(user);
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Inserted : " + userMaster.Uid);
                return await Task.Run(() => userMaster.Uid);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        
        /// <summary>
        /// Service Method used to Edit user data
        /// </summary>
        /// <param name="userMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> editUser(PtplUserMaster userMaster)
        {
            try
            {
                _context.PtplUserMaster.Update(userMaster);
                _context.SaveChanges();
                _logger.LogInformation("Record Updated : " + userMaster.Uid);
                return await Task.Run(() => userMaster.UserId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to Delete user 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteUser(int userId)
        {
            try
            {

                 var user= _context.PtplUserMaster.Find(userId);
                 user.IsActive = false;
                if (user != null)
                    _context.PtplUserMaster.Update(user);
                else
                {
                    _logger.LogInformation("Services: deleteUser : User Not Found" );
                    return await Task.Run(() => "User Not Found") ;
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + userId);
                return await Task.Run(() => "User Deleted Successfully" + userId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch user data
        /// </summary>
        /// <returns>Returns user list</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplUserMaster>> getAllUsers()
        {
            try
            {
                //return await Task.Run(() => _context.PtplUserMaster.Where(x=>x.IsActive==true).ToList()); 
                return await Task.Run(() => _context.PtplUserMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to validate user credentials
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>Returns user master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplUserMaster> Authenticate(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.PtplUserMaster.SingleOrDefault(x => x.UserName == userName && x.IsActive==true);
            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return await Task.Run(()=> user);
        }

        /// <summary>
        /// THis Method use for vrefying password Hash.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="storedHash"></param>
        /// <param name="storedSalt"></param>
        /// <returns>boolean</returns>
        /// <param name="password"></param>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");


            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }


            return true;
        }


        /// <summary>
        /// Service Method used to fetch User data by id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Returns user details</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplUserMaster> getUserById(int UserId)
        {
            try
            {
                var result = (from user in _context.PtplUserMaster
                              where user.Uid == UserId && user.IsActive == true
                              select user).SingleOrDefault();
                return await Task.Run(() => result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to fetch User data by id
        /// </summary>
        /// <returns>Returns user details</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplUserMaster>> getAssigneeUser()
        {
            try
            {
                var UserList = (from user in _context.PtplUserMaster where user.IsActive== true
                                select user).ToList();
                return await Task.Run(() => UserList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }


        /// <summary>
        /// Service Method used to send mail to  User by id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Returns user details</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> sentMailToUserByUId(int UserId)
        {
            try
            {
                
                var EmailId = _context.PtplUserMaster.Find(UserId).Email;
                var mailconfig = _context.PtplConfigureEmail.FirstOrDefault();

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(mailconfig.HostName);

                mail.From = new MailAddress(mailconfig.From);
                mail.To.Add(EmailId);
                mail.Subject = "New task: SOP Logger::ID – Task to complete SOP for %SOP WI Title; %Equipment"; //mailconfig.Subject;
                mail.Body = mailconfig.Subject;
                SmtpServer.Port = mailconfig.Port;
                SmtpServer.Credentials = new System.Net.NetworkCredential(mailconfig.UserName, mailconfig.Password);
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

                _logger.LogError("mail send to" + EmailId);
                return await Task.Run(() => UserId);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        public async Task<PtplUserMaster> GetUserPassword(int userid)
        {
            var user = _context.PtplUserMaster.Where(usr => usr.Uid == userid)
                .Select(usr => new PtplUserMaster
              {
                  PasswordHash = usr.PasswordHash,
                  PasswordSalt = usr.PasswordSalt,
                  UserId = usr.UserId
              }).FirstOrDefault();

            return await Task.Run(() => user); 

        }
        public async Task<string> ChangeUserPass(PtplUserMaster userInfo)
        {
            try
            {
                var user = _context.PtplUserMaster.Find(userInfo.Uid);
                user.PasswordHash = userInfo.PasswordHash;
                user.PasswordSalt = userInfo.PasswordSalt;
                user.Password = userInfo.Password;
                _context.PtplUserMaster.Update(user);
                _context.SaveChanges();
                return await Task.Run(() => "Password changed successfully");
            }catch(Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
            
        }
    }
}
