﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IMachineServices
    {
        Task<int> addupdateMachine(PtplMachineMaster machineMaster);
        Task<string> deleteMachine(int machineId);
        Task<List<PtplMachineMaster>> getAllMachines();
        Task<PtplMachineMaster> getMachineById(int machineId);

        Task<List<PtplMachineMaster>> getMachineByAreaId(int AreaId);
    }
}
