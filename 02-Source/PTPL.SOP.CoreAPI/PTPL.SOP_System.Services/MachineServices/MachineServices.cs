﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class MachineServices:IMachineServices
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<MachineServices> _logger;

        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="MachineServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{MachineServices}" /></param>
        public MachineServices(PTPL_SOPContext context, ILogger<MachineServices> logger)
        {
            _context = context;
            _logger = logger;
        }

        #endregion Constructors
        #region Methods
        /// <summary>
        /// Service Method used to insert or update the Machine master
        /// </summary>
        /// <param name="MachineMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateMachine(PtplMachineMaster MachineMaster)
        {

            try
            {
                if (MachineMaster.Uid == 0)
                {
                    MachineMaster.CreatedBy = 1;
                    MachineMaster.CreatedDate = DateTime.Now;
                    _context.PtplMachineMaster.Add(MachineMaster);
                    _logger.LogInformation("Record Inserted : " + MachineMaster.Uid);
                }
                else
                {
                    _context.PtplMachineMaster.Update(MachineMaster);
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + MachineMaster.Uid);
                }
                _context.SaveChanges();
                return await Task.Run(() => MachineMaster.Uid);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete Machine 
        /// </summary>
        /// <param name="MachineId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteMachine(int MachineId)
        {
            try
            {

                var Machine = _context.PtplMachineMaster.Find(MachineId);
                if (Machine != null)
                    _context.PtplMachineMaster.Remove(Machine);
                else
                {
                    _logger.LogInformation("Services: deleteMachine : Machine Not Found");
                    return await Task.Run(() => "Machine Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + MachineId);
                return await Task.Run(() => "Machine Deleted Successfully" + MachineId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch Machine data
        /// </summary>
        /// <returns>Returns list of machine</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplMachineMaster>> getAllMachines()
        {
            try
            {
                return await Task.Run(() => _context.PtplMachineMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to fetch Machine data by id
        /// </summary>
        /// <param name="machineId"></param>
        /// <returns>Returns machine master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplMachineMaster> getMachineById(int machineId)
        {
            try
            {
                return await Task.Run(() => _context.PtplMachineMaster.Find(machineId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        
        /// <summary>
        /// Service Method used to fetch Machine data by id
        /// </summary>
        /// <param name="areaId"></param>
        /// <returns>Returns machine master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplMachineMaster>> getMachineByAreaId(int areaId)
        {
            try
            {
                var machineList = (from mm in _context.PtplMachineMaster
                                   join am in _context.PtplAreaMaster on mm.AreaId equals am.Uid
                                   where mm.AreaId == areaId
                                   select mm).ToList();
                return await Task.Run(() => machineList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Methods
    }
}
