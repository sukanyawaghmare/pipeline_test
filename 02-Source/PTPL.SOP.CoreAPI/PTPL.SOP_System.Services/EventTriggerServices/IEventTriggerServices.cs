﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IEventTriggerServices
    {
        Task<int> addupdateEventTrigger(PtplEventTriggerMaster EventTriggerMaster);
        Task<string> deleteEventTrigger(int userId);
        Task<List<PtplEventTriggerMaster>> getAllEventTrigger();
        Task<PtplEventTriggerMaster> getEventTriggerById(int machineId);
    }
}
