﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
   public class EventTriggerServices:IEventTriggerServices
    {
        #region Fields  
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<EventTriggerServices> _logger;
        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventTriggerServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{EquipmentServices}" /></param>
        public EventTriggerServices(PTPL_SOPContext context, ILogger<EventTriggerServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors
        #region Methods

        /// <summary>
        /// Service Method used to insert or update the EventTrigger master
        /// </summary>
        /// <param name="EventTriggerMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<int> addupdateEventTrigger(PtplEventTriggerMaster EventTriggerMaster)
        {

            try
            {
                if (EventTriggerMaster.Uid == 0)
                {
                    EventTriggerMaster.CreatedBy = 1;
                    EventTriggerMaster.CreationDate = DateTime.Now;
                    _context.PtplEventTriggerMaster.Add(EventTriggerMaster);
                    _logger.LogInformation("Record Inserted : " + EventTriggerMaster.Uid);
                }
                else
                {
                    _context.PtplEventTriggerMaster.Update(EventTriggerMaster);
                    _context.SaveChanges();
                    _logger.LogInformation("Record Updated : " + EventTriggerMaster.Uid);
                }
                _context.SaveChanges();
                return await Task.Run(() => EventTriggerMaster.Uid);

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete EventTrigger 
        /// </summary>
        /// <param name="EventTriggerId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteEventTrigger(int EventTriggerId)
        {
            try
            {

                var EventTrigger = _context.PtplEventTriggerMaster.Find(EventTriggerId);
                if (EventTrigger != null)
                    _context.PtplEventTriggerMaster.Remove(EventTrigger);
                else
                {
                    _logger.LogInformation("Services: deleteEventTrigger : EventTrigger Not Found");
                    return await Task.Run(() => "EventTrigger Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + EventTriggerId);
                return await Task.Run(() => "EventTrigger Deleted Successfully" + EventTriggerId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch EventTrigger data
        /// </summary>
        public async Task<List<PtplEventTriggerMaster>> getAllEventTrigger()
        {
            try
            {
                return await Task.Run(() => _context.PtplEventTriggerMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch EventTrigger data by id
        /// </summary>
        /// <param name="EventTriggerId"></param>
        /// <returns>Returns event trigger master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplEventTriggerMaster> getEventTriggerById(int EventTriggerId)
        {
            try
            {
                return await Task.Run(() => _context.PtplEventTriggerMaster.Find(EventTriggerId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Methods
    }
}
