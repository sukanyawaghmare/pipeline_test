﻿using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface ILoggerAttachmentService
    {
        Task<List<int>> addupdateLoggerAttachment(List<PtplLoggerAttachment> loggerAttachment);
        Task<List<PtplLoggerAttachment>> getAllloggerAttachments();
        Task<string> deleteloggerAttachment(int AttachmentId);
        Task<string> deleteloggerAttachmentByDocId(int DocumentId);
        Task<PtplLoggerAttachment> getloggerAttachmentById(int AttachmentId);
        Task<List<PtplLoggerAttachment>> getloggerAttachmentByDocId(int DocId);

        
    }
}
