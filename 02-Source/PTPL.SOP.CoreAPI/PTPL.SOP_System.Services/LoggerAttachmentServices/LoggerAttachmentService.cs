﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class LoggerAttachmentService : ILoggerAttachmentService
    {
        #region Fields
        /// <summary>
        /// Defines the _context it is used to connect to the database
        /// </summary>
        private readonly PTPL_SOPContext _context;


        /// <summary>
        /// Private Logger is use to log error in the log files.
        /// </summary>
        private readonly ILogger<LoggerAttachmentService> _logger;

        private static int count = 01;

        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="LoggerAttachmentService" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{LoggerAttachmentService}" /></param>

        public LoggerAttachmentService(PTPL_SOPContext context, ILogger<LoggerAttachmentService> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors

        #region Methods

        /// <summary>
        /// Service Method used to insert or update the loggerAttachment master
        /// </summary>
        /// <param name="loggerAttachment"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<int>> addupdateLoggerAttachment(List<PtplLoggerAttachment> loggerAttachment)
        {

            try
            {

                if (loggerAttachment[0].AttachmentId == 0)
                {
                    _context.PtplLoggerAttachment.AddRange(loggerAttachment);
                    _context.SaveChanges();
                }
                else
                {
                    _context.PtplLoggerAttachment.UpdateRange(loggerAttachment);
                    _context.SaveChanges();
                }
                _logger.LogInformation("Record Updated");
                var temp = loggerAttachment.Select(x => x.AttachmentId);
                return await Task.Run(() => loggerAttachment.Select(x => x.AttachmentId).ToList());

            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to fetch loggerAttachment data
        /// </summary>
        /// <returns>Returns list of loggerAttachment</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplLoggerAttachment>> getAllloggerAttachments()
        {
            try
            {
                return await Task.Run(() => _context.PtplLoggerAttachment.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete loggerAttachment 
        /// </summary>
        /// <param name="loggerAttachmentId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteloggerAttachment(int AttachmentId)
        {
            try
            {

                var loggerAttachment = _context.PtplLoggerAttachment.Find(AttachmentId);
                if (loggerAttachment != null)
                    _context.PtplLoggerAttachment.Remove(loggerAttachment);
                else
                {
                    _logger.LogInformation("Services: deleteloggerAttachment : loggerAttachment Not Found");
                    return await Task.Run(() => "loggerAttachment Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + AttachmentId);
                return await Task.Run(() => "loggerAttachment Deleted Successfully" + AttachmentId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to Delete loggerAttachment 
        /// </summary>
        /// <param name="loggerAttachmentId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteloggerAttachmentByDocId(int DocumentId)
        {
            try
            {

                var loggerAttachment = _context.PtplLoggerAttachment.Where(x => x.DocumentId == DocumentId);
                if (loggerAttachment != null)
                    _context.PtplLoggerAttachment.RemoveRange(loggerAttachment);
                else
                {
                    _logger.LogInformation("Services: deleteloggerAttachment : loggerAttachment Not Found");
                    return await Task.Run(() => "loggerAttachment Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + DocumentId);
                return await Task.Run(() => "loggerAttachment Deleted Successfully" + DocumentId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete loggerAttachment 
        /// </summary>
        /// <param name="loggerAttachmentId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplLoggerAttachment> getloggerAttachmentById(int AttachmentId)
        {
            try
            {
                return await Task.Run(() => _context.PtplLoggerAttachment.Find(AttachmentId));
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Service Method used to Delete loggerAttachment 
        /// </summary>
        /// <param name="loggerAttachmentId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplLoggerAttachment>> getloggerAttachmentByDocId(int DocId)
        {
            try
            {
                return await Task.Run(() => _context.PtplLoggerAttachment.Where(x=>x.DocumentId==DocId).ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        #endregion Method

    }
}
