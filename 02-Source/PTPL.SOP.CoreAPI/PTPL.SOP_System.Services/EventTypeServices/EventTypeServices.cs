﻿using Microsoft.Extensions.Logging;
using PTPL.SOP_System.DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public class EventTypeServices : IEventTypeServices
    {
        #region Fields
        // <summary>
        // Defines the _context it is used to connect to the database
        // </summary>
        private readonly PTPL_SOPContext _context;


        // <summary>
        // Private Logger is use to log error in the log files.
        // </summary>
        private readonly ILogger<EventTypeServices> _logger;

        #endregion Fields
        #region Constructors
        /// <summary>
        ///     Initializes a new instance of the <see cref="EventTypeServices" /> class.
        /// </summary>
        /// <param name="context">The context<see cref="PTPL_SOPContext" /></param>
        /// <param name="logger">The logger<see cref="ILogger{EventTypeServices}" /></param>

        public EventTypeServices(PTPL_SOPContext context, ILogger<EventTypeServices> logger)
        {
            _context = context;
            _logger = logger;
        }
        #endregion Constructors
        #region Methods
        // <summary>
        // This service used to Add Event type.
        // </summary>
        /// <param name="SopEventMaster"></param>
        /// <returns>Returns int value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<int>> addEvents(List<PtplEventMaster> sopEventTypes)
        {
            try
            {
                _context.PtplEventMaster.AddRange(sopEventTypes);
                _context.SaveChanges();
                _logger.LogInformation("EventTypes Record Inserted : " + sopEventTypes.Select(a => a.Uid));
                return await Task.Run(() => sopEventTypes.Select(a => a.Uid).ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        /// <summary>
        /// Service Method used to Delete Event 
        /// </summary>
        /// <param name="EventId"></param>
        /// <returns>Returns string value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<string> deleteEvent(int EventId)
        {
            try
            {

                var Event = _context.PtplEventMaster.Find(EventId);
                if (Event != null)
                    _context.PtplEventMaster.Remove(Event);
                else
                {
                    _logger.LogInformation("Services: deleteEvent : Event Not Found");
                    return await Task.Run(() => "Event Not Found");
                }
                _context.SaveChanges();
                _logger.LogInformation("Record Deleted : " + EventId);
                return await Task.Run(() => "Event Deleted Successfully" + EventId);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        // <summary>
        // This service used Get All Event types.
        // </summary>
        /// <returns>Returns list of event master</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<List<PtplEventMaster>> getAllEventTypes()
        {
            try
            {
                return await Task.Run(() => _context.PtplEventMaster.ToList());
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }
        // <summary>
        // This service used Get EventTypes By ID.
        // </summary>
        /// <param name="EventTypeId"></param>
        /// <returns>Returns event master value</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-404"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Too many connection"></exception>
        /// <exception cref="Connection Timed Out"></exception>
        /// <exception cref="SqlConnection Error"></exception>
        ///  /// <exception cref="Sql parameter mismatch"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        public async Task<PtplEventMaster> getEventTypesById(int eventTypeId)
        {
            try
            {
                PtplEventMaster sopEventTypes = (from a in _context.PtplEventMaster where a.Uid == eventTypeId select a).SingleOrDefault();
                return await Task.Run(() => sopEventTypes);
            }
            catch (Exception ex)
            {
                _logger.LogError($"It broke due to {ex.Message}");
                throw;
            }
        }

        #endregion Methods
    }
}
