﻿using PTPL.SOP_System.DataEntities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PTPL.SOP_System.Services
{
    public interface IEventTypeServices
        {
        Task<List<int>> addEvents(List<PtplEventMaster> sopEventTypes);
        Task<string> deleteEvent(int eventTypeId);
        Task<List<PtplEventMaster>> getAllEventTypes();
        Task<PtplEventMaster> getEventTypesById(int eventTypeId);

    }
}
