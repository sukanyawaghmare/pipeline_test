﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;

namespace SwaggerUI
{
    public class SwaggerUI
    {
        /// <summary>
        /// This property is used to set/get the relevent api title
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// This property is used to set/get the relevent api version
        /// </summary>
        public string Version { get; set; }
        public string XMLPath { get; set; }
        public string Description { get; set; }
        public SwaggerUI(string title, string description, string xmlPath, string version)
        {
            XMLPath = xmlPath;
            Title = title;
            Version = version;
            Description = description;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Version, new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = Version,
                    Title = Title,
                    Description = Description
                });

                // Set the comments path for the Swagger JSON and UI.               
                //c.IncludeXmlComments(XMLPath);
            });
        }




        public void Configure(IApplicationBuilder app, string jsonPath)
        {
            if (jsonPath == "")
                jsonPath = "/swagger/v1/swagger.json";
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();



            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint(jsonPath, Title + " " + Version);
            //});

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "v1.0");
                c.EnableFilter();
                c.DisplayRequestDuration();
                c.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                c.DefaultModelRendering(Swashbuckle.AspNetCore.SwaggerUI.ModelRendering.Example);
                c.DefaultModelExpandDepth(3);
            });




        }
    }
}
