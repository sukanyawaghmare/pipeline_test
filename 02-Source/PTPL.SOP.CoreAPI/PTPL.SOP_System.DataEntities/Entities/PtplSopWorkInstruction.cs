﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplSopWorkInstruction
    {
        public PtplSopWorkInstruction()
        {
            PtplSopLogger = new HashSet<PtplSopLogger>();
            PtplSopWimachine = new HashSet<PtplSopWimachine>();
        }

        public int Uid { get; set; }
        public string SopNumber { get; set; }
        public string SopTemplateName { get; set; }
        public string SopTemplateDesc { get; set; }
        public int? SopCategory { get; set; }
        public string OriginalEstimateTime { get; set; }
        public int? MachineId { get; set; }
        public int? EventType { get; set; }
        public int? AssigneeId { get; set; }
        public int? AreaId { get; set; }
        public string FileName { get; set; }
        public byte[] FileStream { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string FileVersion { get; set; }
        public bool? WiFlag { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PtplAreaMaster Area { get; set; }
        public virtual PtplUserMaster Assignee { get; set; }
        public virtual PtplEventMaster EventTypeNavigation { get; set; }
        public virtual ICollection<PtplSopLogger> PtplSopLogger { get; set; }
        public virtual ICollection<PtplSopWimachine> PtplSopWimachine { get; set; }
    }
}
