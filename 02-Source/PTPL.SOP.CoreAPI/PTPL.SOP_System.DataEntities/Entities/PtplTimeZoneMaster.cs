﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplTimeZoneMaster
    {
        public int Uid { get; set; }
        public string TimeZones { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PtplConfigurePreferences PtplConfigurePreferences { get; set; }
    }
}
