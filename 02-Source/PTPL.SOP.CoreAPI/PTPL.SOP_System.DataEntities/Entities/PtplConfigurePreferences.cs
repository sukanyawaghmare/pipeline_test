﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplConfigurePreferences
    {
        public int Uid { get; set; }
        public int? ServerTimeZone { get; set; }
        public bool? EmailNotification { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PtplTimeZoneMaster U { get; set; }
    }
}
