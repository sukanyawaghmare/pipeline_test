﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplCategoryMaster
    {
        public PtplCategoryMaster()
        {
            PtplSopLogger = new HashSet<PtplSopLogger>();
        }

        public int Uid { get; set; }
        public string CategoryName { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<PtplSopLogger> PtplSopLogger { get; set; }
    }
}
