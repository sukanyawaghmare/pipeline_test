﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplSopWimachine
    {
        public int Id { get; set; }
        public int? WiId { get; set; }
        public int? MachineId { get; set; }

        public virtual PtplSopWorkInstruction Wi { get; set; }
    }
}
