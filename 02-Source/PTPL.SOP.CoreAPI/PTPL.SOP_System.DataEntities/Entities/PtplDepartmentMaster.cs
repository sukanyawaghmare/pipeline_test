﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplDepartmentMaster
    {
        public PtplDepartmentMaster()
        {
            PtplUserMaster = new HashSet<PtplUserMaster>();
        }

        public int DeptId { get; set; }
        public string DeptName { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<PtplUserMaster> PtplUserMaster { get; set; }
    }
}
