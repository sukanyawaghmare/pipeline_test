﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplAreaMaster
    {
        public PtplAreaMaster()
        {
            PtplMachineMaster = new HashSet<PtplMachineMaster>();
            PtplSopLogger = new HashSet<PtplSopLogger>();
            PtplSopWorkInstruction = new HashSet<PtplSopWorkInstruction>();
        }

        public int Uid { get; set; }
        public string AreaName { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<PtplMachineMaster> PtplMachineMaster { get; set; }
        public virtual ICollection<PtplSopLogger> PtplSopLogger { get; set; }
        public virtual ICollection<PtplSopWorkInstruction> PtplSopWorkInstruction { get; set; }
    }
}
