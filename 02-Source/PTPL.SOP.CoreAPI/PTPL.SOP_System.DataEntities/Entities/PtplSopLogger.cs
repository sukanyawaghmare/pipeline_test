﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplSopLogger
    {
        public PtplSopLogger()
        {
            PtplLoggerAttachment = new HashSet<PtplLoggerAttachment>();
        }

        public int SopDocId { get; set; }
        public string SopName { get; set; }
        public string SopDesc { get; set; }
        public int? EventType { get; set; }
        public int? SopCategory { get; set; }
        public int? MachineId { get; set; }
        public int? SopwiId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime PlanFinishDate { get; set; }
        public DateTime? ActualFinishDate { get; set; }
        public string ComplianceStatus { get; set; }
        public string Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? AreaId { get; set; }
        public int? Assignee { get; set; }
        public string SopLoggerNumber { get; set; }

        public virtual PtplAreaMaster Area { get; set; }
        public virtual PtplUserMaster AssigneeNavigation { get; set; }
        public virtual PtplEventMaster EventTypeNavigation { get; set; }
        public virtual PtplMachineMaster Machine { get; set; }
        public virtual PtplCategoryMaster SopCategoryNavigation { get; set; }
        public virtual PtplSopWorkInstruction Sopwi { get; set; }
        public virtual ICollection<PtplLoggerAttachment> PtplLoggerAttachment { get; set; }
    }
}
