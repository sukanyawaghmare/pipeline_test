﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplUserMaster
    {
        public PtplUserMaster()
        {
            PtplSopLogger = new HashSet<PtplSopLogger>();
            PtplSopWorkInstruction = new HashSet<PtplSopWorkInstruction>();
        }

        public int Uid { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Role { get; set; }
        public int? DepartmentId { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Location { get; set; }
        public string PhotoPath { get; set; }
        public byte[] ProfilePicture { get; set; }
        public bool? DeleteFlag { get; set; }
        public bool IsActive { get; set; }
        public bool? IsBlocked { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PtplDepartmentMaster Department { get; set; }
        public virtual PtplUserRoles RoleNavigation { get; set; }
        public virtual ICollection<PtplSopLogger> PtplSopLogger { get; set; }
        public virtual ICollection<PtplSopWorkInstruction> PtplSopWorkInstruction { get; set; }
    }
}
