﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplEventTriggerSchedule
    {
        public int EventTriggerId { get; set; }
        public int WiSopId { get; set; }
        public int EventType { get; set; }
        public string Title { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Occurance { get; set; }
        public int MachineId { get; set; }
        public int Assignee { get; set; }
        public string Condition { get; set; }
        public string ExternalEvents { get; set; }
        public DateTime? TimeValue { get; set; }
        public DateTime EventDateTime { get; set; }
        public bool? Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
