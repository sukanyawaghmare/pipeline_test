﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplEventMaster
    {
        public PtplEventMaster()
        {
            PtplSopLogger = new HashSet<PtplSopLogger>();
            PtplSopWorkInstruction = new HashSet<PtplSopWorkInstruction>();
        }

        public int Uid { get; set; }
        public string EventType { get; set; }
        public int? EventStatus { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual ICollection<PtplSopLogger> PtplSopLogger { get; set; }
        public virtual ICollection<PtplSopWorkInstruction> PtplSopWorkInstruction { get; set; }
    }
}
