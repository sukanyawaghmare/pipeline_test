﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplUserRoles
    {
        public PtplUserRoles()
        {
            PtplUserMaster = new HashSet<PtplUserMaster>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<PtplUserMaster> PtplUserMaster { get; set; }
    }
}
