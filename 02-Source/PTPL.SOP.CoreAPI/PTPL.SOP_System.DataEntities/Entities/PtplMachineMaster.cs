﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplMachineMaster
    {
        public PtplMachineMaster()
        {
            PtplSopLogger = new HashSet<PtplSopLogger>();
        }

        public int Uid { get; set; }
        public string MachineName { get; set; }
        public int? AreaId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PtplAreaMaster Area { get; set; }
        public virtual ICollection<PtplSopLogger> PtplSopLogger { get; set; }
    }
}
