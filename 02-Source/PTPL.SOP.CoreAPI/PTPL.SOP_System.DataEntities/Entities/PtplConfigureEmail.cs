﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplConfigureEmail
    {
        public int EmailId { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public int Port { get; set; }
        public bool? IsBodyHtml { get; set; }
        public string Body { get; set; }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
