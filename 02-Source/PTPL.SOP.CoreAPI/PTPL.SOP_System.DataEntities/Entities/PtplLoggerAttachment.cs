﻿using System;
using System.Collections.Generic;

namespace PTPL.SOP_System.DataEntities
{
    public partial class PtplLoggerAttachment
    {
        public int AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string Description { get; set; }
        public byte[] FileStream { get; set; }
        public string FileSize { get; set; }
        public string FileName { get; set; }
        public string FileExentaion { get; set; }
        public int? DocumentId { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public virtual PtplSopLogger Document { get; set; }
    }
}
