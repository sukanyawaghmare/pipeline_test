﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
        public partial class ConfigureDTO
        {
            public int Uid { get; set; }
            public string ServerTimeZone { get; set; }
            public bool? EmailNotification { get; set; }
            public int? CreatedBy { get; set; }
            public DateTime? CreatedDate { get; set; }
            public int? ModifiedBy { get; set; }
            public DateTime? ModifiedDate { get; set; }

           // public virtual PtplTimeZoneMaster U { get; set; }
        }
    
}
