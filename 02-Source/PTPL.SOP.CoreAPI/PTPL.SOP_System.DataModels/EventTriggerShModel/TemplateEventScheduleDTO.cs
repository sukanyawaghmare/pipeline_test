﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
   public class TemplateEventDTO
    {
        public int Uid { get; set; }
        public string SopNumber { get; set; }
        public string SopTemplateName { get; set; }
        public string SopTemplateDesc { get; set; }
        public int? SopCategory { get; set; }
        public string OriginalEstimateTime { get; set; }
        public int? EquipmentType { get; set; }
        public int? EventType { get; set; }
        public int? EventTrigger { get; set; }
        public int? AssigneeId { get; set; }
        public int? AreaId { get; set; }
        public string FileName { get; set; }
        public byte[] FileStream { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string FileVersion { get; set; }
        public bool? WiFlag { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        
    }

    public class EventTriggerScheduleTempDTO
    {
        public int EventTriggerId { get; set; }
        public int WiSopId { get; set; }
        public int EventType { get; set; }
        public string Title { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Occurance { get; set; }
        public int Equipment { get; set; }
        public int Assignee { get; set; }
        public string Condition { get; set; }
        public string ExternalEvents { get; set; }
        public string TimeValue { get; set; }
        public bool? Status { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
    public class TemplateEventScheduleDTO
    {
        public TemplateEventDTO templateEventDTO { get; set; }
        public List<EventTriggerScheduleTempDTO> eventTriggerScheduleTempDTO { get; set; }
    }
}
