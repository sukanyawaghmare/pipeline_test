﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
    public class EventTriggerScheduleDTO
    {
        public int EventTriggerId { get; set; }
        public int WiSopId { get; set; }
        public int EventType { get; set; }
        public string Title { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int Occurance { get; set; }
        public int MachineId { get; set; }
        public int Assignee { get; set; }
        public string Condition { get; set; }
        public string ExternalEvents { get; set; }
        public string TimeValue { get; set; }
        public bool? Status { get; set; }
    }


    public class GetEventTriggerScheduleDTO
    {
        public int EventTriggerId { get; set; }
        public int WiId { get; set; }
        public string WiSopId { get; set; }
        public string SopNumber { get; set; }
        public string EventType { get; set; }
        public string Title { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string Occurance { get; set; }
        public string Machine { get; set; }
        public string Assignee { get; set; }
        public string Condition { get; set; }
        public string ExternalEvents { get; set; }
        public DateTime? TimeValue { get; set; }
        public bool? Status { get; set; }
    }
}
