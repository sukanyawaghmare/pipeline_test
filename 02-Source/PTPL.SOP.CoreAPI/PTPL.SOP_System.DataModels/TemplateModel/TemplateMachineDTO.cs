﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
   public class TemplateMachineDTO
    {
        public int Uid { get; set; }
        public string sopNumber { get; set; }
        public string SopTemplateName { get; set; }
        public string OriginalEstimateTime { get; set; }
        public int? AreaId { get; set; }
        public string SopTemplateDesc { get; set; }
        public int? SopCategory { get; set; }
        public int? EquipmentType { get; set; }
        public int? EventType { get; set; }
        public int? EventTrigger { get; set; }
        public int? AssigneeId { get; set; }
        public byte[] FileStream { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string FileVersion { get; set; }
        public int? CreatedBy { get; set; }
        public int[] machinIds { get; set; }
        public string fileName { get; set; }
        public bool? WiFlag { get; set; } = false;
    }


}
