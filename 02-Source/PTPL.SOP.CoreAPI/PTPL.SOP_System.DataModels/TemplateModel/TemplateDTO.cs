﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
    public class TemplateDTO
    {

        public int Uid { get; set; }
        public string TemplateName { get; set; }
       // public string EquipmentType { get; set; }
        public string SopTemplateDesc { get; set; }
        public string OriginalEstimateTime { get; set; }
        public string FileName { get; set; }
        public string SopNumber { get; set; }
        public string Category { get; set; }
        public int? EventTypeId { get; set; }
        public string EventType { get; set; }
        //public int? EventTriggerId { get; set; }
        //public string EventTriggerName { get; set; }
        public int? AssigneeId { get; set; }
        public string AssigneeName { get; set; }
        public int? AreaId { get; set; }
        public string AreaName { get; set; }
       // public byte[] FileStream { get; set; }
        public string FilePath { get; set; }
        public string FileSize { get; set; }
        public string FileVersion { get; set; }


        [DataMember(Name = "MachineDTOs")]
        public List<MachineDTO> MachineDTOs { get; set; }

    }
    [DataContract]
    public class MachineDTO
    {
        public int MachineId { get; set; }
        public string MachineName { get; set; }
        public bool IsUsed { get; set; }
    }

    public class SopInfo
    {
        public int CategoryId { get; set; }
        public int EquipmentId { get; set; }
        public int EventId { get; set; }
        public int MachineId { get; set; }
        public int TriggerId { get; set; }
    }

    public class GetFileNameDTO
    {
        public int Id { get; set; }
        public string fileName { get; set; }
    }
    // public class GetTemplateEvent
    //{
    //    public TemplateDTO TemplateDTO { get; set; }
    //    public List<EventTriggerScheduleDTO> EventTriggerScheduleDTOs { get; set; }

    //}

}
