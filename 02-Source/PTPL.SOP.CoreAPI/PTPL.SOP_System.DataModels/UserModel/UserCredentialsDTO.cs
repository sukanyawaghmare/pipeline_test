﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
    [DataContract]
    public class UserCredentialsDTO
    {
        
        [DataMember(Name ="UserName")]
        [Required]
        public string UserName { get; set; }
        [DataMember(Name ="Password")]
        [Required]
        public string Password { get; set; }
    }
}
