﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
    [DataContract]
     public class UserMasterDTO
    {

        [DataMember(Name ="Uid")]
        public int Uid { get; set; }
        [DataMember(Name = "UserName")]
        public string UserName { get; set; }
        [DataMember(Name = "Password")]
        public string Password { get; set; }

        [DataMember(Name = "FirstName")]
        public string FirstName { get; set; }
        [DataMember(Name = "LastName")]
        public int? role { get; set; }
        public string LastName { get; set; }
        [DataMember(Name = "ProfilePicture")]
        public byte[] ProfilePicture { get; set; }
        [DataMember(Name = "Email")]
        public string Email { get; set; }

        [DataMember(Name = "ContactNumber")]
        public string ContactNumber { get; set; }

        [DataMember(Name = "Location")]
        public string Location { get; set; }

        [DataMember(Name = "isActive")]
        public bool isActive { get; set; }

    }
    [DataContract]
    public class UserChangePassDTO
    {

        [DataMember(Name = "Uid", Order = 1000)]
        public int Uid { get; set; }

        [Required]
        //[StringLength(15, MinimumLength = 5, ErrorMessage = "Current Password Password is required.")]
        [DataMember(Name = "Password", Order = 1030)]
        public string Password { get; set; }

        [Required]
        //[StringLength(15, MinimumLength = 5, ErrorMessage = "New Password is required.")]
        [DataMember(Name = "NewPassword", Order = 1030)]
        public string NewPassword { get; set; }

    }
}
