﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;
using System.Text;

namespace PTPL.SOP_System.DataModels
{
    [DataContract]
    public class DocumentDTO
    {
        //
        public int SopDocId { get; set; }
        public string SopName { get; set; }
        public string SopDesc { get; set; }
        public string EventName { get; set; }
        public string SopCategory { get; set; }
        public string WINumber { get; set; }
        public string Status { get; set; }
        public string Area { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? PlannedFinishedDate { get; set; }
        public string Assinee { get; set; }
        public string ComplanceStatus { get; set; }
        public string Equipment { get; set; }
        //public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? WiID { get; set; }

    }

    public class SopLoggerDTO
    {

        public int SopDocId { get; set; }
        public string SopName { get; set; }
        public string SopDesc { get; set; }
        public int? EventType { get; set; }
        public int? SopCategory { get; set; }
        public int? MachineId { get; set; }
       // public string MachineName { get; set; }
        public int? SopwiId { get; set; }
        public string SopwiName { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? PlanFinishDate { get; set; }
        public DateTime? ActualFinishDate { get; set; }
        public string ComplianceStatus { get; set; }
        public string Status { get; set; }
       
       
        public int?  AreaId { get; set; }
        public int AreaName { get; set; }
        public int? AssigneeId { get; set; }
        public string  AssigneeName { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }


    }

    public class LoggerUpdateStatus
    {
        public int sopDocId { set; get; }
        public DateTime? ActualFinishDateDate { set; get; }
        public string complianceStatus { set; get; }
        public string status { set; get; }
    }

    public class SopLoggerByUserDTO
    {
        public int SopDocId { get; set; }
        public string SopName { get; set; }
        public string SopDesc { get; set; }
        public string SopNumber { get; set; }
        public int? SopwiId { get; set; }
        public string SopwiName { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? PlanFinishDate { get; set; }
        public string EventType { get; set; }
        // public DateTime? ActualFinishDate { get; set; }
        public string ComplianceStatus { get; set; }
        public string Status { get; set; }
        public string FileName { get; set; }
        public int AssigneeId { get; set; }
        public string AssigneeName { get; set; }
        public string CreatedBy { get; set; }
        //public DateTime? CreatedDate { get; set; }
        public string SopLoggerNumber { get; set; }
        public string Category { get; set; }
        public string Area { get; set; }
        public string Equipment { get; set; }
        public string PendingDuration { get; set; }
    }
}

