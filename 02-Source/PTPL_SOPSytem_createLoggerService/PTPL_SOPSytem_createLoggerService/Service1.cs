﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using ConfigurationManager = System.Configuration.ConfigurationManager;

namespace PTPL_SOPSytem_createLoggerService
{
    /// <summary>
    /// This service is use to implement a logic to read event trigger is scheduled on which datetime
    /// </summary>
    public partial class Service1 : ServiceBase
    {
        /// <summary>
        /// Varaible declaration for the service project.
        /// </summary>

        #region varaible
        Timer timer = new Timer();
        static readonly string serverIpAddress = ConfigurationManager.AppSettings["ipAddress"];
        static readonly int portNO = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
        static readonly int intervalTime = Convert.ToInt32(ConfigurationManager.AppSettings["interval"]);
        static readonly string consString = ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
        static readonly bool emailNotification = Convert.ToBoolean(ConfigurationManager.AppSettings["emailNotification"]);
        static string apiurl = string.Empty;
        static string apiEndPoint = string.Empty;
        static string apiVersion = string.Empty;
        #endregion

        /// <summary>
        /// This is an configure component which is use to setup service.
        /// </summary>
        public Service1()
        {
            InitializeComponent();
        }

        #region methods

        /// <summary>
        /// This is OnStart Method which accepting argument and will run once service is started
        /// </summary>
        /// <param name="args"></param>
        /// <returns>return to calling method</returns>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected override void OnStart(string[] args)
        {
            WriteToFile("SOPWI Listner Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);

            timer.Interval = intervalTime; //number in seconds  1000= 1 second
            timer.Enabled = true;
        }

        /// <summary>
        /// This is OnStop method which is invoke once service is Stopped
        /// </summary>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected override void OnStop()
        {
            WriteToFile("SOPWI Listner Service is stopped at " + DateTime.Now);
        }

        /// <summary>
        /// This is OnElapsedTime method which will invoked as soon as given time is Elaspsed.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            WriteToFile("SOPWI Listner Service is recall at " + DateTime.Now);

            //string exefileLocation = "";
            //exefileLocation = @"D:\LeanLogic\LeanLogic_VisiSOP\visiSOP_R&D\PTPL_visiSOPsystem_EmailNotification\PTPL_visiSOPsystem_EmailNotification\bin\Debug\PTPL_visiSOPsystem_EmailNotification.exe";
            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //process.StartInfo = new System.Diagnostics.ProcessStartInfo(exefileLocation);
            ////process.StartInfo.Arguments = argument;
            //process.Start();

            GetEventTriggerScheduled();
        }

        /// <summary>
        /// This method is use to log every event which is fired in service e:g (Info,Error,Warning etc).
        /// </summary>
        /// <param name="Message"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private static void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }

        /// <summary>
        /// This method is use to implement the logic to identify event occurance for the given datetime.
        /// We will be sending calculated result and respective Work-InstrunctionId to the SOPLogger listener.
        /// </summary>
        /// <param name=""></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="SqlConnection Error"></exception>
        /// <exception cref="SqlParameter mitchmatch"></exception>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected string GetEventTriggerScheduled()
        {
            var watch = Stopwatch.StartNew();
            WriteToFile("Invoked getEventTriggerScheduled method at:  " + DateTime.Now);
            DataTable dt = new DataTable();
            int wiId = 0;
            int assigneeUserId = 0;
            string returnResult = "false";
            try
            {
                DateTime todayDate = DateTime.Now;
                todayDate = todayDate.AddSeconds(-todayDate.Second);
                string mtodayDate = todayDate.ToString("yyyy-MM-dd HH:mm:ss");
                int occurance = 0;
                DateTime _mtodayDate = DateTime.ParseExact(mtodayDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                var query = "SELECT wiSOPId,startTime,occurance,assignee FROM  ptpl_eventTriggerSchedule WHERE starttime= '" + _mtodayDate.ToString("yyyy-MM-dd HH:mm:ss") + "' ";

                using (SqlConnection con = new SqlConnection(consString))
                {
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        con.Open();
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {
                            sda.Fill(dt);
                        }
                        if (dt.Rows.Count > 0)
                        {
                            DateTime mSystemDateTime = DateTime.Now;
                            wiId = Convert.ToInt32(dt.Rows[0]["wiSOPId"].ToString());
                            DateTime mOccuranceDateTime = Convert.ToDateTime(dt.Rows[0]["startTime"].ToString());
                            occurance = Convert.ToInt32(dt.Rows[0]["occurance"].ToString());
                            assigneeUserId = Convert.ToInt32(dt.Rows[0]["assignee"].ToString());

                            switch (occurance)
                            {
                                case 1: // None
                                    if (mOccuranceDateTime.ToString("yyyy/MM/dd:hh:mm") == mSystemDateTime.ToString("yyyy/MM/dd:hh:mm"))
                                    {
                                        Console.WriteLine("Occurance = None");
                                        returnResult = wiId + "," + "true" + "," + assigneeUserId;
                                    }
                                    break;
                                case 2: // Hourly /// it will create Daily for per hour
                                    if (mOccuranceDateTime.ToString("hh:mm") == mSystemDateTime.ToString("hh:mm"))
                                    {
                                        Console.WriteLine("Occurance = Hourly");
                                        returnResult = wiId + "," + "true" + "," + assigneeUserId;
                                    }
                                    break;
                                case 3: // Daily
                                    if (mOccuranceDateTime.ToString("yyyy/MM/dd:hh:mm") == mSystemDateTime.ToString("yyyy/MM/dd:hh:mm"))
                                    {
                                        Console.WriteLine("Occurance = Daily");
                                        returnResult = wiId + "," + "true" + "," + assigneeUserId;
                                    }
                                    break;
                                case 4: // Weekly
                                    if ((mOccuranceDateTime.DayOfWeek == mSystemDateTime.DayOfWeek) && (mOccuranceDateTime.ToString("yyyy/MM/dd:hh:mm") == mSystemDateTime.ToString("yyyy/MM/dd:hh:mm")))
                                    {
                                        Console.WriteLine("Occurance = Weekly");
                                        returnResult = wiId + "," + "true" + "," + assigneeUserId;
                                    }
                                    break;
                                case 5: // Monthly
                                    if ((mOccuranceDateTime.Month == mSystemDateTime.Month) && (mOccuranceDateTime.DayOfWeek == mSystemDateTime.DayOfWeek) && (mOccuranceDateTime.ToString("yyyy/MM/dd:hh:mm") == mSystemDateTime.ToString("yyyy/MM/dd:hh:mm")))
                                    {
                                        Console.WriteLine("Occurance = Monthly");
                                        returnResult = wiId + "," + "true" + "," + assigneeUserId;
                                    }
                                    break;
                                case 6: // Yearly
                                    if (mOccuranceDateTime.Year == mSystemDateTime.Year)
                                    {
                                        Console.WriteLine("Occurance = Yearly");
                                        returnResult = wiId + "," + "true" + "," + assigneeUserId;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            WriteToFile("Info: Event trigger schedule for the datetime : " + DateTime.Now + " is not found " + $" | Execution Time: {watch.ElapsedMilliseconds} ms");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile("Error : Exception occured due to: " + ex.Message + DateTime.Now + $" | Execution Time: {watch.ElapsedMilliseconds} ms");
            }
            WriteToFile("Info: WI-Listener retrun with " + returnResult + " at " + DateTime.Now + $" | Execution Time: {watch.ElapsedMilliseconds} ms");

            //serverListerner(returnResult.ToString());
            IdentifyIfEventValid(returnResult);
            return returnResult;
        }

        /// <summary>
        /// This method is use to send recieved value from the getEventTriggerScheduled() method to sopLoggerListener.
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="SqlConnection Error"></exception>
        /// <exception cref="IP-address is Not valid or not exist"></exception>
        /// <exception cref="port no is Validat or not exist"></exception>
        /// <exception cref="Server listener is down"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        /// <remarks>This method is not in use for now.</remarks>
        public void ServerListerner(string sendData)
        {
            try
            {
                var watch = Stopwatch.StartNew();
                WriteToFile("WI-Listener send result to listener " + sendData);

                //---listen at the specified IP and port no.---
                IPAddress localAdd = IPAddress.Parse(serverIpAddress);
                TcpListener listener = new TcpListener(localAdd, portNO);
                //Console.WriteLine("Listening...");
                listener.Start();
                //---incoming client connected---
                TcpClient client = listener.AcceptTcpClient();

                //---get the incoming data through a network stream---
                NetworkStream nwStream = client.GetStream();
                byte[] buffer = new byte[client.ReceiveBufferSize];
                byte[] outBuffer = new byte[System.Text.ASCIIEncoding.ASCII.GetByteCount(sendData)];

                //---read incoming stream---
                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);
                byte[] bytesToSend = ASCIIEncoding.ASCII.GetBytes(sendData);

                //---write back the text to the client---
                //Console.WriteLine("Sending back : " + dataReceived);
                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                client.Close();
                listener.Stop();
                WriteToFile("WI-Listener return to the method  " + DateTime.Now + $" | Execution Time: {watch.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                WriteToFile("Error occured while sending data to SOP-Logger Listener : " + ex.Message);
            }

        }

        /// <summary>
        /// This method is use to identify weather or not retrun parameter is valid to procced request to create sopLogger API.
        /// </summary>
        /// <param name="sendData"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        private void IdentifyIfEventValid(string returnResult)
        {
            if (returnResult.Contains("true"))
            {
                var watch = Stopwatch.StartNew();
                var str = returnResult.Split(',');
                int wiId = Convert.ToInt32(str[0]);
                string result = str[1];
                int assigneeId = Convert.ToInt32(str[2]);

                WriteToFile("WI-Listener identify valid event scheduler for respective wi-Id = " + wiId);
                var apiResult = GetEventSchedulerDetails(wiId);
                WriteToFile("API return with result at  " + DateTime.Now + $" | Execution Time: {watch.ElapsedMilliseconds} ms");
               
                if(emailNotification == true)
                {
                    watch = Stopwatch.StartNew();
                    var apiEmailResult = SendEmailNotification(assigneeId);
                    WriteToFile("API return with result at  " + DateTime.Now + $" | Execution Time: {watch.ElapsedMilliseconds} ms");
                }
                else
                {
                    WriteToFile("Email notification is set to false in app.config file.");
                }
            }
        }

        /// <summary>
        /// This method is use to call API to create sopLogger for respective Work Instruction (wiId).
        /// </summary>
        /// <param name="wiId"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected static string GetEventSchedulerDetails(int wiId)
        {
            string apiResult = string.Empty;
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    //apiurl = apiEndPoint + apiVersion + "/EventTriggerSchedule/GetEventTriScheTemplateByWIId?Id=" + wiId;
                    apiurl = "http://3.216.232.120/visiSOPAPIver1/api/v1.0/EventTriggerSchedule/GetEventTriScheTemplateByWIId?Id=" + wiId;
                    using (HttpResponseMessage response = client.GetAsync(apiurl).Result)
                    {
                        //WriteToFile("response from API:  " + response);
                        if (response.IsSuccessStatusCode)
                        {  
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            apiResult = jo["eventTriggerScheduleData"].ToString();
                            WriteToFile("api retrun with result = SOP-logger for event scheduled wi-Id = " + wiId + "  is created successfully + " + response);
                        }
                        else
                        {
                            apiResult = "Data not saved into SOPLogger: due to :" + response;
                            WriteToFile("api retrun with result = Data not saved into SOPLogger: due to :" + response);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile(ex.Message);
            }

            return apiResult;
        }

        /// <summary>
        /// This method is use to call API to send conformation email to assigned user after successfully creating the SOP logger object.
        /// </summary>
        /// <param name="wiId"></param>
        /// <returns>Success-StatusCode-200</returns>
        /// <exception cref="Error-StatusCode-400"></exception>
        /// <exception cref="Error-StatusCode-500"></exception>
        /// <exception cref="Input string is not proper"></exception>
        /// <remarks>Code documentation and Review is pending</remarks>
        protected static string SendEmailNotification(int assigneeId)
        {
            string apiResult = string.Empty;
            apiurl = string.Empty;
            try
            {
                using (var client = new HttpClient())
                {
                    //apiurl = apiEndPoint + apiVersion + "/EventTriggerSchedule/GetEventTriScheTemplateByWIId?Id=" + wiId;
                    apiurl = "http://3.216.232.120/visiSOPAPIver1/api/v1.0/UserMaster/SentMailToUserByUId?Id=" + assigneeId;
                    using (HttpResponseMessage response = client.GetAsync(apiurl).Result)
                    {
                        //WriteToFile("response from API:  " + response);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseData = response.Content.ReadAsStringAsync().Result;
                            var jo = JObject.Parse(responseData);
                            apiResult = jo["userData"].ToString();
                            WriteToFile("api retrun with result = SOP-logger created confirmation email is sent successfully to  " + assigneeId + ", " + response);
                        }
                        else
                        {
                            apiResult = "Email not sent : due to :" + response;
                            WriteToFile("api retrun with result = Email not sent :: due to :" + response);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToFile(ex.Message);
            }

            return apiResult;
        }
        #endregion
    }
}
